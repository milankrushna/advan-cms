<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Page_model extends CI_Model
{



 /**
	 * Front end Page for this model.
	 *
	 * Maps to the following URL
	 * 		controller Name Index
	 * @author [[Milan Krushna, Subhashree Jena]]
	 * @output [[ Front end Home page,Innerpage and gallery ]]         
	 */
  
     function gettourDetails($tours=''){
         return $this->db->get_where('tours',array('alias'=>$tours))->row();
     }
     function get_all_styledata_pg(){
         return $this->db->get('tour_style')->result();
     }
     function get_all_country(){
         $this->db->where('id !=','19');
         return $this->db->get('country')->result();
     }
     function get_tours_trip($tid){
         return $this->db->get_where('tours_trip',array('tour_id'=>$tid))->result();
     }
     function get_tours_day($tid){
         return $this->db->get_where('tours_day',array('tour_id'=>$tid))->result();
     }

   function get_country_tours($countryId,$tour="",$order="",$id=""){

    $this->db->select('tours.*,travel_maping.*,travel_maping.id as tvmId,tours.id as id,country.countryname,country.alias as country_alias');
			$this->db->from('tours');
            $this->db->join('travel_maping', 'tours.id = travel_maping.type_id');
            $this->db->join('country', 'country.id = travel_maping.country_id');
            if($countryId !="all"){
            $this->db->where('travel_maping.country_id',$countryId);
            }
            $this->db->where('travel_maping.travel_type',1);
            if($id<=1 && $id==""){
				$ids=0;
			}else{
			$ids = $id-1;
			}
            
        $this->db->limit($tour,$ids*$tour);
        if($order !=""){
            $this->db->order_by('tours.name',$order);
        }else{
            $this->db->order_by('tours.id','DESC');
            
        }
			$query = $this->db->get();
			return $query->result();

    }
    function get_country_tours_row($countryId){

        $this->db->select('tours.*,travel_maping.*,travel_maping.id as tvmId,tours.id as id,country.countryname,country.alias as country_alias');
                $this->db->from('tours');
                $this->db->join('travel_maping', 'tours.id = travel_maping.type_id');
                $this->db->join('country', 'country.id = travel_maping.country_id');
            if($countryId !="all"){

                $this->db->where('travel_maping.country_id',$countryId);
            }
                $this->db->where('travel_maping.travel_type',1);
                $query = $this->db->get();
                return $query->num_rows();
    
        }
   function get_country_hotels($countryId){

    $this->db->select('hotels.*,travel_maping.*,travel_maping.id as tvmId,hotels.id as id');
			$this->db->from('hotels');
            $this->db->join('travel_maping', 'hotels.id = travel_maping.type_id');
            $this->db->where('travel_maping.country_id',$countryId);
            $this->db->where('travel_maping.travel_type',2);
            $query = $this->db->get();
            
            //echo $this->db->last_query();
			return $query->result();

    }

    

   function get_country_experience($countryId){

    $this->db->select('experiance.*,travel_maping.*,travel_maping.id as tvmId,experiance.id as id');
			$this->db->from('experiance');
            $this->db->join('travel_maping', 'experiance.id = travel_maping.type_id');
            $this->db->where('travel_maping.country_id',$countryId);
            $this->db->where('travel_maping.travel_type',3);
			$query = $this->db->get();
			return $query->result();

    }
    function get_related_tours($countryId,$tid){
        $this->db->select('tours.*,travel_maping.*,travel_maping.id as tvmId,tours.id as id');
        $this->db->from('tours');
        $this->db->join('travel_maping', 'tours.id = travel_maping.type_id');
        $this->db->where('travel_maping.country_id',$countryId);
        $this->db->where('tours.id !=',$tid);
        $this->db->where('travel_maping.travel_type',1);
        $this->db->limit(3);
        $this->db->order_by('tours.id','RANDOM');

        $query = $this->db->get();
        return $query->result();
    }

  function  get_tours_lcn($countryId=''){
    $this->db->select('tours.*,travel_maping.*,travel_maping.id as tvmId,tours.id as id');
			$this->db->from('tours');
            $this->db->join('travel_maping', 'tours.id = travel_maping.type_id');
            $this->db->where('travel_maping.country_id',$countryId);
            $this->db->where('travel_maping.travel_type',1);
            $this->db->limit(3);
            $this->db->order_by('tours.id','RANDOM');
			$query = $this->db->get();
			return $query->result();
    }

    public function get_country(){
        $this->db->select('country.*, country_page.name as page_name,country_page.menu_image as image');
			$this->db->from('country');
            $this->db->join('country_page', 'country.id = country_page.country_id');
            $this->db->where('country.id !=',19);
            //$this->db->limit(10);
			$query = $this->db->get();
			return $query->result();
    }
    public function get_country_home(){
        $this->db->select('country.*, country_page.name as page_name,country_page.menu_image as image');
			$this->db->from('country');
            $this->db->join('country_page', 'country.id = country_page.country_id');
            $this->db->order_by('country.id','random');

            $this->db->limit(6);
			$query = $this->db->get();
			return $query->result();
    }

    function tour_style(){
        $this->db->order_by('name','ASC');
        return $this->db->get('tour_style')->result_array();
    }

    function getCountryData($location){
        $this->db->select('country.*,country_page.*, country_page.name as page_name,country_page.menu_image as image,country_page.id as cpId,country.id as id');
        $this->db->from('country');
        $this->db->join('country_page', 'country.id = country_page.country_id');
        $this->db->where('country.alias',$location);
        $query = $this->db->get();
        return $query->row();
    }
    
    function get_regionData($countryId=""){
        $this->db->order_by('name','ASC');
        return $this->db->get_where('region',array('countryid'=>$countryId))->result();
    }
    function get_styledata(){
        $this->db->order_by('name','ASC');
        return $this->db->get('tour_style')->result();
    }
    function get_style_tours($sid,$tour,$order,$id){

        $this->db->select('tours.*,travel_maping.*,travel_maping.id as tvmId,tours.id as id,country.countryname,country.alias as country_alias');
        $this->db->from('tours');
        $this->db->join('travel_maping', 'tours.id = travel_maping.type_id');
        $this->db->join('country', 'country.id = travel_maping.country_id');
        $this->db->where('travel_maping.tours_style_id',$sid);
        $this->db->where('travel_maping.travel_type',1);

        if($id<=1 && $id==""){
            $ids=0;
        }else{
        $ids = $id-1;
        }
        
    $this->db->limit($tour,$ids*$tour);
    if($order !=""){
        $this->db->order_by('tours.name',$order);
    }else{
        $this->db->order_by('tours.id','DESC');
        
    }

        $query = $this->db->get();
        return $query->result();
    }

    function get_style_tours_row($sid){

        $this->db->select('tours.*,travel_maping.*,travel_maping.id as tvmId,tours.id as id,country.countryname,country.alias as country_alias');
        $this->db->from('tours');
        $this->db->join('travel_maping', 'tours.id = travel_maping.type_id');
        $this->db->join('country', 'country.id = travel_maping.country_id');
        $this->db->where('travel_maping.tours_style_id',$sid);
        $this->db->where('travel_maping.travel_type',1);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function getfilter_style($sid){

        $this->db->select('tours.*,travel_maping.*,travel_maping.id as tvmId,tours.id as id,country.countryname,country.alias as country_alias');
        $this->db->from('tours');
        $this->db->join('travel_maping', 'tours.id = travel_maping.type_id');
        $this->db->join('country', 'country.id = travel_maping.country_id');
        if($sid['ftype'] == 'style'){
        $this->db->where('travel_maping.tours_style_id',$sid['specificId']);
        if(!empty($sid['selCountry'])){
            $ccc = explode(',',$sid['selCountry']);
            $this->db->where_in('travel_maping.country_id',$ccc);
        }
        }
        if($sid['ftype'] == 'country'){
            $this->db->where('travel_maping.country_id',$sid['specificId']);
            if(!empty($sid['selCountry'])){
                $ccc = explode(',',$sid['selCountry']);
                $this->db->where_in('travel_maping.tours_style_id',$ccc);
            }
            }
        if(!empty($sid['selDay'])){
            $cccd = explode(',',$sid['selDay']);
         //   print_r($cccd);
            foreach($cccd as $cd){
                if($cd == 1){
                    $this->db->where('tours.day >= 0 AND tours.day <=4');    
                }
                if($cd == 2){
                    $this->db->where('tours.day >= 5 AND tours.day <=7');    
                }
                if($cd == 3){
                    $this->db->where('tours.day >= 8 AND tours.day <=11');    
                }
                if($cd == 4){
                    $this->db->where('tours.day >= 11');    
                }
            }
        }
        
        $this->db->where('travel_maping.travel_type',1);
        $query = $this->db->get();
       return $query->result();
    }
    function get_ctour_home(){

        $this->db->select('tours.*,travel_maping.*,travel_maping.id as tvmId,tours.id as id,country.countryname,country.alias as calias');
        $this->db->from('tours');
        $this->db->join('travel_maping', 'tours.id = travel_maping.type_id');
        $this->db->join('country', 'country.id = travel_maping.country_id');
        //$this->db->where('travel_maping.tours_style_id',$sid);
        $this->db->where('travel_maping.travel_type',1);
        $this->db->limit(6);
        $this->db->order_by('tours.id','RANDOM');
        $query = $this->db->get();
        return $query->result();
    }
    function get_styledata_pg($location){
        $this->db->where('alias',$location);
        return $this->db->get('tour_style')->row();
    }
    
	public function slider($type)
	{
		
		$this->db->select('*');
		$this->db->from('slider');
        $this->db->where('type',$type);
        $this->db->where('status',1);
		$query = $this->db->get();
		$slider =  $query->result_array();
		return $slider;
	
	}
	
	public function common()
	{
		
		$this->db->select('*');
		$this->db->from('common');		 
		$query = $this->db->get();
		$slider =  $query->result_array();
		return $slider;
	
	}
	
    
	
	public function get_page($name){
		$query  = $this->db->get_where('pages',array('url' => $name))->row_array();		  
		//echo $this->db->last_query();exit;
		return $query;
	}
	
	
	public function order_section($id = NULL)
	{
		$query  = $this->db->get_where('section',array('id =' => $id))->row_array();		  
		return $query;		
	}
	
	public function get_footer(){
		$query  = $this->db->get_where('section',array('name' => 'footer'))->row_array();		  
		return $query;
	}


	public function social(){
		
		$query = $this->db->get('social')->result_array();
		return $query;
	}
	
	public function products(){
		$query  = $this->db->get_where('section',array('name' => 'products'))->row_array();		  
		return $query;
	}


	public function  menu(){
	
			$all_menu =  array();
		
		/*	$parent  = $this->db->get_where('menu',array('parent_menu' => 0))->result_array();*/		 
			
			$this->db->from('menu');
			$this->db->where('status', 1);
			$this->db->where('parent_menu', 0);
			$this->db->order_by("sort", "asc");
			$parent = $this->db->get()->result_array(); 
			
			 
			foreach($parent as $pr){
				//$child  = $this->db->get_where('menu',array('parent_menu' => $pr['id']))->result_array();
				
			$this->db->from('menu');
            $this->db->where('status',1);
			$this->db->where('parent_menu =', $pr['id']);
			$this->db->order_by("sort", "asc");
			$child = $this->db->get()->result_array(); 
				
				
						  
				if(count($child)!=0 ){
					$pr['child'] =  $child;
					$all_ch = array();
				foreach($child as $ch){
					
			$this->db->from('menu');
			$this->db->where('parent_menu =', $ch['id']);
			$this->db->order_by("sort", "asc");
			$fst_child = $this->db->get()->result_array();
									
					//$fst_child  = $this->db->get_where('menu',array('parent_menu' => $ch['id']))->result_array();
				$all_fst = array();
				foreach($fst_child as $fc){
			
			$this->db->from('menu');
			$this->db->where('parent_menu =', $fc['id']);
			$this->db->order_by("sort", "asc");
			$sec_child = $this->db->get()->result_array();
					
				//	$sec_child  = $this->db->get_where('menu',array('parent_menu' => $fc['id']))->result_array();
					$fc['sec_child'] = $sec_child;
					$all_fst[] = $fc;
				}
					
					$ch['fst_child'] = $all_fst;
					$all_ch[] = $ch;
				
				
				}
	
				$pr['child'] = $all_ch;
				}else{
					$pr['child'] = 0;
				
					
				}				
			 $all_menu[]  =  $pr;
			
			
			}
		
			/*print("<pre>");
			print_r($all_menu);
			exit;*/
			
			return $all_menu;	
	
	}	
	public function  left_menu(){
	
			$all_menu =  array();
		
		/*	$parent  = $this->db->get_where('menu',array('parent_menu' => 0))->result_array();*/		 
			
			$this->db->from('menu');
			$this->db->where('status2', 1);
			$this->db->where('parent_menu', 0);
			$this->db->order_by("sort_2", "asc");
			$parent = $this->db->get()->result_array(); 
			
			 
			foreach($parent as $pr){
				//$child  = $this->db->get_where('menu',array('parent_menu' => $pr['id']))->result_array();
				
			$this->db->from('menu');
                $this->db->where('status2', 1);
			$this->db->where('parent_menu =', $pr['id']);
			$this->db->order_by("sort_2", "asc");
			$child = $this->db->get()->result_array(); 
				
				
						  
				if(count($child)!=0 ){
					$pr['child'] =  $child;
					$all_ch = array();
				foreach($child as $ch){
					
			$this->db->from('menu');
                    $this->db->where('status2', 1);
			$this->db->where('parent_menu =', $ch['id']);
			$this->db->order_by("sort_2", "asc");
			$fst_child = $this->db->get()->result_array();
									
					//$fst_child  = $this->db->get_where('menu',array('parent_menu' => $ch['id']))->result_array();
				$all_fst = array();
				foreach($fst_child as $fc){
			
			$this->db->from('menu');
                    $this->db->where('status2', 1);
			$this->db->where('parent_menu =', $fc['id']);
			$this->db->order_by("sort_2", "asc");
			$sec_child = $this->db->get()->result_array();
					
				//	$sec_child  = $this->db->get_where('menu',array('parent_menu' => $fc['id']))->result_array();
					$fc['sec_child'] = $sec_child;
					$all_fst[] = $fc;
				}
					
					$ch['fst_child'] = $all_fst;
					$all_ch[] = $ch;
				
				
				}
	
				$pr['child'] = $all_ch;
				}else{
					$pr['child'] = 0;
				
					
				}				
			 $all_menu[]  =  $pr;
			
			
			}
		
			/*print("<pre>");
			print_r($all_menu);
			exit;*/
			
			return $all_menu;	
	
	}	
	
	
	
	public function gallery(){
		
		$query = $this->db->get('album')->result_array();
		return $query;
	}
	
	public function album_image($id){
		
		$query  = $this->db->get_where('gallery_images',array('id =' => $id))->result_array();			return $query;	
		
	}


 function get_inn_image($id){
		$query  = $this->db->get_where('inner_images',array('image_id =' => $id))->row_array();			
		return $query;	

}

function get_other_image($image_id, $id){


		
		$this->db->select('*');
		$this->db->from('inner_images');		 
		$this->db->Where('id !=',$id);
		$this->db->Where('image_id',$image_id);
		$this->db->limit(6);		 
		$query = $this->db->get();
		$slider =  $query->result_array();
		return $slider;
		

} 
	
	
	public function gallery_images($id = NULL)
	{
		//echo $id;exit;
		$query  = $this->db->get_where('gallery_images',array('gallery_id =' => $id))->result_array();			return $query;		
	}


	public function get_page_image($page_id){
			


		$this->db->select('*');
		$this->db->from('page_images');		 
		$this->db->where('page_id =', $page_id);
		//$this->db->limit(15);
		$query = $this->db->get();
		return $query->result_array();						
	}



public function get_pg_image($id){


			$this->db->select('page_images.*, pages.name as page_name, menu.link as menu_link');
			$this->db->from('page_images');
			$this->db->join('pages', 'page_images.page_id = pages.id');
			$this->db->join('menu', 'page_images.page_id = menu.page_id');
			$this->db->where('page_images.id ='.$id);
			$query = $this->db->get();
			return $query->row_array();
}	
	


	public function get_inner_image($page_id){
			
		$this->db->select('*');
		$this->db->from('inner_images');		 
		$this->db->where('image_id =', $page_id);
		$this->db->limit(15);
		$query = $this->db->get();
		return $query->result_array();						
	}	
	
	
	
	
	
	public function bottom_slider(){
		$query = $this->db->get('product')->result_array();
		return $query;	
	}
	

	public function get_events(){
		$query = $this->db->get('events');
		return $query->result_array();
	}
	
    
    function getEventsByCtgy($ctgy){
        $this->db->limit(10);
        $this->db->where('category',$ctgy);
        $this->db->order_by('id','DESC');
       return $this->db->get('events')->result();
    }
    
    function getEventsRow($url){
        
        $this->db->where('alias',$url);
       return $this->db->get('events')->row();
    }

    
    function getCommonRow($link){
        
        $this->db->where('url',$link);
        return $this->db->get('common')->row();
    }
    
    function get_album($id=""){
        
    /*    if($id<=1 && $id==""){
				$ids=0;
			}else{
			$ids = $id-1;
			}
      */  
        //$this->db->limit(15,$ids*15);
        $this->db->order_by('id','ASC');
        return $this->db->get('album')->result();
        
    }
    
    
    function get_images($album){
     
        $this->db->select('gallery_images.*');
        $this->db->from('gallery_images');
        $this->db->join('album','album.id = gallery_images.album_id');
        $this->db->where('album.alias',$album);
        $this->db->order_by('gallery_images.id','ASC');
        return  $this->db->get()->result();
      
    }
    
    function get_video($id){
        
        if($id<=1 && $id==""){
				$ids=0;
			}else{
			$ids = $id-1;
			}
        
        $this->db->limit(9,$ids*9);
        $this->db->order_by('id','DESC');
        return $this->db->get('video_gallery')->result();
        
    }
    
    
    function get_event($type){
        
        $this->db->limit(5);
        $this->db->where('category',$type);
        $this->db->order_by('id','DESC');
        return $this->db->get('events')->result();
    }
    
    function all_event($id,$type){
        
            if($id<=1 && $id==""){
			$ids=0;
			}else{
			$ids = $id-1;
			}
        
                $this->db->limit(15,$ids*15);
                $this->db->order_by('id','DESC');
        return  $this->db->get_where('events',array('category'=>$type))->result();
        
    }
    
    function getBlogPost($alias = ""){
        
         return  $this->db->get_where('events',array('alias'=>$alias))->row();
        
    }
    
    ///
    function get_xtra_book($type = ""){
        
        
//         if($id<=1 && $id==""){
//			$ids=0;
//			}else{
//			$ids = $id-1;
//			}
//        
//        
        /// $this->db->limit(12,$ids*12);
        $this->db->order_by('id','DESC');
        $this->db->where('type',$type);
        return $this->db->get('gallery')->result();
        
    }
    
    
    function getGalleryByType($type = ""){
        $this->db->limit(10);
        $this->db->order_by('id','DESC');
        $this->db->where('type',$type);
        return $this->db->get('gallery')->result();
    }
  
  /**
	 * Front end Page for this model.
	 *
	 * Maps to the following URL
	 * 		controller Name Index
	 * @author [[Subhashree]]
	 * @output [[ Front end Home page,Innerpage and gallery ]]         
	 */
  
  
    function get_inner_slider($id){
		
		$sngl['image'] = "";
		$this->db->where('parent_id',$id);
		$this->db->where('status', 1);
		 $this->db->order_by('slider_order','RANDOM');
        $query = $this->db->get('slider')->result_array();
		
		return $query;
		 
	}
	
	function get_common_section($id){
		
		$this->db->where('id',$id);
		$query = $this->db->get('common')->row_array();
	    return $query;
	    
	 }
	
	function get_news(){
		
		$this->db->limit(5);
		$this->db->where('category','news');
        $this->db->where('status','1');
		$this->db->order_by('date','DESC');
		$ext = $this->db->get('events')->result_array();
		return $ext;
	}
	
	function get_service(){
		
		$this->db->limit(4);
		$this->db->where('category','service');
        $this->db->order_by('order_short','ASC');
		$cond = $this->db->get('events')->result_array();
		return $cond;
	}
	
	function get_inner_page($url){
		
		//$this->db->where('url',$url);
		//return $this->db->get('pages')->row_array();
		//$this->db->where('author_id',$url);
     // $post = $this->db->get('pages')->num_rows();
         
        
	     $this->db->select('pages.*,author.name as authorname,author.profile_pic as image');
         $this->db->from('pages');
         $this->db->join('author','pages.author_id = author.id');
       // $this->db->where('pages.status',1);
         $this->db->where('url',$url);
        
         return $this->db->get()->row_array();
    
}	
	function get_contest_page($url){
	
	     $this->db->select('pages.*');
         $this->db->from('pages');
        /// $this->db->join('author','pages.author_id = author.id');
       // $this->db->where('pages.status',1);
         $this->db->where('url',$url);
        
         return $this->db->get()->row_array();
    
}	function get_inner_page_preview($url){
		
		//$this->db->where('url',$url);
		//return $this->db->get('pages')->row_array();
		//$this->db->where('author_id',$url);
     // $post = $this->db->get('pages')->num_rows();
         
        
	     $this->db->select('pages.*,author.name as authorname,author.profile_pic as image');
         $this->db->from('pages');
         $this->db->join('author','pages.author_id = author.id');
        //$this->db->where('pages.status',1);
         $this->db->where('pages.id',$url);
        
         return $this->db->get()->row_array();
    
}
    
    
    
    
    
function get_all_post($post){
    
    $this->db->where('author_id',$post);
    $this->db->where('status',1);
    $this->db->where_not_in('cat_id','SponsersAd');
   return  $this->db->get('pages')->num_rows();
    
}
    
        
function get_no_article($post_art){
    
    $this->db->where('author_id',$post_art);
    $this->db->where_not_in('cat_id','SponsersAd');
     $this->db->where('status',1);
   return  $this->db->get('pages')->num_rows();
    
}


    
    function all_author($url1){
        
         $this->db->select('pages.*,author.name as authorname,author.profile_pic as image');
         $this->db->from('pages');
         $this->db->join('author','pages.author_id = author.id');
         $this->db->where('pages.status',1);
         $this->db->where('author_id',$url1);
        
         return $this->db->get()->row_array();
        
        
    }
    
    
    
    
    function get_select_author($url1){
        
      
         $this->db->select('pages.*,author.name as authorname,author.profile_pic as image');
         $this->db->from('pages');
         $this->db->join('author','pages.author_id = author.id');
         $this->db->where('pages.status',1);
        $this->db->where('author_id',$url1);
         $this->db->where_not_in('cat_id','SponsersAd');
      
        return $this->db->get()->result_array();
        
    }
    
    
    
    function get_related_ad($subid,$pid){
        
      //  echo $subid;exit;
       $this->db->order_by('id','RANDOM');
        $this->db->where('sub_catid',$subid);
         $this->db->where('status',1);
         $this->db->where_not_in('cat_id','SponsersAd');
         $this->db->where_not_in('id',$pid);
        $this->db->limit(3);
        return $this->db->get('pages')->result_array();
        
    }
    
    function get_all_news(){
        
        
        //$this->db->limit(5);
		$this->db->where('category','news');
		$this->db->where('status','1');
		$this->db->order_by('date','DESC');
		$ext = $this->db->get('events')->result_array();
		return $ext;
        
    } 
	
	function create_testimonial($testdata){
		$query=$this->db->insert('testimonial',$testdata);
		return $query;
	}

    
    
    function right_tab($id){
        
        $this->db->order_by('sort','ASC');
        $this->db->where('status', 1);
        $this->db->where('parent_menu',$id);
        return $this->db->get('menu')->result_array();
        
    }
    
    
    public function approve_testimonials(){
       
        $this->db->where('status',1);
        $this->db->ORDER_BY('order_short','DESC');
        $test = $this->db->get('testimonial')->result_array();
           return $test;
  
    
    }
    
    function get_cond_menu($cond){ 
        $this->db->order_by('sort_2','ASC');
        return $this->db->get_where('menu',$cond)->result_array();
        
        
    }
   
    function get_all_bigcategory(){
       
         $this->db->order_by('sort_2','ASC');
         $this->db->where('big_article',1);
        ///$this->db->limit(3)
        return $this->db->get('menu')->result_array();
        
       
    }
    
      function get_big_articles($artid='',$subcatid=''){
    
         $this->db->select('*');
         $this->db->from('pages');
         $this->db->where('status',1);
         $this->db->where('id',$artid);
         $this->db->order_by('id','DESC');
         $this->db->limit(1);
         $article =  $this->db->get();
        
        if($article->num_rows()>0){
            return $article->result_array();
        }else{
            
        $this->db->select('*');
         $this->db->from('pages');
         $this->db->where('status',1);
         $this->db->where('sub_catid',$subcatid);
         $this->db->or_where('cat_id',$subcatid);
         $this->db->order_by('id','DESC');
         $this->db->limit(1);
        return $this->db->get()->result_array();
            
        }
        
        
    }
    function get_all_medcategory(){
        
       
         $this->db->order_by('id','DESC');
         $this->db->where('medium_article',1);
        
        return $this->db->get('menu')->result_array();
        
    }
    
    function get_med_articles($artid='',$subcatid=''){
        $this->db->select('*');
         $this->db->from('pages');
         $this->db->where('status',1);
         $this->db->where('id',$artid);
         $this->db->order_by('id','DESC');
         $this->db->limit(1);
         $article =  $this->db->get();
        
        if($article->num_rows()>0){
            return $article->result_array();
        }else{
            
        $this->db->select('*');
         $this->db->from('pages');
         $this->db->where('status',1);
         $this->db->where('sub_catid',$subcatid);
         $this->db->or_where('cat_id',$subcatid);
         $this->db->order_by('id','DESC');
         $this->db->limit(1);
        return $this->db->get()->result_array();
            
        }
    }
    
    function get_sponser_ad(){
        $this->db->where('cat_id','SponsersAd');
        $this->db->order_by('id','DESC');
         $this->db->where('status',1);
        $this->db->limit(1);
      return  $this->db->get('pages')->result_array();
        
    }
    
    function get_latest(){
        $this->db->select('*');
        $this->db->from('pages');
         $this->db->where('status',1);
        $this->db->where_not_in('cat_id','SponsersAd');
        $this->db->order_by('id','DESC');
        $this->db->limit(3);
     return   $this->db->get()->result_array();
        
        
    }
    
    function get_video_gallery(){
        
        $this->db->select('*');
        $this->db->from('video_gallery');
        $this->db->order_by('id','DESC');
        $this->db->limit(1);
        return $this->db->get()->row_array();
    }
   
    function home_get_top_ad(){
        $this->db->where('status',1);
        $this->db->where('position',1);
        $this->db->where('page','home');
       $this->db->order_by('id','DESC'); 
      return  $this->db->get('advertise')->result_array();
        
        
        
    } function article_get_top_ad(){
        $this->db->where('status',1);
        $this->db->where('position',1);
        $this->db->where('page','article');
       $this->db->order_by('id','DESC'); 
      return  $this->db->get('advertise')->result_array();
        
        
        
    }
    
    function get_top_ad(){
        $this->db->where('status',1);
        $this->db->where('position',1);
       $this->db->order_by('id','DESC'); 
      return  $this->db->get('advertise')->result_array();
        
        
        
    }
    
    function get_bottom_ad(){
        $this->db->where('status',1);
        $this->db->where('position',2);
       $this->db->order_by('id','DESC'); 
      return  $this->db->get('advertise')->result_array();
        
        
        
    }
    
    function home_get_right_ad(){
        $this->db->where('status',1);
        $this->db->where('position',3);
        $this->db->where('page','home');
       $this->db->order_by('id','DESC'); 
      return  $this->db->get('advertise')->result_array();
    
    }  
     
    function article_get_right_ad(){
        $this->db->where('status',1);
        $this->db->where('position',3);
        $this->db->where('page','article');
       $this->db->order_by('id','DESC'); 
      return  $this->db->get('advertise')->result_array();
    
    } 
    
    function get_right_ad(){
        $this->db->where('status',1);
        $this->db->where('position',3);
       
       $this->db->order_by('id','DESC'); 
      return  $this->db->get('advertise')->result_array();
        
        
        
    }
    
    function get_sponser_ad_view($url1){
        $this->db->where('cat_id','SponsersAd');
        $this->db->where('url',$url1);
         $this->db->where('status',1);
        $this->db->order_by('id','DESC');
        $this->db->limit(1);
      return  $this->db->get('pages')->row_array();
        
    }
    
    
     function get_category_page($name='',$id=''){
       
         
         $rtn = array();
          if($id<=1 && $id==""){
			$ids=0;
			}else{
			$ids = $id-1;
			}
         
         
        $cond['link'] = $name;
        
       $categ =  $this->db->get_where('menu',$cond)->row_array();
        
         if(!empty($categ)){
         
        if($categ['parent_menu'] == 0 && $categ['page_id']==0){
          
            $cond_page['pages.cat_id'] = $categ['id'];
            
        }else{
            
            $cond_page['pages.sub_catid'] = $categ['id'];
            
        }
         
      ////  $this->db->limit(2,$ids*2);
     //   $this->db->order_by('id','DESC');
     //  $alldata =  $this->db->get_where('pages',$cond_page)->result_array();
         
         
         $this->db->select('pages.*,author.name as authorname');
         $this->db->from('pages');
         $this->db->join('author','pages.author_id = author.id');
         $this->db->where('pages.status',1);
        
         $this->db->where($cond_page);
          $this->db->order_by('id','ASC');
         $this->db->limit(10,$ids*10);
         $alldata = $this->db->get()->result_array();
         
        $rtn['c_data'] = $alldata;
        $rtn['c_name'] = $categ['menu'];
        $rtn['allcdata'] = $categ;
         return $rtn;
         }else{
             return array();
         }
         
    } 
    
    function get_all_page($id){
        
         if($id<=1 && $id==""){
			$ids=0;
			}else{
			$ids = $id-1;
			}
         
        
         $this->db->select('pages.*,author.name as authorname');
         $this->db->from('pages');
         $this->db->join('author','pages.author_id = author.id');
         $this->db->where('pages.status',1);
         $this->db->where_not_in('pages.cat_id','SponsersAd');
         $this->db->order_by('id','DESC');
         $this->db->limit(10,$ids*10);
         return  $this->db->get()->result_array();
        

    }
    
    
    function get_most_popular_page($id){
        
         if($id<=1 && $id==""){
			$ids=0;
			}else{
			$ids = $id-1;
			}
         
        
         $this->db->select('pages.*,author.name as authorname');
         $this->db->from('pages');
         $this->db->join('author','pages.author_id = author.id');
         $this->db->where('pages.status',1);
         $this->db->where_not_in('pages.cat_id','SponsersAd');
         $this->db->order_by('pages.visit','DESC');
         $this->db->limit(10,$ids*10);
         return  $this->db->get()->result_array();
        

    }
      function get_all_rows(){
        
         $this->db->select('pages.*,author.name as authorname');
         $this->db->from('pages');
         $this->db->join('author','pages.author_id = author.id');
         $this->db->where('pages.status',1);
         $this->db->where_not_in('pages.cat_id','SponsersAd');
          $this->db->order_by('id','DESC');
         return $this->db->get()->num_rows();
        
      
    }
    
    
    function get_category_rows($name=''){
         
        $cond['link'] = $name;
       $categ =  $this->db->get_where('menu',$cond)->row_array();
        
        if(!empty($categ)){
        if($categ['parent_menu'] == 0 && $categ['page_id']==0){
            $cond_page['cat_id'] = $categ['id'];
        }else{
            $cond_page['sub_catid'] = $categ['id'];
        }
       
            $this->db->select('pages.*,author.name as authorname');
         $this->db->from('pages');
         $this->db->join('author','pages.author_id = author.id');
         $this->db->where('pages.status',1);
         $this->db->where($cond_page);
          $this->db->order_by('id','DESC');
        return  $alldata = $this->db->get()->num_rows();
            
       ///return  $this->db->get_where('pages',$cond_page)->num_rows();
        }
    }
    
    
    function get_search_rows($page){
        
        
        
        if(empty($page)){
            return 0;
        }
        
        $this->db->select('pages.*,author.name as authorname');
         $this->db->from('pages');
         $this->db->join('author','pages.author_id = author.id');
         $this->db->where('pages.status',1);
        
        if(!empty($page['name'])){
            
            $this->db->like('pages.name',$page['name']);
            $this->db->or_like('pages.right',$page['right']);
            
            return $this->db->get()->num_rows();


        }else{
            return 0;
        }
        
        
    }
    
    

     function get_most_popluar(){
         
        $this->db->where_not_in('visit',0);
         $this->db->where_not_in('cat_id','SponsersAd');
         $this->db->order_by('visit','DESC');
         $this->db->where('status',1);
         $this->db->limit(3);
         return $this->db->get('pages')->result_array();
     }
    
    
    function get_search_article($page,$id=''){
        
        if(empty($page)){
            return array();
        }
        
        if($id<=1 && $id==""){
			$ids=0;
			}else{
			$ids = $id-1;
			}
        
        
        $this->db->select('pages.*,author.name as authorname');
         $this->db->from('pages');
         $this->db->join('author','pages.author_id = author.id');
         $this->db->where('pages.status',1);
        $this->db->order_by('pages.id','DESC');
        $this->db->limit(10,$ids*10);
        if(!empty($page['name'])){
            
            $this->db->like('pages.name',$page['name']);
            $this->db->or_like('pages.right',$page['right']);
            
            return $this->db->get()->result_array();


        }
        
    }
    
    function get_cat_subcat($id){
     return $this->db->get_where('menu',array('id'=>$id))->row_array()['menu'];
        
    }
    
    
    
  function get_team_member(){
      
      $this->db->where('status',1);
      $this->db->order_by('id','DESC');
     return $this->db->get('team')->result_array();
  }
    
    function get_static_page($url_page){
    
      return  $this->db->get_where('common',array('url'=>$url_page))->row_array();
        
    }
    
    
    function youtube_video(){
        $this->db->order_by('id','DESC');
        return $this->db->get('video_gallery')->result_array();
    }
    
    
}



