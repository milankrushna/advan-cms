<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css?ver=1.0.1">
    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <!-- Material Design -->
    <link href="<?php echo base_url(); ?>assets/css/mdb.css?ver=1.0" rel="stylesheet">
    <!-- custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/style.css?ver=1.0.9" rel="stylesheet">
    <!-- fav icon  -->
    <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon.ico" type="image/x-icon">
</head>

<body>
    <!-------404 page not found--------->
    <div class="error-page">
        <div class="err-box">
            <h1>
                <span class="big-font">404</span><br>
                <span class="mid-font"><?php echo $heading; ?></span><br>
                <span class="small-font"><?php echo $message; ?></span><br>
                <span style="position:relative;top:50px;"><a href="<?php echo base_url(); ?>" class="btn btn-success">Back to Home</a></span>
            </h1>
        </div>
    </div>
    
    <!-------404 page not found--------->
  

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/tether.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <!-- core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/mdb.min.js"></script>
    <!-- smoothscroll core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/smoothscroll.js"></script>
    <script>//video play js
    $(document).ready(function() {
  $('.play-ico').on('click', function(ev) {
 $(this).fadeOut();
    $(".video-frame iframe")[0].src += "&autoplay=1";
    ev.preventDefault();
 
  });
});
    </script>
    <script>//search js
        $('.search-icon').click(function(){
        $('.search-form').stop().css('top','0');
        $('.close-btn').show();
        });
        $('.close-btn').click(function(){
        $('.search-form').stop().css('top','-400px');
        $('.close-btn').hide();
        });
    </script>
    <script>//scroll top fixed js
       $(document).ready(function(){
	   $(window).bind('scroll', function() {
	   var navHeight = $( window ).height() - 0;
			 if ($(window).scrollTop() > navHeight) {
				 $('.navbar').addClass('fixed');
			 }
			 else {
				 $('.navbar').removeClass('fixed');
			 }
		});
	});
    </script>
    <script>
        $('.social-main .share-btn').click(function(){
        $(this).siblings().slideToggle('slow');
        });
    </script>
</body>




</html>