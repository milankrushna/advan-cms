<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller {

	/**
	 * Front end Page for this controller.
	 *
	 * Maps to the following URL
	 * 		controller Name Index
	 * @author [[Milan Krushna & Subhashree]]
	 * @output [[ Front end Home page,Innerpage and gallery ]]         
	 */
	 function __construct()
	{
		parent::__construct();
         
         header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		
        //$this->load->driver('cache');
         ///$this->cache->clean();
         $this->load->library(array('form_validation','session'));
		$this->load->helper('url');
		$this->load->model('page_model');
		$this->load->database();
		$this->load->helper('language');
         
		
        /**
        * [[Common Section Setup]]       
        */
        
         $this->data['menu'] = $this->page_model->menu();
         $this->data['webdata'] = $this->db->get_where('my_website',array('id'=>'1'))->row();
         $this->data['right_menu'] = $this->page_model->right_tab('7');
         $this->data['menuCountry'] = $this->page_model->get_country();
         
         $this->data['tourStyle'] = $this->page_model->tour_style();
         
        

         //retrive footer section data
        $this->data['sectionwhy'] = $this->page_model->get_common_section('1');
	    $this->data['sectiondivercity'] = $this->page_model->get_common_section('2');
	    $this->data['sectionphyl'] = $this->page_model->get_common_section('6');
	    $this->data['sectiontourLove'] = $this->page_model->get_common_section('7');
	// $this->data['section5'] = $this->page_model->get_common_section('26');
	// $this->data['section6'] = $this->page_model->get_common_section('28');
	// $this->data['section7'] = $this->page_model->get_common_section('29');
	}
		
  /**
	 * Front end Page for this model.
	 *
	 * Maps to the following URL
	 * 		controller Name Index
	 * @author [[Subhashree]]
	 * @output [[ Front end Home page,Innerpage and gallery ]]         
	 */
  
	
	function index(){
	

        
        // print("<pre>");
        // print_r($this->data['tourStyle']);exit;
        
	///retrive Slider
    $this->data['slider'] = $this->page_model->get_inner_slider('1');
    

    // print("<pre>");
    // print_r($this->data['menuCountry']);exit;

        
	
	//$this->data['section1'] = $this->page_model->get_common_section('22');
	//$this->data['section2'] =  $this->page_model->get_common_section('23');
	
        
	//$this->data['news'] = $this->page_model->get_news();
    $this->data['service'] = $this->page_model->get_service();
    $this->data['menuCountryhome'] = $this->page_model->get_country_home();
    $this->data['tourhome'] = $this->page_model->get_ctour_home();
	//$this->data['left_menu'] = $this->page_model->left_menu(); 
	
        // $this->data['sponserad'] = $this->page_model->get_sponser_ad();
        // $this->data['latest_news'] = $this->page_model->get_latest();
        // $this->data['video_gallery'] = $this->page_model->get_video_gallery();
        
    //     $this->data['most_popular'] = $this->page_model->get_most_popluar();
    //    $this->data['top_advertise'] = $this->page_model->home_get_top_ad();
    //    $this->data['right_advertise'] = $this->page_model->home_get_right_ad();
      
      
		$this->load->view('common_page/header',$this->data);
		$this->load->view('page/mainbody',$this->data);
		$this->load->view('common_page/footer',$this->data);
	
	
	}

    
    
    
    
    

	
	function inner_page($url="",$url1='',$url2=''){
		///echo "bfjkgdjksg";
         $this->data['latest_news1'] = $this->page_model->get_latest();
         $this->data['sponser_ad'] =  $this->page_model->get_sponser_ad();
       $this->data['right_advertise'] = $this->page_model->home_get_right_ad();
     ////category pages 
		if($url == 'category'){
             
     	 
$this->load->library('pagination');

$config['base_url'] = site_url('category/'.$url1);
$config['total_rows'] = $this->page_model->get_category_rows($url1); 
$config['per_page'] = 10;  
$config['uri_segment'] = 3;
$config['use_page_numbers'] = TRUE;        
$config['cur_tag_open'] ='<li class="page-item active"><a class="page-link">';
$config['cur_tag_close'] ='</a></li>';
$config['full_tag_open'] = '<ul class="pagination pg-purple">';
$config['full_tag_close'] = '</ul>';

$config['first_tag_open'] = '<li class="page-item">';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li class="page-item">';
$config['last_tag_close'] = '</li>';
$config['next_tag_open'] = '<li class="page-item">';
$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li class="page-item">';
$config['prev_tag_close'] = '</li>';
$config['num_tag_open'] = '<li class="page-item">';
$config['num_tag_close'] = '</li>';
            
$this->pagination->initialize($config); 
$this->data['pagination'] =  $this->pagination->create_links();
          
            
 $allCtgData= $this->page_model->get_category_page($url1,$url2);
    
        //    print("<pre>");
        //     print_r($allCtgData);
        //    print("<pre>");exit;
  //print_r($allCtgData);exit;
            
            
if(!empty($allCtgData)){           
$this->data['category'] = $allCtgData['c_data']; 
$this->data['cname'] = $allCtgData['c_name']; 
      $this->data['pg_title'] = $allCtgData['allcdata']['meta_title'];        
      $this->data['seo_desc'] = $allCtgData['allcdata']['meta_desc'];          
      $this->data['keyword_seo'] = $allCtgData['allcdata']['meta_keyword'];          
            
//       echo $this->data['pagination'];    
  // print("<pre>");
  //print_r($this->data['category']);exit;
//    
         
}
            
        $this->load->view('common_page/header',$this->data);
		$this->load->view("page/inner_category",$this->data);
		$this->load->view('common_page/footer',$this->data);
     
             
            
        }else if($url == 'sponserad'){
        
        ////Your sponseradd code goes here
        //url will be  site_url('sponserad/$page['url']') ;  
            
          //  echo $url1;
            
            
          $this->data['sponser_ad_view'] = $ad = $this->page_model->get_sponser_ad_view($url1);   
            
           
           //  print("<pre>");
 // print_r($this->data['sponser_ad_view']);exit;
            
            if(!empty($ad)){ 
        
         if($ad['cat_id'] == 'SponsersAd'){ 
            $st_ad = $ad['visit'];
            $dt_ad =  $ad['id'];
            $data = ++$st_ad;
            // echo $data;exit;
            
            $view = $this->db->update('pages',array('visit' => $data), array('id' => $dt_ad));
            }
                
        $this->data['pg_title'] = $ad['name'].'|'.$this->data['webdata']->meta_title;  
            
            
        $this->load->view('common_page/header',$this->data);
		$this->load->view('page/inner_sponser_ad',$this->data);
		$this->load->view('common_page/footer',$this->data);
          
            }else{
                show_404();
            }
		
            
        
        }else if($url == 'team'){
             $this->data['section_team1'] = $this->page_model->get_common_section('30');
             $this->data['section_team2'] = $this->page_model->get_common_section('31');
            
            
            $this->data['team'] = $this->page_model->get_team_member();
            
        //   print("<pre>");
    ///  print_r($this->data['team']);exit;
        
       $this->load->view('common_page/header',$this->data);
		$this->load->view('page/team');
		$this->load->view('common_page/footer',$this->data);
            
            
            
        ////Your Team code goes here
        
        }else if($url == 'contactUs'){
            
        $this->data['section_contact'] = $commn = $this->page_model->get_common_section('32');
          
            
	$name = $this->input->post('name');
	$email = $this->input->post('email');
	$subject = $this->input->post('subject');
	//$address = $this->input->post('address');
	$feedback = $this->input->post('message');
    
      // echo $name;
      /// echo $email;
      // echo $subject;
     //  echo $feedback;exit;
    
if(!empty($name) && !empty($email) && !empty($subject) && !empty($feedback)){

	$feedmail_msg = '
<!DOCTYPE html>
<html lang="en">
  
    <body>

    <table style="min-width: 700px;min-height: 400px;margin: auto;border: 20px solid #0077b8;margin-top: 20px;background: #fff;width: 65%;" class="feedbackDetail">
        <center><img src="'.base_url().'admin/assets/user/images/logo.png" class="animated fadeIn img-responsive"></center>
        <tbody >
            <tr><td style="background-color:#c3ddf4;"><h3 style="text-align:center; margin:8px 0px;">Feedback</h3></td></tr> 
            <tr>
            <td style=" width: 100%;padding: 10px;background-color: aliceblue;" ><strong>Name: </strong>'.$name.'</td>
            </tr>
            <tr>
            <td style=" width: 100%;padding: 10px;background-color: aliceblue;" ><strong>Email: </strong>'.$email.'</td>
            </tr>
            <tr>
            <td style=" width: 100%;padding: 10px;background-color: aliceblue;" ><strong>Subject: </strong>'.$subject.'</td>
            </tr>
            <tr>
            <td style=" width: 100%;padding: 10px;background-color: aliceblue;" ><strong>Message: </strong>'.$feedback.'</td>
            </tr>
        </tbody>
    </table>
 
    </body>
</html>
';
$form=$email;

    
$config['mailtype'] = 'html';
$this->load->library('email',$config);
///$this->email->initialize($config);     
$this->email->from($email,$name);
$this->email->to('info@mycitylinks.in'); 
$this->email->subject('Mycitylink');
$this->email->message($feedmail_msg);	

   

if($this->email->send()){
 
$this->session->set_flashdata('message','Your Feedback Submitted Successfully');
 //echo 'success';exit;
 
}else{
    
$this->session->set_flashdata('message','Something Error,Try again.');
    
}
redirect('contactUs');

}
            
      
      $this->data['pg_title'] = $commn['meta_title'];        
      $this->data['seo_desc'] = $commn['meta_desc'];          
      $this->data['keyword_seo'] = $commn['meta_keyword'];
            
  // redirect('contactUs');
 //exit;
        $this->load->view('common_page/header',$this->data);
		$this->load->view('page/ContactUs');
		$this->load->view('common_page/footer',$this->data);
        
        }else if($url == 'sub_mycitylinks'){
            
            $submail = $this->input->post('email_id');
            
           
            if(!empty($submail)){

	$feedmail_msg = '
<!DOCTYPE html>
<html lang="en">
  
    <body>

    <table style="min-width: 700px;min-height: 400px;margin: auto;border: 20px solid #0077b8;margin-top: 20px;background: #fff;width: 65%;" class="feedbackDetail">
        <center><img src="'.base_url().'admin/assets/user/images/logo.png" class="animated fadeIn img-responsive"></center>
        <tbody >
            <tr><td style="background-color:#c3ddf4;"><h3 style="text-align:center; margin:8px 0px;">Subscribe Email</h3></td></tr>
            <tr>
            <td style=" width: 100%;padding: 10px;background-color: aliceblue;" ><strong>Email: </strong>'.$submail.'</td>
            </tr>
        </tbody>
    </table>
 
    </body>
</html>
';
$form=$submail;

    
$config['mailtype'] = 'html';
$this->load->library('email',$config);
///$this->email->initialize($config);     
$this->email->from($submail,$form);
$this->email->to('info@mycitylinks.in'); 
$this->email->subject('Mycitylink');
$this->email->message($feedmail_msg);	
if($this->email->send()){
    
    echo "Your Email Successfully  subscribed";
    
}else{
    echo "Something Error Try againg later";
    
}
            }else{
    echo "Something Missing.";
                
            }
            
            
        }else if($url == 'search_article'){
            
            
            if(!empty($_REQUEST['per_page'])){
          $pagenum = $_REQUEST['per_page'];
            }else{
          $pagenum = 0;
            }
            $page['name'] = $_REQUEST['article'];
            $page['right'] = $_REQUEST['article'];
            
            
            if(empty($_REQUEST['article'])){
              $this->data['pg_title'] = $page['name'];
              $this->data['article_key'] = $_REQUEST['article'];        
              $this->data['search'] = array();//$this->page_model->get_search_article($page,$pagenum);
              $this->data['pagination'] = "";
            }else{
            $this->data['pg_title'] = $page['name'];   
            
            
            $this->load->library('pagination');

$config['base_url'] = site_url('search_article?article='.$_REQUEST['article']);
$config['total_rows'] = $this->page_model->get_search_rows($page,$pagenum); 
$config['per_page'] = 10;  
$config['uri_segment'] = 3;
$config['use_page_numbers'] = TRUE;  
$config['reuse_query_string'] = TRUE;
$config['enable_query_strings'] = TRUE;
$config['page_query_string'] = TRUE;
$config['cur_tag_open'] ='<li class="page-item active"><a class="page-link">';
$config['cur_tag_close'] ='</a></li>';
$config['full_tag_open'] = '<ul class="pagination pg-purple">';
$config['full_tag_close'] = '</ul>';

$config['first_tag_open'] = '<li class="page-item">';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li class="page-item">';
$config['last_tag_close'] = '</li>';
$config['next_tag_open'] = '<li class="page-item">';
$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li class="page-item">';
$config['prev_tag_close'] = '</li>';
$config['num_tag_open'] = '<li class="page-item">';
$config['num_tag_close'] = '</li>';
            
$this->pagination->initialize($config); 
$this->data['pagination'] =  $this->pagination->create_links();
            
   $this->data['article_key'] = $_REQUEST['article'];        
   $this->data['search'] = $this->page_model->get_search_article($page,$pagenum);
            }
            
        $this->load->view('common_page/header',$this->data);
		$this->load->view('page/search_page',$this->data);
		$this->load->view('common_page/footer',$this->data);
            
        
        
        }else if($url == 'author'){
            
      //  echo "am here";exit;
            
        // $this->data['all_author'] = $all = $this->page_model->all_author(); 
            
      //   print("<pre>");
      // print_r($all);exit;
            
            
           $this->data['auth_article'] =  $this->page_model->get_select_author($url1); 
             
            $this->data['author_page'] = $all = $this->page_model->all_author($url1);

              
            
        
               $this->data['no_post'] = $this->page_model->get_no_article($all['author_id']);
          
     
           // print("<pre>");
          //  print_r($this->data['author_page']);exit;
            
            
       $this->load->view('common_page/header',$this->data);
        $this->load->view('page/category_author',$this->data);
      $this->load->view('common_page/footer',$this->data);
            
            
       
            
            
                  
        }else if($url == 'all_article'){
         
            
              $this->load->library('pagination');

$config['base_url'] = site_url('all_article');
$config['total_rows'] = $this->page_model->get_all_rows();            
$config['per_page'] = 10;  
$config['uri_segment'] = 2;  
$config['use_page_numbers'] = TRUE;  

$config['cur_tag_open'] ='<li class="page-item active"><a class="page-link">';
$config['cur_tag_close'] ='</a></li>';
$config['full_tag_open'] = '<ul class="pagination pg-purple">';
$config['full_tag_close'] = '</ul>';

$config['first_tag_open'] = '<li class="page-item">';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li class="page-item">';
$config['last_tag_close'] = '</li>';
$config['next_tag_open'] = '<li class="page-item">';
$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li class="page-item">';
$config['prev_tag_close'] = '</li>';
$config['num_tag_open'] = '<li class="page-item">';
$config['num_tag_close'] = '</li>';
   
            
$this->pagination->initialize($config); 
$this->data['pagination'] =  $this->pagination->create_links();
           
$allData = $this->page_model->get_all_page($url1);
    
          
if(!empty($allData)){           
$this->data['category'] = $allData; 
$this->data['pg_title'] = 'Latest Article'.'|'.$this->data['webdata']->meta_title;        
$this->data['hed_title'] = 'Latest Article';        
            
//       echo $this->data['pagination'];    
   // print("<pre>");
  //print_r($this->data['category']);exit;
//    
 
    
        $this->load->view('common_page/header',$this->data);
		$this->load->view("page/inner_category",$this->data);
		$this->load->view('common_page/footer',$this->data);
    
}      
            
        }else if($url == 'most_popular'){
         
            
              $this->load->library('pagination');

$config['base_url'] = site_url('most_popular');
$config['total_rows'] = $this->page_model->get_all_rows();            
$config['per_page'] = 10;  
$config['uri_segment'] = 2;  
$config['use_page_numbers'] = TRUE;  

$config['cur_tag_open'] ='<li class="page-item active"><a class="page-link">';
$config['cur_tag_close'] ='</a></li>';
$config['full_tag_open'] = '<ul class="pagination pg-purple">';
$config['full_tag_close'] = '</ul>';

$config['first_tag_open'] = '<li class="page-item">';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li class="page-item">';
$config['last_tag_close'] = '</li>';
$config['next_tag_open'] = '<li class="page-item">';
$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li class="page-item">';
$config['prev_tag_close'] = '</li>';
$config['num_tag_open'] = '<li class="page-item">';
$config['num_tag_close'] = '</li>';
   
            
$this->pagination->initialize($config); 
$this->data['pagination'] =  $this->pagination->create_links();
           
$allData = $this->page_model->get_most_popular_page($url1);
    
          
if(!empty($allData)){           
$this->data['category'] = $allData; 
$this->data['pg_title'] = 'Most Popular Article'.'|'.$this->data['webdata']->meta_title;        
$this->data['hed_title'] = 'Most Popular';        
            
//       echo $this->data['pagination'];    
   // print("<pre>");
  //print_r($this->data['category']);exit;
//    
 
    
        $this->load->view('common_page/header',$this->data);
		$this->load->view("page/inner_category",$this->data);
		$this->load->view('common_page/footer',$this->data);
    
}      
            
        }else if($url == 'content'){
            
         $this->data['contentstatis'] = $commn = $this->page_model->get_static_page($url1);
         
      $this->data['pg_title'] = $commn['meta_title'];        
      $this->data['seo_desc'] = $commn['meta_desc'];          
      $this->data['keyword_seo'] = $commn['meta_keyword'];    
            
          if(empty($url1) || empty($this->data['contentstatis'])){
              show_404();
          }
            
        $this->load->view('common_page/header',$this->data);
		$this->load->view('page/static_page',$this->data);
		$this->load->view('common_page/footer',$this->data);
            
           /// print_r($this->data['contentstatis']);
             
        }else if($url == 'videogallery'){
            
         $this->data['video'] = $this->page_model->youtube_video();
          
            
            //print("<pre>");
            //print_r($this->data['video']);
            
        $this->load->view('common_page/header',$this->data);
		$this->load->view('page/videogall',$this->data);
		$this->load->view('common_page/footer',$this->data);
            
           /// print_r($this->data['contentstatis']);
             
        }else if($url == 'preview_load_system'){
            
            $this->data['top_advertise'] = $this->page_model->article_get_top_ad();
       $this->data['right_advertise'] = $this->page_model->article_get_right_ad();
            
        $this->data['pages'] = $pdata = $this->page_model->get_inner_page_preview($url1);
    
            /*if(empty($pdata) || $url2 != $pdata['id']){
                show_404();
            }*/
            
            
            if(!empty($pdata['author_id'])){ 
        $this->data['all_post'] = $this->page_model->get_all_post($pdata['author_id']);
            }
            
         
  //print("<pre>");
 //print_r($this->data['pages']);exit;
      
        if(!empty($this->data['pages'])){
           
            if($pdata['cat_id']!= 'SponsersAd'){ 
            $st = $pdata['visit'];
            $dt =  $pdata['id'];
            $data = ++$st;
             
            $view = $this->db->update('pages',array('visit' => $data), array('id' => $dt));
            }
    
        $this->data['related_ad'] = $redata = $this->page_model->get_related_ad($pdata['sub_catid'],$pdata['id']);
   
      $this->data['pg_title'] = $pdata['name'];        
            
		
		$this->load->view('common_page/header',$this->data);
		$this->load->view('page/inner_detail',$this->data);
		$this->load->view('common_page/footer',$this->data);
            
                 
            }else{
                show_404();
            }
		
            
            
            
        }else if($url == 'contest'){
        
            $this->data['top_advertise'] = $this->page_model->article_get_top_ad();
       $this->data['right_advertise'] = $this->page_model->article_get_right_ad();
            
		$this->data['pages'] = $pdata = $this->page_model->get_contest_page($url1);
    if(empty($pdata)){
                show_404();
            }
            
            if(!empty($this->data['pages'])){
           
            if($pdata['cat_id']!= 'SponsersAd'){ 
            $st = $pdata['visit'];
            $dt =  $pdata['id'];
            $data = ++$st;
             
            $view = $this->db->update('pages',array('visit' => $data), array('id' => $dt));
            }
            
            $this->data['pg_title'] = $pdata['name'];  
    
        
            
		$this->load->view('common_page/header',$this->data);
		$this->load->view('page/inner_contest',$this->data);
		$this->load->view('common_page/footer',$this->data);
            
                 
            }else{
                show_404();
            }    
                
                
            
    }else{ 
            /////page detail 
            
            
       $this->data['top_advertise'] = $this->page_model->article_get_top_ad();
       $this->data['right_advertise'] = $this->page_model->article_get_right_ad();
            
		$this->data['pages'] = $pdata = $this->page_model->get_inner_page($url);
    if(empty($pdata)){
                show_404();
            }
            
            if(!empty($pdata['author_id'])){ 
        $this->data['all_post'] = $this->page_model->get_all_post($pdata['author_id']);
            }
            
         
  //print("<pre>");
 //print_r($this->data['pages']);exit;
      
        if(!empty($this->data['pages'])){
           
            if($pdata['cat_id']!= 'SponsersAd'){ 
            $st = $pdata['visit'];
            $dt =  $pdata['id'];
            $data = ++$st;
             
            $view = $this->db->update('pages',array('visit' => $data), array('id' => $dt));
            }
    
        $this->data['related_ad'] = $redata = $this->page_model->get_related_ad($pdata['sub_catid'],$pdata['id']);
   
      $this->data['pg_title'] = $pdata['name'];        
            
		
		$this->load->view('common_page/header',$this->data);
		$this->load->view('page/inner_detail',$this->data);
		$this->load->view('common_page/footer',$this->data);
            
                 
            }else{
                show_404();
            }
		
        
        }
	}
   	
  

    
	
	    function album($id = ""){
         

$this->data['pg_title'] = "Album";
		 
$this->load->library('pagination');

$config['base_url'] = site_url('morealbum');
$config['total_rows'] = $this->db->get('album')->num_rows();
$config['per_page'] = 15; 
$config['uri_segment'] = 3;
$config['use_page_numbers'] = TRUE;        
$config['cur_tag_open'] ='<li><a style="color:red">';
$config['cur_tag_close'] ='</a></li>';
$config['full_tag_open'] = '<div class="pagination">';
$config['full_tag_close'] = '</div>';

 
$this->pagination->initialize($config); 
$this->data['pagination'] =  $this->pagination->create_links();
$this->data['album'] = $this->page_model->get_album($id);
        
        
        $this->load->view('common_page/header',$this->data);
		$this->load->view("page/album",$this->data);
		$this->load->view('common_page/footer',$this->data);
        
    }
	
	
	
    function photogallery($album=""){
        
        if($album==""){
            show_404();
        }
            
		$this->data['pg_title'] = "Photo Gallery";
        $this->data['images'] = $this->page_model->get_images($album);
        
        $this->load->view("common_page/header",$this->data);
		$this->load->view("page/photoGallery",$this->data);
		$this->load->view("common_page/footer",$this->data);
        
    }
    
	function get_inrmenu(){
        
        
        $menu['parent_menu'] =""; 
        $id = $this->input->post('id');
        $cond['id'] = $id;
        $menu = $this->db->get_where('menu',$cond)->row_array();
       if(!empty($menu)){
	   	
        if($menu['parent_menu'] == 0){
            $cond2['parent_menu'] = $menu['id'];
          
             $this->db->order_by('sort','ASC');
            $allmenu = $this->db->get_where('menu',$cond2)->result_array();
          echo '<ul class="links wow fadeInUp animated">';
            
            foreach($allmenu as $lm){
                if($lm['custom'] == 0){ if($lm['link'] == '#'){ $nmn = '#';  }else{ $nmn = site_url($lm['link']);} }else{ $nmn = $lm['link']; }
                
     echo   '<li><a href="'.$nmn.'">'.$lm['menu'].'</a></li>';
                                    
                                 
                
            }
            
            echo '</ul>';
            
            //  echo 'No Data Found';
        }else{
            $cond2['parent_menu'] = $menu['parent_menu'];
          $this->db->order_by('sort','ASC');
            $allmenu = $this->db->get_where('menu',$cond2)->result_array();
          echo '<ul class="links wow fadeInUp animated">';
            
            foreach($allmenu as $lm){
                if($lm['custom'] == 0){ if($lm['link'] == '#'){ $nmn = '#';  }else{ $nmn = site_url($lm['link']);} }else{ $nmn = $lm['link']; }
                
     echo   '<li><a href="'.$nmn.'">'.$lm['menu'].'</a></li>';
                                    
                                 
                
            }
            
            echo '</ul>';
        }
        
        }
    }
	
	
/**
* @name          create testimonial
* @author        Subhashree Jena
* @revised       null
* @Description   create testimonial in innerpage 
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
	
	
	
    function create_testimonial(){
       

		$this->form_validation->set_rules('first_name', "First name", 'required');

		if ($this->form_validation->run() == TRUE)
		{
			$date = $this->input->post('date');
			$data['first_name'] = $this->input->post('first_name');
			$data['last_name'] = $this->input->post('last_name');
			$data['company'] = $this->input->post('company');
			$data['phone'] = $this->input->post('phone');
			$data['email'] = $this->input->post('email');
			$data['subject'] = $this->input->post('subject');
			$data['comment'] = $this->input->post('comment');
			$data['date'] =  date("Y-m-d", strtotime($date));
            if($_FILES['file']['name'] !=""){
			$upload = $this->upload_image('file');
            if($upload != ""){
                $data['cover_pic'] = $upload;
            }else{
            $this->session->set_flashdata('Error','Something Error, try Again');
            redirect('testimonial-write');
            }
            }
			
			$event_id =  $this->page_model->create_testimonial($data);
			if($event_id)
			{
                $this->session->set_flashdata('success','Your testimonial Successfully Posted');
			redirect('testimonial-write');
			}
		}
		else
		{
			echo "not uplaod";	
		}
        
    }
        
   
/**
* @name          upload and resize 
* @author        Subhashree Jena
* @revised       null
* @Description   uploading and resizeing file
* @Param String  $field is field name of file
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
	
 
    
public function upload_image($field){
 
        $new_img =  strtotime("now").'.jpg';
        $config['upload_path'] = './admin/upload/events/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']	= '30000000';
        $config['max_width'] = '10240000';
        $config['max_height'] = '70000';
        $config['file_name'] = $new_img;

$this->load->library('upload', $config);
$this->upload->initialize($config);
 if($this->upload->do_upload($field)){
     
   $this->resize_image($new_img);
   $data = array('upload_data' => $this->upload->data());
     
     return $data['upload_data']['file_name'];
  
 }else{
  
     echo $this->upload->display_errors();exit;
 $this->session->set_flashdata('message',$this->upload->display_errors());
     return false; 
     
 }

  
}
  
    function resize_image($new_img){
        
        
      $this->load->library('image_lib');
 


            $src_path = 'upload/events/' . $new_img;
            $des_path =  'upload/events/min_events/' . $new_img;
 
            $config['image_library']    = 'gd2';
            $config['source_image']     = $src_path;
            $config['new_image']        = $des_path;
            $config['maintain_ratio']   = TRUE;
            $config['width']            = 241;
            $config['height']           = 179;
            

            $this->image_lib->initialize($config);
            $this->image_lib->resize();
        
    }



    /////adhvan-------
    
    function location($location=""){
        if($location == ""){
            show_404();
        }

        $this->data['countryData'] = $cdata = $this->page_model->getCountryData($location);
        if(empty($this->data['countryData'])){
            show_404();
        }
        $countryId = $cdata->id;

        $this->data['regionData'] = $this->page_model->get_regionData($countryId);
        $this->data['styleData'] = $this->page_model->get_styledata();
        $this->data['tours'] = $this->page_model->get_tours_lcn($countryId);

        

        //    print("<pre>");
        //    print_r($this->data['tours']);exit;

        $this->load->view('common_page/header',$this->data);
		$this->load->view('location/locationWiseData',$this->data);
		$this->load->view('common_page/footer',$this->data);
    }

    function experience($location="",$tours="",$order="any",$pid=""){
//echo $tours;

$this->data['allstyleData'] =   $this->page_model->get_all_styledata_pg();
$this->data['allCountryData'] =   $this->page_model->get_all_country();

$this->data['location']="";
$this->data['noofdisp']="";
$this->data['order']="";
$this->data['pageno']="";
        if($location == ""){
            show_404();
        }
       /// echo $tours;
        $this->data['countryData'] = $cdata = $this->page_model->getCountryData($location);
        $countryId  = "";
if($location != "all"){
        $countryId = $cdata->id;
}
        //$this->data['regionData'] = $this->page_model->get_regionData($countryId);
        $this->data['styleData'] = $this->page_model->get_styledata();

        if($tours=="" || is_numeric($tours)){

//here $tour is no of result will show

if(empty($tours)){
    $tours = 12;
}

if($location == "all"){
    $countryId = 'all';
 }

$this->load->library('pagination');

$config['base_url'] = site_url('experience/'.$location.'/'.$tours.'/'.$order);;
$config['total_rows'] = $this->page_model->get_country_tours_row($countryId); 
$config['per_page'] = $tours;  
$config['uri_segment'] = 5;
$config['use_page_numbers'] = TRUE;        
$config['cur_tag_open'] ='<li class="page-item active"><a class="page-link">';
$config['cur_tag_close'] ='</a></li>';
$config['full_tag_open'] = '<ul class="pagination pg-purple">';
$config['full_tag_close'] = '</ul>';

$config['first_tag_open'] = '<li class="page-item">';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li class="page-item">';
$config['last_tag_close'] = '</li>';
$config['next_tag_open'] = '<li class="page-item">';
$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li class="page-item">';
$config['prev_tag_close'] = '</li>';
$config['num_tag_open'] = '<li class="page-item">';
$config['num_tag_close'] = '</li>';
            
$this->pagination->initialize($config); 
$this->data['pagination'] =  $this->pagination->create_links();
          
$this->data['location']=$location;
$this->data['noofdisp']=$tours;
$this->data['order']=$order;
$this->data['pageno']=$pid;
 ///$allCtgData= $this->page_model->get_category_page($url1,$url2);
    

        $this->data['countryTours'] = $this->page_model->get_country_tours($countryId,$tours,$order,$pid);


        //    print("<pre>");
        //    print_r($this->data['countryData']);
        //    print_r($this->data['tourData']);exit;

        $this->load->view('common_page/header',$this->data);
		$this->load->view('tours/countryTours',$this->data);
        $this->load->view('common_page/footer',$this->data);
        
        }else{
            $this->data['tourData'] = $trdata = $this->page_model->gettourDetails($tours);
            if(empty($this->data['tourData'])){
                show_404();
            }

            $tid = $trdata->id;
            $this->data['tourTrip'] = $this->page_model->get_tours_trip($tid);   
            $this->data['tourDay'] = $this->page_model->get_tours_day($tid);   
            $this->data['relateTours'] = $this->page_model->get_related_tours($countryId,$tid);   

            //     print("<pre>");
            //    print_r($this->data['relateTours']);exit;
            //    print_r($this->data['tourData']);
            $this->load->view('common_page/header',$this->data);
            $this->load->view('tours/tourDetails',$this->data);
            $this->load->view('common_page/footer',$this->data);
        }
    }
function filter_style(){
   /// print_r($this->input->post());
   $this->data['countryTours'] = $this->page_model->getfilter_style($this->input->post());
   $this->load->view('tours/filterResult',$this->data);

}

    function style($location="",$tours="",$order="any",$pid=""){
        //echo $tours;
                if($location == ""){
                    show_404();
                }
                $this->data['allstyleData'] =   $this->page_model->get_all_styledata_pg();
                $this->data['allCountryData'] =   $this->page_model->get_all_country();
                $this->data['styleData'] = $sttlda =  $this->page_model->get_styledata_pg($location);
                if(empty($tours)){
                    $tours = 12;
                }     
                $sid = $sttlda->id;
$this->load->library('pagination');

$config['base_url'] = site_url('style/'.$location.'/'.$tours.'/'.$order);;
$config['total_rows'] = $this->page_model->get_style_tours_row($sid); 
$config['per_page'] = $tours;  
$config['uri_segment'] = 5;
$config['use_page_numbers'] = TRUE;        
$config['cur_tag_open'] ='<li class="page-item active"><a class="page-link">';
$config['cur_tag_close'] ='</a></li>';
$config['full_tag_open'] = '<ul class="pagination pg-purple">';
$config['full_tag_close'] = '</ul>';

$config['first_tag_open'] = '<li class="page-item">';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li class="page-item">';
$config['last_tag_close'] = '</li>';
$config['next_tag_open'] = '<li class="page-item">';
$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li class="page-item">';
$config['prev_tag_close'] = '</li>';
$config['num_tag_open'] = '<li class="page-item">';
$config['num_tag_close'] = '</li>';
            
$this->pagination->initialize($config); 
$this->data['pagination'] =  $this->pagination->create_links();
          
$this->data['location']=$location;
$this->data['noofdisp']=$tours;
$this->data['order']=$order;
$this->data['pageno']=$pid;
 ///$allCtgData= $this->page_model->get_category_page($url1,$url2);
    
                // $this->data['countryData'] = $cdata = $this->page_model->getCountryData($location);
        
                // $countryId = $cdata->id;
        
                // $this->data['regionData'] = $this->page_model->get_regionData($countryId);
                
                $this->data['countryTours'] = $this->page_model->get_style_tours($sid,$tours,$order,$pid);
        
        
                //    print("<pre>");
                //    print_r($this->data['countryTours']);
                //    print_r($this->data['styleData']);exit;
        
                $this->load->view('common_page/header',$this->data);
                $this->load->view('tours/styleTours',$this->data);
                $this->load->view('common_page/footer',$this->data);
                
                
            }

    function hotels($location="",$tours=""){
        $this->data['sectionhotel'] = $this->page_model->get_common_section('8');

        $this->load->view('common_page/header',$this->data);
		$this->load->view('hotels/countryHotels',$this->data);
        $this->load->view('common_page/footer',$this->data);
        
    }
    function adhvanpage($location=""){
        $this->data['data'] = $this->db->get_where('common',array('url'=>$location))->row();

        if(empty( $this->data['data'] )){
            show_404();
        }

        $this->load->view('common_page/header',$this->data);
		$this->load->view('tours/contentPage',$this->data);
        $this->load->view('common_page/footer',$this->data);
        
    }
    /*function experiance($location="",$tours=""){
//echo $tours;
        if($location == ""){
            show_404();
        }

        $this->data['countryData'] = $cdata = $this->page_model->getCountryData($location);

        $countryId = $cdata->id;

        $this->data['regionData'] = $this->page_model->get_regionData($countryId);
        $this->data['styleData'] = $this->page_model->get_styledata();

        if($tours==""){
        $this->data['countryTours'] = $this->page_model->get_country_tours($countryId);


        //    print("<pre>");
        //    print_r($this->data['countryData']);
        //    print_r($this->data['tourData']);exit;

        $this->load->view('common_page/header',$this->data);
		$this->load->view('tours/countryTours',$this->data);
        $this->load->view('common_page/footer',$this->data);
        
        }else{
            $this->data['tourData'] = $trdata = $this->page_model->gettourDetails($tours);
            if(empty($this->data['tourData'])){
                show_404();
            }

            $tid = $trdata->id;
            $this->data['tourTrip'] = $this->page_model->get_tours_trip($tid);   
            $this->data['tourDay'] = $this->page_model->get_tours_day($tid);   

            //     print("<pre>");
            //    print_r($this->data['countryData']);
            //    print_r($this->data['tourData']);exit;
            $this->load->view('common_page/header',$this->data);
            $this->load->view('tours/tourDetails',$this->data);
            $this->load->view('common_page/footer',$this->data);
        }
    }
*/

}



