
<div class="parallax" style="background-image:url(<?php echo base_url((!empty($countryData->bannerFile))?$countryData->bannerFile : "images/south-asia.jpg" ); ?>)"></div>
<div class="container">
<div class="row">
	<div class="col-lg-2"></div>
    <div class="col-lg-8 bannerbot-in">
    <h2 class="text-center"><?php echo $countryData->name; ?></h2>
    <div class="top-ban-search w-100 m-auto">
        
 <div class="row">
 <div class="col-lg-3 txt-4">Tour Finder </div>
 <div class="col-lg-3 txt-4"><select class="mdb-select md-form" required>
  <option value=""  >Region</option>
<?php foreach($regionData as $rd){ ?>
  <option value="<?php echo $rd->alias; ?>"><?php echo $rd->name; ?></option>
<?php  } ?>
</select></div>
 <div class="col-lg-3 txt-4"><select class="mdb-select md-form" required>
  <option value=""  >Style</option>
  <?php foreach($styleData as $sd){ ?>
  <option value="<?php echo $sd->alias; ?>"><?php echo $sd->name; ?></option>
<?php  } ?>
</select> </div>
 <div class="col-lg-3"> <button type="button" class="btn btn-success w-50">Go</button> </div>
 </div>

 </div>
    </div>
    <div class="col-lg-2"></div>
</div>
</div>



<div class="container mt-4 mb-5 text-center">
    <img src="<?php echo base_url('images/india-tour.png'); ?>"  class="mt-4">

<h2 class="mt-4 mb-4"><?php echo $countryData->title; ?></h2>
<p class="pb-3 txt-1"><?php echo $countryData->description; ?></p>
<!-- <button type="button" class="btn btn-outline-success">Learn More</button> -->
</div>

<section class="hm-asia mt-5 pb-5">
 <div class="container">
 <div class="text-center">
 <img src="<?php echo base_url('images/globe.png'); ?>"  class="mt-n4">
 </div>

 <div class="text-center">
 <h2 class="pb-3 pt-3">EXPLORE OUR <?php echo $countryData->countryname; ?></h2>
 <p class="txt-1"><?php echo $countryData->explore_desc; ?></p>
 
 </div> 
 <div class="text-center">
 <img src="<?php echo base_url($countryData->explore_image); ?>" class="img-fluid mb-4"/>
 </div>
 <div class="searching w-75 m-auto">
 <div class="row">
 <div class="col-lg-2 txt-3">Show Now </div>
 <div class="col-lg-3 txt-3"><select class="mdb-select md-form">
  <option value="experiences">EXPERIENCES</option>
</select></div>
 <div class="col-lg-1 txt-3">IN </div>
 <div class="col-lg-3 txt-3"><select id="cntry-sho" class="mdb-select md-form">
  <option value="" >PLEASE SELECT</option>
  <?php  foreach($menuCountry as $k=>$mct){ ?>
  <option value="<?php echo $mct->alias; ?>" ><?php echo $mct->page_name; ?></option>
  <?php } ?>
</select> </div>
 <div class="col-lg-3"> <button onclick="lets_go()" type="button" class="btn btn-success w-50">Let's Go</button> </div>
 </div>
 </div>
 <?php if(!empty($countryData->certification)){  ?>
 <div class="text-center mt-5 mb-5">
 <h2 class="pb-3 pt-3">TRAVELIFE CERTIFICATION</h2>
 <p class="txt-1"><?php echo $countryData->certification; ?></p>
 </div>
 <?php } ?>
 </div>
</section>
<script>
function lets_go(){
var cci = $('#cntry-sho').val();
if(cci != ''){
location.href= '<?php echo base_url('experience'); ?>/'+cci;
}
}
</script>
<section class="mt-5">
 <div class="container">
 <div class="text-center">
 <img src="<?php echo base_url('images/india-tour.png'); ?>"  class="mt-n4">
 </div>

 <div class="text-center">
 <h2 class="pb-3 pt-3"><?php echo $countryData->countryname; ?>  TOURS</h2>
 <p class="txt-1 pb-4"><?php echo $countryData->tour_desc; ?></p>
 
 </div> 
 

 <?php if(!empty($tours)){  ?>


 <div class="row mt-3 pb-4">
 <?php  foreach($tours as $ct){ ?>
 <div class="col-md-4 india">
  <a href="<?php echo base_url('experience/'.$countryData->alias.'/'.$ct->alias); ?>">
<div class="love-india-4" >
<img src="<?php echo base_url($ct->main_image); ?>" style="width:100%" alt="" class="image">
<div class="overlay"></div>
 <div class="india-txt-top">
 <?php echo $ct->name;?>
 </div>
 
 <div class="india-mid"></div>
 <div class="india-txt-bottom"><?php echo $ct->day.' Days / '.$ct->night.' Nights';?></div>
 
 </div>
 </a>
 
 </div>
 <?php } ?>
 <div class="col-md-12 text-center">
 <div class="m-auto pt-3">
 <a href="<?php  echo base_url('experience/'.$countryData->alias); ?>"><button type="button" class="btn btn-outline-success text-center">View More</button></a>


 </div>
 </div>
 </div>
 <?php  } ?>
 
 </div>
</section>

<section class="mt-5">
 <div class="container">
 <div class="text-center">
 <img src="<?php echo base_url('images/india-tour.png'); ?>"  class="mt-n4">
 </div>
<?php $weatherData = unserialize($countryData->weather_data);
$month = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
?>
 <div class="text-center">
 <h2 class="pb-3 pt-3">WHEN TO GO & WEATHER</h2>
 <p class="txt-1 pb-4"><?php echo $countryData->weather_desc; ?></p>
<table id="customers">
 <tr>
    <td style="width: 180px;" bgcolor="#004e43" class="text-white">REGION</td>
    <?php foreach($month as $k=>$mth1){ ?>
    <th><?php echo $k+1; ?><br/><span><?php  echo $mth1; ?></span></th>
    <?php } ?>
  </tr>
  <?php 
    $o = 0;
     foreach($weatherData as $ik=>$wea){ ?>
 <tr>

    <td bgcolor="#004e43" class="text-white"><?php echo $wea['region']; ?></td>
    <?php foreach($month as $k2=>$mth2){ ?>
    <th><i class="fa-<?php echo $wea[$mth2]; ?>"></i></th>
    <?php } ?>
    
  </tr>
  
     <?php } ?>
</table>

 </div> 
 
 <div class="row mt-3">
 <div class="col-lg-2 text-center"><i class="fa-wet_1"></i><br/>
 Pleasant weather, no rain</div>
 <div class="col-lg-2 text-center"><i class="fa-wet_2"></i><br/>
 High heat and humidity</div>
 
 <div class="col-lg-2 text-center"><i class="fa-wet_3"></i><br/>
 Tropical climate, possible intermittent rain</div>
 <div class="col-lg-2 text-center"><i class="fa-wet_4"></i><br/>
 Tropical climate, high chances of rain</div>
 <div class="col-lg-2 text-center"><i class="fa-wet_5"></i><br/>
 Possible risk of typhoons and storms</div>
 <div class="col-lg-2 text-center"><i class="fa-wet_6"></i><br/>
 Cool to cold temperature (at night)</div>
 
 </div>
 
 </div>
</section>
<section class="hm-asia mt-5 pb-5">
 <div class="container">
 <div class="text-center">
 <img src="<?php echo base_url('images/india-tour.png'); ?>"  class="mt-n4">
 </div>

 <div class="text-center">
 <h2 class="pb-3 pt-3">Get In Touch</h2>
 <p class="txt-1 pb-4">We love to talk travel. If you have any questions, please don't hesitate to get in touch. We're here to help!</p>
 <button type="button" class="btn btn-outline-success">Enquire</button>
 </div> 
 
 
 
 </div>
</section>