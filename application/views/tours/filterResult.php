
<div class="gridview">
<div class="row">
<?php foreach($countryTours as $ct){ ?>

<div class="col-md-4">
<div class="pack">
<a href="<?php echo base_url('experience/'.$ct->country_alias.'/'.$ct->alias); ?>">
<div class="img-bg1 img-cntr">
<div class="tour-heading"><?php echo $ct->name;?></div>
  <img src="<?php echo base_url($ct->main_image); ?>" style="width:100%" alt="">

<div class="flag">
<img src="<?php echo base_url('images/icon-thailand.png'); ?> " class="img-fluid"/>
</div>
</div>
</a>
<h3><?php echo $ct->day.' Days / '.$ct->night.' Nights';?></h3>
<!-- <ul>
    <?php $ptd = explode('//',$ct->tours_point);
    foreach($ptd as $pt){
    ?>
<li><?php  echo $pt;  ?></li>
    <?php } ?>
</ul> -->
<a type="button" href="<?php echo base_url('experience/'.$ct->country_alias.'/'.$ct->alias); ?>" class="btn btn-secondary btn-sm">VIEW TOUR</a> <button type="button" class="btn btn-outline-success btn-sm w-40">SHARE</button>

</div>

</div>

<?php } ?>


</div>
</div>
<div class="listview hide">

<?php foreach($countryTours as $ct){ ?>

<div class="row prolist-odd">
<div class="col-lg-4 col-md-4 col-12">
<div class="img-bg1">

<img src="<?php echo base_url($ct->main_image); ?>" class="img-fluid" alt="">

</div>

</div>
<div class="col-lg-8 col-md-8 col-12">
<div class="tour-heading-list"><a href="<?php echo base_url('experience/'.$ct->country_alias.'/'.$ct->alias); ?>"><?php echo $ct->name;?></a></div>
<h3><?php echo $ct->day.' Days / '.$ct->night.' Nights';?></h3>
<a type="button" href="<?php echo base_url('experience/'.$ct->country_alias.'/'.$ct->alias); ?>" class="btn btn-secondary btn-sm">VIEW TOUR</a> <button type="button" class="btn btn-outline-success btn-sm w-40">SHARE</button>

</div>


</div>
<?php  } ?>
</div>
