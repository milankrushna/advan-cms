<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<style>


.panel-default>.panel-heading {
  color: #333;
  background-color:#2d9171;
  border-color: #e4e5e7;
  padding: 0;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

.panel-default>.panel-heading a {
  display: block;
  padding: 10px 15px;
  color:#fff;
  text-decoration:none;
}

.panel-default>.panel-heading a:after {
  content: "";
  position: relative;
  top: 1px;
  display: inline-block;
  font-family: 'Glyphicons Halflings';
  font-style: normal;
  font-weight: 400;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  float: right;
  transition: transform .25s linear;
  -webkit-transition: -webkit-transform .25s linear;
}

.panel-default>.panel-heading a[aria-expanded="true"] {
  background-color: #2d9171;
}

.panel-default>.panel-heading a[aria-expanded="true"]:after {

content:"\e114";
 color:#fff;

  
HTMLUseful for copying and pasting into your HTML (Webfont with CSS or SVG with JS).<i class="fas fa-angle-down"></i>
Download SVGPerfect for when you want to use just one icon as a vector on the desktop or in your own icon workflow.

}

.panel-default>.panel-heading a[aria-expanded="false"]:after {
content:"\e113";

  color:#fff;
}

.accordion-option {
  width: 100%;
  float: left;
  clear: both;
  margin: 15px 0;
}

.accordion-option .title {
  font-size: 20px;
  font-weight: bold;
  float: left;
  padding: 0;
  margin: 0;
}

.accordion-option .toggle-accordion {
  float: right;
  font-size: 16px;
  color: #6a6c6f;
}

.accordion-option .toggle-accordion:before {
  content: "Expand All";
}

.accordion-option .toggle-accordion.active:before {
  content: "Collapse All";
}

</style>



<div class="parallax" style="background-image:url(<?php echo base_url($tourData->main_image); ?>)"></div>
<div class="container">
<div class="row">
	<div class="col-lg-2"></div>
    <div class="col-lg-8 bannerbot-in">
    <h2 class="text-center"><?php echo $tourData->name;?></h2>
    <h3 class="text-center text-white"><?php echo $tourData->day.' Days / '.$tourData->night.' Nights';?></h3>
    
    </div>
    <div class="col-lg-2"></div>
</div>
</div>



<div class="container mt-4 mb-5 text-center">
    <img src="<?php echo base_url('assets/images/india-tour.png'); ?>"  class="mt-4">

<h2 class="mt-4 mb-4">OVERVIEW</h2>
<p class="pb-3 txt-1"><?php echo $tourData->overview_title;

$overViewImg = (!empty($tourData->tourImage))? explode(',',$tourData->tourImage) : array();

?></p>

<div class="row mb-5">
    <?php foreach($overViewImg as $ovi){ ?>
<div class="col-lg-4 col-md-4 col-12"><img src="<?php echo base_url($ovi); ?>" class="img-fluid"/></div>
    <?php } ?>

</div>


<p class="txt-1"><?php echo $tourData->content;?></p>
<div class="share">
<button type="button" class="btn btn-outline-success"> <i class="fas fa-print"></i> Print</button><button type="button" class="btn btn-outline-success"> <i class="fas fa-share-alt"></i> Share</button><button type="button" class="btn btn-outline-success"> <i class="far fa-list-alt"></i> Enquire</button>
</div>
</div>

<section class="hm-asia mt-5 pb-5">
 <div class="container">
 <div class="text-center">
 <img src="<?php echo base_url('assets/images/globe.png'); ?>"  class="mt-n4">
 </div>

<?php if(!empty($tourTrip)){ ?>
 <div>
 
 <h2 class="text-center pb-3 pt-3">Trip Highlight</h2>
<div class="row">
 <div class="col-lg-6 col-md-6 col-12"><img src="<?php echo base_url($tourData->trip_image);  ?>" class="img-fluid mt-3"/></div>
  <div class="col-lg-6 col-md-6 col-12">
      <?php  foreach($tourTrip as $ttp){ ?>
  	<div class="txt-5"><i class="far fa-hand-point-right"></i> <?php echo $ttp->title;?></div>
    <p><?php echo $ttp->description;?></p>
      <?php  } ?>

  </div>
  </div>
  
 </div> 
      <?php } ?>
 
 
 </div>
</section>
 
<section class="mt-5">
 <div class="container">
 <div class="text-center">
 <i class="fas fa-clipboard-list"></i>
 
 </div>
 <div class="text-center">
 <h2 class="pb-3 pt-3">ITINERARY</h2>
 <p class="txt-1 pb-4"><?php echo 'Essential '.$countryData->countryname.' - ' . $tourData->day.' Days / '.$tourData->night.' Nights';?></p>
 <iframe src="<?php echo $tourData->map_url;?>" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
 </div> 

<div class="bs-example">


<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
<?php foreach($tourDay as $tday){ ?>    
    
<div class="panel panel-default">
      <div class="panel-heading" role="tab" id="heading<?php echo $tday->id; ?>">
        <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $tday->id; ?>" aria-expanded="true" aria-controls="collapseOne">
        <?php echo 'DAY - '.$tday->day_no.' '.$tday->title; ?>
        </a>
      </h4>
      </div>
      <div id="collapse<?php echo $tday->id; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $tday->id; ?>">
        <div class="panel-body">
        <h4><?php echo $tday->day_title ?></h4>
                    <p><?php echo $tday->description ?></p>
        </div>
      </div>
    </div>
    <?php  } ?>
    
  </div>

</div>




<div class="share text-center">
<button type="button" class="btn btn-outline-success"> <i class="fas fa-print"></i> Print</button><button type="button" class="btn btn-outline-success"> <i class="fas fa-share-alt"></i> Share</button><button type="button" class="btn btn-outline-success"> <i class="far fa-list-alt"></i> Enquire</button>
</div>
 <h3 class="text-center mt-5 mb-5">IMPORTANT INFORMATION</h3>
 <div class="service-name">Service</div>
 <div class="service-name-bor"></div>
 <div class="row">
 <div class="col-lg-12 col-md-12 col-12 service">
 <?php echo $tourData->services;?>
 </div>
 
 </div>
 
 </div>
</section>
<section class="hm-asia mt-5 pb-5">
 <div class="container">
 <div class="text-center">
 <img src="<?php echo base_url('assets/images/india-tour.png'); ?>"  class="mt-n4">
 </div>

 <div class="text-center">
 <h2 class="pb-3 pt-3">Similar Tour You May Like</h2>
 
 
 </div> 
 <?php if(!empty($relateTours)){  ?>


<div class="row">
<?php  
// echo "<pre>";
// print_r($relateTours);

foreach($relateTours as $ct){ ?>
<div class="col-md-4 india">
 <a href="<?php echo base_url('experience/'.$countryData->alias.'/'.$ct->alias); ?>">
<div class="love-india-4" >
<img src="<?php echo base_url($ct->main_image); ?>" style="width:100%" alt="" class="image">
<div class="overlay"></div>
<div class="india-txt-top">
<?php echo $ct->name;?>
</div>

<div class="india-mid"></div>
<div class="india-txt-bottom"><?php echo $ct->day.' Days / '.$ct->night.' Nights';?></div>

</div>
</a>

</div>
<?php } ?>
<div class="col-md-12 text-center">
<div class="m-auto pt-3 share">
<a href="<?php  echo base_url('experience/'.$countryData->alias); ?>"><button type="button" class="btn btn-outline-success text-center">View More</button></a>

</div>
</div>
</div>
<?php  } ?>
 
  <div class="m-auto pt-3">
  <div class="view">
 <!-- <button type="button" class="btn btn-outline-success text-center">View More</button> -->
 </div>

 </div>
 </div>
 
 </div>


<div class="mt-5 pb-5">
 <div class="container">
 <div class="text-center">
<i class="far fa-comments"></i>
 </div>

 <div class="text-center">
 <h2 class="pb-3 pt-3">Get In Touch</h2>
 <p class="txt-1 pb-4">We love to talk travel. If you have any questions, please don't hesitate to get in touch. We're here to help!</p>
 <div class="share">
<button type="button" class="btn btn-outline-success"> <i class="far fa-list-alt"></i> Enquire</button>
</div>
 </div> 
 
 
 
 </div>
 </div>
</section>