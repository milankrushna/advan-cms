<style>
  .hide{
    display:none;
  }
  .img-cntr{
    position : relative;

  }
  .prolist-odd {
    margin-bottom: 15px;
}
  .tour-heading{
    color: #fff;
    font-size: 19px;
    font-weight: bold;
    text-transform: capitalize;
    padding-top: 24px;
    line-height: 20px;
    width: 100%;
    position: absolute;
    text-align: center;
  }
  .tour-heading-list{
    font-size: 19px;
    font-weight: bold;
    text-transform: capitalize;
    padding-top: 24px;
    line-height: 20px;
    width: 100%;
    margin-bottom: 10px;
  }
  .flag {
    position: absolute;
    bottom: 5px;
}
</style>
<div class="parallax" style="background-image:url(<?php echo base_url((!empty($countryData->bannerFile))?$countryData->bannerFile : "images/south-asia.jpg" ); ?>)"></div>

<div class="container mt-4 mb-5 text-center">
<h2 class="mt-4 mb-4">AMAZING <?php  echo (!empty($countryData->countryname))?$countryData->countryname : "All Country"; ?></h2>
<p class="pb-3 txt-1"></p>

</div>

<div class="container">
<div class="row">
<div class="col-lg-5 grid"><button onclick="show_listview()" type="button" class="btn btn-outline-secondary"> <i class="fas fa-list"></i> List View</button>
<button type="button" onclick="show_gridview()" class="btn btn-outline-secondary"> <i class="fas fa-th"></i> Grid View</button></div>
<div class="col-lg-3"> 
<div class="grid-rgt "><span class="resfound">RESULTS FOUND: <span class="pr-2"><?php echo count($countryTours); ?> |</span></span>  VIEW : </div>
</div>
<div class="col-lg-2 grid-rgt"><form style="float:left;" class="mr-2">
<select onchange="setDisplayitem(this.value)">
<option <?php echo ($noofdisp == 12)? "selected" : ""; ?> >12</option>
<option <?php echo ($noofdisp == 15)? "selected" : ""; ?>>15</option>
<option <?php echo ($noofdisp == 21)? "selected" : ""; ?>>21</option>
<option <?php echo ($noofdisp == 30)? "selected" : ""; ?>>30</option>
</select>
</form>
<script>

function setDisplayitem(nodis=''){
  location.href="<?php  echo base_url('experience/'.$location) ?>/"+nodis;
}
</script>
 PER PAGE</div>
<div class="col-lg-2">
<div class="btn-group w-100">
    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
       Sort By
    </button>
    <div class="dropdown-menu">
      <a onclick="order_by('ASC')" class="dropdown-item" href="#">A to Z</a>
      <a onclick="order_by('DESC')" class="dropdown-item" href="#">Z to A</a>
    </div>
  </div>
</div>
</div>
<script>

function order_by(odr){
  location.href="<?php  echo base_url('experience/'.$location.'/'.$noofdisp) ?>/"+odr;

}
</script>


</div>
</div>
<div class="container mt-5">
<div class="row">
<div class="col-lg-3 col-md-4 col-12">
<div class="mb-3">
<button onclick="location.reload();" type="button" class="btn btn-outline-success"> <i class="fas fa-times"></i> RESET OPTIONS</button>
</div>
<form>
<div class="sidebar">
<div class="box-in">
<h5>Search Tour</h5>
</div>
<div class="box-down pb-2 pt-2 pl-2">
<input type="text" placeholder="Type Tour Name" ><i class="fas fa-search"></i>

</div>
</div>

<div class="sidebar">
<div class="box-in">
<h5>Styles</h5>
</div>
<div class="box-down">
<div class="filters search-box">
          <ul class="list-group  call-ajax-event">
                        
          <?php foreach($allstyleData as $asd){ ?>                       
            
            <li>
              <div class="roundedOne">
                <input type="checkbox" class="filterCountry" name="country[]" value="<?php echo $asd->id; ?>" id="<?php echo "lbl".$asd->id; ?>" >
                <label for="<?php echo "lbl".$asd->id; ?>"><span><?php echo $asd->name; ?></span></label>
              </div>
            </li>
                        
 <?php  } ?>                  
        
            
          </ul>
        </div>

</div>
</div>

<div class="sidebar">
<div class="box-in">
<h5>Duration</h5>
</div>
<div class="box-down">
<div class="filters search-box">
          <ul class="list-group  call-ajax-event">
                        
            
          <li>
              <div class="roundedOne">
                <input class="daysel" type="radio" name="days[]" value="1" id="cambodia">
                <label for="cambodia"><span>2-4 Days</span></label>
              </div>
            </li>
                        
                        
            
            <li>
              <div class="roundedOne">
                <input class="daysel" type="radio" name="days[]" value="2" id="china">
                <label for="china"><span>5-7 Days</span></label>
              </div>
            </li>
                        
                        
            
            <li>
              <div class="roundedOne">
                <input class="daysel" type="radio" name="days[]" value="3" id="indonesia">
                <label for="indonesia"><span>8-10 Days</span></label>
              </div>
            </li>
                        
                      
            
            <li>
              <div class="roundedOne">
                <input class="daysel" type="radio" name="days[]" value="4" id="japan">
                <label for="japan"><span>More than 11 days</span></label>
              </div>
            </li>
            
            
          </ul>
        </div>

</div>
</div>

</form>
</div>

<div class="col-lg-9 col-md-8 col-12">
<div id="ressect">
<div class="gridview">
<div class="row">
<?php foreach($countryTours as $ct){ ?>

<div class="col-md-4">
<div class="pack">
<a href="<?php echo base_url('experience/'.$ct->country_alias.'/'.$ct->alias); ?>">
<div class="img-bg1 img-cntr">
<div class="tour-heading"><?php echo $ct->name;?></div>
  <img src="<?php echo base_url($ct->main_image); ?>" style="width:100%" alt="">

<div class="flag">
<img src="<?php echo base_url('images/icon-thailand.png'); ?> " class="img-fluid"/>
</div>
</div>
</a>
<h3><?php echo $ct->day.' Days / '.$ct->night.' Nights';?></h3>
<!-- <ul>
    <?php $ptd = explode('//',$ct->tours_point);
    foreach($ptd as $pt){
    ?>
<li><?php  echo $pt;  ?></li>
    <?php } ?>
</ul> -->
<a type="button" href="<?php echo base_url('experience/'.$ct->country_alias.'/'.$ct->alias); ?>" class="btn btn-secondary btn-sm">VIEW TOUR</a> <button type="button" class="btn btn-outline-success btn-sm w-40">SHARE</button>

</div>

</div>

<?php } ?>


</div>
</div>
<div class="listview hide">

<?php foreach($countryTours as $ct){ ?>

<div class="row prolist-odd">
<div class="col-lg-4 col-md-4 col-12">
<div class="img-bg1">

<img src="<?php echo base_url($ct->main_image); ?>" class="img-fluid" alt="">

</div>

</div>
<div class="col-lg-8 col-md-8 col-12">
<div class="tour-heading-list"><a href="<?php echo base_url('experience/'.$ct->country_alias.'/'.$ct->alias); ?>"><?php echo $ct->name;?></a></div>
<h3><?php echo $ct->day.' Days / '.$ct->night.' Nights';?></h3>
<a type="button" href="<?php echo base_url('experience/'.$ct->country_alias.'/'.$ct->alias); ?>" class="btn btn-secondary btn-sm">VIEW TOUR</a> <button type="button" class="btn btn-outline-success btn-sm w-40">SHARE</button>

</div>


</div>
<?php  } ?>
</div>
</div>

<div class="row mt-5">
<div class="col-lg-9 col-md-8 col-12">
<div class="tc-results resfound"><?php echo count($countryTours); ?> Product Found</div>
</div>
 <div class="col-lg-3 col-md-4 col-12 resfound">
 <?php echo $pagination;   ?>
<?php /*<ul class="pagination pagination-sm">
  <li class="page-item"><a class="page-link" href="#">Previous</a></li>
  <li class="page-item"><a class="page-link" href="#">1</a></li>
  <li class="page-item"><a class="page-link" href="#">2</a></li>
  <li class="page-item"><a class="page-link" href="#">3</a></li>
  <li class="page-item"><a class="page-link" href="#">Next</a></li>
</ul> */ ?>
</div>

</div>

</div>


</div>
</div>



<script>




var vaf = {}; 
$('.filterCountry').change(()=>{
  var checkCntr =$( ".filterCountry" ).val( );
  var favoritec = [];
            $.each($("input[name='country[]']:checked"), function(){            
                favoritec.push($(this).val());
            });  
            
 
         vaf.selCountry = favoritec.join(", ");
         get_filterData(vaf);
         
         console.log(vaf);
});


$('.daysel').change(()=>{
  var favorited = [];
            $.each($("input[name='days[]']:checked"), function(){            
                favorited.push($(this).val());
            });

         vaf.selDay = favorited.join(", ");
         get_filterData(vaf);
});

function  get_filterData(vaf){
  vaf.specificId = '<?php echo $countryData->id; ?>';
  vaf.ftype = 'country';
  $.post("<?php echo base_url('filter_style/getdata') ?>",vaf,(resp)=>{
    $('.resfound').hide();
    $('#ressect').html(resp); 
  });
}


function show_gridview(){

  $('.gridview').removeClass('hide');
$('.listview').addClass('hide');
}
function show_listview(){
  $('.listview').removeClass('hide');
$('.gridview').addClass('hide');
}
</script>