
        <link href="<?php echo base_url(); ?>assets/gallery/css/gallery.css" rel="stylesheet" type="text/css">

		
		
		  <!----------container body---------->
        <div class="container">
            <div class="main-container">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="col-lg-11 col-md-11 col-sm-11">
                        <div class="content-left">
                            <div class="service-section">
	<h3 class="contnt-heading wow fadeInUp animated"><?php if(!empty($pages['name'])){ echo $pages['name']; }else{ echo $pg_title; } ?></h3>
                                <div class="content-wrapper wow fadeInUp animated">
                                  
								  
								   <div class="gallery-main">
                
                <div class="demo-gallery">
            <ul id="lightgallery" class="list-unstyled row"> 
                <?php foreach($images as $img){  ?>
                <li class="photo-gallery"  data-src="<?php echo base_url('admin/images/gallery/original/'.$img->imgname);?>" data-sub-html="<?php echo $img->caption; ?>" >
                    <a href="#">
                        <img class="img-responsive" src="<?php echo base_url('admin/images/gallery/thumb/'.$img->imgname);?>" alt="Thumb-1">
                    </a>
                    <h4 class="img-title"><?php echo $img->caption; ?></h4>
                </li>
                <?php } ?>
                
            </ul>
        </div>
            </div>
								  
                                   
                                    </div>
                                </div>
                            </div>
                        
                        </div>

 </div>
                   
                </div>
            </div>
            
		
		
		
		


<script src="<?php echo base_url(); ?>assets/gallery/galleryJS/lightgallery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/gallery/galleryJS/lg-autoplay.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/gallery/galleryJS/lg-fullscreen.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/gallery/galleryJS/lg-zoom.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/gallery/galleryJS/lg-thumbnail.min.js"></script>
        <script>
        lightGallery(document.getElementById('lightgallery'));
        </script>
