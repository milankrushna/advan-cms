<div class="wrapper-view">
        <div class="container">
                <h3 style="color: #0072bc;"><?php echo $article_key.' - Search Result'; ?>   </h3>
            <div class="row">
                <?php 
                if(!empty($search)){ 
                foreach($search as $catgy){ ?>
                
                <div class="col-md-6">
                    <div class="page-view">
                            <!--Card-->
                        <div class="card pageView first">

                            <!--Card image-->
                            <div class="view overlay hm-white-slight">
                                <img src="<?php echo base_url(); ?>admin/upload/page/min_page1/<?php echo (!empty($catgy['sec_1_data']))? $catgy['sec_1_data'] : 'no-img.png';?>" class="img-fluid" alt="">
                                <a href="<?php echo site_url($catgy['url']);?>">
                                    <div class="mask waves-effect waves-light"></div>
                                </a>
                            </div>
<!--                            <p class="editor">Author @<span class="editor-name"><?php echo $catgy['authorname'];?></span> </p>-->
                            <!--/.Card image-->

                            <!--Social shares-->
                            <!--Button-->
                             <div class="social-main">
                                <a class="btn-floating btn-action share-toggle primary-color-dark share-btn"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                            <div class="social-share">
                                <div class="social">
                                    <!--Facebook-->
                                  <a href="javascript:void(0)" onclick="facebook_click('<?php echo site_url($catgy['url']); ?>','<?php echo $catgy['name'];?>')" target="_blank" class="btn btn-rounded light-blue"><i class="fa fa-facebook"></i></a>
                                    <!--Twitter-->
                                    <a  href="javascript:void(0)" onclick="tweet_click('<?php echo site_url($catgy['url']); ?>','<?php echo $catgy['name'];?>');" target="_blank" class="btn btn-info btn-rounded"><i class="fa fa-twitter"></i></a>
                                    <!--Google -->
                                <a href="javascript:void(0)" onclick="google_click('<?php echo site_url($catgy['url']); ?>','<?php echo $catgy['name'];?>');" target="_blank"  class="btn btn-deep-orange btn-rounded"><i class="fa fa-google-plus"></i></a>
                                   <a class="social-btn btn btn-whatsapp" href="whatsapp://send?text=<?php echo site_url($catgy['url']); ?>" data-action="share/whatsapp/share"><i class="fa fa-whatsapp"></i></a>
                                    
                                    <!--pinterest-->
<!--                                  <a href="https://www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark" data-pin-custom="true"><button type="button" class="btn danger-color-dark waves-effect waves-light btn-rounded"><i class="fa fa-pinterest-p" aria-hidden="true"></i></button></a>-->
                                    

                       
                                </div>
                            </div>
                            </div>
                            <!--/Social buttons-->
                            <!--Card content-->
                            <div class="card-block">
                                <!--Title-->
                                <h4 class="card-title heading-page"><a href="<?php echo site_url($catgy['url']);?>"><?php echo substr($catgy['name'],0,65); echo (strlen($catgy['name']) > 65)? '...' : '';  ?></a></h4>
                        
                                <!--Text-->
<!--                                <p class="card-text"><?php echo strip_tags(substr($catgy['right'],0,50)); ?>...</p>-->
                            </div>
                            <!--/.Card content-->

                            <!-- Card footer -->
                  <?php /*  <div class="card-data">
                                <ul>
              <!--                                    <li><i class="fa fa-clock-o"></i><?php  echo date('M', strtotime($catgy['date'])); ?>  <?php echo  date('d', strtotime($catgy['date'])); ?>,<?php echo  date('Y', strtotime($catgy['date'])); ?></li>-->
                                   
<!--                                    <li><a href="#"><i class="fa fa-comments-o"></i><span class="fb-comments-count" data-href="<?php echo site_url($catgy['url']);?>"></span></a></li>-->
                                  
                                    
                                   
<!--                                       <li><i class="fa fa-eye"></i><?php echo $catgy['visit'];?></li>-->
                                </ul>
                            </div> */ ?>
                            <!-- Card footer -->
                        </div>
                            <!--/.Card-->
                        
                    </div>
                </div>
                
                <?php } 
                }else{ ?>
              
              <h2 style="text-align: center;width: 100%;">No data found .</h2>
               
                
              <?php  } ?>
                
              <div class="pagination"><div><?php echo $pagination; ?></div></div>
               
                
          
                
            </div>
        </div>
    </div>

<script>
function google_click(url) {
//u=location.href;
    
window.open('https://plus.google.com/share?u='+url, '_blank','location=yes,height=570,width=520,scrollbars=yes,status=yes');
 }
</script>



<script>
function tweet_click(url,name){ 
    
//u=location.href;
//t = document.title;
    
window.open('https://twitter.com/intent/tweet?text='+encodeURIComponent(name)+'&url='+encodeURIComponent(url)+'&via=MyCityLinks', '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
}
</script>

<script>
function facebook_click(url,name){ 

window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(url)+'&t='+encodeURIComponent(name),
'sharer','location=yes,height=570,width=520,scrollbars=yes,status=yes');
return false;

   
 }
</script>


<script async defer src="//assets.pinterest.com/js/pinit.js"></script>
