<div class="wrapper-view">
        <div class="container">
            <div class="row">
                
                <?php 
                if(!empty($category)){ 
                foreach($category as $catgy){ ?>
                
                <div class="col-md-6">
                    <div class="page-view">
                            <!--Card-->
                        <div class="card pageView first">

                            <!--Card image-->
                            <div class="view overlay hm-white-slight">
                                <img src="<?php echo base_url(); ?>admin/upload/page/min_page1/<?php echo (!empty($catgy['sec_1_data']))? $catgy['sec_1_data'] : 'no-img.png';?>" class="img-fluid" alt="">
                                <a href="#">
                                    <div class="mask waves-effect waves-light"></div>
                                </a>
                            </div>
                            <p class="editor">Author @<span class="editor-name"><?php echo $catgy['authorname'];?></span> </p>
                            <!--/.Card image-->

                            <!--Social shares-->
                            <!--Button-->
                            <div class="social-main">
                               <!-- <a class="btn-floating btn-action share-toggle primary-color-dark share-btn"><i class="fa fa-share-alt" aria-hidden="true"></i></a>-->
                            <!--<div class="social-share">
                                <div class="social">
                                    <!--Facebook-->
                                   <!-- <a class="btn btn-rounded light-blue"><i class="fa fa-facebook"></i></a>
                                    <!--Twitter-->
                                  <!--  <a class="btn btn-info btn-rounded"><i class="fa fa-twitter"></i></a>
                                    <!--Google -->
                                   <!-- <a class="btn btn-deep-orange btn-rounded"><i class="fa fa-google-plus"></i></a>
                                </div>
                            </div>-->
                            </div>
                            <!--/Social buttons-->
                            <!--Card content-->
                            <div class="card-block">
                                <!--Title-->
                                <h4 class="card-title heading-page"><a href="<?php echo site_url($catgy['url']);?>"><?php echo $catgy['name'];?></a> </h4>
                                <hr>
                                <!--Text-->
                                <p class="card-text"><?php echo substr($catgy['right'],0,50);?>...</p>
                            </div>
                            <!--/.Card content-->

                            <!-- Card footer -->
                            <div class="card-data">
                                <ul>
                                    
                        
                                    
                                    <li><i class="fa fa-clock-o"></i><?php  echo date('M', strtotime($catgy['date'])); ?>  <?php echo  date('d', strtotime($catgy['date'])); ?>,<?php echo  date('Y', strtotime($catgy['date'])); ?></li>
                                   
                                    <li><a href="#"><i class="fa fa-comments-o"></i><span class="fb-comments-count" data-href="<?php echo site_url($catgy['url']);?>"></span></a></li>
                                  
                                    
                                   
                                    <li><i class="fa fa-eye"></i><?php echo $catgy['visit'];?></li>
                                </ul>
                            </div>
                            <!-- Card footer -->
                        </div>
                            <!--/.Card-->
                        
                    </div>
                </div>
                
                <?php } 
                }else{ ?>
              
              <h2 style="text-align: center;width: 100%;">No data found .</h2>
               
                
              <?php  } ?>
                
               
                <div class="pagination"><nav><?php echo $pagination; ?></nav></div>
                
          
                
            </div>
        </div>
    </div>