  <div style="color:#F00"><?php echo $this->session->flashdata('message');?></div>

<div class="wrapper-view">
<div class="container">
<div class="row min-"><!--        Contact us details-->             
<section class="section contact pb-5">                 <!--Section heading-->
<h2 class="section-heading h1 pt-4 "><?php echo 'Video Gallery'; ?></h2>
<!--Section description-->
<div class="row"><!--Grid column-->
    <?php foreach($video as $vd){ ?>
    
    <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="vBox">
            <iframe width="560" height="315" src="<?php echo $vd['video_url']; ?>" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
   <?php  } ?>
    
    
</div>
</section></div>
</div>
</div>