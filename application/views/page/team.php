                      <?php  
function getbgclr(){
    
$bgclr[] = 'card-up teal lighten-2';
$bgclr[] = 'card-up indigo darken-2';
$bgclr[] = 'card-up purple darken-2';
$bgclr[] = 'card-up stylish-color';
$bgclr[] = 'card-up cyan lighten-3';
$bgclr[] = 'card-up  teal accent-3';
$bgclr[] = 'card-up  orange lighten-3';
$bgclr[] = 'card-up  brown lighten-1';
$bgclr[] = 'card-up blue-grey lighten-2';
$bgclr[] = 'card-up rgba-pink-strong';
$bgclr[] = 'card-up rgba-purple-strong';
$bgclr[] = 'card-up rgba-teal-strong';
    
    shuffle($bgclr);
    return $bgclr[0];  
    
}



    ?> 

   <div class="wrapper-view">
        <div class="container">
            <div class="row">
<!--                team main section-->
                <div class="our-team">
<!--                    team image-->
                 <!--   <div class="team-img">
                        <img src="<?php echo base_url(); ?>assets/img/citylink-team.png" class="img-fluid" alt="our-team">
                    </div>-->
<!--                    team content-->
                    <h4 class="heading4 text-justify"><?php echo $section_team1['content'];?></h4>
                    
<!--                    team members list-->
                    <div class="team-list">
                        <!--Section: Testimonials v.1-->
                        <section class="section">
                            <!--Section heading-->
                            <h1 class="section-heading text-center">Our Team Members</h1>
                            <!--Section sescription-->
                            <p class="section-description"><?php echo $section_team2['content'];?></p>

                            <div class="row">

                                <!--First column -->
                                
                                
                                <?php   if(!empty($team)){ foreach($team as $tm){ ?>
                                <div class="col-lg-3 col-md-12 mb-r">
     
                                    <!--Card-->
                                    <div class="card testimonial-card">

                                        <!--Background color-->
                                        <div class="<?php echo getbgclr(); ?>"></div>
                                        <!--Avatar-->
                                        <div class="avatar"><img src="<?php echo base_url(); ?>admin//upload/events/min_events/<?php echo $tm['cover_pic']?>" class="rounded-circle img-responsive">
                                        </div>

                                        <div class="card-block">
                                            <!--Name-->
                                            <h4 class="card-title"><?php echo $tm['name']?></h4>
                                            <p class="card-content"><b>Designation :</b> <span><?php echo $tm['designation']?></span></p>
                                        </div>

                                    </div>
                                    <!--/.Card-->


                                </div>
                                <?php } }else{  ?>
                                <h3 style="text-align:center">No Data Found</h3>
           
                                <?php } ?>
                              

                            </div>

                        </section>
                        <!--Section: Testimonials v.1-->
                    </div>
                    
<!--                    team members list end-->
                </div>
            </div>
        </div>
    </div>
    
<!--    footer section started-->
