

<div id="demo" class="carousel slide banner" data-ride="carousel">

<!-- Indicators -->
<ul class="carousel-indicators">
<?php $tlt =0; foreach($slider as $sldr){ ?>
  <li data-target="#demo" data-slide-to="<?php  echo $tlt; ?>" class="<?php if($tlt == 0){ echo 'active'; } ?>"></li>
<?php $tlt++ ; } ?>
</ul>

<!-- The slideshow -->
<div class="carousel-inner">
<?php //echo  ($sldr['link'] != '')? $sldr['link'] : '#'; ?>
<?php $tl =1; foreach($slider as $sldr){ ?>
  <div class="carousel-item <?php if($tl == 1){ echo 'active'; } ?>">
    <img src="<?php echo base_url(); ?>/adhvan-admin/upload/gallery/slider/<?php echo $sldr['image']; ?>" alt="<?php echo $sldr['caption'];?>">
  </div>
  <?php $tl++ ; } ?>
</div>

<!-- Left and right controls -->
<a class="carousel-control-prev" href="#demo" data-slide="prev">
  <span class="carousel-control-prev-icon"></span>
</a>
<a class="carousel-control-next" href="#demo" data-slide="next">
  <span class="carousel-control-next-icon"></span>
</a>
</div>
<div class="container">
<div class="row">
  <div class="col-lg-2"></div>
  <div class="col-lg-8 bannerbot">
  <h2>EXPERIENCE OUR ASIA</h2>
  <input type="text" placeholder="Search..">
  <button type="submit"><i class="fas fa-search"></i></button>
  <button type="button" class="btn btn-secondary banner-btn hm-ban"> <i class="far fa-comments"></i> Get In Touch</button>
<button type="button" class="btn btn-secondary  banner-btn"> <i class="fas fa-envelope-open-text"></i> Newsletter</button>
  </div>
  <div class="col-lg-2"></div>
</div>
</div>



<div class="container mt-5 mb-5 text-center">
<img src="images/adhavan-inner-logo.png" class="m-auto img-fluid">
<h1 class="mt-3">Why Adhvan ?</h1>
<div class="pb-3 txt-1"><?php echo $sectionwhy['content']; ?></div>
<a href="<?php echo base_url('adhvanpage/why-adhvan'); ?> "><button type="button" class="btn btn-outline-success">Learn More</button></a>
</div>

<section class="hm-asia mt-5 pb-5">
<div class="container">
<div class="text-center">
<img src="images/globe.png"  class="mt-n4">
</div>

<div class="text-center">
<h2 class="pb-3 pt-3">Diversity of Asia</h2>
<div class="txt-1"><?php echo $sectiondivercity['content']; ?></div>
 <div class="row gallery">
 <div class="col-lg-3 col-md-4 col-12">
<a href="<?php echo base_url('location/myanmar'); ?>"><img src="images/myanmar.png" class="img-fluid"></a>
 </div>
 <div class="col-lg-4 col-md-4 col-12">
<a href="<?php echo base_url('location/vietnam'); ?>"> <img src="images/vietnam.png" class="img-fluid"></a>
 </div>
 <div class="col-lg-5 col-md-4 col-12">
 <a href="<?php echo base_url('location/thailand'); ?>"><img src="images/thailand.png" class="img-fluid"></a>
 </div>
 </div>
 <div class="row gallery">
 <div class="col-lg-4 col-md-4 col-12">
 <a href="<?php echo base_url('location/malaysia'); ?>"><img src="images/malaysia-home.png" class="img-fluid"></a>
 </div>
 <div class="col-lg-5 col-md-4 col-12">
 <a href="<?php echo base_url('location/india'); ?>"><img src="images/india.png" class="img-fluid"></a>
 </div>
 
 <div class="col-lg-3 col-md-4 col-12">
<a href="<?php echo base_url('location/cambodia'); ?>"> <img src="images/cambodia.png" class="img-fluid"></a>
 </div>
 </div>
 <div class="row gallery">
 <div class="col-lg-5 col-md-4 col-12">
 <a href="<?php echo base_url('location/laos'); ?>"><img src="images/laos-home.png" class="img-fluid"></a>
 </div>
 <div class="col-lg-3 col-md-4 col-12">
<a href="<?php echo base_url('location/bhutan'); ?>"> <img src="images/bhutan.png" class="img-fluid"></a>
 </div>
 <div class="col-lg-4 col-md-4 col-12">
 <a href="<?php echo base_url('location/singapore'); ?>"><img src="images/singapore.png" class="img-fluid"></a>
 </div>
 
 </div>

</div> 
<!--
<div class="row gallery mt-3 mb-3">
<?php foreach($menuCountryhome as $mch){ ?>

<div class="col-lg-4 col-md-4 col-12">
<a href="<?php echo base_url('location/'.$mch->alias); ?>"><img src="<?php echo base_url($mch->image); ?>" class="img-fluid"></a>
</div>

<?php  } ?>
-->
</div>

</div>
</section>
<div class="row pt-5">
<div class="container text-center">
<h3>Our Philosophy</h3>
<div class="pb-3 txt-1"><?php echo $sectionphyl['content']; ?></div>
<button type="button" class="btn btn-outline-success">Learn More</button>
</div>
</div>
<section class="hm-asia mt-5">
<div class="container">
<div class="text-center">
<img src="images/india-tour.png"  class="mt-n4">
</div>

<div class="text-center">
<h2 class="pb-3 pt-3">Tour We Love</h2>
<div class="txt-1 pb-4"><?php echo $sectiontourLove['content']; ?></div>

</div> 
<?php /*?>
<div class="row">

<?php foreach($tourhome as $ct){ ?>

<div class="col-lg-4 col-md-6 col-12">
<div class="pack">
<a href="<?php echo base_url('experience/'.$ct->calias.'/'.$ct->alias); ?>">
<div class="img-bg1" style="background-image:url(<?php echo base_url($ct->main_image); ?>)">
<div class="txt-2"><?php echo $ct->name;?></div>
<div class="flag">
<img src="<?php echo base_url('images/icon-thailand.png'); ?> " class="img-fluid"/>
</div>
</div>
</a>
<h3><?php echo $ct->day.' Days / '.$ct->night.' Nights';?></h3>
<!-- <ul>
    <?php $ptd = explode('//',$ct->tours_point);
    foreach($ptd as $pt){
    ?>
<li><?php  echo $pt;  ?></li>
    <?php } ?>
</ul> -->
<a type="button" href="<?php echo base_url('experience/'.$ct->calias.'/'.$ct->alias); ?>" class="btn btn-secondary btn-sm">VIEW TOUR</a> <button type="button" class="btn btn-outline-success btn-sm w-40">SHARE</button>

</div>

</div>

<?php } ?>

</div><?php */?>
<div class="row">
 <div class="col-lg-4 col-md-2 col-12 india">
  <a href="<?php echo base_url('experience/india/10-days-kerala-houseboat-tour-with-mumbai'); ?>">
<div class="love-india-1">

 <div class="india-txt-top2">
 KERALA HOUSEBOAT TOUR WITH MUMBAI
 </div>
 
 <div class="india-mid2"></div>
 <div class="india-txt-bottom2">10 Days & 9 Night</div>
 
 </div>
 </a>
 
 
 
 
 
 </div><div class="col-lg-4 col-md-2 col-12 india">
  <a href="<?php echo base_url('experience/india/10-days-rediscovering-ladakh'); ?> ">
<div class="love-india-2">

 <div class="india-txt-top2">
 REDISCOVERING LADAKH
 </div>
 
 <div class="india-mid2"></div>
 <div class="india-txt-bottom2">10 Days & 9 Night</div>
 
 </div>
 </a>
 
 
 
 
 
 </div><div class="col-lg-4 col-md-2 col-12 india">
  <a href="<?php echo base_url('experience/india/09-days-delhi-with-taj-backwaters'); ?>">
<div class="love-india-3">

 <div class="india-txt-top2">
 DELHI WITH TAJ & BACKWATERS
 </div>
 
 <div class="india-mid"></div>
 <div class="india-txt-bottom2">9 Days & 8 Night</div>
 
 </div>
 </a>
 
 
 
 
 
 </div>
 </div>
 <div class="row mt-3 pb-4">
 <div class="col-lg-4 col-md-2 col-12 india">
  <a href="<?php echo base_url('experience/india/08-days-kerala-backwaters-tour'); ?>">
<div class="love-india-4">

 <div class="india-txt-top2">
KERALA BACKWATERS TOUR
 </div>
 
 <div class="india-mid"></div>
 <div class="india-txt-bottom2">7 Days & 6 Night</div>
 
 </div>
 </a>
 
 
 
 
 
 </div><div class="col-lg-4 col-md-2 col-12 india">
  <a href="<?php echo base_url('experience/india/14-days-north-india-with-golden-temple-shiml'); ?>">
<div class="love-india-5">

 <div class="india-txt-top2">
 NORTH INDIA WITH GOLDEN TEMPLE & SHIMLA
 </div>
 
 <div class="india-mid"></div>
 <div class="india-txt-bottom2">14 Days & 13 Night</div>
 
 </div>
 </a>
 
 
 
 
 
 </div><div class="col-lg-4 col-md-2 col-12 india">
  <a href="<?php echo base_url('experience/india/17-days-culture-nature-traditions-of-south-india-with-nizams'); ?>">
<div class="love-india-6">

 <div class="india-txt-top2">
 CULTURE, NATURE & TRADITIONS OF SOUTH INDIA 
 </div>
 
 <div class="india-mid"></div>
 <div class="india-txt-bottom2">17 Days & 16 Night</div>
 
 </div>
 </a>
 
 
 
 
 
 </div>
 <div class="m-auto pt-3">
 <button type="button" class="btn btn-outline-success text-center">Learn More</button>

 </div>
 </div>

<div class="row pt-5 pb-5">
<div class="container text-center subscribe">
<h3>SUBSCRIBE TO OUR NEWSLETTER!</h3>
<div class="txt-1">The world of Adhvan Travel is always evolving. Stay in touch and we'll keep you up to speed!</div>
<form>
<input type="text" class="form-control" placeholder="Email Address" id="usr">
<button type="button" class="btn btn-success">Subscribe</button>
</form>
</div>
</div>
</div>
</section>
