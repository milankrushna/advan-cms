<div class="wrapper-view">
        <div class="container">
            <div class="row">
                
                <div class="authorbox hm-green-slight">
                            <div class="authBx">
                                <div class="userimg">
                                    <img src="<?php echo base_url()?>admin/upload/events/min_events/<?php echo $author_page['image'];?>" alt="">
                                </div>
                                <h3 class="user-heading"><a href="#"><?php echo $author_page['authorname'];?></a> </h3>
                            </div>
                            <div class="authBx text-right">
                                <button class="btn btn-info"><?php echo $no_post; ?></button>
                            </div>
                        </div>
                
                 <?php 
                if(!empty($auth_article)){ 
                foreach($auth_article as $catgy){ ?>
                
                <div class="col-md-6">
                    <div class="page-view">
                            <!--Card-->
                        <div class="card pageView first">

                            <!--Card image-->
                            <div class="view overlay hm-white-slight">
                                <a href="<?php echo site_url($catgy['url']);?>"><img src="<?php echo base_url(); ?>admin/upload/page/min_page1/<?php echo $catgy['sec_1_data'];?>" class="img-fluid" alt=""></a>
                                <a href="<?php echo site_url($catgy['url']);?>">
                                    <div class="mask waves-effect waves-light"></div>
                                </a>
                            </div>
                            <p class="editor">Author @<span class="editor-name"><?php echo $catgy['authorname'];?></span> </p>
                            <!--/.Card image-->

                            <!--Social shares-->
                            <!--Button-->
                            <div class="social-main">
                               <!-- <a class="btn-floating btn-action share-toggle primary-color-dark share-btn"><i class="fa fa-share-alt" aria-hidden="true"></i></a>-->
                            <div class="social-share">
                                <div class="social">
                                    <!--Facebook-->
                                   <!--<a href="javascript:void(0)" onclick="facebook_click()" target="_blank" class="btn btn-rounded light-blue"><i class="fa fa-facebook"></i></a>
                                    <!--Twitter-->
                                    <a  href="javascript:void(0)" onclick="tweet_click();" target="_blank" class="btn btn-info btn-rounded"><i class="fa fa-twitter"></i></a>
                                    <!--Google -->
                                <a href="javascript:void(0)" onclick="google_click();" target="_blank"  class="btn btn-deep-orange btn-rounded"><i class="fa fa-google-plus"></i></a>
                                    
                                    <!--pinterest-->
                                  <a  data-pin-do="buttonBookmark" data-pin-tall="true" data-pin-color="red"  href="//www.pinterest.com/pin/create/button/"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_red_28.png" /></a>
                                    
                                </div>
                            </div>
                            </div>
                            <!--/Social buttons-->
                            <!--Card content-->
                            <div class="card-block">
                                <!--Title-->
                                <h4 class="card-title heading-page"><a href="<?php echo site_url($catgy['url']);?>"><?php echo $catgy['name'];?></a> </h4>
                                <hr>
                                <!--Text-->
                                <p class="card-text"><?php echo substr($catgy['short_content'],0,50);?>...</p>
                            </div>
                            <!--/.Card content-->

                            <!-- Card footer -->
                            <div class="card-data">
                                <ul>
                                    
                        
                                    
                                    <li><i class="fa fa-clock-o"></i><?php  echo date('M', strtotime($catgy['date'])); ?>  <?php echo  date('d', strtotime($catgy['date'])); ?>,<?php echo  date('Y', strtotime($catgy['date'])); ?></li>
                                   
                                    <li><a href="<?php echo site_url($catgy['url']);?>"><i class="fa fa-comments-o"></i><span class="fb-comments-count" data-href="<?php echo site_url($catgy['url']);?>"></span></a></li>
                                  
                                    
                                   
                                    <li><i class="fa fa-eye"></i><?php echo $catgy['visit'];?></li>
                                </ul>
                            </div>
                            <!-- Card footer -->
                        </div>
                            <!--/.Card-->
                        
                    </div>
                </div>
                
                <?php } 
                }else{ ?>
              
              <h2 style="text-align: center;width: 100%;">No data found .</h2>
               
                
              <?php  } ?>
                
               
                
                
          
                
            </div>
        </div>
    </div>
<script>
function tweet_click(){ 
    
u=location.href;
t = document.title;
    
window.open('https://twitter.com/intent/tweet?text='+encodeURIComponent(t)+'&url='+encodeURIComponent(u), '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
}
</script>

<script>
function facebook_click(){ 
t = document.title;
u=location.href;
window.open('http://www.facebook.com/sharer.php?u='+
encodeURIComponent(u)+'&t='+encodeURIComponent(t),
'sharer','location=yes,height=570,width=520,scrollbars=yes,status=yes');
return false;
   
 }
</script>


<script async defer src="//assets.pinterest.com/js/pinit.js"></script>