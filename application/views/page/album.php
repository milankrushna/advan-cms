        <link href="<?php echo base_url(); ?>assets/gallery/css/gallery.css" rel="stylesheet" type="text/css">


		
		<!---------include carousel--------->
<?php   if(!empty($slider1)){ ?>
    <div class="mainCarousel">
            <div id="slider" class="owl-carousel owl-theme carousel-width">
            
            <?php 
            foreach($slider1 as $sl){  ?>
            
              <div class="item"><img src="<?php echo base_url(); ?>admin/upload/gallery/slider/<?php echo $sl['image']; ?>" alt="pnconline slider"></div>
            
             
            <?php } ?>
            </div>
           
        </div>
<?php } ?>
        <!----------container body---------->
        <div class="container">
            <div class="main-container">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="col-lg-11 col-md-11 col-sm-11">
                        <div class="content-left">
                            <div class="service-section">
                                <h3 class="contnt-heading wow fadeInUp animated"><?php if(!empty($pages['name'])){ echo $pages['name']; }else{ echo $pg_title; } ?></h3>
                                <div class="content-wrapper wow fadeInUp animated">
                                   
								      <div class="gallery-main">
               
                <div class="demo-gallery">
            <ul id="lightgallery" class="list-unstyled row">
                 
                <?php foreach($album as $ab){ ?>
                <li class="photo-gallery" >
                    <a href="<?php echo site_url('page/photogallery/'.$ab->alias).'#lg=1&slide=0'; ?>">
                        <img class="img-responsive" src="<?php echo base_url(); ?>admin/images/gallery/thumb/<?php echo  $ab->cover_pic;  ?>" alt="Thumb-1">
                    </a>
                    <h4 class="img-title"><?php  if(strlen($ab->name) >14 ){echo substr($ab->name,0,15).'...'; }else{echo $ab->name; } ?></h4>
                </li>
                <?php } ?>
            </ul>
        </div>
                
                
               <div class="pull-right" ><?php echo $pagination;  ?><!--  <img class="ldr" style="position: relative;bottom: 40px;display: none;" src="<?php echo base_url('assets/user/images/loader.gif'); ?>">--></div>
                
            </div>
                             
                                  
                                   
                                    </div>
                                </div>
                            </div>
                        
                        </div>

 </div>
                   
                </div>
            </div>
            

<script>
   $(document).ready(function(){
        $('a.paginate').click(function (event){
            $('.ldr').show();
     event.preventDefault();
    $.ajax({
        url: $(this).attr('href')
        ,success: function(response) {
            $('.ldr').hide();
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $('#resp').html(response);
        }
     })
     return false;
//alert($(this).attr('href'));
});
    });

</script>
