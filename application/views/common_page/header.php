 <?php  $webdata = $this->db->get_where('my_website',array('id'=>'1'))->row();  ?>
       <?php $web_title = "";  if(!empty($pg_title)){
                   $web_title = $pg_title; 
                   }else if(empty($pages['title']) || $pages['title']=='home'){ 
                   $web_title = $webdata->title; 
                   }else{
                   $web_title = $pages['title']; 
} 

if(!empty($pages['sec_1_data'])){
    $ogimg = base_url()."admin/upload/page/".$pages['sec_1_data'];
}else{
    $ogimg = base_url()."admin/upload/page/min_page2/Share-image.jpg";
}

if(!empty($pages['right'])){
    $ogdesc  = substr($pages['right'],0,300);
    $ogdesc = strip_tags($ogdesc);
}else if(!empty($seo_desc)){
   $ogdesc =  $seo_desc;
}else{
   $ogdesc =  $webdata->meta_description; 
}

if(!empty($keyword_seo)){
$ogkey = $keyword_seo;
    
}else{
$ogkey = $webdata->meta_keyword;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo $web_title; ?></title>
    
<meta name="description" content="<?php echo $ogdesc; ?>">
<meta name="keywords" content="<?php echo $ogkey; ?>">
    
<meta property="fb:app_id" content="90909090900" />
<meta property="og:title" content="<?php echo $web_title; ?>" />
<meta property="og:description" content="<?php echo $ogdesc.' ...'; ?>" />
<meta property="og:url" content="<?php echo current_url(); ?>" />    
<meta property="og:image" content="<?php echo $ogimg; ?>" />
<meta property="og:type" content="article" />
<meta property="og:site_name" content="<?php echo $webdata->meta_title; ?>" />
<?php if(!empty($pages['tags'])){
$tags = explode(',',$pages['tags']);    
foreach($tags as $tg){    
?>
<meta property="article:tag" content="<?php echo $tg; ?>" />
<?php }} ?>
<meta property="twitter:card" content="summary_large_image" />
<meta property="twitter:site" content="@adhvan" />
<meta property="twitter:title" content="<?php echo $web_title; ?>" />
<meta property="twitter:description" content="<?php echo $ogdesc.' ...'; ?>" />
<meta property="twitter:url" content="<?php echo current_url(); ?>" />
<meta property="twitter:image:src" content="<?php echo $ogimg; ?>" />
<meta property="twitter:domain" content="<?php echo $webdata->meta_title; ?>" />

    
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/css?family=Fjalla+One|Roboto&display=swap" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/menu.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/css/mega-menu.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css"/>   
<script src="<?php echo base_url(); ?>assets/js/jquery-2.1.3.min.js"></script>
    
    
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.10";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<style>
.menu-sub .box-type-2 ul li a,
li.nav-icon.nav-icon-classic a {
    padding:10px 0px;
}</style>

</head>
  
    
<nav class="main-nav white stick-fixed  js-transparent main-nav-line small-height">
    <div class="full-wrapper relative clearfix">
        <div class="nav-logo-wrap"><a href="<?php echo base_url(); ?>" class="logo exo-logo1 fadeInLoad exo-logo2 small-height"></a></div>
        <div class="navbar-mobile small-height" style="height: 65px; line-height: 65px; width: 65px;"><i class="fa fa-bars"></i></div>
        <div class="inner-nav navbar-desktop">
            <ul class="clearlist scroll scroll-nav">
                <li class="menu-select"><a href="#" class="menu-has-sub" style="height: 65px; line-height: 65px;"><span>Destinations</span></a>
                    <div class="menu-sub menu-sub-effect1 top-nav-dest mega-dest" style="display: none;">
                        <div class="row tab-wrap-img mb-10">
                        <?php  foreach($menuCountry as $k=>$mct){ ?>
                            <div class="col-sm-5ths">
                                <div class=" tab-img"> <a href="<?php echo base_url('location/'.$mct->alias); ?>"><span><?php echo $mct->countryname; ?></span> <img src="<?php echo base_url($mct->image); ?>" alt="<?php echo $mct->page_name; ?>" title="<?php echo $mct->page_name; ?>" class="resp"></a></div>
                                <div
                                    class="mega-dest-link">
                                    <p><a href="<?php echo base_url('experience/'.$mct->alias); ?>">All Journey</a></p>
                                    <!-- <p><a href="<?php echo base_url('hotels/'.$mct->alias); ?>">Preferred Hotels</a></p> -->
                                    <p><a href="<?php echo base_url('location/'.$mct->alias); ?>">Experiences</a></p>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="col-sm-5ths col-nav-multi">
                        <!-- <?php echo base_url('location/multi-country'); ?> -->
        <div class="img-overlay-black tab-img"> <a href="#0"><span>MULTI COUNTRY</span> <img src="<?php  echo base_url('assets/images/multi-country.png'); ?>" alt="Destinations multi-country" title="Destinations multi-country" class="resp"></a></div>
        <div
            class="mega-dest-link">
            <p><a href="<?php echo base_url('experience/all'); ?>">All Journey</a></p>
    </div>

                       
    
    </div>
    </div>
    </li>
    <li class="menu-select"><a href="#" class="menu-has-sub" style="height: 65px; line-height: 65px;"><span>TOUR STYLES</span></a>
        <div class="menu-sub menu-sub-effect1 mega-style" style="display: none;">
          
          <?php
          $noStyle = count($tourStyle);
          $z = 0; $nc = $noStyle /5;
          for($x=0;$x<= $nc; $x++){
          ?>
            <div class="box-type-2">
                <ul>
                <?php for($g=0;$g <= 2; $g++){ 
                  if($noStyle > $z){
                    $cv = $z++; 

                    ?>
                    <li class="nav-icon nav-icon-classic"><a href="<?php echo base_url('style/'.$tourStyle[$cv]['alias']); ?>"><?php echo $tourStyle[$cv]['name']; ?></a></li>
                <?php  } } ?>
                  
                </ul>
            </div>
          <?php } ?>
            
        </div>
    </li>
    <li class="menu-select"><a href="#" class="menu-has-sub" style="height: 65px; line-height: 65px;"><span>ABOUT</span></a>
        <div class="menu-sub menu-sub-effect1 mega-about2" style="display: none;">
            <div class="col-md-12 col-12">
                <div class="row tab-wrap-img">
                
                    <div class="col-md-4 col-12 col-sm-6">
                        <div class=" tab-img"><a href="#"><span>Adhvan Story</span> <img src="<?php echo base_url(); ?>images/about1.png"  class="resp"></a></div>
                    </div>
                    <div class="col-md-4 col-12 col-sm-6">
                        <div class=" tab-img mega-about-mouseon4"><a href="<?php echo base_url('adhvanpage/why-adhvan'); ?>"><span>Why Adhvan </span> <img src="<?php echo base_url(); ?>images/about2.png?v=0" class="resp"></a></div>
                    </div>
                    <div class="col-md-4 col-12 col-sm-6">
                        <div class=" tab-img"><a href="#"><span>Adhvan Core Values<br> </span> <img src="<?php echo base_url(); ?>images/about3.png" class="resp"></a></div>
                    </div>
                     <div class="col-md-4 col-12 col-sm-6">
                        <div class=" tab-img"><a href="<?php echo base_url('adhvanpage/our-philosophy'); ?>"><span>Our Philosophy</span> <img src="<?php echo base_url(); ?>images/about4.png" class="resp"></a></div>
                    </div>
                    <div class="col-md-4 col-12 col-sm-6">
                        <div class=" tab-img"><a href="#"><span>Responsible Adhvan</span> <img src="<?php echo base_url(); ?>images/about5.png" class="resp"></a></div>
                    </div>
                    
                    <div class="col-md-4 col-12 col-sm-6">
                        <div class=" tab-img"><a href="#"><span>Careers</span><img src="<?php echo base_url(); ?>images/about6.png" class="resp"></a></div>
                    </div>
                    
                </div>
            </div>
        </div>
    </li>
    
<?php /*?>    <li class="menu-select"><a href="<?php echo base_url('hotels/adhvan'); ?>" style="height: 65px; line-height: 65px;"><span>Hotel</span></a></li>
<?php */?><?php /*?>    <li class="menu-select"><a href="<?php echo base_url('adhvanpage/our-philosophy'); ?>" style="height: 65px; line-height: 65px;"><span>About Adhvan</span></a></li>
    <li class="menu-select"><a href="<?php echo base_url('adhvanpage/diversity-of-asia'); ?>" style="height: 65px; line-height: 65px;"><span>Diversity of Asia</span></a></li>
    <li class="menu-select"><a href="<?php echo base_url('adhvanpage/why-adhvan'); ?>" style="height: 65px; line-height: 65px;"><span>Why Adhvan</span></a></li>
<?php */?>    
<li class="menu-select"><a href="<?php echo base_url('adhvanpage/contact-us'); ?>" style="height: 65px; line-height: 65px;"><span>Contact Us</span></a></li>

    <li class="menu-select"><a href="<?php echo base_url('category/blog'); ?>" style="height: 65px; line-height: 65px;"><span>BLOG</span></a></li>
    <li class="menu-select"><a href="#" data-toggle="modal" data-target="#Modal-1" style="height: 65px; line-height: 65px;"><span style="padding:7px;background:rgba(0, 0, 0, 0.2);color:#ffffff">ENQUIRE</span></a></li>
    <li></li>
    
    </ul>
    </div>
    </div>
</nav>
