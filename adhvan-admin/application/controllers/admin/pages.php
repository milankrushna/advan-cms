<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

	function __construct()
	{
		parent::__construct();
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->model('page_model');
		$this->load->database();  
		$this->lang->load('auth');
		$this->load->helper('language');
		$this->header['menu']=2;
               $this->header['webdata'] = $this->db->get_where('my_website',array('id'=>'1'))->row();

		 if (!$this->ion_auth->logged_in() && !$this->ion_auth->is_admin())
    {
      redirect('admin/auth/login', 'refresh');
    }
	}
 
   
/**
* @name          index
* @author        Milan Krushna
* @revised       null
* @Description   display listing of pages  
* @Param String  $msg is which condition satisfy that messge will display
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
	
 
 function index($msg=NULL){
 	
		if($msg == '1'){
				$this->data['msg'] = "<p style='color:red'>Page Deleted Successfully!</p>";
			}else if($msg == '2'){
					$this->data['msg'] = "<p style='color:red'>Unable To Delete The Page!<br>It has been published</p>";
			}else if($msg == '3'){
					$this->data['msg'] = "<p style='color:red'>You Dont have Access to delete!</p>";
			}else{
				$this->data['msg'] = "<p style='color:green'>Listing of Articles!</p>";
			}
		$this->data['pages'] = $this->page_model->get_pages();
      
		$this->load->view("common_admin/header",$this->header);
		//$this->load->view("common/menu");
		$this->load->view("pages/view_pages",$this->data);
		$this->load->view("common_admin/footer");	
		
 }

   
/**
* @name          create page 
* @author        Milan Krushna and Subhashree Jena
* @revised       null
* @Description   create new page
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
	

function create_page()
	{
	//	echo "i am here";exit;
		$this->data['title'] = "Create page";
		
       ///  $this->data['slider'] = $this->page_model->slider_listing();
    
      $this->data['page'] = $this->page_model->get_all_pages();
      $this->data['category'] = $this->page_model->get_category_menu();
      $this->data['author'] = $this->page_model->get_author();
      $this->data['tags'] = $this->page_model->get_tags();
	 
  //  print("<pre>");
//   print_r( $this->data['author']);exit;
     
    
    
      if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('admin/auth', 'refresh');
		}

    $valerr = array();
		$this->form_validation->set_rules('name', "Page Name", 'required|is_unique[pages.name]');
		//$this->form_validation->set_rules('name', "Meta Keyword", 'required|is_unique[pages.name]');
		//$this->form_validation->set_rules('name', "Meta Description", 'required|is_unique[pages.name]');        
	
		//print_r($_REQUEST);

 if(!empty($this->input->post())){
    
    if ($this->form_validation->run() == FALSE)
    {
      $valerr['status'] = 'formerr';
      $valerr['message'] = validation_errors();
        echo json_encode($valerr);
        exit;
        
    }
 }
		if ($this->form_validation->run() == TRUE)
		{
			$data['cat_id'] = $catg_id = $this->input->post('category');
			 $stp =  $this->input->post('save_type');
            $data['sub_catid'] = $subcat_ids =  $this->input->post('sub_cat');
			
            if($stp == 2 || $stp==3){
                $data['status'] = 2;
            }else{
                $data['status'] = $stp;
                
            ///for latest article in homepage
            if($subcat_ids=='' && $catg_id !=''){
                $updt['article_id'] = 0;
                $cnds['id'] = $catg_id;
                $this->db->update('menu',$updt,$cnds);
            }
                
                
                ///for latest article in homepage
            if($subcat_ids!='' || $subcat_ids!='0'){
                $updt['article_id'] = 0;
                $cnds['id'] = $subcat_ids;
                $this->db->update('menu',$updt,$cnds);
            }
                
                
            }
            
            $data['visit'] = $this->input->post('visit');
			
			//$data['url'] =  $this->create_url($this->input->post('name'));	
			
            //seo
                //  $data['meta_title'] = $this->input->post('meta_title');  
                //  $data['meta_desc'] = $this->input->post('meta_desc');  
                //  $data['meta_keyword'] = $this->input->post('meta_keyword');  
            
			$data['android'] = $this->input->post('android');
			$data['ios'] = $this->input->post('ios');
			
			$data['name'] = $this->input->post('name');
			$data['author_id'] = $this->input->post('author');
			$data['author_id'] = $this->input->post('author');
            $data['tags'] = $this->input->post('tags');
			$data['right'] = $this->input->post('txt_content');
			$data['short_content'] = $this->input->post('short_content');
            $date = $this->input->post('date');
			$data['date'] = (!empty($date))? date("Y-m-d", strtotime($date)) : date('Y-m-d');
			
            $pid = $this->input->post('rtn_pid');
        
            if(!empty($_FILES)){
            if($_FILES['cover_pic']['name'] !=""){
            $image1=$this->upload_page_image('cover_pic');
		  if($image1!=false){
            $data['sec_1_data'] = $image1;
          }else{
              
             // echo 2;exit;
              //redirect('admin/slider/add_slide');
          } 
            } }
            
             
           /* if($_FILES['author_img']['name'] !=""){
            $author=$this->upload_page_image('author_img');
		  if($author!=false){
            $data['author_pic'] = $author;
          }else{
              
             // echo 2;exit;
              //redirect('admin/slider/add_slide');
          } 
            }*/
            
		   /// $data['sec_1_data']=$this->upload_page_image('cover_img');
		  ///  $data['author_pic']=$this->upload_page_image('author_img');
		
            if($pid == 0){
                //$data['url'] = $this->input->post('url'); 
                $data['url'] =  $this->create_url($this->input->post('name'));	
			$page_id =  $this->page_model->create_page($data);
		$resp = array();
         //$page_id = 525;       
			if($page_id)
			{
                 //echo 1;exit;
                $resp['status'] = '1';
                $resp['pid'] = $page_id;
                $resp['sv_type'] = $stp;
                $resp['message'] = 'Page Successfully Created';
                
                
				//$this->session->set_flashdata('message','page successfully created');
				//redirect("admin/pages/edit_page/".$page_id);
			}else{
              $resp['status'] = '1';
                $resp['pid'] = 0;
                $resp['sv_type'] = $stp;
                $resp['message'] = 'Something Error, Try again';  
            }
            }else{
                
               $update =  $this->page_model->update_page($data,$pid); 
              if($update){  
                $resp['status'] = '1';
                $resp['pid'] = $pid;
                $resp['sv_type'] = $stp;
                $resp['message'] = 'Page Successfully Updated';
            }else{
              $resp['status'] = '1';
                $resp['pid'] = $pid;
                $resp['sv_type'] = $stp;
                $resp['message'] = 'Something Error, Try again';  
            }
            
        }
        echo json_encode($resp);
            
		}
		else
		{
            
            
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		$this->load->view("common_admin/header",$this->header);
		$this->load->view("pages/create_page",$this->data);
		$this->load->view("common_admin/footer");	
		}
	}
    
    
    function get_sub(){
        
         $cond['parent_menu'] = $_GET['prob_id'];
        
      $this->data['subcategory'] = $subcat = $this->page_model->get_ct_menu($cond);
        
        //print("<pre>");
      //  print_r($subcat);exit;
        
     echo "<option value=''>Select Subcategory</option>";
      foreach($subcat as $cs){
          echo "<option value=".$cs->id.">".$cs->menu."</option>";
      }    
    }
    
    
    
	
    
     
/**
* @name          upload and resize image 
* @author        Milan Krushna
* @revised       null
* @Description   uploading and resizeing image  
* @Param String  $field is fieldname of image file  
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
	  
    
  public function upload_page_image($field){
    
    $file_ext = explode('/',$_FILES[$field]['type'])[1]; 
    
    $new_img =  strtotime("now").'.'.$file_ext; 
   
    
        $config['upload_path'] = './upload/page/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif|JPG|JPEG|PNG|pdf|PDF';
        $config['max_size']	= '2000000000';
        $config['max_width'] = '102400000';
        $config['max_height'] = '700000000';
        $config['file_name'] = $new_img;

$this->load->library('upload', $config);
$this->upload->initialize($config);
 if($this->upload->do_upload($field)){
     
    
     ///checking file is pdf or image
     if($file_ext != 'pdf'){
     }

    $this->resize_pg_image1($new_img);
    $this->resize_pg_image2($new_img);
    $this->resize_pg_image3($new_img);

   $data = array('upload_data' => $this->upload->data());
     
    return  $data['upload_data']['file_name'];
  
 }else{
  
 $this->session->set_flashdata('message',$this->upload->display_errors());
     //return false; 
     
echo $this->upload->display_errors();
}
 

 }

  protected function resize_pg_image1($new_img){


            $this->load->library('image_lib');
       
          
            $src_path = './upload/page/'.$new_img;
            $des_path =  './upload/page/min_page1/'.$new_img;

            $config['image_library']    = 'gd2';
            $config['source_image']     = $src_path;
            $config['new_image']        = $des_path;
            $config['maintain_ratio']   = TRUE;
            $config['width']            = 900;
            $config['height']            = 550;
            $config['quality']   = 100;

            $this->image_lib->initialize($config);
            $this->image_lib->resize();
      

    }
    
    protected function resize_pg_image2($new_img){


            $this->load->library('image_lib');
       
          
            $src_path = './upload/page/'.$new_img;
            $des_path =  './upload/page/min_page2/'.$new_img;

            $config['image_library']    = 'gd2';
            $config['source_image']     = $src_path;
            $config['new_image']        = $des_path;
            $config['maintain_ratio']   = TRUE;
            $config['width']            = 500;
            $config['height']            = 300;
            $config['quality']   = 100;

            $this->image_lib->initialize($config);
            $this->image_lib->resize();
      

    }
    
    protected function resize_pg_image3($new_img){


            $this->load->library('image_lib');
       
          
            $src_path = './upload/page/'.$new_img;
            $des_path =  './upload/page/min_page3/'.$new_img;

            $config['image_library']    = 'gd2';
            $config['source_image']     = $src_path;
            $config['new_image']        = $des_path;
            $config['maintain_ratio']   = TRUE;
           // $config['width']            = "";
            $config['height']            = 117;
            $config['quality']   = 100;

            $this->image_lib->initialize($config);
            $this->image_lib->resize();
      

    }
    
    
    
	   
/**
* @name          edit page 
* @author        Milan Krushna
* @revised       null
* @Description   retreive edit page
* @Param String  $id is id of pages table 
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
	
	
	function edit_page($id="")
	{
		 
    
   $this->data['page'] = $page = $this->page_model->edit_page($id);
$this->data['select_cat'] =$this->page_model->get_category_menu();
$this->data['author'] = $this->page_model->get_author();
	$this->data['tags'] = $this->page_model->get_tags();
$cond['parent_menu'] =$page->cat_id;
$this->data['sub_category'] = $this->page_model->get_ct_menu($cond);

	    // $this->data['slider'] = $this->page_model->slider_listing();
		/*print("<pre>");
		print_r($this->data['page']);
		exit;*/
		
		$this->data['title'] = "Edit Page";
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('admin/auth', 'refresh');
		}
        
        $valerr = array();
		//validate form input
		$this->form_validation->set_rules('name', "Article Name", 'required');
        
    if(!empty($this->input->post())){
    
    if ($this->form_validation->run() == FALSE)
    {
      $valerr['status'] = 'formerr';
      $valerr['message'] = validation_errors();
        echo json_encode($valerr);
        exit;
        
    }
 }  
		if ($this->form_validation->run() == TRUE)
		{
		
            //seo
           //  $data['meta_title'] = $this->input->post('meta_title');  
             //     $data['meta_desc'] = $this->input->post('meta_desc');  
                //  $data['meta_keyword'] = $this->input->post('meta_keyword');  
            
			$data['cat_id'] = $this->input->post('category');
			$data['sub_catid'] = $subcat_ids =$this->input->post('sub_cat');
            $data['android'] = $this->input->post('android');
			$data['ios'] = $this->input->post('ios');
			if($this->input->post('save_type')!=""){
            $stp = $this->input->post('save_type');
                
                 if($stp == 2){
                $data['status'] = 2;
            }else if($stp == 1){
                $data['status'] = 1;
                     
                     ///for latest article in homepage
            if($subcat_ids!='' || $subcat_ids!='0'){
                
            
                $updt['article_id'] = 0;
                $cnds['id'] = $subcat_ids;
                $this->db->update('menu',$updt,$cnds);
              
            }
            }
                
            }
			$data['right'] = $this->input->post('txt_content');	
            $data['tags'] = $this->input->post('tags');
			$data['short_content'] = $this->input->post('short_content');	
			$data['name'] = $this->input->post('name');
			$data['author_id'] = $this->input->post('author');
			///$data['en_btnslider'] = $this->input->post('en_btnslider');
			////$data['title'] = $this->input->post('title');
			///$data['meta_keyword'] =$this->input->post('meta_keyword');
			///$data['meta_description']=$this->input->post('meta_description');
            $date = $this->input->post('date');
            $data['date'] =  date("Y-m-d", strtotime($date));
            
			
           
			 
			
           if(!empty($_FILES)){
                     if($_FILES['cover_pic']['name'] !=""){
					 	 
                        $image1=$this->upload_page_image('cover_pic');
		  if($image1!=false){
            $data['sec_1_data'] = $image1;
          }else{
              
        $valerr['status'] = 'formerr';
      $valerr['message'] = $this->session->flashdata('message');
              
              echo json_encode($valerr);exit;
              
          }   
                         //$data['sec_1_data']=$this->upload_page_image('upload_image1');
					 } }
            
           /*  if($_FILES['author_img']['name'] !=""){
					 	 
                        $author_pic=$this->upload_page_image('author_img');
		  if($author_pic!=false){
            $data['author_pic'] = $author_pic;
          }else{
              
              //echo 2;exit;
              redirect("admin/pages/edit_page/".$id);
          }   
                         
					 }*/
                    
            
			
            $resp = array();
			$page_id =  $this->page_model->update_page($data,$id);			
			//echo $new_category_id;exit;
            
             //$update =  $this->page_model->update_page($data,$pid); 
              if($page_id){  
                $resp['status'] = '1';
                $resp['pid'] = $id;
                $resp['sv_type'] = $stp;
                $resp['message'] = 'Page Successfully Updated';
            }else{
              $resp['status'] = '1';
                $resp['pid'] = $id;
                $resp['sv_type'] = $stp;
                $resp['message'] = 'Something Error, Try again';  
            }
            
            echo json_encode($resp);
            
			/*if($page_id == true)
			{
                
                ///echo 1;exit;
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("admin/pages/edit_page/".$id);
			} */
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
		$this->load->view("common_admin/header",$this->header);
		$this->load->view("pages/edit_page",$this->data);
		$this->load->view("common_admin/footer");	
		}
	}
	


/**
* @name          delete Image
* @author        Milan Krushna
* @revised       null
* @Description   delete page 
* @Param String  $id is id of pages table 
* @copyright     Copyright (C) 2017 powered by Wayindia
*/


		function remove_img(){
			
			$pid  = $this->input->post('pgid');
   			$typ  = $this->input->post('nof');
			
			$cond['id'] = $pid;
	 
			if($typ == 1){
			$data['sec_1_data'] = '';
			}else{
			$data['sec_2_data'] = '';
			}
										
			$this->db->update('pages',$data,$cond);	
			echo 1;					
			}


	   
/**
* @name          delete page
* @author        Milan Krushna
* @revised       null
* @Description   delete page 
* @Param String  $id is id of pages table 
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
	
	
function delete_pg($id){
	$page_id =  $this->page_model->delete_pg($id);	
	echo  1;
    ///redirect("admin/pages/index/1");
}

   


    protected function create_url($strin){
        
        return rtrim(strtolower(str_replace(' ','-',preg_replace('/\s\s+/','-',preg_replace('/[^A-Za-z0-9\-]/', ' ',$strin)))),'/[^A-Za-z0-9\-]/');
    }
    

       
/**
* @name          change url
* @author        Milan Krushna
* @revised       null
* @Description   change url 
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
	
    function change_url(){
        
        
        $resp = array();
        
        $data['pageid'] = $this->input->post('page_id');
        $data['newurl'] = $this->input->post('url_data');
        
        $url_mgt =  $this->page_model->url_management($data); 
        
         
        if($url_mgt == 1){
           
            $resp['status'] =1;
            $resp['message'] =  base_url().'<a href="'.base_url().$data['newurl'].'" target="framename">'.$data['newurl'].'</a>.php <a class="fa fa-edit" style="cursor:pointer;" onclick="modify_url('.$data['pageid'].','."'".$data['newurl']."'".');"></a>';
       
        }else{
            
            $resp['status'] = 0;
            $resp['message'] = $url_mgt;
            ///echo $url_mgt;

        }
        
        echo json_encode($resp); 
         
    }
    
   function create_sponser(){
       $this->data['title'] = "Create Sponsers";
		
    
    
      
      //$this->data['category'] = $this->page_model->get_category_menu();

      if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('admin/auth', 'refresh');
		}

		$this->form_validation->set_rules('name', "Sponsers Name", 'required|is_unique[pages.name]');
		//$this->form_validation->set_rules('name', "Meta Keyword", 'required|is_unique[pages.name]');
		//$this->form_validation->set_rules('name', "Meta Description", 'required|is_unique[pages.name]');        
		    
		if ($this->form_validation->run() == TRUE)
		{
			$data['cat_id'] = $this->input->post('category');
			//$data['sub_catid'] = $this->input->post('sub_cat');
			
			$data['name'] = $this->input->post('name');
			$data['url'] =  $this->create_url($this->input->post('name'));	
			$data['author'] = $this->input->post('author');
			$data['right'] = $this->input->post('txt_content');
            $date = $this->input->post('date');
			$data['date'] =  date("Y-m-d", strtotime($date));
			
            
            
            if($_FILES['cover_img']['name'] !=""){
            $image1=$this->upload_page_image('cover_img');
		  if($image1!=false){
            $data['sec_1_data'] = $image1;
          }else{
              
             // echo 2;exit;
              //redirect('admin/slider/add_slide');
          } 
            }
            

                     
			/*print("<pre>");	 
			print_r($data);
			exit*/;
			$page_id =  $this->page_model->create_page($data);
		
			if($page_id)
			{
                 //echo 1;exit;
				$this->session->set_flashdata('message','page successfully created');
				redirect("admin/pages/edit_sponser/".$page_id);
			}
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		$this->load->view("common_admin/header",$this->header);
		$this->load->view("pages/create_sponser",$this->data);
		$this->load->view("common_admin/footer");	
		}
    
   }     
   
    function create_contest(){
       $this->data['title'] = "Create Contest";
		
    
      //$this->data['category'] = $this->page_model->get_category_menu();

      if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('admin/auth', 'refresh');
		}

		$this->form_validation->set_rules('name', "Sponsers Name", 'required|is_unique[pages.name]');
		//$this->form_validation->set_rules('name', "Meta Keyword", 'required|is_unique[pages.name]');
		//$this->form_validation->set_rules('name', "Meta Description", 'required|is_unique[pages.name]');        
		    
		if ($this->form_validation->run() == TRUE)
		{
			$data['cat_id'] = $this->input->post('category');
			//$data['sub_catid'] = $this->input->post('sub_cat');
			$data['name'] = $this->input->post('name');
			$data['url'] =  $this->create_url($this->input->post('name'));	
			//$data['author'] = $this->input->post('author');
			$data['right'] = $this->input->post('txt_content');
            $date = $this->input->post('date');
			$data['date'] =  date("Y-m-d", strtotime($date));
			
            
            
            if($_FILES['cover_img']['name'] !=""){
            $image1=$this->upload_page_image('cover_img');
		  if($image1!=false){
            $data['sec_1_data'] = $image1;
          }else{
              
             // echo 2;exit;
              //redirect('admin/slider/add_slide');
          } 
            }
            

                     
			/*print("<pre>");	 
			print_r($data);
			exit*/;
			$page_id =  $this->page_model->create_page($data);
		
			if($page_id)
			{
                 //echo 1;exit;
				$this->session->set_flashdata('message','Contest successfully created');
				redirect("admin/pages/edit_contest/".$page_id);
			}
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		$this->load->view("common_admin/header",$this->header);
		$this->load->view("pages/create_contest",$this->data);
		$this->load->view("common_admin/footer");	
		}
    
   } 
    
    function all_sponsers(){
        
          $this->data['sponsers'] = $this->page_model->get_sponsers();
        
        $this->load->view("common_admin/header",$this->header);
		$this->load->view("pages/view_sponser",$this->data);
		$this->load->view("common_admin/footer");
		
    }
function all_contest(){
        
          $this->data['sponsers'] = $this->page_model->get_contest();
        
        $this->load->view("common_admin/header",$this->header);
		$this->load->view("pages/view_contest",$this->data);
		$this->load->view("common_admin/footer");
		
    }

    function edit_sponser($id=""){
        
         $this->data['page'] = $page = $this->page_model->edit_page($id);
///$this->data['select_cat'] =$this->page_model->get_category_menu();
         
         

$cond['parent_menu'] =$page->cat_id;
//$this->data['sub_category'] = $this->page_model->get_ct_menu($cond);

	    // $this->data['slider'] = $this->page_model->slider_listing();
		/*print("<pre>");
		print_r($this->data['page']);
		exit;*/
		
		$this->data['title'] = "Edit Sponsers";
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('admin/auth', 'refresh');
		}
		//validate form input
		$this->form_validation->set_rules('name', "Sponsers Name", 'required');
		if ($this->form_validation->run() == TRUE)
		{
		
			//$data['cat_id'] = $this->input->post('category');
			//$data['sub_catid'] = $this->input->post('sub_cat');
            
            
			$data['right'] = $this->input->post('txt_content');	
			$data['name'] = $this->input->post('name');
			$data['author'] = $this->input->post('author');
			///$data['en_btnslider'] = $this->input->post('en_btnslider');
			////$data['title'] = $this->input->post('title');
			///$data['meta_keyword'] =$this->input->post('meta_keyword');
			///$data['meta_description']=$this->input->post('meta_description');
            $date = $this->input->post('date');
            $data['date'] =  date("Y-m-d", strtotime($date));
            
			
           
			
			//print("<pre>");
			//print_r($_FILES);
			//exit;
			
           
                     if($_FILES['cover_img']['name'] !=""){
					 	 
                        $adimage1=$this->upload_page_image('cover_img');
		  if($adimage1!=false){
            $data['sec_1_data'] = $adimage1;
          }else{
              
              //echo 2;exit;
              redirect("admin/pages/edit_sponser/".$id);
          }   
                         
					 }
                    
            
			
            
			$page_id =  $this->page_model->update_page($data,$id);			
			//echo $new_category_id;exit;
			if($page_id == true)
			{
                
                ///echo 1;exit;
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("admin/pages/edit_sponser/".$id);
			}
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
		$this->load->view("common_admin/header",$this->header);
		$this->load->view("pages/edit_sponser",$this->data);
		$this->load->view("common_admin/footer");	
		}
	
    }
        function edit_contest($id=""){
        
         $this->data['page'] = $page = $this->page_model->edit_page($id);
///$this->data['select_cat'] =$this->page_model->get_category_menu();
         
         

$cond['parent_menu'] =$page->cat_id;
//$this->data['sub_category'] = $this->page_model->get_ct_menu($cond);

	    // $this->data['slider'] = $this->page_model->slider_listing();
		/*print("<pre>");
		print_r($this->data['page']);
		exit;*/
		
		$this->data['title'] = "Edit Sponsers";
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('admin/auth', 'refresh');
		}
		//validate form input
		$this->form_validation->set_rules('name', "Sponsers Name", 'required');
		if ($this->form_validation->run() == TRUE)
		{
		
			//$data['cat_id'] = $this->input->post('category');
			//$data['sub_catid'] = $this->input->post('sub_cat');
            
            
			$data['right'] = $this->input->post('txt_content');	
			$data['name'] = $this->input->post('name');
			///$data['author'] = $this->input->post('author');
			///$data['en_btnslider'] = $this->input->post('en_btnslider');
			////$data['title'] = $this->input->post('title');
			///$data['meta_keyword'] =$this->input->post('meta_keyword');
			///$data['meta_description']=$this->input->post('meta_description');
            $date = $this->input->post('date');
            $data['date'] =  date("Y-m-d", strtotime($date));
            
			
           
			
			//print("<pre>");
			//print_r($_FILES);
			//exit;
			
           
                     if($_FILES['cover_img']['name'] !=""){
					 	 
                        $adimage1=$this->upload_page_image('cover_img');
		  if($adimage1!=false){
            $data['sec_1_data'] = $adimage1;
          }else{
              
              //echo 2;exit;
              redirect("admin/pages/edit_contest/".$id);
          }   
                         
					 }
                    
            
			
            
			$page_id =  $this->page_model->update_page($data,$id);			
			//echo $new_category_id;exit;
			if($page_id == true)
			{
                
                ///echo 1;exit;
				$this->session->set_flashdata('message', 'Contest Updated'); 
				redirect("admin/pages/edit_contest/".$id);
			}
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
		$this->load->view("common_admin/header",$this->header);
		$this->load->view("pages/edit_contest",$this->data);
		$this->load->view("common_admin/footer");	
		}
	
    }
    
    
    	
function delete_sponser($id){
	$page_id =  $this->page_model->delete_ad_sponser($id);	
	redirect("admin/pages/all_sponsers/1");
}
    
function delete_contest($id){
	$page_id =  $this->page_model->delete_ad_sponser($id);	
	redirect("admin/pages/all_contest/1");
}

 
    
    function create_footer()
	{
	//	echo "i am here";exit;
		$this->data['title'] = "Create page";
		
         $this->data['slider'] = $this->page_model->slider_listing();
    
    
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('admin/auth', 'refresh');
		}

		$this->form_validation->set_rules('name', "Page Name", 'required|is_unique[pages.name]');
		//$this->form_validation->set_rules('name', "Meta Keyword", 'required|is_unique[pages.name]');
		//$this->form_validation->set_rules('name', "Meta Description", 'required|is_unique[pages.name]');        
		    
		if ($this->form_validation->run() == TRUE)
		{
			$data['name'] = $this->input->post('name');
			//$data['pg_head'] = $this->input->post('pg_head');
			
			$data['right'] = $this->input->post('txt_content');
			//$data['url'] =  $this->create_url($this->input->post('name'));	
         //	$data['en_gallery'] = $this->input->post('en_gallery');
		//	$data['en_slider'] = $this->input->post('en_slider');
		//	$data['en_btnslider'] = $this->input->post('en_btnslider');
		//	$data['title'] = $this->input->post('title');
		//	$data['meta_keyword']=$this->input->post('meta_keyword');
		//    $data['meta_description']=$this->input->post('meta_description');
            
            
          /*  if($_FILES['upload_image1']['name'] !=""){
            $image1=$this->upload_page_image('upload_image1');
		  if($image1!=false){
            $data['sec_1_data'] = $image1;
          }else{
              
             // echo 2;exit;
              //redirect('admin/slider/add_slide');
          } 
            }
            if($_FILES['upload_image2']['name'] !=""){
            $image2=$this->upload_page_image('upload_image2');
		  if($image2!=false){
            $data['sec_2_data'] = $image2;
          }else{
              
              //echo 2;exit;
              //redirect('admin/slider/add_slide');
          }
            }
		    //$data['sec_1_data']=$this->upload_page_image('upload_image1');
		    //$data['sec_2_data']=$this->upload_page_image('upload_image2');
		    
            
            $data['sec_1_link']=$this->input->post('imagelink');
		    $data['sec_2_link']=$this->input->post('pdflink');*/
										      
               
                     
			/*print("<pre>");	 
			print_r($data);
			exit*/;
			$page_id =  $this->page_model->create_page($data);
		
			if($page_id)
			{
                 //echo 1;exit;
				$this->session->set_flashdata('message','page successfully created');
				redirect("admin/pages/edit_page/".$page_id);
			}
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		$this->load->view("common_admin/header",$this->header);
		$this->load->view("pages/create_footer_page",$this->data);
		$this->load->view("common_admin/footer");	
		}
	}
	
    function edit_footer($id="")
	{
		 $this->data['page'] =  $this->page_model->edit_page($id);		
	   //  $this->data['slider'] = $this->page_model->slider_listing();
		/*print("<pre>");
		print_r($this->data['page']);
		exit;*/
		
		$this->data['title'] = "Edit Page";
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('admin/auth', 'refresh');
		}
		//validate form input
		$this->form_validation->set_rules('title', "Title", 'required');
		if ($this->form_validation->run() == TRUE)
		{
		
			$data['name'] = $this->input->post('name');
            $data['right'] = $this->input->post('txt_content');	
			/* $data['pg_head'] = $this->input->post('pg_head');
			
			$data['en_gallery'] = $this->input->post('en_gallery');
			$data['en_slider'] = $this->input->post('en_slider');
			$data['en_btnslider'] = $this->input->post('en_btnslider');
			$data['title'] = $this->input->post('title');
			$data['meta_keyword'] =$this->input->post('meta_keyword');
			$data['meta_description']=$this->input->post('meta_description');
			$data['sec_1_link']=$this->input->post('sec_1_link');
			$data['sec_2_link']=$this->input->post('sec_2_link');
			
			
			//print("<pre>");
			//print_r($_FILES);
			//exit;
			
			
           
                     if($_FILES['upload_image1']['name'] !=""){
					 	 
                        $image1=$this->upload_page_image('upload_image1');
		  if($image1!=false){
            $data['sec_1_data'] = $image1;
          }else{
              
              //echo 2;exit;
              redirect("admin/pages/edit_page/".$id);
          }   
                         //$data['sec_1_data']=$this->upload_page_image('upload_image1');
					 }
                    if($_FILES['upload_image2']['name'] !=""){
					 	
                         $image2=$this->upload_page_image('upload_image2');
		  if($image2!=false){
            $data['sec_2_data'] = $image2;
          }else{
              
              //echo 2;exit;
              redirect("admin/pages/edit_page/".$id);
          }
                        
                        //$data['sec_2_data']=$this->upload_page_image('upload_image2');
					 }*/
                
                	 
            
            
			$page_id =  $this->page_model->update_page($data,$id);			
			//echo $new_category_id;exit;
			if($page_id == true)
			{
                
                ///echo 1;exit;
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("admin/pages/edit_page/".$id);
			}
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
		$this->load->view("common_admin/header",$this->header);
		$this->load->view("pages/edit_footer_page",$this->data);
		$this->load->view("common_admin/footer");	
		}
	}
	
      function all_footer(){
          
          
          $this->data['footer'] = $this->page_model->get_footer_pages();
        
        $this->load->view("common_admin/header",$this->header);
		$this->load->view("pages/view_footer",$this->data);
		$this->load->view("common_admin/footer");
          
      }
   /* public function upload_image($field){
 
        $new_img =  now().'.jpg';
        $config['upload_path'] = './upload/page/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']	= '30000';
        $config['max_width'] = '102400';
        $config['max_height'] = '70000';
        $config['file_name'] = $new_img;

$this->load->library('upload', $config);
$this->upload->initialize($config);
 if($this->upload->do_upload($field)){
     
   $this->resize_image($new_img);
   $data = array('upload_data' => $this->upload->data());
     
     return $data['upload_data']['file_name'];
  
 }else{
  
  $this->session->set_flashdata('message',$this->upload->display_errors());
     return false; 
     
 }

  
}
  
    function resize_image($new_img){
        
        
      $this->load->library('image_lib');
 


            $src_path = 'upload/page/' . $new_img;
            $des_path =  'upload/page/min_page/' . $new_img;
 
            $config['image_library']    = 'gd2';
            $config['source_image']     = $src_path;
            $config['new_image']        = $des_path;
            $config['maintain_ratio']   = TRUE;
            $config['width']            = 241;
            $config['height']           = 179;
            

            $this->image_lib->initialize($config);
            $this->image_lib->resize();
        
    }*/
    
   
    function view_comment(){
        $url = $this->input->get('comment');
        $this->data['cmt_url'] = $url;
        
        $this->load->view('pages/view_comment',$this->data);
        
    }
    
    function draftcnpt($id=''){
        
     
        $sts = $this->input->post('sts');
        
        if($sts == 0){
            $upsts = 1; 
        }else{
            $upsts = 0;
        }
      
        if($this->db->update('pages',array('status'=>$upsts),array('id'=>$id))){
          echo $upsts;  
        }
        
    
    }
    
    
    
    
}

