<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Location extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));
        $this->load->model('location_model');
        $this->data['menu'] = "location";
       $this->data['webdata'] = $this->db->get_where('my_website',array('id'=>'1'))->row();   
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
	}

	// redirect if needed, otherwise display the user list
	public function country($id='')
	{
            $this->data['index_heading'] = 'Country Listing';
            $this->data['index_subheading'] = '';
            $this->data['alldata'] = array();
            $this->data['sear_key'] = '';

            if(!empty($_GET['search_data'])){
            
                $this->data['sear_key'] = $sk = $_GET['search_data'];
              $this->data['alldata'] =  $this->location_model->country_search($sk);   
                
            }else{
            $this->data['alldata'] =  $this->location_model->getstar('country',$id);
            }
		    $this->load->view('common_admin/header', $this->data);
			$this->load->view('location/country_listing', $this->data);
			$this->load->view('common_admin/footer', $this->data);
		}
    
        function country_page($cid=""){
            if($cid==""){
                redirect('admin/location/country');
            }
           $this->data['page_data'] = $pageData = $this->location_model->get_country_page($cid);
           $this->data['cid'] = $cid;

            if(!empty($pageData)){
                $this->data['title'] = "Modify Country Page";
                $this->load->view('common_admin/header', $this->data);
                $this->load->view('location/edit_country_page', $this->data);
                $this->load->view('common_admin/footer', $this->data);
            }else{
                $this->data['title'] = "Create Country Page";
                
                $this->load->view('common_admin/header', $this->data);
                $this->load->view('location/create_country_page', $this->data);
                $this->load->view('common_admin/footer', $this->data);
            }

        }
    


        function create_country_page($cid=''){
            $this->data['cid'] = $cid;

            $cpData = $this->input->post();

            $wdd = $cpData['weather_data'];
            $cpData['weather_data'] = serialize($wdd);
            $cpData['country_id'] = $cid;

            $this->db->insert('country_page',$cpData);
            redirect('admin/location/country_page/'.$cid);

        }
        function update_country_page($cid=''){
            $this->data['cid'] = $cid;

        //   print("<pre>");
            $cpData = $this->input->post();

            // print_r( $cpData);exit;
            $wdd = $cpData['weather_data'];
            $cpData['weather_data'] = serialize($wdd);
          // print_r($cpData);exit;
            $this->db->update('country_page',$cpData,array('country_id'=>$cid));
            redirect('admin/location/country_page/'.$cid);

        }


    function country_data(){
        
        $data = $this->location_model->getstar('country',$id,$lmt);
        
        print("<pre>");
        print_r($_GET);
        
    }
    
    
    function new_country(){
         $resp =array();
        $data['countryname'] = $newcnt= $this->input->post('cname');
        $data['alias'] = $this->create_url($this->input->post('cname'));

        
        if($newcnt!=''){
        $this->db->insert('country',$data);
        $insert = $this->db->insert_id();
            $resp['status']=1;
            $resp['message']='country Created';  
            $resp['id']=$insert;  
            $resp['newcntry']=$newcnt;  
            
        }else{
             $resp['status']=0;
            $resp['message']='something Error try again';  
      
            
        }
               echo json_encode($resp); 

    }
    
    
    function update_country(){
        
        $resp =array();
        
        $data['countryname'] = $this->input->post('cname');
        $data['alias'] = $this->create_url($this->input->post('cname'));

        $cond['id'] = $this->input->post('cid');
        
        if($this->db->update('country',$data,$cond)){
            $resp['status']=1;
            $resp['message']='country Update';
        }else{
            $resp['status']=0;
            $resp['message']='something Error try again';  
        }
            
       echo json_encode($resp); 
        
    }
    
    function delete_country($id =''){
    
        if($this->db->delete('country',array('id'=>$id))){
            echo 1;
        }else{
            echo "Something error try again";
        }
    }
    

    public function state($id='')
	{
            $this->data['index_heading'] = 'State Listing';
            $this->data['index_subheading'] = '';
        
        $cid = $this->input->get('country');
            $this->data['cid'] = $cid;
            
        $this->data['country'] = $this->location_model->getstar('country');
            
            $lmt = 30;
            $this->data['alldata'] =  $this->location_model->getstate($cid,$id,$lmt);
        
		    $this->load->view('common_admin/header', $this->data);
			$this->load->view('location/state_listing', $this->data);
			$this->load->view('common_admin/footer', $this->data);
        }
        
        protected function create_url($strin){
        
            return rtrim(strtolower(str_replace(' ','-',preg_replace('/\s\s+/','-',preg_replace('/[^A-Za-z0-9\-]/', ' ',$strin)))),'/[^A-Za-z0-9\-]/');
        }
        
    function new_state(){
        
        $data['countryid'] = $this->input->post('cid');
        $this->data['countryname'] = $this->input->post('cname');
        $data['statename'] = $this->input->post('state');
        $data['alias'] = $this->create_url($this->input->post('state'));
        
        $this->data['state'] = $this->location_model->create_state($data);
        $this->data['type'] = 'create';
        
        $output = $this->load->view('location/ajax_view',$this->data,true);
        
            $resp['status']=1;
            $resp['message']='State Created';  
            $resp['newstate']=$output; 
        echo json_encode($resp);
        
    }
    
    function update_state(){
        
      $data['countryid'] = $this->input->post('cid');
        $this->data['countryname'] = $this->input->post('cname');
        $data['statename'] = $this->input->post('state');
        $data['alias'] = $this->create_url($this->input->post('state'));

        $cond['id'] = $this->input->post('sid');
        
        $this->data['state'] = $this->location_model->update_state($data,$cond);
        $this->data['type'] = 'update';
        
        $output = $this->load->view('location/ajax_view',$this->data,true);
        
            $resp['status']=1;
            $resp['message']='State Created';  
            $resp['newstate']=$output; 
        echo json_encode($resp);  
        
    }
    
    
    function delete_state($id =''){
    if($this->db->delete('state',array('id'=>$id))){
            echo 1;
        }else{
            echo "Something error try again";
        }
    }


        public function region($id='')
	{
            $this->data['index_heading'] = 'Region Listing';
            $this->data['index_subheading'] = '';
        
        $cid = $this->input->get('country');
            $this->data['cid'] = $cid;
            
        $this->data['country'] = $this->location_model->getstar('country');
            
            $lmt = 30;
            $this->data['alldata'] =  $this->location_model->getregion($cid,$id,$lmt);
		    $this->load->view('common_admin/header', $this->data);
			$this->load->view('location/region_listing', $this->data);
			$this->load->view('common_admin/footer', $this->data);
		}
    
    
        function new_region(){
        
            $data['countryid'] = $this->input->post('cid');
            $this->data['countryname'] = $this->input->post('cname');
            $data['name'] = $this->input->post('state');
            $data['alias'] = $this->create_url($this->input->post('state'));
            
            $this->data['state'] = $this->location_model->create_region($data);
            $this->data['type'] = 'create_region';
            
            $output = $this->load->view('location/ajax_view',$this->data,true);
            
                $resp['status']=1;
                $resp['message']='Region Created';  
                $resp['newstate']=$output; 
            echo json_encode($resp);
            
        }

        function update_region(){
        
            $data['countryid'] = $this->input->post('cid');
              $this->data['countryname'] = $this->input->post('cname');
              $data['name'] = $this->input->post('state');
            $data['alias'] = $this->create_url($this->input->post('state'));

              $cond['id'] = $this->input->post('sid');
              
              $this->data['state'] = $this->location_model->update_region($data,$cond);
              $this->data['type'] = 'update_region';
              
              $output = $this->load->view('location/ajax_view',$this->data,true);
              
                  $resp['status']=1;
                  $resp['message']='Region Updated';  
                  $resp['newstate']=$output; 
              echo json_encode($resp);  
              
          }
          
          
          function delete_region($id =''){
          if($this->db->delete('region',array('id'=>$id))){
                  echo 1;
              }else{
                  echo "Something error try again";
              }
          }
   
    function district($id = ''){
        
            $this->data['index_heading'] = 'Destination Listing';
            $this->data['index_subheading'] = '';
            
            $this->data['country'] = $this->location_model->getstar('country');
            
      $this->data['cnt_id'] =  $cond['country_id'] = $this->input->get('country_2');
       $this->data['sta_id'] = $cond['state_id'] = $this->input->get('state_id_2');
            
            $lmt = 30;
            $this->data['alldata'] =  $this->location_model->getcity($cond,$lmt);
        
		    $this->load->view('common_admin/header', $this->data);
			$this->load->view('location/district_listing', $this->data);
			$this->load->view('common_admin/footer', $this->data);
        
    }
    
    function city($id = ''){
        
            $this->data['index_heading'] = 'City Listing';
            $this->data['index_subheading'] = '';
            
            $this->data['country'] = $this->location_model->getstar('country');
            
      $this->data['cnt_id'] =  $cond['c_id'] = $this->input->get('country_2');
       $this->data['sta_id'] = $cond['s_id'] = $this->input->get('state_id_2');
       $this->data['dist_id'] = $cond['d_id'] = $this->input->get('distId_2');
            
            $lmt = 30;
            $this->data['alldata'] =  $this->location_model->getDistrictCity($cond,$lmt);
        
		    $this->load->view('common_admin/header', $this->data);
			$this->load->view('location/city_listing', $this->data);
			$this->load->view('common_admin/footer', $this->data);
        
    }
    
    
    
    
    function get_cnt_state($cid=''){
        $stateid = $this->input->post('sid');
        
        $data = $this->db->get_where('state',array('countryid'=>$cid))->result();
        
        echo "<option value=''>Select City</option>";
      foreach($data as $cs){
          if($stateid == $cs->id){ $selct ='selected'; }else{ $selct ='';  }
          echo "<option ".$selct." value=".$cs->id.">".$cs->statename."</option>";
      } 
        
    }
    
       
    function get_state_district($cid=''){
        $stateid = $this->input->post('sid');
        
        $data = $this->db->get_where('city',array('state_id'=>$cid))->result();
        
        echo "<option value=''>Select District</option>";
      foreach($data as $cs){
          if($stateid == $cs->id){ $selct ='selected'; }else{ $selct ='';  }
          echo "<option ".$selct." value=".$cs->id.">".$cs->city."</option>";
      } 
        
    }
    
    function get_dis_city($cid=''){
        $stateid = $this->input->post('sid');
        
        $data = $this->db->get_where('district_city',array('d_id'=>$cid))->result();
        
        echo "<option value=''>Select Sub City</option>";
      foreach($data as $cs){
          if($stateid == $cs->id){ $selct ='selected'; }else{ $selct ='';  }
          echo "<option ".$selct." value=".$cs->id.">".$cs->name."</option>";
      } 
        
    }
    
    
    function get_cnt_city($cid='',$sid=''){
        
          $cityid = $this->input->post('sid');
        
        $data = $this->db->get_where('city',array('country_id'=>$cid,'state_id'=>$sid))->result();
        
        echo "<option value=''>Select District </option>";
      foreach($data as $cs){
          if($cityid == $cs->id){ $selct ='selected'; }else{ $selct ='';  }
          echo "<option ".$selct." value=".$cs->id.">".$cs->city."</option>";
      } 
        
        
    }
    
    function new_city(){
        
       $this->data['country_id'] = $data['country_id'] = $this->input->post('cid');
        $this->data['state_id'] = $data['state_id'] = $this->input->post('stateid');
       $this->data['city'] = $data['city'] = $this->input->post('city');
        $this->data['countryname'] = $this->input->post('cname');
        $this->data['statename'] = $this->input->post('statename');
        $data['alias'] = $this->create_url($this->input->post('city'));

        
        $this->data['city_id'] = $this->location_model->create_city($data);
        $this->data['type'] = 'create_city';
        
        $output = $this->load->view('location/ajax_view',$this->data,true);
        
            $resp['status']=1;
            $resp['message']='City Created';  
            $resp['newstate']=$output; 
        echo json_encode($resp);
        
    }
    
    function new_dist_city(){
        
        $this->data['country_id'] = $data['c_id'] = $this->input->post('cid');
         $this->data['state_id'] = $data['s_id'] = $this->input->post('stateid');
         $this->data['d_id'] = $data['d_id'] = $this->input->post('districtId');
        $this->data['city'] = $data['name'] = $this->input->post('city');

         $this->data['countryname'] = $this->input->post('cname');
         $this->data['statename'] = $this->input->post('statename');
         $this->data['districtName'] = $this->input->post('districtName');
         
         $this->data['city_id'] = $this->location_model->create_district_city($data);
         $this->data['type'] = 'create_district_city';
         
         $output = $this->load->view('location/ajax_view',$this->data,true);
         
             $resp['status']=1;
             $resp['message']='City Created';  
             $resp['newstate']=$output; 
         echo json_encode($resp);
         
     }


    function update_city(){
        
       $this->data['country_id'] = $data['country_id'] = $this->input->post('cid');
        $this->data['state_id'] = $data['state_id'] = $this->input->post('stateid');
       $this->data['city'] = $data['city'] = $this->input->post('city');
        $this->data['countryname'] = $this->input->post('cname');
        $this->data['statename'] = $this->input->post('statename');
        $this->data['city_id'] =$con['id'] = $this->input->post('city_id');
        $data['alias'] = $this->create_url($this->input->post('city'));
        
        $update = $this->location_model->update_city($data,$con);
        $this->data['type'] = 'update_city';
        
        $output = $this->load->view('location/ajax_view',$this->data,true);
        
            $resp['status']=1;
            $resp['message']='City Updated';  
            $resp['newstate']=$output; 
        echo json_encode($resp);
        
    }


    
    function update_district_city(){
        
        $this->data['country_id'] = $data['c_id'] = $this->input->post('cid');
        $this->data['state_id'] = $data['s_id'] = $this->input->post('stateid');
        $this->data['d_id'] = $data['d_id'] = $this->input->post('districtId');
       $this->data['city'] = $data['name'] = $this->input->post('city');

        $this->data['countryname'] = $this->input->post('cname');
        $this->data['statename'] = $this->input->post('statename');
        $this->data['districtName'] = $this->input->post('districtName');

         $this->data['city_id'] =$con['id'] = $this->input->post('cityId');
         
         $update = $this->location_model->update_district_city($data,$con);
         $this->data['type'] = 'update_district_city';
         
         $output = $this->load->view('location/ajax_view',$this->data,true);
         
             $resp['status']=1;
             $resp['message']='City Updated';  
             $resp['newstate']=$output; 
         echo json_encode($resp);
         
     }
    
    function delete_city($id =''){
    if($this->db->delete('city',array('id'=>$id))){
            echo 1;
        }else{
            echo "Something error try again";
        }
    }
    
    
    
    
}



