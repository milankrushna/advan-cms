<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends CI_Controller {

/**
* Name:  Gallery
*
* Author:  Milan Krushna
* 		   milankrushna@gmail.com   
*	  	   @milu_babu
*
* Created on 02-11-2016
*/

    
    
function __construct()
  {
    parent::__construct();

    $this->load->library('ion_auth');
    $this->load->library('form_validation');
    $this->load->helper('url');
    $this->load->model('admin/gallery_model');
    $this->load->library('session');
    $this->load->helper(array('form', 'url'));
    date_default_timezone_set("Asia/Kolkata");

    //$this->load->library('pagination');

    if (!$this->ion_auth->logged_in())
    {
      redirect('admin/auth', 'refresh');
    }
                $this->data['heading'] = "";
                $this->data['subHeading'] = "";
   $this->header['menu']='gallery';
   $this->header['webdata'] = $this->db->get_where('my_website',array('id'=>'1'))->row();

  }

    
    
    function my_album(){
               
                $this->data['heading'] = "Image Gallery";
        
                $this->data['galleries'] = $this->gallery_model->get_all_album();
                $this->load->view('common_admin/header',$this->header);  
		        $this->load->view("admin/gallery/view_album",$this->data);
                $this->load->view('common_admin/footer');
        
    }
    
    
    
//Create A new Image Album With Cover Pic
      function create_album(){
      
      
                $this->data['heading'] = "Create Album";

		//validate form input
		$this->form_validation->set_rules('caption', "caption", 'required|is_unique[album.name]');

		if ($this->form_validation->run() == TRUE)
		{
             $caption  =  $this->input->post('caption');
            
            if($upload = $this->upload_image('file')){
			$data['cover_pic'] =  $upload;
            $data['name'] =  $caption;
            $data['alias'] =  $this->create_url($caption);
            }else{
               redirect("admin/gallery/create_album"); 
            }
			
       
                
			if($this->gallery_model->create_album($data))
			{
				$this->session->set_flashdata('message', 'Album Successfully Created');
				redirect("admin/gallery/create_album");
			}
		}
		else
		{
		
            $this->session->set_flashdata('message', validation_errors());
                $this->load->view('common_admin/header',$this->header);  
		        $this->load->view("admin/gallery/create_album",$this->data);
                $this->load->view('common_admin/footer');



		}  
          
          
        }
     
    
    
//Create A new Image Album With Cover Pic
      function edit_album($id = ""){
      
      if($id ==""){
          show_404();
      }else{
          $cond['id'] = $id;
          $this->data['album'] = $this->gallery_model->get_album($cond);
      }
          
          
        $this->data['heading'] = "Edit Album";
		//validate form input
		$this->form_validation->set_rules('caption', "caption", 'required');

		if ($this->form_validation->run() == TRUE)
		{
            if(!empty($_FILES['file']['name'])){
           
            if($upload = $this->upload_image('file')){
			$data['cover_pic'] =  $upload;
            
            }else{
               redirect(current_url()); 
            }
            }
            
            $caption = $this->input->post('caption');
			 $data['name'] =  $caption;
       
            ///$data['alias'] =  $this->create_url($caption);
            
			if($this->gallery_model->update_album($data,$cond))
			{
				$this->session->set_flashdata('message', 'Album Successfully Updated');
				redirect(current_url());
			}
		}
		else
		{
		
            $this->session->set_flashdata('message', validation_errors());
                $this->load->view('common_admin/header',$this->header);  
		        $this->load->view("admin/gallery/create_album",$this->data);
                $this->load->view('common_admin/footer');



		}  
          
          
        }
    
        
        function delete_album($id){
            if($id == ""){
                show_404();
            }
            if($this->db->delete('album',array('id'=>$id))){
                redirect('admin/gallery/my_album');
            }
        }
    
    
  
    
    
    
 ////New Image upload Form+Function
        function photo_gallery($id = "")
            {

            if($id == ""){
                show_404();
            }
            
          $condAlbum['id'] = $id;
          $condPhoto['album_id'] = $id;
          
          $this->data['album'] = $this->gallery_model->get_album($condAlbum);
          $this->data['data'] = $this->gallery_model->get_gallery($condPhoto);


          $this->data['heading'] = "Image Gallery";
          $this->data['subHeading'] = $this->data['album']->name;
          $this->data['id'] = $id;
          $this->data['option'] = null;


                $this->load->view('common_admin/header',$this->header);
                $this->load->view('admin/gallery/photo_gallery',$this->data);
                $this->load->view('common_admin/footer');

            }



    ////update Gallery CAption

    function update_caption($id=""){

        if($id==""){
            show_404();
        }

        $data['caption'] = addslashes($this->input->post('caption'));

        $this->db->update('gallery_images',$data,array('id'=>$id));

        redirect('admin/gallery/photo_gallery/'.$this->input->post('evid'));
    }



    function createGallery($id = ""){

        if($id == ""){
                show_404();
            }



          $cond['album_id'] = $id;
          $cond2['id'] = $id;
          $this->data['album'] =  $this->gallery_model->get_album($cond2);
          $this->data['data'] = $this->gallery_model->get_gallery($cond);


          $this->data['heading'] = "Image Gallery";
          $this->data['subHeading'] = $this->data['album']->name;
          $this->data['id'] = $id;

        

                $this->load->view('common_admin/header',$this->header);
                $this->load->view('admin/gallery/newGallery',$this->data);
                $this->load->view('common_admin/footer');

    }

    ////upload a new image after or before a exiting image

    function newgallery($id="",$evid = ""){

        $type = $_GET['type'];
        if($type == 'next' || $type == 'prev'){


          $cond['event_id'] = $evid;
          $this->data['event'] =  $this->gallery_model->get_event($evid);
          $this->data['data'] = $this->gallery_model->get_gallery($cond);
          $this->data['id'] = $evid;
          $this->data['imageData'] = $this->gallery_model->imageRow($id);

        $this->data['heading'] = "Image Gallery";
        $this->data['subHeading'] = $this->data['event']->event_name;


                $this->load->view('common_admin/header',$this->header);
                $this->load->view('admin/gallery/rangeGallery',$this->data);
                $this->load->view('common_admin/footer');


        }else{
            show_404();
        }

    }

////upload Multiple image
    function galleryUp($id = ""){

            if($id == ""){
                show_404();
            }

        
        $galleryId = $this->input->post('galleryId');
        $caption = $this->input->post('caption');
        $date = $this->input->post('date');

        //checking upload path is available or not, If not it will  create
       $this->check_path();


        $this->load->library('upload');

     if(!empty($_FILES['userfile'])){

         $files = $_FILES;

    $cpt = count($_FILES['userfile']['name']);

    for($i=0; $i<$cpt; $i++){



        ///checking File is Selected or not
          if(!empty($files['userfile']['name'][$i])){

        $_FILES['userfile']['name']= $files['userfile']['name'][$i];
        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
        $_FILES['userfile']['size']= $files['userfile']['size'][$i];



//             echo $i.'<br>';
//             echo $galleryId[$i].'<br>';
//             echo $galleryId[$i].'<br>';

       ////Setup Configuration Using A callBack function
        $this->upload->initialize($this->set_upload_options());
        if($this->upload->do_upload()){

            $data =  $this->upload->data();
            $this->resize_image($data['file_name']);


            $gallery2['imgname'] = $data['file_name'];
            $gallery2['album_id'] = $id;
            $gallery2['caption'] = addslashes($caption[$i]);
//            if(!empty($date)){
//            $gallery2['event_date'] = $date;
//            }else{
//            $gallery2['event_date'] = date('Y-m-d G:i:s');
//            }
//                echo 'new';
//        print_r($gallery2);
            $this->gallery_model->create_gallery($gallery2);




        }else{
          echo  $this->upload->display_errors();
        }

        }

        }


       }
        $capt = count($galleryId);

        for($i2=0; $i2<$capt; $i2++){

            $gallery5['caption'] = $caption[$i2];
            $this->gallery_model->update_gallery_cap($gallery5,$galleryId[$i2]);

        }

        redirect('admin/gallery/photo_gallery/'.$id);

    }


      ///upload image and Resize
    
    protected function upload_image($file_name){
        $this->check_path();
        $this->load->library('upload');
        
        $this->upload->initialize($this->set_upload_options());
        
        if($this->upload->do_upload($file_name)){
            
            $data =  $this->upload->data();
            
            //send image For resizeing
            $this->resize_image($data['file_name']);
            
            return $data['file_name'];
        }else{
            
            $this->session->set_flashdata('message', $this->upload->display_errors());
            return FALSE;
        }
    } 
    
    
    
    ///Setup required Configuration
    protected function set_upload_options(){


        $new_img =  date('Ymd');
        $config['upload_path'] = './images/gallery/original/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png|JPEG|JPG';
        $config['max_size']	= '10240';
        $config['max_width'] = '102400';
        $config['max_height'] = '72000';
        $config['file_name'] = $new_img;
        return $config;
    }



    ////resize Image using CI Library.
   protected function resize_image($new_img){


            $this->load->library('image_lib');
            $src_path = './images/gallery/original/'. $new_img;
            $des_path =  './images/gallery/thumb/'. $new_img;

            $config['image_library']    = 'gd2';
            $config['source_image']     = $src_path;
            $config['new_image']        = $des_path;
            $config['maintain_ratio']   = TRUE;
            $config['width']            = 150;
            $config['height']           = 150;

            $this->image_lib->initialize($config);
            $this->image_lib->resize();

            $src_path = './images/gallery/original/'. $new_img;
            $des_path =  './images/gallery/medium/'. $new_img;

            $config['image_library']    = 'gd2';
            $config['source_image']     = $src_path;
            $config['new_image']        = $des_path;
            $config['maintain_ratio']   = TRUE;
            $config['width']            = 376;
            $config['height']           = 285;

            $this->image_lib->initialize($config);
            $this->image_lib->resize();


    }

    ////checking path is exit or not and Create if not exit
    function check_path(){

        $this->load->helper('directory');
        if(file_exists('./images/gallery/original/')){

            return TRUE;

        }else{

             mkdir('./images/gallery/original/', 0755, TRUE);
             mkdir('./images/gallery/thumb/', 0755, TRUE);
             mkdir('./images/gallery/medium/', 0755, TRUE);

             fopen('./images/gallery/index.html', "w");
             fopen('./images/gallery/original/index.html', "w");
             fopen('./images/gallery/thumb/index.html', "w");
             fopen('./images/gallery/medium/index.html', "w");
            return TRUE;
        }

    }


    ///insert Video Url for video gallery
    function new_video_gallery($id=""){

        

          //$cond['event_id'] = $id;
          //$this->data['event_id'] = $id;
          //$this->data['event'] = $this->gallery_model->get_event($id);
          $this->data['data'] = $this->gallery_model->get_video_gallery();

          $this->data['heading'] = "Video Gallery";




        $this->form_validation->set_rules('videolink[]', 'Video Link', 'required|xss_clean|prep_url');

        if ($this->form_validation->run() == FALSE)
		{

                $this->load->view('common_admin/header',$this->header);
                $this->load->view('admin/gallery/create_video_gallery',$this->data);
                $this->load->view('common_admin/footer');

        }else{

            $galleryId = $this->input->post('galleryId');
            $video = $this->input->post('videolink');
            $caption = $this->input->post('caption');

          for($i = 0; $i<count($video); $i++){

        
                  $galleryVideo[$i]['video_url'] = $video[$i];
                  //$galleryVideo[$i]['event_id'] = $id;
                  $galleryVideo[$i]['v_caption'] = $caption[$i];

          }

          
                $this->gallery_model->insert_video($galleryVideo);
                
                redirect('admin/gallery/video_gallery');
        }

    }

    ///all videos
    function editVideo($id=""){
        

        $cond['id'] = $id;
          
          $this->data['video'] = $this->gallery_model->video_gallery($cond);

          $this->data['heading'] = "Video Gallery";
          $this->data['subHeading'] = "Edit Video";
$this->form_validation->set_rules('videolink', 'Video Link', 'required|xss_clean');
$this->form_validation->set_rules('caption', 'Video Title', 'required|xss_clean');
        
        if ($this->form_validation->run() == FALSE)
		{
        
                $this->load->view('common_admin/header',$this->header);
                $this->load->view('admin/gallery/editvideo',$this->data);
                $this->load->view('common_admin/footer');
        
        }else{
           $data['video_url'] = $this->input->post('videolink');
           $data['v_caption'] = $this->input->post('caption');
          
            $this->gallery_model->update_video($data,$id);
            $this->session->set_flashdata('message','Video Gallery Successfully Updated');
            redirect('admin/gallery/editVideo/'.$id);
        }
        
    }
    ///edit video Gallery
    function video_gallery($id=""){
        
        
        
          $cond['event_id'] = $id;
          $this->data['event_id'] = $id;
          //$this->data['event'] = $this->gallery_model->get_event($id);
          $this->data['data'] = $this->gallery_model->get_video_gallery($cond);

          $this->data['heading'] = "Video Gallery";

                $this->load->view('common_admin/header',$this->header);
                $this->load->view('admin/gallery/videolisting',$this->data);
                $this->load->view('common_admin/footer');
        
        
        
    }
    

    function delete_gallery(){
        $imageid = $this->input->post('imageId');
        $path = $this->input->post('path');

        if($imageid != ""){
            if($this->db->delete('gallery_images',array('id'=>$imageid))){
        unlink($path);
        unlink(str_replace("thumb","original",$path));
                echo 1;
            }
        }

    } function delete_audio_gallery(){
        $imageid = $this->input->post('id');
        ///$path = $this->input->post('link');

        if($imageid != ""){
            if($this->db->delete('gallery',array('id'=>$imageid))){

                echo 1;
            }
        }

    }
    function delete_video_gallery(){
        $videoId = $this->input->post('videoId');

        if($videoId != ""){
            if($this->db->delete('video_gallery',array('id'=>$videoId))){

                echo 1;
            }else{
                echo "Something Error in Server, please try again";
            }
        }

    } function delete_book_gallery(){
        $videoId = $this->input->post('id');

        if($videoId != ""){
            if($this->db->delete('gallery',array('id'=>$videoId))){

                echo 1;
            }else{
                echo "Something Error in Server, please try again";
            }
        }

    }

    
    
    
    
    function audio_gallery(){
        
        
            $this->data['data'] = $this->gallery_model->getXtraGallery('audio');
        
                $this->load->view('common_admin/header',$this->header);
                $this->load->view('admin/gallery/audioListing',$this->data);
                $this->load->view('common_admin/footer');
        
        
    }
    
    
    function create_audio(){
        
    
        $this->data['heading'] = "New Audio";
        
$this->form_validation->set_rules('caption', 'Caption', 'required');
$this->form_validation->set_rules('type', 'Type', 'required');
        
        if ($this->form_validation->run() == FALSE)
		{
                $this->load->view('common_admin/header',$this->header);
                $this->load->view('admin/gallery/newAudio',$this->data);
                $this->load->view('common_admin/footer');
        
        }else{
            
            
            $data['caption'] = $this->input->post('caption');
            $data['type'] = 'audio';
            if(!empty($_FILES['audio']['name'])){
                $data['source'] = "";
                $data['file'] = $this->uploadAudio();
            }else if($this->input->post('source') !=""){
                $data['source'] = $this->input->post('source');
                $data['file'] = "";
            }

            if($this->gallery_model->create_audio($data)){
                $this->session->set_flashdata('message','your Audio File uploaded Successfully');
                
                redirect(current_url());
            }

        }
        
    }
    
    
    
    //edit audio gallery
        function edit_audio($id = ""){
        
            if($id == ""){
                show_404();
            }
            
    $this->data['audio'] = $this->gallery_model->audio_row($id);  
            if($this->data['audio']->type != 'audio'){
                show_error('something Error');
            }
           
            
            $this->data['heading'] = "Edit Audio";
            $this->data['subheading'] = $this->data['audio']->caption;
        
$this->form_validation->set_rules('caption', 'Caption', 'required');
        
          
            
            
        if ($this->form_validation->run() == FALSE)
		{
                $this->load->view('common_admin/header',$this->header);
                $this->load->view('admin/gallery/newAudio',$this->data);
                $this->load->view('common_admin/footer');
        
        }else{
            
            
            $data['caption'] = $this->input->post('caption');
            $data['type'] = 'audio';
            if(!empty($_FILES['audio']['name'])){
                $data['source'] = "";
                $data['file'] = $this->uploadAudio();
            }else if($this->input->post('source') !=""){
                $data['source'] = $this->input->post('source');
                $data['file'] = "";
            }
 
            if($this->gallery_model->update_audio($data,$id)){
                $this->session->set_flashdata('message','your Audio File uploaded Successfully');
                
                redirect(current_url());
            }

        }
        
    }
    
    
    
    ////Audio Uploading 
    protected function uploadAudio(){
        $new_file =  now()."_".$_FILES['audio']['name'];

    $tmpName  = $_FILES['audio']['tmp_name'];  
       
       if(substr($_FILES["audio"]["type"],0,5) == 'audio'){
      
    
    //$this->dashboard_model->update_image($agent_id,$new_img);
    $data =array( 'upload_data' => move_uploaded_file($tmpName,"./upload/gallery/music/$new_file"));

	return $new_file;
       }else{
           return false;
       }
    }
    
    
    ///book listing
     function book_gallery(){
        
        
            $this->data['data'] = $this->gallery_model->getXtraGallery('book');
        
                $this->load->view('common_admin/header',$this->header);
                $this->load->view('admin/gallery/bookListing',$this->data);
                $this->load->view('common_admin/footer');
        
        
    }
    
    
    
    
    /////create book gallery
      function create_book(){
        
    
        $this->data['heading'] = "New Book";
        
$this->form_validation->set_rules('caption', 'Caption', 'required');
$this->form_validation->set_rules('type', 'Type', 'required');
        
        if ($this->form_validation->run() == FALSE)
		{
                $this->load->view('common_admin/header',$this->header);
                $this->load->view('admin/gallery/newbook',$this->data);
                $this->load->view('common_admin/footer');
        
        }else{
            
            
            $data['caption'] = $this->input->post('caption');
            $data['type'] = 'book';
            $data['cover_pic'] = $this->uploadCoverpic('cover_pic');
            if(!empty($_FILES['pdf']['name'])){
                $data['source'] = "";
                $data['file'] = $this->uploadPdf();
            }else if($this->input->post('source') !=""){
                $data['source'] = $this->input->post('source');
                $data['file'] = "";
            }

            if($this->gallery_model->create_audio($data)){
                $this->session->set_flashdata('message','your Book uploaded Successfully');
                
                redirect(current_url());
            }

        }
        
    }
    
    
    
      
    //edit audio gallery
        function edit_book($id = ""){
        
            if($id == ""){
                show_404();
            }
            
    $this->data['audio'] = $this->gallery_model->audio_row($id);  
            
          
            
            $this->data['heading'] = "Edit Audio";
            $this->data['subheading'] = $this->data['audio']->caption;
        
            if($this->data['audio']->type != 'book'){
                show_error('something Error');
            }
$this->form_validation->set_rules('caption', 'Caption', 'required');
        
          
            
            
        if ($this->form_validation->run() == FALSE)
		{
                $this->load->view('common_admin/header',$this->header);
                $this->load->view('admin/gallery/edit_book',$this->data);
                $this->load->view('common_admin/footer');
        
        }else{
            
            
            $data['caption'] = $this->input->post('caption');
            
            if(!empty($_FILES['cover_pic']['name'])){
            $data['cover_pic'] = $this->uploadCoverpic('cover_pic');
            }
            
            if(!empty($_FILES['pdf']['name'])){
                $data['source'] = "";
                $data['file'] = $this->uploadPdf();
            }else if($this->input->post('source') !=""){
                $data['source'] = $this->input->post('source');
                $data['file'] = "";
            }
 
            if($this->gallery_model->update_audio($data,$id)){
                $this->session->set_flashdata('message','your Book uploaded Successfully');
                
                redirect(current_url());
            }

        }
        
    }
    
    
    
    
    ///Upload Pdf To Server
    function uploadPdf(){
        $new_file =  now()."_".$_FILES['pdf']['name'];
    $tmpName  = $_FILES['pdf']['tmp_name'];  
      
    $my_file = $_FILES["pdf"]["name"];
    $imageFileType = pathinfo($my_file,PATHINFO_EXTENSION);
      
       if($imageFileType == 'pdf'){
    //$this->dashboard_model->update_image($agent_id,$new_img);
    $data =array( 'upload_data' => move_uploaded_file($tmpName,"./upload/gallery/book/$new_file"));

	return $new_file;
       }else{
           return false;
       }
    }
    
    ///Upload Cover Pic of PDF
    function uploadCoverpic($field=""){
        
        
         $this->load->library('upload');
        
        $new_img =  date('Ymd');
        $config['upload_path'] = './upload/gallery/book/cover_pic/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png|JPEG|JPG';
        $config['max_size']	= '10240';
        $config['max_width'] = '102400';
        $config['max_height'] = '72000';
        $config['file_name'] = $new_img;
        
        $this->upload->initialize($config);
        if($this->upload->do_upload($field)){
            
            $data =  $this->upload->data();
            
            //send image For resizeing
            $this->resize_cover_pic($data['file_name']);
            
            return $data['file_name'];
        }
        
        
    }
    
    
    ///Resizeing Cover Pic 
    function resize_cover_pic($new_img){
        
        
         $this->load->library('image_lib');
            $src_path = './upload/gallery/book/cover_pic/'. $new_img;
            $des_path =  './upload/gallery/book/cover_pic/'. $new_img;

            $config['image_library']    = 'gd2';
            $config['source_image']     = $src_path;
            $config['new_image']        = $des_path;
            $config['maintain_ratio']   = TRUE;
            $config['width']            = 200;
            $config['height']           = 200;

            $this->image_lib->initialize($config);
            $this->image_lib->resize();
        
    }
    
    

    protected function create_url($strin){
        
        return rtrim(strtolower(str_replace(' ','-',preg_replace('/\s\s+/','-',preg_replace('/[^A-Za-z0-9\-]/', ' ',$strin)))),'/[^A-Za-z0-9\-]/');
    }

function print_data($data){
        print("<pre>");
            print_r($data);exit;
    }
    

    
//    function getfiles(){
//        $this->load->helper('directory');
//        $dir = "./images/gallery/original/";
//        $handle = directory_map($dir);
//        ///$handle = fopen($dir,"w");
//        echo count($handle);
//        //print("<pre>");
//        //print_r($handle);
//        
//        
//        foreach($handle as $hd => $val){
//            
//           $id =  substr(substr($val,0,-4),2,50);
//            
//            
//            $this->db->update('gallery_images',array('caption'=>'f_'.$id.'.jpg'),array('id'=>$id));
//        }
//        
//        
//        
//    }



}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
