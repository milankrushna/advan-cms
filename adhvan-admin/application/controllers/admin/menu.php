<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Menu extends CI_Controller {



	function __construct()

	{

		parent::__construct();

        
         $this->load->driver('cache');
         $this->cache->clean();
		$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->model('menu_model');
		$this->load->database();
		$this->lang->load('auth');
		$this->load->helper('language');	
		$this->header['menu']='menu';
        $this->header['webdata'] = $this->db->get_where('my_website',array('id'=>'1'))->row();
 if (!$this->ion_auth->logged_in() && !$this->ion_auth->is_admin())
    {
      redirect('admin/auth/login', 'refresh');
    }

	}




/**
* @name          index
* @author        Milan Krushna
* @revised       null
* @Description   display the user list
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
	


	//redirect if needed, otherwise display the user list

	function index()

	{

		$menulist = $this->menu_model->get_menulist();

		$all_menu = array();	

		foreach($menulist as $ml){

			if($ml['parent_menu'] == '0' ){

				$ml['parent_menu'] = "N/A";

			}else{

				$ml['parent_menu'] = $this->menu_model->get_parent($ml['parent_menu']);

			}

		$all_menu[] = $ml;

		}


		$this->data['menulist'] =  $all_menu;
		$this->data['msg'] = 'Listing of Menu!';
		$this->load->view("common_admin/header",$this->header);
		$this->load->view("menu/view_menu",$this->data);
		$this->load->view("common_admin/footer");	

	}


/**
* @name          inner listing
* @author        Milan Krushna
* @revised       null
* @Description   display inner menu listing
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
	



function inner_listing()

	{

		$innerlist = $this->menu_model->get_innerlist();

		$all_menu = array();	

		foreach($innerlist as $il){

			if($il['parent_menu'] == '0' ){

				$il['parent_menu'] = "-------";

			}else{

				$il['parent_menu'] = $this->menu_model->get_parent($il['parent_menu']);

			}

		$all_menu[] = $il;

		}

	
    
    

		$this->data['innerlist'] =  $all_menu;
		$this->data['msg'] = 'Listing of Menu!';
		$this->load->view("common_admin/header",$this->header);
		$this->load->view("menu/inner_view",$this->data);
		$this->load->view("common_admin/footer");	

	}


/**
* @name          menu active deactive
* @author        Subhashree Jena
* @revised       null
* @Description   check status active or deactive
* @Param String  $id is id of menu table
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
	



   //status check 
    function menu_deactive($id=""){
        
        $sts = $this->input->post('sts');
        
        if($sts == 0){
            $upsts = 1; 
        }else{
            $upsts = 0;
        }
      
        if($this->db->update('menu',array('status'=>$upsts),array('id'=>$id))){
          echo $upsts;  
        }
        
        
	    }
    
/**
* @name          inner menu active deactive
* @author        Subhashree Jena
* @revised       null
* @Description   check status inner menu active or deactive
* @Param String  $id is id of menu table
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
	
    
 function inner_deactive($id=""){
        
        $sts = $this->input->post('sts');
        
        if($sts == 0){
            $upsts = 1; 
        }else{
            $upsts = 0;
        }
      
        if($this->db->update('menu',array('status2'=>$upsts),array('id'=>$id))){
          echo $upsts;  
        }
        
        
    }
    


   
/**
* @name          create menu
* @author        Milan Krushna
* @revised       null
* @Description   create new menu
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
	



	function create_menu()

	{

	//	echo "i am here";exit;

		$this->data['title'] = "Create Menu";

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())

		{

			redirect('admin/auth', 'refresh');

		}

		

		$this->data['menu'] = $this->menu_model->all_menu();

		$this->data['parents']  = $this->menu_model->all_parents()->result_array();

		$this->data['pages']  = $this->menu_model->all_pages()->result_array();

		//validate form input
//is_unique[menu.menu]
		$this->form_validation->set_rules('menu', "Menu Name", 'required|is_unique[menu.menu]');

		//$this->form_validation->set_rules('parent_menu', "Parent Menu", 'xss_clean');

		$this->form_validation->set_rules('link', "Link Url", 'required|xss_clean');

		

		if ($this->form_validation->run() == TRUE)

		{
            
            
            $nmmnu = $this->input->post('nm_menu');
            
            if($nmmnu != ''){
                    
                  $data['meta_title'] = $this->input->post('meta_title');  
                  $data['meta_desc'] = $this->input->post('meta_desc');  
                  $data['meta_keyword'] = $this->input->post('meta_keyword');  
                }
            
			$page_id = $this->input->post('page_id');
        
            $custom = $this->input->post('custom');
           if($custom == 0){
               
               if(!empty($page_id) && $nmmnu==''){
            $url = $this->menu_model->get_page_url($page_id);
            }else{
                 
            $url = $this->create_url($this->input->post('menu'));    
            }
                $data['custom'] = 0;
            }else if($custom == 1){
                
                $url = $this->input->post('link');
                $data['custom'] = 1;
            }
            
            
            
            $data['menu'] = $this->input->post('menu');
          ///  $data['catgy_menu'] = $this->input->post('ctgy_menu');
            $data['ct_menu'] = $nmmnu;
            $data['target'] = $this->input->post('target');
            $data['parent_menu'] = $this->input->post('parent_menu');
            $data['link'] = $url;
            $data['page_id'] = $page_id;
            $data['status'] = 1;
            $data['status2'] = 1;
            
            
            //$file_name =  $_FILES['menu_image']['name'];
            
           /* if(!empty($_FILES['menu_image']['name'])){
            $menu_img = $this->image_upload('menu_image');
                
                  if($menu_img!=false){
                    $data['image'] = $menu_img;
                  }else{
                      echo 2;exit;
                      //redirect('admin/menu/create_menu');
                  }
                
            }else{
                
             $data['image'] = "";   
            } */
         
           
            
			$new_menu_id =  $this->menu_model->create_menu($data);

			//echo $new_category_id;exit;

			if($new_menu_id)

			{ 
                 //echo 1;exit;

				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("admin/menu/edit_menu/".$new_menu_id);

			}

		}

		else

		{

			//display the create group form

			//set the flash data error message if there is one

			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));



		$this->load->view("common_admin/header",$this->header);
		$this->load->view("menu/create_menu",$this->data);
		$this->load->view("common_admin/footer");	



		}

	}

    
     protected function create_url($strin){
        
        return rtrim(strtolower(str_replace(' ','-',preg_replace('/\s\s+/','-',preg_replace('/[^A-Za-z0-9\-]/', ' ',$strin)))),'/[^A-Za-z0-9\-]/');
    }
    
   
/**
* @name          image upload and resize
* @author        Subhashree Jena
* @revised       null
* @Description   image and resizing of image
* @Param String  $field is field name of file
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
	
       
   function image_upload($field){
       
       
    
    $new_img =  now().'.'.explode('/',$_FILES['menu_image']['type'])[1];
   
    
        $config['upload_path'] = './upload/menu/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif|JPG|JPEG|PNG';
        $config['max_size']	= '2000000000';
        $config['max_width'] = '102400000';
        $config['max_height'] = '700000000';
        $config['file_name'] = $new_img;

$this->load->library('upload', $config);
$this->upload->initialize($config);
 if($this->upload->do_upload($field)){
     
  $this->resize_image($new_img);
   $data = array('upload_data' => $this->upload->data());
     
    return  $data['upload_data']['file_name'];
  
 }else{ 
  
  $this->session->set_flashdata('message',$this->upload->display_errors());
     return false; 
     
 }

       
       
       
   }
    
    

    
    function resize_image($new_img){
        
        
             $this->load->library('image_lib');
       
          
            $src_path = './upload/menu/'.$new_img;
            $des_path =  './upload/menu/min_menu/'.$new_img;

            $config['image_library']    = 'gd2';
            $config['source_image']     = $src_path;
            $config['new_image']        = $des_path;
            $config['maintain_ratio']   = TRUE;
            $config['width']            = 200;
            $config['height']            = 200;
         

            $this->image_lib->initialize($config);
            $this->image_lib->resize();
      	
        
    }
    
   
/**
* @name          edit_menu
* @author        Milan Krushna
* @revised       null
* @Description   edit , update menu in menu listing
* @Param String  $id is id of menu table
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
	

	function edit_menu($id)

	{

		if(!$id || empty($id))

		{

			redirect('admin/auth', 'refresh');

		}



		$this->data['title'] = 'Edit Menu';



		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())

		{

			redirect('admin/auth', 'refresh');

		}



			$this->data['menu']  = $this->menu_model->edit_menu($id);

			$this->data['parents']  = $this->menu_model->all_parents()->result_array();

			$this->data['pages']  = $this->menu_model->all_pages()->result_array();	

		//validate form input

		$this->form_validation->set_rules('menu', "Menu Name", 'required|xss_clean');

		$this->form_validation->set_rules('parent_menu', "Parent Menu", 'xss_clean');

		$this->form_validation->set_rules('link', "Link Url", 'required|xss_clean');



		if (isset($_POST) && !empty($_POST))

		{

			if ($this->form_validation->run() === TRUE)

			{
            $page_id = $this->input->post('page_id');
           
		 $nmmnu = $this->input->post('nm_menu');
                
                if($nmmnu != ''){
                    
                  $data['meta_title'] = $this->input->post('meta_title');  
                  $data['meta_desc'] = $this->input->post('meta_desc');  
                  $data['meta_keyword'] = $this->input->post('meta_keyword');  
                }
                
                $custom = $this->input->post('custom');
            if($custom == 0){
               if(!empty($page_id) && $nmmnu==''){
            $url = $this->menu_model->get_page_url($page_id);
            }else{
            $url = $this->create_url($this->input->post('menu'));    
            }
                $data['custom'] = 0;
            }else if($custom == 1){
                
                $url = $this->input->post('link');
                $data['custom'] = 1;
            }
            
            
            $data['menu'] = $this->input->post('menu');
            $data['ct_menu'] = $nmmnu;
            $data['target'] = $this->input->post('target');
            $data['parent_menu'] = $this->input->post('parent_menu');
            $data['link'] = $url;
            $data['page_id'] = $page_id;
                
            //$file_name =  $_FILES['menu_image']['name'];    
               
                
                
                $menu_update = $this->menu_model->update_menu($id, $data);

            //print("<pre>");
          ///  print_r($menu_update);exit;

				if($menu_update)

				{

                  //  echo 1;exit;
					//$this->session->set_flashdata('message', "Menu Updated!");

				}

				else

				{

					$this->session->set_flashdata('message', $this->ion_auth->errors());

				}

				redirect("admin/menu/edit_menu/".$id);

			}

		}



		//set the flash data error message if there is one

		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

	//pass the user to the view


			$this->load->view("common_admin/header",$this->header);
			$this->load->view("menu/edit_menu",$this->data);
			$this->load->view("common_admin/footer");	

	}

    
    
    function get_ctgy_artl(){
        
        $mnu_id = $this->input->post('id');
        
        $this->db->select('id,name');
        $this->db->where('cat_id',$mnu_id);
        $this->db->or_where('sub_catid',$mnu_id);
        $this->db->where('sub_catid',$mnu_id);
        $this->db->where('status',1);
        $this->db->order_by('id','DESC');
        $subcat = $this->db->get('pages')->result();
       
        //echo json_encode($subcat);
        
        echo "<option value=''>Articles</option>";
      foreach($subcat as $cs){
          echo "<option value=".$cs->id.">".$cs->name."</option>";
      }  
        
            
        
    }
    
    
    
   
/**
* @name          delete menu
* @author        Milan Krushna
* @revised       null
* @Description   delete menu in menu listing 
* @Param String  $id is id of menu table 
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
	
		

    
	function delete_menu($id){

			$this->data['menu']  = $this->menu_model->delete_menu($id);
redirect("admin/menu/index");
	}

	   
/**
* @name          tree view
* @author        Milan Krushna
* @revised       null
* @Description   tree view of menu
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
	

	function tree_view(){

		

			$this->data['msg'] = "Tree View of Menu";

			$this->load->view("common_admin/header",$this->header);
		

			$this->load->view("menu/tree_view");

			$this->load->view("common_admin/footer");	

	}

	   
/**
* @name          get tree
* @author        Milan Krushna
* @revised       null
* @Description   retrive inner list menu
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
	
	function get_tree(){

		$menulist = $this->menu_model->get_menulist();
        
/*		$all_menu = array();	

		foreach($menulist as $ml){

			if($ml['parent_menu'] == '0' ){

				$ml['parent_menu'] = "-------";

			}else{

				$ml['parent_menu'] = $this->menu_model->get_parent($ml['parent_menu']);

			}

		$all_menu[] = $ml;

		}

*/		echo json_encode($menulist);
        
		exit;

	}
	   
/**
* @name          get tree inner
* @author        Milan Krushna
* @revised       null
* @Description   retrive inner list menu
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
	
	
	function get_treeinner(){

		$menulist = $this->menu_model->get_innerlist();
        
/*		$all_menu = array();	

		foreach($menulist as $ml){

			if($ml['parent_menu'] == '0' ){

				$ml['parent_menu'] = "-------";

			}else{

				$ml['parent_menu'] = $this->menu_model->get_parent($ml['parent_menu']);

			}

		$all_menu[] = $ml;

		}

*/		echo json_encode($menulist);
        
		exit;

	}


	
	   
/**
* @name          make link
* @author        Milan Krushna
* @revised       null
* @Description   retrive link
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
	

	function make_link(){

	

		$id = $_REQUEST['page_id'];

		$name  = $this->menu_model->get_link($id);

		echo str_replace("admin/","",base_url($name));exit;

	}

	
	   
/**
* @name          order menu
* @author        Milan Krushna
* @revised       null
* @Description   ordering/sorting of menu
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/

	function order_menu(){

		$menulist = $this->menu_model->view_menuorder();

			$this->data['menulist'] = $menulist;

			$this->load->view("common_admin/header",$this->header);
			$this->load->view("menu/ordersort",$this->data);
			$this->load->view("common_admin/footer");			

	}
	
	
		   
/**
* @name          inner order menu
* @author        Subhashree Jena
* @revised       null
* @Description   ordering/sorting of inner menu 
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
    
    function inner_order_menu(){

		$menulist = $this->menu_model->view_inner_menuorder()->result_array();

			$this->data['menulist'] = $menulist;

			$this->load->view("common_admin/header",$this->header);
			$this->load->view("menu/inner_order",$this->data);
			$this->load->view("common_admin/footer");			

	}

		   
/**
* @name          save order
* @author        Milan Krushna
* @revised       null
* @Description   update menu order
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/

	function save_order(){

	$menu = $_POST['menu'];

	for ($i = 0; $i < count($menu); $i++) {

		$sql = "UPDATE `menu` SET `sort`=" . $i . " WHERE `id`='" . $menu[$i] . "'";

		$this->db->query($sql);

	}

	

 }

	   
/**
* @name          order sub menu
* @author        Milan Krushna
* @revised       null
* @Description   order sub menu of menu 
* @Param String  $id is id of menu table
* @copyright     Copyright (C) 2017 powered by Wayindia
*/

	function order_submenu($id){

			$menulist = $this->menu_model->view_suborder($id)->result_array();

			//$menulist = $this->menu_model->view_menuorder()->result_array();

			$menu  = $this->menu_model->edit_menu($id);

			

			$this->data['menu'] = $menu;

			$this->data['menulist'] = $menulist;

			$this->data['parent_id'] = $id;

			$this->load->view("common_admin/header",$this->header);
			$this->load->view("menu/suborder",$this->data);
			$this->load->view("common_admin/footer");

	}	

	   
/**
* @name          order inner menu
* @author        Milan Krushna
* @revised       null
* @Description   ordering/ sorting inner menu
* @Param String  $id is id of menu table
* @copyright     Copyright (C) 2017 powered by Wayindia
*/

		function order_innermenu($id){

			$menulist = $this->menu_model->view_subinnermenu($id)->result_array();

			//$menulist = $this->menu_model->view_menuorder()->result_array();

			$menu  = $this->menu_model->edit_menu($id);

			

			$this->data['menu'] = $menu;

			$this->data['menulist'] = $menulist;

			$this->data['parent_id'] = $id;

			$this->load->view("common_admin/header",$this->header);
			$this->load->view("menu/inner_suborder",$this->data);
			$this->load->view("common_admin/footer");

	}	
      	   
/**
* @name          save inner order
* @author        Milan Krushna
* @revised       null
* @Description   update inner menu order
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
 
       function save_innerorder(){

	$menu = $_POST['menu'];

	for ($i = 0; $i < count($menu); $i++) {

		$sql = "UPDATE `menu` SET `sort_2`=" . $i . " WHERE `id`='" . $menu[$i] . "'";

		$this->db->query($sql);

	}
}
    	   
/**
* @name          update footer menu
* @author        Subhahsree Jena
* @revised       null
* @Description   update footer menu 
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
    
        function update_ftr_menu(){
			
		$data['ft_menu'] = $this->input->post('status');	
		//$data['big_article'] = $this->input->post('big_art');	
		//$data['medium_article'] = $this->input->post('med_art');	
		$id['id'] = $this->input->post('id');	
		
		
		 
         $check_update = $this->menu_model->update_checkbox($data,$id);	
			
			
			echo $check_update;
			
		}

    
     function update_big_artl(){
			
			
		$data['big_article'] = $this->input->post('big_art');	
		$data['article_id'] = $this->input->post('arti_id');	
		
		$id['id'] = $this->input->post('id');	
		
		
		 
         $check_update = $this->menu_model->update_checkbox($data,$id);	
			
			
			echo $check_update;
			
		}
    
    
    function update_med_artl(){
			
			
		$data['medium_article'] = $this->input->post('med_art');	
		$data['article_id'] = $this->input->post('arti_id');	
		
		$id['id'] = $this->input->post('id');	
		
		
		 
         $check_update = $this->menu_model->update_checkbox($data,$id);	
			
			
			echo $check_update;
			
		}
    
    
    
	   
/**
* @name          footer menu
* @author        Milan Krushna
* @revised       null
* @Description   retrive footer menu list
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/

function footer_menu()

	{

		$menulist = $this->menu_model->get_menulist();

		$all_menu = array();	

		foreach($menulist as $ml){

			if($ml['parent_menu'] == '0' ){

				$ml['parent_menu'] = "-------";

			}else{

				$ml['parent_menu'] = $this->menu_model->get_parent($ml['parent_menu']);

			}

		$all_menu[] = $ml;

		}

	

		$this->data['menulist'] =  $all_menu;
		$this->data['msg'] = 'Listing of Menu!';
		$this->load->view("common_admin/header",$this->header);
		$this->load->view("menu/view_footer_menu",$this->data);
		$this->load->view("common_admin/footer");	

	}


function remove_img(){
			
			$pid  = $this->input->post('pgid');
   			
			
			$cond['id'] = $pid;
	 
			
			$data['image'] = '';
			
										
			$this->db->update('menu',$data,$cond);	
			echo 1;					
			}







}
