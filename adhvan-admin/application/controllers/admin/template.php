<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Template extends CI_Controller {



	function __construct()

	{

		parent::__construct();

		$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->model('color_model');
		$this->load->database();
		$this->lang->load('auth');
		$this->load->helper('language');	
		$this->header['menu']='temp';
        $this->header['webdata'] = $this->db->get_where('my_website',array('id'=>'1'))->row();
 if (!$this->ion_auth->logged_in() && !$this->ion_auth->is_admin())
    {
      redirect('admin/auth/login', 'refresh');
    }

	}
		   
/**
* @name          color picker
* @author        Subhashree Jena
* @revised       null
* @Description   update color picker 
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
    
    
    function color_picker(){
    	
    	//$this->data['template'] = $this->color_model->color_picker();
        
       
        $this->form_validation->set_rules('temp[menu_bg]', "Menu Background", 'required');
        $this->form_validation->set_rules('temp[banner_dot]', "Banner Dot", 'required');
        $this->form_validation->set_rules('temp[top_btn]', "Top Button", 'required');
        $this->form_validation->set_rules('temp[top_btn_hvr]', "Top Button Hover", 'required');
        $this->form_validation->set_rules('temp[inr_head]', "Inner Head", 'required');
        $this->form_validation->set_rules('temp[home_head]', "Home Head", 'required');
        $this->form_validation->set_rules('temp[font_clr]', "Font Color", 'required');
        $this->form_validation->set_rules('temp[left_menu]', "Left Menu", 'required');
        $this->form_validation->set_rules('temp[link_clr]', "Link Color", 'required');
        $this->form_validation->set_rules('temp[link_clr_hvr]', "Link Color Hover", 'required');
        $this->form_validation->set_rules('temp[link_btn]', "Link Button", 'required');
        $this->form_validation->set_rules('temp[link_btn_hvr]', "Link Button Hover", 'required');
        $this->form_validation->set_rules('temp[content_area]', "Content Area", 'required');
        $this->form_validation->set_rules('temp[bg_clr]', "Background Color", 'required');
        $this->form_validation->set_rules('temp[footer_clr]', "Footer Color", 'required');
        $this->form_validation->set_rules('temp[top_clr]', "Top Color", 'required');
        
        if ($this->form_validation->run() == TRUE)
		{
    
    	$data=$this->input->post('temp');
    	$ext = serialize($data);
    	
    	if($this->color_model->color_picker($ext)){
			redirect('admin/template/color_picker');
		}
    	
    	}else{
		
    	$this->data['select_color'] = $this->color_model->select_color();
    	$this->load->view("common_admin/header",$this->header);
		$this->load->view("template/colorpicker",$this->data);
		$this->load->view("common_admin/footer");
	}

}

		   
/**
* @name          static link
* @author        Subhashree Jena
* @revised       null
* @Description   update static link
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
    

   
   function static_link(){
   	
   	
   	    $this->form_validation->set_rules('temp[contact]', "Contact Us", 'required');
        $this->form_validation->set_rules('temp[con_tact]', "Contact", 'required');
        $this->form_validation->set_rules('temp[sales]', "Sales Reps", 'required');
        $this->form_validation->set_rules('temp[customer_login]', "Customer Login", 'required');
        $this->form_validation->set_rules('temp[register_today]', "Register Today", 'required');
        $this->form_validation->set_rules('temp[you_tube]', "Youtube", 'required');
        $this->form_validation->set_rules('temp[pnc_brochure]', "Download PNC Brochure", 'required');
        $this->form_validation->set_rules('temp[live_chat]', "Click here for LIVE CHAT start Now", 'required');
    
          
       
   	if ($this->form_validation->run() == TRUE)
		{
    
    	$data=$this->input->post('temp');
    	
    	
    	if($_FILES['bg_image']['name'] !=""){
		$bg_image = $this->upload_image('bg_image');	
		}
		
    	if($_FILES['home_image']['name'] !=""){
		$home_image = $this->upload_home_image('home_image');	
		}
    	
    	if($_FILES['youtube_image']['name'] !=""){
			$youtube_image = $this->upload_youtube_image('youtube_image');
		}
    	
    	
    	$ext = serialize($data);
    	
        
    	if($this->color_model->static_link($ext)){
		
            redirect('admin/template/static_link');
		}
    	
    	}else{
   	
   	$this->data['select_color'] = $this->color_model->select_color();
   	$this->load->view("common_admin/header",$this->header);
		$this->load->view("template/staticlink",$this->data);
		$this->load->view("common_admin/footer");
   	
   	
   }
}
    		   
/**
* @name          upload image, home page and home page
* @author        Milan Krushna
* @revised       null
* @Description   upload image, home page and home page
* @Param String  $id is id of slider table
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
    
        public function upload_image($field){
 
           
        $new_img =  'body_bg.png';
        $config['upload_path'] = './background_image/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']	= '30000';
        $config['max_width'] = '102400';
        $config['max_height'] = '70000';
        $config['file_name'] = $new_img;
        $config['overwrite'] = true;

$this->load->library('upload', $config);
$this->upload->initialize($config);
 if($this->upload->do_upload($field)){
     
   //$this->resize_image($new_img);
   $data = array('upload_data' => $this->upload->data());
     
     return $data['upload_data']['file_name'];
  
 }else{
  
  
  echo $this->upload->display_errors();exit;
  $this->session->set_flashdata('message',$this->upload->display_errors());
     return false; 
     
 }

  
}

        public function upload_home_image($field){
 
           
        $new_img = 'productbg.png';
        $config['upload_path'] = './background_image/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']	= '30000';
        $config['max_width'] = '102400';
        $config['max_height'] = '70000';
        $config['file_name'] = $new_img;
        $config['overwrite'] = true;

$this->load->library('upload', $config);
$this->upload->initialize($config);
 if($this->upload->do_upload($field)){
     
   //$this->resize_image($new_img);
   $data = array('upload_data' => $this->upload->data());
     
     return $data['upload_data']['file_name'];
  
 }else{
  
  
  echo $this->upload->display_errors();exit;
  $this->session->set_flashdata('message',$this->upload->display_errors());
     return false; 
     
 }

  
}


     public function upload_youtube_image($field){
 
           
        $new_img =  'youtube.png';
        $config['upload_path'] = './background_image/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']	= '30000';
        $config['max_width'] = '102400';
        $config['max_height'] = '70000';
        $config['file_name'] = $new_img;
        $config['overwrite'] = true;

$this->load->library('upload', $config);
$this->upload->initialize($config);
 if($this->upload->do_upload($field)){
     
   //$this->resize_image($new_img);
   $data = array('upload_data' => $this->upload->data());
     
     return $data['upload_data']['file_name'];
  
 }else{
  
  
  echo $this->upload->display_errors();exit;
  $this->session->set_flashdata('message',$this->upload->display_errors());
     return false; 
     
 }

  
}
 




}