<?php defined('BASEPATH') OR exit('No direct script access allowed');

class social extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->helper('url');

		$this->load->model('social_model');
				
		// Load MongoDB library instead of native db driver if required
		$this->config->item('use_mongodb', 'ion_auth') ?
		$this->load->library('mongo_db') :
		$this->load->database();

		$this->lang->load('auth');
		$this->load->helper('language');
		$this->header['menu'] = 2;
	}

	//redirect if needed, otherwise display the user list
	function index($msg=NULL)
	{
	
		if($msg == '1'){
				$this->data['msg'] = "<p style='color:red'>Event Deleted Successfully!</p>";
			}else if($msg == '2'){
					$this->data['msg'] = "<p style='color:red'>Unable To Delete The Event!<br>It has Images inside </p>";
			}else if($msg == '3'){
					$this->data['msg'] = "<p style='color:red'>You Dont have Access to delete!</p>";
			}else{
				$this->data['msg'] = "<p style='color:green'>Listing of Events!</p>";
			}
	
	
		$this->data['socials'] = $this->social_model->get_socials()->result();
		/*print("<pre>");
		print_r($this->data['events']);
		exit;*/

		$this->load->view("common_admin/header",$this->header);
		$this->load->view("social/view_social",$this->data);
		$this->load->view("common_admin/footer");	
	}

// create a new group
	function add_social()
	{
	//	echo "i am here";exit;
		$this->data['title'] = "Add Event";
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('admin/auth', 'refresh');
		}

		$this->form_validation->set_rules('date', "Date", 'xss_clean');

		if ($this->form_validation->run() == TRUE)
		{
		
			$data = $this->input->post('social');
			$data['image'] =  $this->upload_slide();
			$social_id =  $this->social_model->create_social($data);
		
			if($social_id)
			{
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("admin/social/index");
			}
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		$this->load->view("common_admin/header",$this->header);

		$this->load->view("social/add_social",$this->data);

		$this->load->view("common_admin/footer");	
		}
	}

	//edit a group
	function edit_social($id)
	{
		$this->data['social'] =  $this->social_model->edit_social($id);		
		$this->data['title'] = "Edit Social";
		
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('admin/auth', 'refresh');
		}
		//validate form input
		$this->form_validation->set_rules('title', "Title", 'xss_clean');
		if ($this->form_validation->run() == TRUE)
		{
		
		$file_name =  $_FILES['file']['name'];
		
		if($file_name !=''){
			$data = $this->input->post('social');
			$data['image'] = $this->upload_slide();
		}else{
		
			$data = $this->input->post('social');
		
		}
			
			
			$social_id =  $this->social_model->update_social($data,$id);
			//echo $new_category_id;exit;
			if($social_id)
			{
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("admin/social/index");
			}
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
		$this->load->view("common_admin/header",$this->header);
		$this->load->view("social/edit_social",$this->data);
		$this->load->view("common_admin/footer");	
		}
	}

function delete_social($id){
	$event_delete = $this->social_model->delete_social($id);
	redirect("admin/social/index");
}


public function upload_slide(){
 		$uniqe =  now();
   		$config['upload_path'] = './upload/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg|doc|txt|pdf|zip|rar';
		$config['max_size']	= '20480';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		$this->load->library('upload',$config);
		//$this->upload->initialize($config);	
        $new_img =  $uniqe."_".$_FILES['file']['name'];
		$agent_id=$this->session->userdata['user_id'];
		
        $tmpName  = $_FILES['file']['tmp_name'];  
       
	   //$this->dashboard_model->update_image($agent_id,$new_img);
        $data =array( 'upload_data' => move_uploaded_file($tmpName,"./upload/gallery/social/$new_img"));
  return $new_img;
  
}


}