<?php defined('BASEPATH') OR exit('No direct script access allowed');

class events extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->model('event_model');
		$this->load->database();
		$this->lang->load('auth');
		
		$this->header['menu'] = "common";
       $this->header['webdata'] = $this->db->get_where('my_website',array('id'=>'1'))->row();
	}



/**
* @name          index
* @author        Milan Krushna
* @revised       null
* @Description   display all the events listing
* @Param String  $msg is declared null, if msg is coming then link are activate
* @copyright     Copyright (C) 2017 powered by Wayindia
*/



	//redirect if needed, otherwise display the user list
	function index($msg=NULL)
	{
		if($msg == '1'){
				$this->data['msg'] = "<p style='color:red'>Event Deleted Successfully!</p>";
			}else if($msg == '2'){
					$this->data['msg'] = "<p style='color:red'>Unable To Delete The Event!<br>It has Images inside </p>";
			}else if($msg == '3'){
					$this->data['msg'] = "<p style='color:red'>You Dont have Access to delete!</p>";
			}else{
				$this->data['msg'] = "<p style='color:green'>Listing of Events!</p>";
			}
	
	
		$this->data['events'] = $this->event_model->get_events()->result();
		
		/*print("<pre>");
		print_r($this->data['events']);
		exit;*/
		
		
		$this->load->view("common_admin/header",$this->header);
		$this->load->view("events/view_events",$this->data);
		$this->load->view("common_admin/footer");	

	}


/**
* @name          add event
* @author        Milan Krushna
* @revised       null
* @Description   create or add new events
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/



// create a new group
	function add_event()
	{
	//	echo "i am here";exit;
		$this->data['title'] = "Add Event";
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('admin/auth', 'refresh');
		}

		$this->form_validation->set_rules('title', "Title", 'required|is_unique[events.title]');

		if ($this->form_validation->run() == TRUE)
		{
			$date = $this->input->post('date');
			$data['description'] = $this->input->post('description');
            $data['sht_desc'] = $this->input->post('sht_desc');
			$data['date'] =  date("Y-m-d", strtotime($date));
			$data['category'] =  $this->input->post('category');
			$data['title'] =  $this->input->post('title');
			$data['alias'] =  $this->create_url($this->input->post('title'));
//            if($_FILES['file']['name'] !=""){
//			$upload = $this->upload_image('file');
//            if($upload != ""){
//                $data['cover_pic'] = $upload;
//            }else{
//               redirect('admin/events/add_event');
//            }
//            }
			
			$event_id =  $this->event_model->create_event($data);
			if($event_id)
			{
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("admin/events/index");
			}
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		$this->load->view("common_admin/header",$this->header);
		$this->load->view("events/add_event",$this->data);
		$this->load->view("common_admin/footer");	
		}
	}

    protected function create_url($string){
       
       return rtrim(strtolower(str_replace(' ','-',preg_replace('/\s\s+/','-',preg_replace('/[^A-Za-z0-9\-]/', ' ',$string)))),'/[^A-Za-z0-9\-]/');
   }
    
    
    
    
/**
* @name          incoming testimonial
* @author        Subhashree Jena
* @revised       null
* @Description   display incoming testimonial listing
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/

    
    // testimonial menu
    function income_testimonial(){
		
		
        
        $this->header['menu'] = 'testi';
		 
		 $this->data['title'] = "Incoming Testimonial";
		$this->data['events'] = $this->event_model->get_income_testimonial();
		// $data['title'] =  $this->input->post('title');
		
		$this->load->view("common_admin/header", $this->header);
		$this->load->view("events/testimonial_list",$this->data);
		$this->load->view("common_admin/footer");
	  
	}
   
   
/**
* @name          approve testimonial
* @author        Subhashree Jena
* @revised       null
* @Description   display approve testmonial listing 
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/

     function approve_testimonial(){
		
		$this->header['menu'] = 'testi';


       $this->data['title'] = "Approve Testimonial";
		//$test=$this->event_model->approve_testimonials();
		$this->data['events'] = $this->event_model->get_approve_testimonial();
		//$this->data['events']=$test;
		//$data['title'] =  $this->input->post('title');
		
		$this->load->view("common_admin/header", $this->header);
		$this->load->view("events/testimonial_list",$this->data);
		$this->load->view("common_admin/footer");
	
	}
  
  
/**
* @name          edit event
* @author        Milan Krushna
* @revised       null
* @Description   edit and update event
* @Param String  $id is id of events table
* @copyright     Copyright (C) 2017 powered by Wayindia
*/


	
    
	//edit a group
	function edit_event($id)
	{
		$this->data['event'] =  $this->event_model->edit_event($id);
		
		$this->data['title'] = "Edit Event";
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('admin/auth', 'refresh');
		}
		//validate form input
		$this->form_validation->set_rules('title', "Title", 'xss_clean');
		if ($this->form_validation->run() == TRUE)
		{
		
			$date = $this->input->post('date');
			$data['description'] = $this->input->post('description');
            $data['sht_desc'] = $this->input->post('sht_desc');
			$data['date'] =  date("Y-m-d", strtotime($date));
			$data['category'] =  $this->input->post('category');
			$data['title'] =  $this->input->post('title');
            
//            if($_FILES["file"]["name"] !=""){
//			$upload = $this->upload_image('file');
//            if($upload != ""){
//                $data['cover_pic'] = $upload;
//            }else{
//               redirect("admin/events/edit_event/".$id); 
//            }
//            }
            
            
			$event_id =  $this->event_model->update_event($data,$id);
			//echo $new_category_id;exit;
			if($event_id)
			{
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("admin/events/edit_event/".$id);
			}
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
		$this->load->view("common_admin/header",$this->header);

		$this->load->view("events/edit_event",$this->data);

		$this->load->view("common_admin/footer");	
		}
	}

  
/**
* @name          delete event
* @author        Milan Krushna
* @revised       null
* @Description   delete event
* @Param String  $id is id deleted in events table
* @copyright     Copyright (C) 2017 powered by Wayindia
*/



function delete_event($id){
	$event_delete = $this->event_model->delete_event($id);
	redirect("admin/events/index");
}

  
/**
* @name          upload image and resize image
* @author        Milan Krushna
* @revised       null
* @Description   ulpoading and resizing image 
* @Param String  $field is input type field name
* @copyright     Copyright (C) 2017 powered by Wayindia
*/



public function upload_image($field){
 
        $new_img =  now().'.jpg';
        $config['upload_path'] = './upload/events/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']	= '30000';
        $config['max_width'] = '102400';
        $config['max_height'] = '70000';
        $config['file_name'] = $new_img;

$this->load->library('upload', $config);
$this->upload->initialize($config);
 if($this->upload->do_upload($field)){
     
   $this->resize_image($new_img);
   $data = array('upload_data' => $this->upload->data());
     
     return $data['upload_data']['file_name'];
  
 }else{
  
  $this->session->set_flashdata('message',$this->upload->display_errors());
     return false; 
     
 }

  
}
  
    function resize_image($new_img){
        
        
      $this->load->library('image_lib');
 


            $src_path = 'upload/events/' . $new_img;
            $des_path =  'upload/events/min_events/' . $new_img;
 
            $config['image_library']    = 'gd2';
            $config['source_image']     = $src_path;
            $config['new_image']        = $des_path;
            $config['maintain_ratio']   = TRUE;
            $config['width']            = 241;
            $config['height']           = 179;
            

            $this->image_lib->initialize($config);
            $this->image_lib->resize();
        
    }
 
   
/**
* @name          active and deactive 
* @author        Milan Krushna
* @revised       null
* @Description   check status whether active or deactive
* @Param String  $id is id of events table to check status is active and deactive
* @copyright     Copyright (C) 2017 powered by Wayindia
*/


 
   //status check 
    function actdeactive($id=""){
        
        $sts = $this->input->post('sts');
        
        if($sts == 0){
            $upsts = 1; 
        }else{
            $upsts = 0;
        }
      
        if($this->db->update('events',array('status'=>$upsts),array('id'=>$id))){
          echo $upsts;  
        }
        
    }    
    
 
      
/**
* @name          active and deactive
* @author        Subhashree Jena
* @revised       null
* @Description   check status is active or not in testimonial
* @Param String  $id is id of events table to check status is active and deactive
* @copyright     Copyright (C) 2017 powered by Wayindia
*/


    
    
    function acti_deact($id=""){
		$sts = $this->input->post('sts');
        
        if($sts == 0){
            $upsts = 1; 
        }else{
            $upsts = 0;
        }
      
        if($this->db->update('testimonial',array('status'=>$upsts),array('id'=>$id))){
          echo $upsts;  
        }
        
        
    }
	
   
      
/**
* @name          event order
* @author        Milan Krushna
* @revised       null
* @Description   update events table 
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/

    
      
    function event_order(){
        $events = $_POST['events'];

	for ($i = 0; $i < count($events); $i++) {

		$sql = "UPDATE `events` SET `order_short`=" . $i . " WHERE `id`='" . $events[$i] . "'";

		$this->db->query($sql);

	}

        
    }
    
        
/**
* @name          testimonial order
* @author        Milan Krushna
* @revised       null
* @Description   update testimonial table 
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
   
    function testimonial_order(){
        $test = $_POST['testimonial'];


	for ($i = 0; $i < count($test); $i++) {

		$sql = "UPDATE `testimonial` SET `order_short`=" . $i . " WHERE `id`='" . $test[$i] . "'";

		$this->db->query($sql);

	}
     print("<pre>");
 print_r($test);

 exit; 
 
        
    }
    
     
/**
* @name          edit testimonial
* @author        Milan Krushna
* @revised       null
* @Description   edit testimonial group
* @Param String  $id is id of testimonial table
* @copyright     Copyright (C) 2017 powered by Wayindia
*/


//edit a testimonial group
	function edit_testimonial($id="")
	{
        
		$this->data['test'] =  $this->event_model->edit_testimonial($id);
		
		$this->data['title'] = "Edit Testimonial";
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('admin/auth', 'refresh');
		}
		//validate form input
		$this->form_validation->set_rules('title', "Title", 'required');
		if ($this->form_validation->run() == TRUE)
		{
		

			$data['title'] =  $this->input->post('title');
			$data['first_name'] = $this->input->post('first_name');
            $data['last_name'] = $this->input->post('last_name');
			$data['company'] =  $this->input->post('company');
			$data['phone'] =  $this->input->post('phone');
			$data['email'] =  $this->input->post('email');
			$data['subject'] =  $this->input->post('subject');
			$data['comment'] =  $this->input->post('comment');
            
            if($_FILES["cover_pic"]["name"] !=""){
			$upload = $this->upload_image('cover_pic');
            if($upload != ""){
                $data['cover_pic'] = $upload;
            }else{
               redirect("admin/events/add_testimonial/".$id); 
            }
            }
            
        
			$test_id =  $this->event_model->update_testimonial($data,$id);
			//echo $new_category_id;exit;
			if($test_id)
			{
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("admin/events/edit_testimonial/".$id);
			}
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
		$this->load->view("common_admin/header",$this->header);

		$this->load->view("events/add_testimonial",$this->data);

		$this->load->view("common_admin/footer");	
		}
}

     
/**
* @name          delete testimonial
* @author        Milan Krushna
* @revised       null
* @Description   delete testimonial 
* @Param String  $id is id deleted in testimonial table
* @copyright     Copyright (C) 2017 powered by Wayindia
*/


    function delete_testimonial($id){
    	
    	
	$event_delete = $this->event_model->delete_testimonial($id);
	redirect("admin/events/income_testimonial");
}

    
    
    //SERVICE
    
         
/**
* @name          edit service
* @author        Milan Krushna
* @revised       null
* @Description   edit existing service
* @Param String  $id is id of events table
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
    
	//edit a service
	function edit_service($id)
	{
		$this->data['event'] =  $this->event_model->edit_event($id);
		$this->header['menu'] = "service";
		$this->data['title'] = "Edit Service";
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('admin/auth', 'refresh');
		}
		//validate form input
		$this->form_validation->set_rules('title', "Title", 'xss_clean');
		if ($this->form_validation->run() == TRUE)
		{
		
			$date = $this->input->post('date');
			$data['description'] = $this->input->post('description');
            $data['sht_desc'] = $this->input->post('sht_desc');
			$data['date'] =  date("Y-m-d", strtotime($date));
			$data['category'] =  $this->input->post('category');
			$data['title'] =  $this->input->post('title');
            
            if($_FILES["file"]["name"] !=""){
			$upload = $this->upload_image('file');
            if($upload != ""){
                $data['cover_pic'] = $upload;
            }else{
                //echo 2;exit;
              redirect("admin/events/edit_service/".$id); 
            }
            }
            
            
			$event_id =  $this->event_model->update_event($data,$id);
			//echo $new_category_id;exit;
			if($event_id)
			{
                
                ///echo 1;exit;
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("admin/events/edit_service/".$id);
			}
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
		$this->load->view("common_admin/header",$this->header);

		$this->load->view("events/edit_service",$this->data);

		$this->load->view("common_admin/footer");	
		}
	}

    
         
/**
* @name          create service
* @author        Milan Krushna
* @revised       null
* @Description   create new service
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
    
	function create_service()
	{
	//	echo "i am here";exit;
        
		$this->header['menu'] = "service";
		$this->data['title'] = "Create Service";
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('admin/auth', 'refresh');
		}

		$this->form_validation->set_rules('title', "Title", 'required|is_unique[events.title]');

		if ($this->form_validation->run() == TRUE)
		{
			$date = $this->input->post('date');
			$data['description'] = $this->input->post('description');
            $data['sht_desc'] = $this->input->post('sht_desc');
			$data['date'] =  date("Y-m-d", strtotime($date));
			$data['category'] =  $this->input->post('category');
			$data['title'] =  $this->input->post('title');
			$data['alias'] =  $this->create_url($this->input->post('title'));
            if($_FILES['file']['name'] !=""){
			$upload = $this->upload_image('file');
            if($upload != ""){
                $data['cover_pic'] = $upload;
            }else{
                //echo 2;exit;
               redirect('admin/events/create_service');
            }
            }
			
            ///print_r($data);exit;
            
			$event_id =  $this->event_model->create_event($data);
			if($event_id)
			{
                ///echo 1;exit;
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("admin/events/edit_service/".$event_id);
			}
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		$this->load->view("common_admin/header",$this->header);
		$this->load->view("events/create_service",$this->data);
		$this->load->view("common_admin/footer");	
		}
	}
 
            
/**
* @name          service
* @author        Milan Krushna
* @revised       null
* @Description   retrive service data form events table
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
    
    
    function service()
	{
	    $this->header['menu'] = "service";

		$this->data['events'] = $this->event_model->get_ser_events()->result();
		
		/*print("<pre>");
		print_r($this->data['events']);
		exit;*/
		
		
		$this->load->view("common_admin/header",$this->header);
		$this->load->view("events/service_listing",$this->data);
		$this->load->view("common_admin/footer");	

	}  
                
/**
* @name          delete service
* @author        Milan Krushna
* @revised       null
* @Description   delete service from events
* @Param String  $id is id deleted service from events
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
    
function delete_service($id){
	$event_delete = $this->event_model->delete_service($id);
	redirect("admin/events/service");
}

    
                
/**
* @name          delete testimonial
* @author        Milan Krushna
* @revised       null
* @Description   delete testimonial from testimonial table
* @Param String  $id is id deleted in testimonial table
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
    
    
function delete_test_monial($id){
	$event_delete = $this->event_model->delete_testimonial($id);
	redirect("admin/events/service");
}
  function delete_client($id){
$this->db->delete('client',array('id'=>$id));
	redirect("admin/events/client");
}
  

    
    /**
* @name          edit testimonial
* @author        Milan Krushna
* @revised       null
* @Description   edit Client
* @Param String  $id is id of testimonial table
* @copyright     Copyright (C) 2017 powered by Wayindia
*/


//edit a testimonial group
	function edit_client($id="")
	{
        
        $cond['id'] = $id;
        
		$this->data['test'] =  $this->event_model->edit_client($id);
		
		$this->data['title'] = "Edit Client";
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('admin/auth', 'refresh');
		}
		//validate form input
		$this->form_validation->set_rules('name', "Title", 'required');
		if ($this->form_validation->run() == TRUE)
		{
		

	
			$data['name'] = $this->input->post('name');
			$data['url'] =  $this->input->post('url');
			
            
            if($_FILES["cover_pic"]["name"] !=""){
			$upload = $this->upload_image('cover_pic');
            if($upload != ""){
                $data['cover_pic'] = $upload;
            }else{
               redirect("admin/events/edit_client/".$id); 
            }
            }
             
        
			$test_id =  $this->db->update('client',$data,$cond);
			//echo $new_category_id;exit;
			if($test_id)
			{
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("admin/events/edit_client/".$id);
			}
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
		$this->load->view("common_admin/header",$this->header);
		$this->load->view("events/edit_client",$this->data);
		$this->load->view("common_admin/footer");	
		}
}

    
    
      /**
* @name          edit testimonial
* @author        Milan Krushna
* @revised       null
* @Description      CREATE CLIENT
* @Param String  $id is id of testimonial table
* @copyright     Copyright (C) 2017 powered by Wayindia
*/


//edit a testimonial group
	function create_client($id="")
	{
        
        $cond['id'] = $id;
        
		///$this->data['test'] =  $this->event_model->edit_client($id);
		
		$this->data['title'] = "Edit Client";
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('admin/auth', 'refresh');
		}
		//validate form input
		$this->form_validation->set_rules('name', "Title", 'required');
		if ($this->form_validation->run() == TRUE)
		{
		
			$data['name'] = $this->input->post('name');
			$data['url'] =  $this->input->post('url');
			
            
            if($_FILES["cover_pic"]["name"] !=""){
			$upload = $this->upload_image('cover_pic');
            if($upload != ""){
                $data['cover_pic'] = $upload;
            }else{
               redirect("admin/events/create_client/"); 
            }
            }
             
        
                $this->db->insert('client',$data);
			$test_id = $this->db->insert_id(); 
			//echo $new_category_id;exit;
			if($test_id)
			{
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				echo 1;  
                ///redirect("admin/events/edit_client/".$test_id);
			}
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
		$this->load->view("common_admin/header",$this->header);
		$this->load->view("events/create_client",$this->data);
		$this->load->view("common_admin/footer");	
		}
}

    
    
    
    
    ////client_listing
      function client(){
		
	

       $this->data['title'] = "client";
		//$test=$this->event_model->approve_testimonials();
          $this->db->order_by('id','DESC');  
         $dtd = $this->db->get('client')->result_array();
		$this->data['events'] = $dtd;
		//$this->data['events']=$test;
		//$data['title'] =  $this->input->post('title');
		
		$this->load->view("common_admin/header", $this->header);
		$this->load->view("events/client",$this->data);
		$this->load->view("common_admin/footer"); 
	
	}
  
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}