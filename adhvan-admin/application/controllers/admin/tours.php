<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tours extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language','form'));
        $this->load->model('location_model');
        $this->load->model('tour_model');
        
        $this->data['menu'] = "travel";
       $this->data['webdata'] = $this->db->get_where('my_website',array('id'=>'1'))->row();   
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
	}


    function index(){
        $this->data['all_tours'] = $this->tour_model->get_tours();

        $this->load->view('common_admin/header', $this->data);
        $this->load->view('tours/view_tours', $this->data);
        $this->load->view('common_admin/footer', $this->data);

    }


    function create_tours(){
        $this->load->library('form_validation');
        $maxid =$this->tour_model->get_newTours_id()->id;
        $this->data['newId'] = ($maxid == "")? 1 :$maxid++; 
        $this->data['country'] = $this->tour_model->getstar('country');
        $this->data['responsible_benifits'] = $this->tour_model->getstar('responsible_benifits');
        $this->data['region'] = $this->tour_model->getstar('region');
        $this->data['tour_style'] = $this->tour_model->getstar('tour_style');


        $this->form_validation->set_rules('name', 'Tour Name', 'required');

        if ($this->form_validation->run() == FALSE)
        {

        $this->load->view('common_admin/header', $this->data);
        $this->load->view('tours/create_tours', $this->data);
        $this->load->view('common_admin/footer', $this->data);

        }else{
          //  print("<pre>");
        
           //tour table data
            $tt_data["name"] = $tName =  $this->input->post("name");
            $tt_data["alias"] = $this->create_url($tName);
            $tt_data["day"] = $this->input->post("day");
            $tt_data["day"] = $this->input->post("day");
            $tt_data["night"] = $this->input->post("night");
            $tt_data["overview_title"] = $this->input->post("overview_title");
            $tt_data["content"] = $this->input->post("content");
            $tt_data["map_url"] = $this->input->post("map_url");
            $tt_data["main_image"] = $this->input->post("main_image");
            $tt_data["trip_image"] = $this->input->post("trip_image");
            $tt_data["services"] = $this->input->post("services");
            $tt_data["notes"] = $this->input->post("notes");
            $tt_data["tours_point"] = $this->input->post("tours_point");
            $tt_data["tourImage"] = implode(',',$this->input->post("tourImage"));
            $inputId ="";
          $inputId = $this->tour_model->insert_tour($tt_data);

           /// print_r($tt_data);
            
            if($inputId){

            //travel Maping data

            $tm_data["travel_type"] = "1";
            $tm_data["type_id"] = $inputId; 
            $tm_data["country_id"] = $this->input->post("country");
            $tm_data["city_id"] = $this->input->post("city");
            $tm_data["sub_city_id"] = $this->input->post("sub_city"); 
            $tm_data["region_id"] = $this->input->post("region_id"); 
            $tm_data["responsible_id"] = $this->input->post("responsible_id"); 
            $tm_data["tours_style_id"] = $this->input->post("tours_style_id"); 

           $this->tour_model->insert_tour_maping($tm_data);

          //  print_r($tm_data);

        ///tour trip data
         $tttp_data =  $this->input->post("tour_high");
        foreach($tttp_data  as $k => $tt){
            $tttp_data[$k]['tour_id'] = $inputId;
        }
        $this->tour_model->insert_tour_trip($tttp_data);
      //  print_r($tttp_data);
            
    }
    redirect("admin/tours/create_tours");
    

        }

    }



    function modify_tour_days($tid=''){

        $this->data['tourId'] = $tid;
       $this->data['tours'] = $this->tour_model->get_single_tours($tid);        
        $ddd = $this->tour_model->get_day_data($tid);

       $dayData = [];

       foreach($ddd as $dd){
           $dayno = $dd['day_no'];
        $dayData[$dayno] = $dd;
       }

//        print("<pre>");        
// print_r($dayData);exit;
       $this->data['dayData'] = $dayData;

       $this->load->library('form_validation');

       $this->form_validation->set_rules('tour_id', 'Tour Name', 'required');

       if ($this->form_validation->run() == FALSE)
       {

       $this->load->view('common_admin/header', $this->data);
       $this->load->view('tours/modify_tour_days', $this->data);
       $this->load->view('common_admin/footer', $this->data);

       }else{
        // print("<pre>");
        // print_r($this->input->post());
        $tourDay = $this->input->post("tour_high");
        
        foreach($tourDay as $k=>$td){
            $cond['tour_id'] = $tid;
            $cond['day_no'] = $td['day_no'];

            $td['tour_id'] = $tid;
           

            $this->tour_model->putDayData($cond,$td);
        }

        redirect("admin/tours/modify_tour_days/".$tid);

       }

    }

function edit_tour($tid=""){
    $cond['id'] = $tid;
    $this->load->library('form_validation');
    $maxid =$this->tour_model->get_newTours_id()->id;
    ///$this->data['newId'] = ($maxid == "")? 1 :$maxid++; 
    $this->data['country'] = $this->tour_model->getstar('country');
    $this->data['responsible_benifits'] = $this->tour_model->getstar('responsible_benifits');
    $this->data['region'] = $this->tour_model->getstar('region');
    $this->data['tour_style'] = $this->tour_model->getstar('tour_style');
    
    $this->data['tourMaping'] = $tourMaping = $this->tour_model->get_tour_maping($tid,1);

    $this->data['city'] = $this->tour_model->get_city($tourMaping->country_id);
    $this->data['sub_city'] = $this->tour_model->get_sub_city($tourMaping->city_id);

    $this->data['tourId'] = $tid;
    $this->data['tourData'] = $this->tour_model->get_single_tours($tid);
    
   /// print_r($this->data['sub_city']);exit;
    $this->data['tourTrip'] = $this->tour_model->get_tour_trip($tid);
        

    $this->form_validation->set_rules('name', 'Tour Name', 'required');

    if ($this->form_validation->run() == FALSE)
    {

    $this->load->view('common_admin/header', $this->data);
    $this->load->view('tours/edit_tours', $this->data);
    $this->load->view('common_admin/footer', $this->data);

    }else{
      //  print("<pre>");
    
       //tour table data
        $tt_data["name"] = $tName =  $this->input->post("name");
        //$tt_data["alias"] = $this->create_url($tName);
        $tt_data["day"] = $this->input->post("day");
        $tt_data["day"] = $this->input->post("day");
        $tt_data["night"] = $this->input->post("night");
        $tt_data["overview_title"] = $this->input->post("overview_title");
        $tt_data["content"] = $this->input->post("content");
        $tt_data["map_url"] = $this->input->post("map_url");
        $tt_data["main_image"] = $this->input->post("main_image");
        $tt_data["trip_image"] = $this->input->post("trip_image");
        $tt_data["services"] = $this->input->post("services");
        $tt_data["notes"] = $this->input->post("notes");
        $tt_data["tours_point"] = $this->input->post("tours_point");
        $tt_data["tourImage"] = implode(',',$this->input->post("tourImage"));
        $inputId ="";
      $inputId = $this->tour_model->update_tour($tt_data,$cond);

       /// print_r($tt_data);
        
        if($inputId){

        //travel Maping data

        $tm_cond["id"] = $tourMaping->id; 

        $tm_data["travel_type"] = "1";
        $tm_data["country_id"] = $this->input->post("country");
        $tm_data["city_id"] = $this->input->post("city");
        $tm_data["sub_city_id"] = $this->input->post("sub_city"); 
        $tm_data["region_id"] = $this->input->post("region_id"); 
        $tm_data["responsible_id"] = $this->input->post("responsible_id"); 
        $tm_data["tours_style_id"] = $this->input->post("tours_style_id"); 

       $this->tour_model->update_tour_maping($tm_data,$tm_cond);

      //  print_r($tm_data);

    ///tour trip data
     $tttp_data =  $this->input->post("tour_high");
    /// print_r($tttp_data);exit;
    foreach($tttp_data  as $k => $tt){
        $tttp_data[$k]['tour_id'] = $tid;
        if($tttp_data[$k]['id']!=""){
        $tt_cond['id'] =  $tttp_data[$k]['id'];
        $this->tour_model->update_tour_trip($tttp_data[$k],$tt_cond);
        }else{
            unset($tttp_data[$k]['id']);
         // print("<pre>");
        //print_r();exit;
            $this->tour_model->insert_tour_trip(array($tttp_data[$k]));
        }
    }
   
  //  print_r($tttp_data);
        
}
redirect("admin/tours/edit_tour/".$tid);


    }


}

function create_hotels(){

    $this->data['title'] = "Create Hotel";

    $this->load->library('form_validation');
        $maxid =$this->tour_model->get_newTours_id()->id;
        $this->data['newId'] = ($maxid == "")? 1 :$maxid++; 
        $this->data['country'] = $this->tour_model->getstar('country');
      
        $this->data['region'] = $this->tour_model->getstar('region');
        $this->data['special'] = $this->tour_model->getstar('special');
    
        $this->form_validation->set_rules('name', 'Hotel Name', 'required');

        if ($this->form_validation->run() == FALSE)
        {

        $this->load->view('common_admin/header', $this->data);
        $this->load->view('hotel/create_hotel', $this->data);
        $this->load->view('common_admin/footer', $this->data);

        }else{
          // print("<pre>");
           // print_r($this->input->post());exit; 
           //tour table data
            $tt_data["name"] = $tName =  $this->input->post("name");
            $tt_data["sub_title"] =  $this->input->post("sub_title");
            $tt_data["alias"] = $this->create_url($tName);
            $tt_data["about"] = $this->input->post("about");
            $tt_data["hotel_address"] = $this->input->post("hotel_address");
            $tt_data["map_url"] = $this->input->post("map_url");
            $tt_data["main_image"] = $this->input->post("main_image");
            $tt_data["why_we_live"] = $this->input->post("why_we_live");
            $tt_data["perfect_for"] = $this->input->post("perfect_for");
            $tt_data["child_policy"] = $this->input->post("child_policy");
            $tt_data["website_link"] = $this->input->post("website_link");
            $tt_data["gallery_image"] = implode(',',$this->input->post("galleryImages"));
            $inputId ="";
          $inputId = $this->tour_model->insert_Hotel($tt_data);

           /// print_r($tt_data);
            
            if($inputId){

            //travel Maping data

            $tm_data["travel_type"] = "2";
            $tm_data["type_id"] = $inputId; 
            $tm_data["country_id"] = $this->input->post("country");
            $tm_data["city_id"] = $this->input->post("city");
            $tm_data["sub_city_id"] = $this->input->post("sub_city"); 
            $tm_data["region_id"] = $this->input->post("region_id"); 
            $tm_data["star_ratings"] = $this->input->post("star_ratings"); 
            $tm_data["special_id"] = $this->input->post("special_id"); 

           $this->tour_model->insert_tour_maping($tm_data);

          //  print_r($tm_data);

     
            
    }
    redirect("admin/tours/edit_hotel/".$inputId);
    

        }
}


function edit_hotel($tid=""){
    $this->data['hotelId'] = $hotelId = $tid;
    $this->data['title'] = "Modify Hotel";

    $this->load->library('form_validation');
        
        $this->data['country'] = $this->tour_model->getstar('country');
        $this->data['tourMaping'] = $tourMaping = $this->tour_model->get_tour_maping($tid,'2');
//print_r($tourMaping);
        $this->data['city'] = $this->tour_model->get_city($tourMaping->country_id);
        $this->data['sub_city'] = $this->tour_model->get_sub_city($tourMaping->city_id);
        $this->data['hotelData'] = $this->tour_model->get_single_hotel($tid);
        $this->data['region'] = $this->tour_model->getstar('region');
        $this->data['special'] = $this->tour_model->getstar('special');
    
        $this->form_validation->set_rules('name', 'Hotel Name', 'required');

        if ($this->form_validation->run() == FALSE)
        {

        $this->load->view('common_admin/header', $this->data);
        $this->load->view('hotel/edit_hotel', $this->data);
        $this->load->view('common_admin/footer', $this->data);

        }else{
          // print("<pre>");
           // print_r($this->input->post());exit; 
           //tour table data
            $tt_data["name"] = $tName =  $this->input->post("name");
            $tt_data["sub_title"] = $tName =  $this->input->post("sub_title");
           /// $tt_data["alias"] = $this->create_url($tName);
            $tt_data["about"] = $this->input->post("about");
            $tt_data["hotel_address"] = $this->input->post("hotel_address");
            $tt_data["map_url"] = $this->input->post("map_url");
            $tt_data["main_image"] = $this->input->post("main_image");
            $tt_data["why_we_live"] = $this->input->post("why_we_live");
            $tt_data["perfect_for"] = $this->input->post("perfect_for");
            $tt_data["child_policy"] = $this->input->post("child_policy");
            $tt_data["website_link"] = $this->input->post("website_link");
            $tt_data["gallery_image"] = implode(',',$this->input->post("galleryImages"));
            $inputId ="";
            $cond['id'] = $hotelId;
          $inputId = $this->tour_model->update_hotel($tt_data,$cond);

           /// print_r($tt_data);
            
            if($inputId){

            //travel Maping data

            $tm_cond["id"] = $tourMaping->id; 

        $tm_data["travel_type"] = "2";

            $tm_data["country_id"] = $this->input->post("country");
            $tm_data["city_id"] = $this->input->post("city");
            $tm_data["sub_city_id"] = $this->input->post("sub_city"); 
            $tm_data["region_id"] = $this->input->post("region_id"); 
            $tm_data["star_ratings"] = $this->input->post("star_ratings"); 
            $tm_data["special_id"] = $this->input->post("special_id"); 

            $this->tour_model->update_tour_maping($tm_data,$tm_cond);

          //  print_r($tm_data);

     
            
    }
    redirect("admin/tours/edit_hotel/".$hotelId);
    

        }
}

function hotels(){
    $this->data['title'] = "Hotel List";
    $this->data['all_hotels'] = $this->tour_model->get_hotels();

    $this->load->view('common_admin/header', $this->data);
    $this->load->view('hotel/view_hotels', $this->data);
    $this->load->view('common_admin/footer', $this->data);

}

function experience(){
    $this->data['title'] = "Experiance List";
    $this->data['all_experiance'] = $this->tour_model->get_experiance();

    $this->load->view('common_admin/header', $this->data);
    $this->load->view('experiance/view_experiance', $this->data);
    $this->load->view('common_admin/footer', $this->data);

}

function create_experience(){

    $this->data['title'] = "Create experiance";

    $this->load->library('form_validation');
        $maxid =$this->tour_model->get_newTours_id()->id;
        $this->data['newId'] = ($maxid == "")? 1 :$maxid++; 
        $this->data['country'] = $this->tour_model->getstar('country');
        $this->data['responsible_benifits'] = $this->tour_model->getstar('responsible_benifits');
        $this->data['region'] = $this->tour_model->getstar('region');
        $this->data['tour_style'] = $this->tour_model->getstar('tour_style');

        $this->form_validation->set_rules('name', 'Experiance Name', 'required');

        if ($this->form_validation->run() == FALSE)
        {

        $this->load->view('common_admin/header', $this->data);
        $this->load->view('experiance/create_experiance', $this->data);
        $this->load->view('common_admin/footer', $this->data);

        }else{
        //   print("<pre>");
        //    print_r($this->input->post());exit; 
           //tour table data
            $tt_data["name"] = $tName =  $this->input->post("name");
            $tt_data["alias"] = $this->create_url($tName);
            $tt_data["map_url"] = $this->input->post("map_url");
            $tt_data["duration"] = $this->input->post("duration");
            $tt_data["overview_desc"] = $this->input->post("overview_desc");
            $tt_data["overview_desc"] = $this->input->post("overview_desc");
            $tt_data["itnerary_title"] = $this->input->post("itnerary_title");
            $tt_data["itnerary_desc"] = $this->input->post("itnerary_desc");
            $tt_data["services"] = $this->input->post("services");
            $tt_data["notes"] = $this->input->post("notes");
            $tt_data["thems_daytrip"] = $this->input->post("thems_daytrip");
            $tt_data["main_image"] = $this->input->post("main_image");
            $tt_data["gallery_image"] = implode(',',$this->input->post("galleryImages"));
            $inputId ="";
          $inputId = $this->tour_model->insert_experiance($tt_data);

           /// print_r($tt_data);
            
            if($inputId){

            //travel Maping data

            $tm_data["type_id"] = $inputId; 

        $tm_data["travel_type"] = "3";
            $tm_data["country_id"] = $this->input->post("country");
            $tm_data["city_id"] = $this->input->post("city");
            $tm_data["sub_city_id"] = $this->input->post("sub_city"); 
            $tm_data["region_id"] = $this->input->post("region_id");
            $tm_data["responsible_id"] = $this->input->post("responsible_id"); 
            $tm_data["tours_style_id"] = $this->input->post("tours_style_id"); 

           $this->tour_model->insert_tour_maping($tm_data);

          //  print_r($tm_data);

     
            
    }
    redirect("admin/tours/edit_experience/".$inputId);
    

        }
}



function edit_experience($tid){
    $cond['id'] = $tid;
    $this->data['title'] = "Modify experiance";

    $this->load->library('form_validation');
    $this->data['country'] = $this->tour_model->getstar('country');
    $this->data['responsible_benifits'] = $this->tour_model->getstar('responsible_benifits');
    $this->data['region'] = $this->tour_model->getstar('region');
    $this->data['tour_style'] = $this->tour_model->getstar('tour_style');
    
    $this->data['tourMaping'] = $tourMaping = $this->tour_model->get_tour_maping($tid,'3');
//print_r($tourMaping);
    $this->data['city'] = $this->tour_model->get_city($tourMaping->country_id);
    $this->data['sub_city'] = $this->tour_model->get_sub_city($tourMaping->city_id);

    $this->data['tourId'] = $tid;
    $this->data['expData'] = $this->tour_model->get_single_experiance($tid);
    
   /// print_r($this->data['sub_city']);exit;
    $this->data['tourTrip'] = $this->tour_model->get_tour_trip($tid);
        
        $this->form_validation->set_rules('name', 'Experiance Name', 'required');

        if ($this->form_validation->run() == FALSE)
        {

        $this->load->view('common_admin/header', $this->data);
        $this->load->view('experiance/edit_experiance', $this->data);
        $this->load->view('common_admin/footer', $this->data);

        }else{
        //   print("<pre>");
        //    print_r($this->input->post());exit; 
           //tour table data
            $tt_data["name"] = $tName =  $this->input->post("name");
            //$tt_data["alias"] = $this->create_url($tName);
            $tt_data["map_url"] = $this->input->post("map_url");
            $tt_data["duration"] = $this->input->post("duration");
            $tt_data["overview_desc"] = $this->input->post("overview_desc");
            $tt_data["overview_desc"] = $this->input->post("overview_desc");
            $tt_data["itnerary_title"] = $this->input->post("itnerary_title");
            $tt_data["itnerary_desc"] = $this->input->post("itnerary_desc");
            $tt_data["services"] = $this->input->post("services");
            $tt_data["notes"] = $this->input->post("notes");
            $tt_data["thems_daytrip"] = $this->input->post("thems_daytrip");
            $tt_data["main_image"] = $this->input->post("main_image");
            $tt_data["gallery_image"] = implode(',',$this->input->post("galleryImages"));
            $inputId ="";
          $inputId = $this->tour_model->upddate_experiance($tt_data,$cond);

           /// print_r($tt_data);
            
            if($inputId){

            //travel Maping data

            $tm_cond["id"] =$tourMaping->id;
            $tm_data["travel_type"] = "3";
            // $tm_data["type_id"] = $inputId; 
            $tm_data["country_id"] = $this->input->post("country");
            $tm_data["city_id"] = $this->input->post("city");
            $tm_data["sub_city_id"] = $this->input->post("sub_city"); 
            $tm_data["region_id"] = $this->input->post("region_id");
            $tm_data["responsible_id"] = $this->input->post("responsible_id"); 
            $tm_data["tours_style_id"] = $this->input->post("tours_style_id"); 

           $this->tour_model->update_tour_maping($tm_data,$tm_cond);

          //  print_r($tm_data);

     
            
    }
    redirect("admin/tours/edit_experience/".$tid);
    

        }
}




function delete_hotel($id =''){
    
    if($this->db->delete('hotels',array('id'=>$id))){
        echo 1;
    }else{
        echo "Something error try again";
    }
}
function delete_tour($id =''){
    
    if($this->db->delete('tours',array('id'=>$id))){
        echo 1;
    }else{
        echo "Something error try again";
    }
}
function delete_experience($id =''){
    
    if($this->db->delete('experiance',array('id'=>$id))){
        echo 1;
    }else{
        echo "Something error try again";
    }
}



	// redirect if needed, otherwise display the user list
	public function tour_style($id='')
	{
            $this->data['index_heading'] = 'Tour Style Listing';
            $this->data['index_subheading'] = '';
            $this->data['alldata'] = array();
            $this->data['sear_key'] = '';

            if(!empty($_GET['search_data'])){
            
            $this->data['sear_key'] = $sk = $_GET['search_data'];
                $this->data['alldata'] =  $this->tour_model->country_search($sk);   
                
            }else{
            $this->data['alldata'] =  $this->tour_model->getstar('tour_style',$id);
            }
		    $this->load->view('common_admin/header', $this->data);
			$this->load->view('tours/tour_style_listing', $this->data);
			$this->load->view('common_admin/footer', $this->data);
		}
	
    function responsible_benifits($id=""){
            
        $this->data['index_heading'] = 'Responsible Benifits';
        $this->data['index_subheading'] = '';
        $this->data['alldata'] = array();
        $this->data['sear_key'] = '';

        if(!empty($_GET['search_data'])){
        
            $this->data['sear_key'] = $sk = $_GET['search_data'];
          $this->data['alldata'] =  $this->tour_model->country_search($sk);   
            
        }else{
        $this->data['alldata'] =  $this->tour_model->getstar('responsible_benifits',$id);
        }
        $this->load->view('common_admin/header', $this->data);
        $this->load->view('tours/responsible_benifits', $this->data);
        $this->load->view('common_admin/footer', $this->data);
    }

    function special($id=""){
            
        $this->data['index_heading'] = 'Special';
        $this->data['index_subheading'] = '';
        $this->data['alldata'] = array();
        $this->data['sear_key'] = '';

        if(!empty($_GET['search_data'])){
        
            $this->data['sear_key'] = $sk = $_GET['search_data'];
          $this->data['alldata'] =  $this->tour_model->country_search($sk);   
            
        }else{
        $this->data['alldata'] =  $this->tour_model->getstar('special',$id);
        }
        $this->load->view('common_admin/header', $this->data);
        $this->load->view('tours/special', $this->data);
        $this->load->view('common_admin/footer', $this->data);
    }


    function new_resp_bnfty(){

        $resp =array();
        $newcnt = $this->input->post('cname');
       
       if($newcnt!=''){
       $this->db->insert('responsible_benifits',array('name'=>$newcnt));
       $insert = $this->db->insert_id();
           $resp['status']=1;
           $resp['message']='special Created';  
           $resp['id']=$insert;  
           $resp['newcntry']=$newcnt;  
           
       }else{
            $resp['status']=0;
           $resp['message']='something Error try again';    
       }
              echo json_encode($resp); 

   }


   function update_resp_bnfty(){
        
    $resp =array();
    
    $data['name'] = $this->input->post('cname');
    $cond['id'] = $this->input->post('cid');
    
    if($this->db->update('responsible_benifits',$data,$cond)){
        $resp['status']=1;
        $resp['message']='special Updated';
    }else{
        $resp['status']=0;
        $resp['message']='something Error try again';  
    }
        
   echo json_encode($resp); 
    
}

function delete_resp_bnft($id =''){
    
    if($this->db->delete('responsible_benifits',array('id'=>$id))){
        echo 1;
    }else{
        echo "Something error try again";
    }
}

function new_special(){

    $resp =array();
    $newcnt = $this->input->post('cname');
   
   if($newcnt!=''){
   $this->db->insert('special',array('name'=>$newcnt));
   $insert = $this->db->insert_id();
       $resp['status']=1;
       $resp['message']='special Created';  
       $resp['id']=$insert;  
       $resp['newcntry']=$newcnt;  
       
   }else{
        $resp['status']=0;
       $resp['message']='something Error try again';    
   }
          echo json_encode($resp); 

}


function update_special(){
    
$resp =array();

$data['name'] = $this->input->post('cname');
$cond['id'] = $this->input->post('cid');

if($this->db->update('special',$data,$cond)){
    $resp['status']=1;
    $resp['message']='special Updated';
}else{
    $resp['status']=0;
    $resp['message']='something Error try again';  
}
    
echo json_encode($resp); 

}

function delete_special($id =''){

if($this->db->delete('special',array('id'=>$id))){
    echo 1;
}else{
    echo "Something error try again";
}
}


    function country_data(){
        
        $data = $this->location_model->getstar('country',$id,$lmt);
        
        print("<pre>");
        print_r($_GET);
        
    }
    
    
    function new_tour_style(){
         $resp =array();
        $newcnt = $this->input->post('cname');
        $pageTitle = $this->input->post('pageTitle');
        $pagecontent = $this->input->post('pagecontent');
        $main_image = $this->input->post('main_image');
        $alias = $this->create_url($newcnt);
        
        if($newcnt!=''){
        $this->db->insert('tour_style',array('name'=>$newcnt,"page_title"=>$pageTitle,"page_content"=>$pagecontent,"alias"=>$alias,'main_image'=>$main_image));
        $insert = $this->db->insert_id();
            $resp['status']=1;
            $resp['message']='Tour Style Created';  
            $resp['id']=$insert;  
            $resp['newcntry']=$newcnt;  
            
        }else{
             $resp['status']=0;
            $resp['message']='something Error try again';  
      
            
        }
               echo json_encode($resp); 

    }
    
    protected function create_url($strin){
        
        return rtrim(strtolower(str_replace(' ','-',preg_replace('/\s\s+/','-',preg_replace('/[^A-Za-z0-9\-]/', ' ',$strin)))),'/[^A-Za-z0-9\-]/');
    }

    function get_tour_style($id = ""){
        $data = $this->db->get_where('tour_style',array("id"=>$id))->row_array();
        if(!empty($data)){
            $data['status'] = 1;
        echo json_encode($data);
        }else{
            $res['status'] = 0;
            $res['message'] = "Nodata found";
            echo json_encode(res);
        }
    }


    function update_style(){
        
        $resp =array();
        
        $data['name'] = $this->input->post('cname');
        $data['page_title'] = $this->input->post('pageTitle');
        $data['page_content'] = $this->input->post('pagecontent');
        $data['main_image'] = $this->input->post('main_image');
        $cond['id'] = $this->input->post('cid');
        
        if($this->db->update('tour_style',$data,$cond)){
            $resp['status']=1;
            $resp['message']='country Update';
        }else{
            $resp['status']=0;
            $resp['message']='something Error try again';  
        }
            
       echo json_encode($resp); 
        
    }
    
    function delete_style($id =''){
    
        if($this->db->delete('tour_style',array('id'=>$id))){
            echo 1;
        }else{
            echo "Something error try again";
        }
    }
    

    public function state($id='')
	{
            $this->data['index_heading'] = 'State Listing';
            $this->data['index_subheading'] = '';
        
        $cid = $this->input->get('country');
            $this->data['cid'] = $cid;
            
        $this->data['country'] = $this->location_model->getstar('country');
            
            $lmt = 30;
            $this->data['alldata'] =  $this->location_model->getstate($cid,$id,$lmt);
        
		    $this->load->view('common_admin/header', $this->data);
			$this->load->view('location/state_listing', $this->data);
			$this->load->view('common_admin/footer', $this->data);
        }
        

        
    function new_state(){
        
        $data['countryid'] = $this->input->post('cid');
        $this->data['countryname'] = $this->input->post('cname');
        $data['statename'] = $this->input->post('state');
        
        $this->data['state'] = $this->location_model->create_state($data);
        $this->data['type'] = 'create';
        
        $output = $this->load->view('location/ajax_view',$this->data,true);
        
            $resp['status']=1;
            $resp['message']='State Created';  
            $resp['newstate']=$output; 
        echo json_encode($resp);
        
    }
    
    function update_state(){
        
      $data['countryid'] = $this->input->post('cid');
        $this->data['countryname'] = $this->input->post('cname');
        $data['statename'] = $this->input->post('state');
        $cond['id'] = $this->input->post('sid');
        
        $this->data['state'] = $this->location_model->update_state($data,$cond);
        $this->data['type'] = 'update';
        
        $output = $this->load->view('location/ajax_view',$this->data,true);
        
            $resp['status']=1;
            $resp['message']='State Created';  
            $resp['newstate']=$output; 
        echo json_encode($resp);  
        
    }
    
    
    function delete_state($id =''){
    if($this->db->delete('state',array('id'=>$id))){
            echo 1;
        }else{
            echo "Something error try again";
        }
    }


        public function region($id='')
	{
            $this->data['index_heading'] = 'Region Listing';
            $this->data['index_subheading'] = '';
        
        $cid = $this->input->get('country');
            $this->data['cid'] = $cid;
            
        $this->data['country'] = $this->location_model->getstar('country');
            
            $lmt = 30;
            $this->data['alldata'] =  $this->location_model->getregion($cid,$id,$lmt);
		    $this->load->view('common_admin/header', $this->data);
			$this->load->view('location/region_listing', $this->data);
			$this->load->view('common_admin/footer', $this->data);
		}
    
    
        function new_region(){
        
            $data['countryid'] = $this->input->post('cid');
            $this->data['countryname'] = $this->input->post('cname');
            $data['name'] = $this->input->post('state');
            
            $this->data['state'] = $this->location_model->create_region($data);
            $this->data['type'] = 'create_region';
            
            $output = $this->load->view('location/ajax_view',$this->data,true);
            
                $resp['status']=1;
                $resp['message']='Region Created';  
                $resp['newstate']=$output; 
            echo json_encode($resp);
            
        }

        function update_region(){
        
            $data['countryid'] = $this->input->post('cid');
              $this->data['countryname'] = $this->input->post('cname');
              $data['name'] = $this->input->post('state');
              $cond['id'] = $this->input->post('sid');
              
              $this->data['state'] = $this->location_model->update_region($data,$cond);
              $this->data['type'] = 'update_region';
              
              $output = $this->load->view('location/ajax_view',$this->data,true);
              
                  $resp['status']=1;
                  $resp['message']='Region Updated';  
                  $resp['newstate']=$output; 
              echo json_encode($resp);  
              
          }
          
          
          function delete_region($id =''){
          if($this->db->delete('region',array('id'=>$id))){
                  echo 1;
              }else{
                  echo "Something error try again";
              }
          }
   
    function district($id = ''){
        
            $this->data['index_heading'] = 'District Listing';
            $this->data['index_subheading'] = '';
            
            $this->data['country'] = $this->location_model->getstar('country');
            
      $this->data['cnt_id'] =  $cond['country_id'] = $this->input->get('country_2');
       $this->data['sta_id'] = $cond['state_id'] = $this->input->get('state_id_2');
            
            $lmt = 30;
            $this->data['alldata'] =  $this->location_model->getcity($cond,$lmt);
        
		    $this->load->view('common_admin/header', $this->data);
			$this->load->view('location/district_listing', $this->data);
			$this->load->view('common_admin/footer', $this->data);
        
    }
    
    function city($id = ''){
        
            $this->data['index_heading'] = 'City Listing';
            $this->data['index_subheading'] = '';
            
            $this->data['country'] = $this->location_model->getstar('country');
            
      $this->data['cnt_id'] =  $cond['c_id'] = $this->input->get('country_2');
       $this->data['sta_id'] = $cond['s_id'] = $this->input->get('state_id_2');
       $this->data['dist_id'] = $cond['d_id'] = $this->input->get('distId_2');
            
            $lmt = 30;
            $this->data['alldata'] =  $this->location_model->getDistrictCity($cond,$lmt);
        
		    $this->load->view('common_admin/header', $this->data);
			$this->load->view('location/city_listing', $this->data);
			$this->load->view('common_admin/footer', $this->data);
        
    }
    
    
    
    
    function get_cnt_state($cid=''){
        $stateid = $this->input->post('sid');
        
        $data = $this->db->get_where('state',array('countryid'=>$cid))->result();
        
        echo "<option value=''>Select City</option>";
      foreach($data as $cs){
          if($stateid == $cs->id){ $selct ='selected'; }else{ $selct ='';  }
          echo "<option ".$selct." value=".$cs->id.">".$cs->statename."</option>";
      } 
        
    }
    
       
    function get_state_district($cid=''){
        $stateid = $this->input->post('sid');
        
        $data = $this->db->get_where('city',array('state_id'=>$cid))->result();
        
        echo "<option value=''>Select Sub City</option>";
      foreach($data as $cs){
          if($stateid == $cs->id){ $selct ='selected'; }else{ $selct ='';  }
          echo "<option ".$selct." value=".$cs->id.">".$cs->city."</option>";
      } 
        
    }
    
    function get_dis_city($cid=''){
        $stateid = $this->input->post('sid');
        
        $data = $this->db->get_where('district_city',array('d_id'=>$cid))->result();
        
        echo "<option value=''>Select Sub City</option>";
      foreach($data as $cs){
          if($stateid == $cs->id){ $selct ='selected'; }else{ $selct ='';  }
          echo "<option ".$selct." value=".$cs->id.">".$cs->name."</option>";
      } 
        
    }
    
    
    function get_cnt_city($cid='',$sid=''){
        
          $cityid = $this->input->post('sid');
        
        $data = $this->db->get_where('city',array('country_id'=>$cid,'state_id'=>$sid))->result();
        
        echo "<option value=''>Select District </option>";
      foreach($data as $cs){
          if($cityid == $cs->id){ $selct ='selected'; }else{ $selct ='';  }
          echo "<option ".$selct." value=".$cs->id.">".$cs->city."</option>";
      } 
        
        
    }
    
    function new_city(){
        
       $this->data['country_id'] = $data['country_id'] = $this->input->post('cid');
        $this->data['state_id'] = $data['state_id'] = $this->input->post('stateid');
       $this->data['city'] = $data['city'] = $this->input->post('city');
        $this->data['countryname'] = $this->input->post('cname');
        $this->data['statename'] = $this->input->post('statename');
        
        $this->data['city_id'] = $this->location_model->create_city($data);
        $this->data['type'] = 'create_city';
        
        $output = $this->load->view('location/ajax_view',$this->data,true);
        
            $resp['status']=1;
            $resp['message']='City Created';  
            $resp['newstate']=$output; 
        echo json_encode($resp);
        
    }
    
    function new_dist_city(){
        
        $this->data['country_id'] = $data['c_id'] = $this->input->post('cid');
         $this->data['state_id'] = $data['s_id'] = $this->input->post('stateid');
         $this->data['d_id'] = $data['d_id'] = $this->input->post('districtId');
        $this->data['city'] = $data['name'] = $this->input->post('city');

         $this->data['countryname'] = $this->input->post('cname');
         $this->data['statename'] = $this->input->post('statename');
         $this->data['districtName'] = $this->input->post('districtName');
         
         $this->data['city_id'] = $this->location_model->create_district_city($data);
         $this->data['type'] = 'create_district_city';
         
         $output = $this->load->view('location/ajax_view',$this->data,true);
         
             $resp['status']=1;
             $resp['message']='City Created';  
             $resp['newstate']=$output; 
         echo json_encode($resp);
         
     }


    function update_city(){
        
       $this->data['country_id'] = $data['country_id'] = $this->input->post('cid');
        $this->data['state_id'] = $data['state_id'] = $this->input->post('stateid');
       $this->data['city'] = $data['city'] = $this->input->post('city');
        $this->data['countryname'] = $this->input->post('cname');
        $this->data['statename'] = $this->input->post('statename');
        $this->data['city_id'] =$con['id'] = $this->input->post('city_id');
        
        $update = $this->location_model->update_city($data,$con);
        $this->data['type'] = 'update_city';
        
        $output = $this->load->view('location/ajax_view',$this->data,true);
        
            $resp['status']=1;
            $resp['message']='City Updated';  
            $resp['newstate']=$output; 
        echo json_encode($resp);
        
    }


    
    function update_district_city(){
        
        $this->data['country_id'] = $data['c_id'] = $this->input->post('cid');
        $this->data['state_id'] = $data['s_id'] = $this->input->post('stateid');
        $this->data['d_id'] = $data['d_id'] = $this->input->post('districtId');
       $this->data['city'] = $data['name'] = $this->input->post('city');

        $this->data['countryname'] = $this->input->post('cname');
        $this->data['statename'] = $this->input->post('statename');
        $this->data['districtName'] = $this->input->post('districtName');

         $this->data['city_id'] =$con['id'] = $this->input->post('cityId');
         
         $update = $this->location_model->update_district_city($data,$con);
         $this->data['type'] = 'update_district_city';
         
         $output = $this->load->view('location/ajax_view',$this->data,true);
         
             $resp['status']=1;
             $resp['message']='City Updated';  
             $resp['newstate']=$output; 
         echo json_encode($resp);
         
     }
    
    function delete_city($id =''){
    if($this->db->delete('city',array('id'=>$id))){
            echo 1;
        }else{
            echo "Something error try again";
        }
    }
    
    
    
    
}



