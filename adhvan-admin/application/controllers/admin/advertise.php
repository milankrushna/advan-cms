<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Advertise extends CI_Controller {

	
	
function __construct()
	{
		parent::__construct();
		
		
		
		$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('ad_model');
		$this->load->database();
		$this->lang->load('auth');
		$this->load->helper('language');
		$sesdata = $this->session->userdata;
		date_default_timezone_set("Asia/Kolkata");	
		 
		 $this->data['webdata'] = $this->db->get_where('my_website',array('id'=>'1'))->row();
	if(!$this->ion_auth->logged_in())
    {
      redirect('admin/auth/login', 'refresh');
    }
    $this->data['menu']='advertise';
	}
		
	
	public function index()
	{
		$this->data['data']=$this->ad_model->get_advertise();

		$this->data['title'] = "Advertise Listing";
			 	$this->load->view('common_admin/header',$this->data);
                $this->load->view('advertise/listing_ad',$this->data);
                $this->load->view('common_admin/footer');
	}
	
	public function create_new_ad()
	{

		$this->data['title'] = "Create Advertise";
		$this->form_validation->set_rules('title', "Title", 'required');
		$this->form_validation->set_rules('position', "Position", 'required');
		//$this->form_validation->set_rules('start_date', "Start Date", 'required');
		//$this->form_validation->set_rules('end_date', "End Date", 'required');
	if ($this->form_validation->run() == TRUE)
		{

$data['position'] = $this->input->post('position');
$data['type'] = $type = $this->input->post('type');
$data['page']  = $this->input->post('page');
$data['display_web']  = $this->input->post('web');
$data['display_mob']  = $this->input->post('mobile');

  
        
$data['entry_date'] = date('Y-m-d');
$data['title'] = $this->input->post('title');
if($type==1){
$data['link'] = $this->input->post('link');
$data['ad_pic'] = $this->upload_image();
}else if($type==2){
$data['script'] = $this->input->post('script');
		}
  
        
       
        
        
	if($this->ad_model->insert_ad($data)){
		$this->session->set_flashdata('msg',"advertise created successfully");
			redirect('admin/advertise');
        
       
		}else{
$this->session->set_flashdata('msg',"Something error try again");
redirect('admin/advertise/create_new_ad');
		}
		}else{

				$this->load->view('common_admin/header',$this->data);
                $this->load->view('advertise/create_ad',$this->data);
                $this->load->view('common_admin/footer');
		}
	}
	


function upload_image(){
	$uniqe =  now();
   
        $new_img =  $uniqe."_".$_FILES['ad_image']['name'];
    //$agent_id=$this->session->userdata['user_id'];
    
        $tmpName  = $_FILES['ad_image']['tmp_name'];  
       
     //$this->dashboard_model->update_image($agent_id,$new_img);
        $data =array( 'upload_data' => move_uploaded_file($tmpName,"./upload/ad_image/$new_img"));
        return $new_img;

}
    
    
	function edit_advertise($id=NULL){
	$this->data['data']=$this->ad_model->get_advertise2($id);
	$this->data['title'] = "Edit Advertise";
		$this->form_validation->set_rules('title', "Title", 'required');
		$this->form_validation->set_rules('position', "Position", 'required');
		//$this->form_validation->set_rules('start_date', "Start Date", 'required');
		//$this->form_validation->set_rules('end_date', "End Date", 'required');
	if ($this->form_validation->run() == TRUE)
		{

$data['position'] = $this->input->post('position');
$data['type'] = $type = $this->input->post('type');
$data['page']  = $this->input->post('page');
$data['display_web']  = $this->input->post('web');
$data['display_mob']  = $this->input->post('mobile');

$data['entry_date'] = date('Y-m-d');
$data['title'] = $this->input->post('title');
if($type==1){
$data['link'] = $this->input->post('link');
if($_FILES['ad_image']['name'] !==""){
$data['ad_pic'] = $this->upload_image();      
}
}else if($type==2){
$data['script'] = $this->input->post('script');
		}

	if($this->ad_model->update_ad($data,$id)){
		$this->session->set_flashdata('msg',"advertise updated successfully");
			redirect('admin/advertise/edit_advertise/'.$id);
		}else{
$this->session->set_flashdata('msg',"Something error try again");
redirect('admin/advertise/edit_advertise/'.$id);
		}
		}else{

				$this->load->view('common_admin/header',$this->data);
                $this->load->view('advertise/edit_ad',$this->data);
                $this->load->view('common_admin/footer');
		}





	}


	function publish(){

		$id = $this->input->post('id');
		$data['status'] = 1;
		if($this->ad_model->update_ad($data,$id)){
			echo 1;
		}else{
			echo "";
		}

	}
	function unpublish(){

		$id = $this->input->post('id');
		$data['status'] = 0;
		if($this->ad_model->update_ad($data,$id)){
			echo 1;
		}else{
			echo "";
		}

	}
	
function delete_advertise($id=NULL){
	if($this->db->delete('advertise',array('id'=>$id))){
		redirect('admin/advertise');
	}
}



}
