<?php defined('BASEPATH') OR exit('No direct script access allowed');

class File_upload extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
		
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->model('page_model');
		$this->load->database();  
		$this->lang->load('auth');
		$this->load->helper('language');
		$this->header['menu']=2;
               $this->header['webdata'] = $this->db->get_where('my_website',array('id'=>'1'))->row();

		 if (!$this->ion_auth->logged_in() && !$this->ion_auth->is_admin())
    {
      redirect('admin/auth/login', 'refresh');
    }
	}
	
	
	public function filetoupload(){
		
		
		$this->load->library('session');	
		$this->form_validation->set_rules('valfld', 'Pdf','required');
		
		if ($this->form_validation->run() == TRUE)
		{
		
            
            
		$image = $this->upload_image('pdf_file');
		if($image!=false){
	 	redirect('admin/file_upload/display_file');
	 	}else{
		
$this->load->view('common_admin/header',$this->header);
		$this->load->view('file_upload/upload_file');
		$this->load->view('common_admin/footer');
		///redirect('admin/file_upload/filetoupload');		
				
		} 
	 	
	  }else{
	  	
	  	$this->load->view('common_admin/header',$this->header);
		$this->load->view('file_upload/upload_file');
		$this->load->view('common_admin/footer');
	  }
	
	
	}  
	
	
	
	            
/**
* @name          upload image
* @author        Subhashree Jena
* @revised       null
* @Description   upload image
* @Param String  $field is input type field name
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
	
	
public function upload_image($field){
 
$this->load->library('session');
        //$new_img =  strtotime("now").'.jpg';
        $config['upload_path'] = './file_upload/';
        $config['allowed_types'] = 'pdf|PDF|jpg|JPG|JPEG|jpeg|PNG|png|DOCX|docx';
        $config['max_size']	= '30000';
        $config['max_width'] = '102400';
        $config['max_height'] = '70000';
        $config['overwrite'] = FALSE;
        //$config['file_name'] = $new_img;

$this->load->library('upload', $config);
$this->upload->initialize($config);
 if($this->upload->do_upload($field)){
     
  // $this->resize_image($new_img);
   $data = array('upload_data' => $this->upload->data());
      $this->session->set_flashdata('mssage','File Successfully Uploaded'); 
     return $data['upload_data']['file_name'];

 }else{
 $erdata = $this->upload->display_errors();
  $this->session->set_flashdata('message','error');
     return false; 
     
 }

  
}

/**
* @name          display file
* @author        Milan Krushna
* @revised       null
* @Description   display uploaded file
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
	



function display_file(){
	
	$this->load->helper('directory');
	$this->data['filedata'] = directory_map('./file_upload/');
	//print_r($map);
	
	
	  	$this->load->view('common_admin/header',$this->header);
		$this->load->view('file_upload/view_file',$this->data);
		$this->load->view('common_admin/footer');
	
}     



/**
* @name          delete file
* @author        Subhashree Jena
* @revised       null
* @Description   delete file 
* @Param String  $filedata is delete file name in file_upload
* @copyright     Copyright (C) 2017 powered by Wayindia
*/
	


function delete_file($filedata){
	
	unlink('./file_upload/'. $filedata);
	redirect('admin/file_upload/display_file');
	
}






  
 }
