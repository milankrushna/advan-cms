<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Team extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->model('team_model');
		$this->load->database();  
		$this->lang->load('auth');
		$this->load->helper('language');
		$this->header['menu']='team';
               $this->header['webdata'] = $this->db->get_where('my_website',array('id'=>'1'))->row();

		 if (!$this->ion_auth->logged_in() && !$this->ion_auth->is_admin())
    {
      redirect('admin/auth/login', 'refresh');
    }
	}
 
    function index($msg=NULL)
	{
		if($msg == '1'){
				$this->data['msg'] = "<p style='color:red'>Event Deleted Successfully!</p>";
			}else if($msg == '2'){
					$this->data['msg'] = "<p style='color:red'>Unable To Delete The Event!<br>It has Images inside </p>";
			}else if($msg == '3'){
					$this->data['msg'] = "<p style='color:red'>You Dont have Access to delete!</p>";
			}else{
				$this->data['msg'] = "<p style='color:green'>Listing of Team!</p>";
			}
	
	
		$this->data['team'] = $this->team_model->get_team();
		
		/*print("<pre>");
		print_r($this->data['events']);
		exit;*/
		
		
		$this->load->view("common_admin/header",$this->header);
		$this->load->view("team/view_team",$this->data);
		$this->load->view("common_admin/footer");	

	}

    function create_team()
	{
	//	echo "i am here";exit;
		$this->data['title'] = "Add Team";
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('admin/auth', 'refresh');
		}

		$this->form_validation->set_rules('name', "Name", 'required|is_unique[team.name]');

		if ($this->form_validation->run() == TRUE)
		{
			
			$data['name'] = $this->input->post('name');
            $data['designation	'] = $this->input->post('designation');
			
			$data['description'] =  $this->input->post('description');
			
           if($_FILES['file']['name'] !=""){
			$upload = $this->upload_image('file');
            if($upload != ""){
                $data['cover_pic'] = $upload;
            }else{
               redirect('admin/team/create_team');
            }
            }
			
			$team =  $this->team_model->create_team($data);
			if($team == 1)
			{
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("admin/team");
			}
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		$this->load->view("common_admin/header",$this->header);
		$this->load->view("team/create_team",$this->data);
		$this->load->view("common_admin/footer");	
		}
	}
    
    function edit_team($id)
	{
		$this->data['team'] =  $this->team_model->edit_team($id);
		
		$this->data['title'] = "Edit Team";
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('admin/auth', 'refresh');
		}
		//validate form input
		$this->form_validation->set_rules('name', "Name", 'xss_clean');
		if ($this->form_validation->run() == TRUE)
		{
		
			$data['name'] = $this->input->post('name');
            $data['designation'] = $this->input->post('designation');
			
			$data['description'] =  $this->input->post('description');
			
            if($_FILES["file"]["name"] !=""){
			$upload = $this->upload_image('file');
            if($upload != ""){
                $data['cover_pic'] = $upload;
            }else{
               redirect("admin/team/edit_team/".$id); 
            }
            }
            
            
			$team_member =  $this->team_model->update_team($data,$id);
			//echo $new_category_id;exit;
			if($team_member)
			{
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("admin/team/edit_team/".$id);
			}
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
		$this->load->view("common_admin/header",$this->header);

		$this->load->view("team/edit_team",$this->data);

		$this->load->view("common_admin/footer");	
		}
	}

  function delete_team($id){
	$team_delete = $this->team_model->delete_team($id);
	redirect("admin/team");
}

    
    public function upload_image($field){
 
        $new_img =  now().'.jpg';
        $config['upload_path'] = './upload/events/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']	= '30000';
        $config['max_width'] = '102400';
        $config['max_height'] = '70000';
        $config['file_name'] = $new_img;

$this->load->library('upload', $config);
$this->upload->initialize($config);
 if($this->upload->do_upload($field)){
     
   $this->resize_image($new_img);
   $data = array('upload_data' => $this->upload->data());
     
     return $data['upload_data']['file_name'];
  
 }else{
  
  $this->session->set_flashdata('message',$this->upload->display_errors());
     return false; 
     
 }

  
}
  
    function resize_image($new_img){
        
        
      $this->load->library('image_lib');
 


            $src_path = 'upload/events/' . $new_img;
            $des_path =  'upload/events/min_events/' . $new_img;
 
            $config['image_library']    = 'gd2';
            $config['source_image']     = $src_path;
            $config['new_image']        = $des_path;
            $config['maintain_ratio']   = TRUE;
            $config['width']            = 241;
            $config['height']           = 179;
            

            $this->image_lib->initialize($config);
            $this->image_lib->resize();
        
    }
    
     
    
    function actdeact($id=""){
        
        $sts = $this->input->post('sts');
        
        if($sts == 0){
            $upsts = 1; 
        }else{
            $upsts = 0;
        }
      
        if($this->db->update('team',array('status'=>$upsts),array('id'=>$id))){
          echo $upsts;  
        }
        
        
    }
    
 
}
