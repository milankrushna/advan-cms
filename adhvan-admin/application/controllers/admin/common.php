<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->model('common_model');
		$this->load->database();
        
		$this->header['menu'] = "common";
               $this->header['webdata'] = $this->db->get_where('my_website',array('id'=>'1'))->row();

		 if (!$this->ion_auth->logged_in() && !$this->ion_auth->is_admin())
    {
      redirect('admin/auth/login', 'refresh');
    }
	}
 




/**
* @name          index
* @author        Milan Krushna
* @revised       null
* @Description   display all the common section listing
* @Param String  $msg is declared null, if msg is coming then link are activate
* @copyright     Copyright (C) 2017 powered by Wayindia
*/


 
 function index($msg=NULL){
 	
		if($msg == '1'){
				$this->data['msg'] = "<p style='color:red'>Page Deleted Successfully!</p>";
			}else if($msg == '2'){
					$this->data['msg'] = "<p style='color:red'>Unable To Delete The Page!<br>It has been published</p>";
			}else if($msg == '3'){
					$this->data['msg'] = "<p style='color:red'>You Dont have Access to delete!</p>";
			}else{
				$this->data['msg'] = "<p style='color:green'>Listing of Pages!</p>";
			}
	
		$this->data['pages'] = $this->common_model->get_common()->result();
		
		$this->load->view("common_admin/header",$this->header);
		$this->load->view("pages/view_common",$this->data);
		$this->load->view("common_admin/footer");	
		
 }


/**
* @name          add section
* @author        Milan Krushna
* @revised       null
* @Description   create new section
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/



function create_common()
	{
	//	echo "i am here";exit;
		$this->data['title'] = "Create Common";
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('admin/auth', 'refresh');
		}

		$this->form_validation->set_rules('name', "Section Name", 'required|is_unique[common.name]');
		if ($this->form_validation->run() == TRUE)
		{
            $name = $this->input->post('name');
			$data['name'] = $name;
			$data['url'] = $this->create_url($name);
			$data['content'] = $this->input->post('txt_content');	
			
			$section_id =  $this->common_model->create_common($data);
		
			if($section_id)
			{
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("admin/common/index");
			}
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		$this->load->view("common_admin/header",$this->header);
		$this->load->view("pages/create_common",$this->data);
		$this->load->view("common_admin/footer");	
		}
	}
	

/**
* @name          edit section
* @author        Milan Krushna
* @revised       null
* @Description   edit common section
* @Param String  $id is id of common table
* @copyright     Copyright (C) 2017 powered by Wayindia
*/


	function edit_common($id)
	{
		$this->data['page'] =  $this->common_model->edit_common($id);		
		
		/*print("<pre>");
		print_r($this->data['page']);
		exit;*/
		
		$this->data['title'] = "Edit Common";
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('admin/auth', 'refresh');
		}
		//validate form input
		$this->form_validation->set_rules('title', "Title", 'xss_clean');
		if ($this->form_validation->run() == TRUE)
		{
		
			$data['name'] = $this->input->post('name');
			$data['content'] = $this->input->post('txt_content');	
			//$data['en_slider'] = $this->input->post('en_slider');	
			
			$common_id =  $this->common_model->update_common($data,$id);			
			
			//echo $new_category_id;exit;
			if($common_id)
			{
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("admin/common/index");
			}
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
		$this->load->view("common_admin/header",$this->header);
		$this->load->view("pages/edit_common",$this->data);
		$this->load->view("common_admin/footer");	
		}
	}
	
	
/**
* @name          delete common section
* @author        Milan Krushna
* @revised       null
* @Description   delete common section
* @Param String  $id is id delete in common table
* @copyright     Copyright (C) 2017 powered by Wayindia
*/


	
function delete_common($id){
	$page_id =  $this->common_model->delete_common($id);	
	redirect("admin/common/index");
}



/**
* @name          create url
* @author        Milan Krushna
* @revised       null
* @Description   create unique url 
* @Param String  $strin is url are in string
* @copyright     Copyright (C) 2017 powered by Wayindia
*/


    
    
    protected function create_url($strin){
        
        return rtrim(strtolower(str_replace(' ','-',preg_replace('/\s\s+/','-',preg_replace('/[^A-Za-z0-9\-]/', ' ',$strin)))),'/[^A-Za-z0-9\-]/');
    }


}