<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {



function __construct()
  {
    parent::__construct();
		//header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");

    $this->load->library('ion_auth');

    $this->load->library('form_validation');
    $this->load->helper('url');
    //$this->load->model('admin/page_model');
    $this->load->library('session');
    $this->load->helper(array('form', 'url'));
    date_default_timezone_set("Asia/Kolkata");

    //$this->load->library('pagination');
    if (!$this->ion_auth->logged_in())
    {
      redirect('admin/auth', 'refresh');
    }


    $this->header['menu']=122;
   $this->header['webdata'] = $this->db->get_where('my_website',array('id'=>'1'))->row();

  }


/**
* @name          index
* @author        Milan Krushna
* @revised       null
* @Description   display dashboard
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/




        function index()
            {


                $this->data['website_data'] = $this->web_data();
           $this->form_validation->set_rules('title', 'Website Title', 'required');
                if ($this->form_validation->run() == TRUE)
                    {

                    $data['title'] = $this->input->post('title');
                    $data['name'] = $this->input->post('name');
                    $data['meta_title'] = $this->input->post('meta_title');
                    $data['meta_description'] = $this->input->post('meta_description');
                   // $data['robots'] = $this->input->post('robots');
                   // $data['meta_author'] = $this->input->post('meta_author');
                   // $data['rating'] = $this->input->post('rating');
                   // $data['language'] = $this->input->post('language');
                   // $data['y_key']=$this->input->post('y_key');
                   // $data['verify_v1'] = $this->input->post('verify_v1');
                  //  $data['msvalidate'] = $this->input->post('msvalidate');
                   // $data['netinsert'] = $this->input->post('netinsert');
                  ///  $data['content_type'] = $this->input->post('content_type');
                  ///  $data['copyright'] = $this->input->post('copyright');
                 //   $data['google_site_verification'] = $this->input->post('google_site_verification');
                 //   $data['viewport'] = $this->input->post('viewport');
                 //   $data['appsts'] = $this->input->post('appsts');
                 //   $data['app_cpl'] = $this->input->post('app_cpl');
                 //   $data['admin_panel_name'] = $this->input->post('admin_panel_name');

                //    $data['description'] =  $this->input->post('description');
                    $data['address'] =  $this->input->post('address');
                    $data['email'] =  $this->input->post('email');
                    $data['phone'] =  $this->input->post('phone');
                    $data['facebook'] =  $this->input->post('facebook');
                    $data['google'] =  $this->input->post('google');
                    $data['twitter'] =  $this->input->post('twitter');
                    $data['tumblr'] =  $this->input->post('tumblr');
                    $data['pinterest'] =  $this->input->post('pinterest');
                    $data['youtube'] =  $this->input->post('youtube');
                    $data['instagram'] =  $this->input->post('instagram');
                    $data['update_date'] =  date('Y-m-d');

                 
                     if($_FILES["logo"]["name"] !=""){
                       $logo  = $this->upload_image('logo');
                     if($logo != false){
                        $data['logo'] = $logo;
                     }
                    }




                if($this->create_website($data)){
               $this->session->set_flashdata('msg','Your Website Data Successfully Updated');
               redirect('admin/dashboard');
                }

    }
    else
    {



                $this->load->view('common_admin/header',$this->header);
                $this->load->view('admin/about',$this->data);
                $this->load->view('common_admin/footer');
    }

  }




/**
* @name          web data
* @author        Milan Krushna
* @revised       null
* @Description   get website data
* @Param String  null
* @copyright     Copyright (C) 2017 powered by Wayindia
*/




    function web_data(){

        return $this->db->get('my_website')->row();

    }



/**
* @name          create website
* @author        Milan Krushna
* @revised       null
* @Description   create website data in dashboard
* @Param String  $data is data are in array format
* @copyright     Copyright (C) 2017 powered by Wayindia
*/


    function create_website($data){

       return $this->db->update('my_website',$data,array('id'=>'1'));



    }


/**
* @name          upload image
* @author        Milan Krushna
* @revised       null
* @Description   image uploading and resize 
* @Param String  $field is input type field name
* @copyright     Copyright (C) 2017 powered by Wayindia
*/





        public function upload_image($field){

$new_img =  'logo.png';
$config['upload_path'] = './assets/user/images/';
$config['allowed_types'] = 'gif|jpg|jpeg|png|JPEG|JPG';
$config['overwrite']	= TRUE;
$config['max_size']	= '1024';
$config['max_width'] = '1024';
$config['max_height'] = '720';
$config['file_name'] = $new_img;

$this->load->library('upload', $config);
$this->upload->initialize($config);

 if($this->upload->do_upload($field)){

  /// $this->resize_image($new_img);
   $data = array('upload_data' => $this->upload->data());

     return $data['upload_data']['file_name'];

 }else{


  $this->session->set_flashdata('message',$this->upload->display_errors());
     return false;

 }


}

    function resize_image($new_img){


      $this->load->library('image_lib');


            $src_path = 'assets/user/images/' . $new_img;
            $des_path =  'assets/user/images/ico/favicon.ico';

            $config['image_library']    = 'gd2';
            $config['source_image']     = $src_path;
            $config['new_image']        = $des_path;
            $config['maintain_ratio']   = TRUE;
            $config['width']            = 64;
            $config['height']           = 64;

            $this->image_lib->initialize($config);
            $this->image_lib->resize();


    }





}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */
