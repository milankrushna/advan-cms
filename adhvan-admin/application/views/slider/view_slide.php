  <link href="<?= base_url();?>/assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" />

                   
   <div class="content-wrapper">
               
              <section class="content-header">
          <h1>Slider Listing</h1>
         
        </section>
        <section class="content">
             <a class="btn btn-primary" href="<?= site_url('admin/slider/create_slide') ?>">New Slide</a>
<div class="box box-primary">
<div class="box-body" >
                       <!-- <?php echo $msg; ?>-->
						<div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                        
                                            <th>Title</th>
                                            <th>Image</th>
											<th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
          <?php foreach ($galleries as $gall):
		  ?>
		<tr class="odd gradeX">

			
			<td><?php echo $gall['title'] ; ?></td>
			<td>	
 <img src="<?php echo base_url('upload/gallery/slider/min_slider/'.$gall['image']); ?>" >                     
	</td>
			<td align="center"> <a class="btn btn-warning" href="<?php echo site_url('admin/slider/edit_slider'."/".$gall['id'])?>" ><i class="fa fa-edit"></i></a>||<a title="Delete" class="btn  btn-danger" href="<?php echo site_url('admin/slider/delete_slider'."/".$gall['id'])?>" onclick="return confirm('Are You sure?');"><i class="fa fa-trash"></i></a>
            </td>
     
            
            
            
     	
	</tr>
		<?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                   
            </div>
</div>
</section>
</div>-->
    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
   <script src="<?= base_url();?>/assets/plugins/datatables/jquery.dataTables.js"></script>

    <script src="<?= base_url();?>/assets/plugins/datatables/dataTables.bootstrap.js"></script>


 