   <link href="<?= base_url();?>/assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" />

                   
    <div class="content-wrapper">
               
              <section class="content-header">
          <h1>Slider Listing</h1>
         
        </section>
        <section class="content">
             <a class="btn btn-primary" href="<?= site_url('admin/slider/add_slide') ?>">New Slide</a>
<div class="box box-primary">
<div class="box-body" >
                        <?php echo $msg; ?>
						<div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Type</th>
                                            <th>Title</th>
                                            
                                            <th>Image</th>
											<th>Link</th>
											<th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
          <?php foreach ($galleries as $gall):
		  ?>
		<tr class="odd gradeX">

			<td><?php echo $gall->type ; ?></td>
			<td><?php echo $gall->title ; ?></td>
			
			<td><img src="<?= base_url();?>/upload/gallery/slider/min_slider/<?php echo $gall->image;?>"   width="150px"  /></td>
						<td><?php echo $gall->link ; ?></td>
			<td align="center"><?php echo anchor("admin/slider/edit_banner/".$gall->id, 'Edit') ;?> | <?php echo anchor("admin/slider/delete_slider/".$gall->id, 'Delete') ;?></td>
		</tr>
		<?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                   
            </div>
</div>
</section>
</div>
    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?= base_url();?>/assets/plugins/datatables/jquery.dataTables.js"></script>

    <script src="<?= base_url();?>/assets/plugins/datatables/dataTables.bootstrap.js"></script>

     <script>

         $(document).ready(function () {

             $('#dataTables-example').dataTable();

         });

    </script>