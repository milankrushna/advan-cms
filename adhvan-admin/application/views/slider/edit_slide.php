  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/ajxupload/upload.css"  />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/ajxupload/jquery.form.js" ></script>
   <div class="content-wrapper ">
     <section class="content-header">
          <h1>Add Section</h1>
        </section>
  <section class="content"> 
<div class="box box-primary">
<div class="box-body">
<div id="infoMessage" style="color:#F00"><?php echo $message;?></div>



			<form   method="post" action="<?= site_url('admin/slider/edit_slider').'/'.$slide->id ;?>" enctype="multipart/form-data" id="editslide" >
                
                    
               
                
		 		<div class="form-group">
			      <label for="text1" class="control-label">Title</label>
                 
						<input  type="text" name="title" class="form-control" value="<?php echo $slide->title; ?>" />
                    
                	</div>

                	
                	<div class="form-group">
                    <label for="text1" class="control-label">Image<br></label>
                            
                                  <input  type="file" class="form-control" name="uploadimage" ><br>
            
                	</div>	
                	
         <div style="padding-bottom: 10px;">       	
    <img src="<?= base_url();?>/upload/gallery/slider/min_slider/<?php echo $slide->image;?>" />         
         </div>                           
           
                        					    
    <!--       Progress Bar-->
       <div id="p_bar" style="display:none;" class="hide">
           <div  class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" id="progressbar" style="width:0%">
                    <span id="statustxt"></span>
                </div>
           </div>
       </div>
       <!--       Progress Bar-->
					
           
           
                	
     <p>    
    <input type="submit"  value="Update" class="btn btn-success" />
	 </p>  
	 
	 </form>
    </div>
    </div>
    </section>
    </div>
    
       <script>


 $(document).ready(function() {
        //elements
        var progressbox     = $('#progressbox');
        var progressbar     = $('#progressbar');
        var statustxt       = $('#statustxt');
        var submitbutton    = $("#SubmitButton");
        var myform          = $("#editslide");
        var output          = $("#output");
        var completed       = '0%';
 
                $(myform).ajaxForm({
                    beforeSend: function() { //brfore sending form
                        document.getElementById("p_bar").className = "show";
                        submitbutton.attr('disabled', ''); // disable upload button
                        statustxt.empty();
                        progressbox.slideDown(); //show progressbar
                        progressbar.width(completed); //initial value 0% of progressbar
                        statustxt.html(completed); //set status text
                        statustxt.css('color','#fff'); //initial color of status text
                    },
                    uploadProgress: function(event, position, total, percentComplete) { //on progress
                        progressbar.width(percentComplete + '%') //update progressbar percent complete
                        statustxt.html(percentComplete + '%'); //update status text
                        if(percentComplete>50)
                            {
                                statustxt.css('color','#fff'); //change status text to white after 50%
                            }

                        },
                    complete: function(response) { // on complete
                        var res = output.html(response.responseText); //update element with received data
                        myform.resetForm();  // reset form
                        submitbutton.removeAttr('disabled'); //enable submit button
                        progressbox.slideUp(); // hide progressbar
                        document.getElementById("p_bar").className = "hide";
                        
                        
if(response.responseText == 1){
alert("Successfully Upload");
}else if(response.responseText == 2){
alert("Please Upload a valid Image File");
}else if(response.responseText == 3){
alert("You have no rights to upload");
}else{
alert("Uploading unsuccessful Please Try again");
}
//location.reload();
                       /// location.reload();
                    }
                   
            });
        });


</script>
