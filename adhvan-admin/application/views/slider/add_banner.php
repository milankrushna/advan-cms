<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/ajxupload/upload.css"  />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/ajxupload/jquery.form.js" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jscolor/jscolor.min.js" ></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!--PAGE CONTENT --> 
  <div class="content-wrapper">
               
              <section class="content-header">
          <h1>Add Banner</h1>
         
        </section>
        <section class="content ">
<div class="box box-primary">
<div class="box-body" >  

<div id="infoMessage" style="color:#F00"><?php echo $this->session->flashdata('message'); ?></div>
    <div class="col-md-7">
<form  id="imgUpload" method="post" action="<?php   echo current_url(); ?>" enctype="multipart/form-data">
				    
                   <!-- <div class="form-group">
                    <label for="text1" class="control-label">Type</label>
                        <select class="form-control" name="slider[type]" required>
                            <option value="">Select type</option>
                            <option value="top">Top</option>
                            <option value="bottom">Bottom</option>
                        </select>
                     
                	</div>-->
    <input type="hidden" name="slider[type]" value="Top">
    
                <!--<div class="form-group">
                    <label for="text1" class="control-label">Slider</label>
                        <select class="form-control" name="slider[parent_id]"  required>
                            <option value="">Select a Slider</option>
                            
                            <?php foreach($slider as $sl){ ?>
                            <option value="<?php echo $sl['id']; ?>"><?php  echo $sl['title']; ?></option>
                            <?php } ?>
                            
                        </select>
                	</div>-->
                                
     <input type="hidden" name="slider[parent_id]]" value="1">
    
    
    
                    <div class="form-group">
                    <label for="text1" class="control-label">Title</label>
                   
							<input type="text" name="slider[title]" class="form-control" >
                     
                	</div>

 					<div class="form-group">
                    
					<label for="text1" class="control-label"> Caption</label>
                    
						
							<textarea class="form-control" name="slider[caption]" ></textarea>							
                    
                	</div>
    
                 <div class="form-group">
                 <label for="text1" class="control-label">Caption Color</label>
                 
                    <div class="input-group my-colorpicker2">
                      <input type="text" class="jscolor" name="slider[capt_clr]" value="000">
                      
                      
                    </div>
                    
                  </div>

                  
    
        

 			
                 <div class="form-group">
				    <label for="text2" class="control-label">Cover Pic</label>
					 <input type="file" name="file" class="form-control">
                     <p style="color:red" >Image Size should be 1300x620 </p>
                	</div>
					
					 <div class="form-group">
                    <label for="text1" class="control-label">Link</label>
							<input type="text" name="slider[link]" class="form-control" >
                	</div>
	    
    <!--       Progress Bar-->
       <div id="p_bar" style="display:none;" class="hide">
           <div  class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" id="progressbar" style="width:0%">
                    <span id="statustxt"></span>
                </div>
           </div>
       </div>
       <!--       Progress Bar-->
    
                        <input type="submit" id="SubmitButton" value="Submit" class="btn btn-success" />
                 
	</form>
    </div>  
    </div>
           </div>
</section>
              </div>

                    <!-- END PAGE CONTENT -->

