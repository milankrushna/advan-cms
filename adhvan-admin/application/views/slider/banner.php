
    <!-- END PAGE LEVEL  STYLES -->
     <!--PAGE CONTENT --> 
  
<div class="content-wrapper">
     <section class="content-header">
        <h1>Banner</h1>
         <br>
          <a class="btn btn-primary" href="<?php echo site_url('admin/slider/create_banner')?>" >Add Banner</a>
      </section>
<section class="content">
<div class="box box-primary ">
<div class="box-body " >

    <div class="col-md-6">
    
<div id="infoMessage" style="color:#F00"><?php echo $this->session->flashdata('message');  ?></div>
        
        <input type="hidden" name="slider_id" value="1" >
    
    <input type="hidden" name="position" value="Top" >

<!--<form action="<?php echo current_url(); ?>" method="post" id="loginform">-->

 			<!--<div class="form-group col-md-5">
                    <label for="text1" class="control-label">Slider</label>
                        <select  class="form-control" name="slider_id"  required>
                            <option value="">Select a Slider</option>
                            
                            <?php foreach($slider as $sl){ ?>
                            <option <?php  if(!empty($sl_id)){   if($sl_id == $sl['id']){  echo "Selected"; } } ?> value="<?php echo $sl['id']; ?>"><?php  echo $sl['title']; ?></option>
                            <?php } ?>
                            
                        </select>
                	</div>-->
    
    
          <!--  <div class="form-group col-md-5"> 
                 <label for="text1" class="control-label">Position</label> 
                        <select class="form-control" name="position" required>
                            <option value="">Select type</option>
                            <option <?php  if(!empty($position)){   if($position == 'top'){  echo "Selected"; } } ?> value="top">Top</option>
                            <option <?php  if(!empty($position)){   if($position == 'bottom'){  echo "Selected"; } } ?> value="bottom">Bottom</option> 
                        </select>
                      
                	</div>-->
    <!--<div class="form-group col-md-2" style="padding-top: 24px;"> 
           <label for="text1" class="control-label"> </label>       
     <button  type="submit"  id="tags"  class="btn btn-success pull-right form-control" style="padding-left: 5px">Submit</button>
                	</div>-->
                	<!--</form>-->
    </div>
    
    <div style="clear:both;"></div>
    
    <div class="col-md-12" >
       <?php if(!empty($sl_id) && !empty($position)){ ?>
        <a class="btn btn-success" href="<?php echo site_url('admin/slider/create_banner') ?>" >New Banner</a>
        <?php } ?>
<?php  if(!empty($banner)){   
        ?>
        
   <div class="box box-primary">
<div class="box-body" >
                        <?php ////echo $msg; ?>
						<div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Type</th>
                                            <th>Title</th>
                                            
                                            <th>Image</th>
											<th>Status</th>
											<th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="sort_slider">
          <?php foreach ($banner as $gall):
		  ?>
		<tr class="odd gradeX" id="slider_<?php echo $gall->id; ?>">

			<td><?php echo $gall->type; ?></td>
			<td><?php echo $gall->title; ?></td>
			
			<td><img src="<?php echo base_url();?>/upload/gallery/slider/min_slider/<?php echo $gall->image;?>"   width="150px"  /></td>
				<td>
<?php 
         if($gall->status == 0){ $anc = 'Deactive'; }else{ $anc = 'Active'; }
    
                          ///  echo anchor('admin/slider/actdeact/'.$gall->id, $anc,array('class' => 'act-deact','title'=>$gall->status));    ?>
                    <a href="<?php echo site_url('admin/slider/actdeact/'.$gall->id); ?>"  class="act-deact" title="<?php echo $gall->status; ?>" ><?php echo $anc; ?></a>
                    
</td>
            
            
			<td align="center"> <a class="btn btn-warning" href="<?php echo site_url('admin/slider/edit_banner'."/".$gall->id)?>" ><i class="fa fa-edit"></i></a>||<a title="Delete" class="btn  btn-danger" href="<?php echo site_url('admin/slider/delete_slider'."/".$gall->id)?>" onclick="return confirm('Are You sure?');"><i class="fa fa-trash"></i></a></td>
            
            
            
            
            
		</tr>
		<?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                   
            </div>
</div>
        
        
        
        

<script src="<?php echo base_url()?>assets/plugins/jQueryUI/jquery-ui.js"></script>
<script>
$(document).ready(
function() {
$("#sort_slider").sortable({
update : function () {
serial = $('#sort_slider').sortable('serialize');
    
    
 
$.ajax({
url: "<?php echo site_url('admin/slider/save_order'); ?>",
type: "post",
data: serial,
error: function(){
alert("theres an error with AJAX");
}
});
    
}
});
}
);


 $('.act-deact').click(function(ev){
             ev.preventDefault();
            
             var dis = this;
             $(dis).html("wait..");
             $.post($(this).attr('href'),{'sts':$(this).attr('title')},function(resp){
                 
                 if(resp == 0){
                    $(dis).html("Deactive");
                    $(dis).attr("title",resp);
                 }else if(resp == 1){
                    $(dis).html("Active");
                    $(dis).attr("title",resp);
                 }
                 
             });  
         });
         

</script>      
        
        
        
        
        
        
        
        
        
        <?php } ?>
    </div>
    
                	</div>
    </div>
           
           </section>
           </div>

              

            
