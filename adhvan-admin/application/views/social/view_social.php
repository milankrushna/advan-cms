     
    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?= base_url();?>/assets/plugins/dataTables/jquery.dataTables.js"></script>

    <script src="<?= base_url();?>/assets/plugins/dataTables/dataTables.bootstrap.js"></script>

     <script>

         $(document).ready(function () {

             $('#dataTables-example').dataTable();

         });

    </script>
   <link href="<?= base_url();?>/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />

<div class="content-wrapper">
               
         <section class="content-header">
          <h1>Socila Detail</h1>
         
        </section>
        <section class="content">
            <a class="btn btn-primary" href="<?= site_url('admin/social/add_social') ?>">New Social Icon</a>
           
      <div class=" box box-primary">
<div class="box-body" >

						<div class="panel-body">
                            <div class="table-responsive">
                               
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Social Icon</th>
                      <th>Link</th>
                      <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
          <?php foreach ($socials as $gall):
      ?>
    <tr class="odd gradeX">
      <td><?php echo $gall->name ; ?></td>
      <td><img src="<?= base_url();?>/upload/gallery/social/<?php echo $gall->image;?>"  /></td>
            <td><?php echo $gall->link ; ?></td>
      <td align="center"><?php echo anchor("admin/social/edit_social/".$gall->id, 'Edit') ;?> | <?php echo anchor("admin/social/delete_social/".$gall->id, 'Delete') ;?></td>
    </tr>
    <?php endforeach;?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                  </div>
</div>

</section>
</div>
    
  