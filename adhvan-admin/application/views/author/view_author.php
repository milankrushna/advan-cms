
    <!-- END PAGE LEVEL  STYLES -->
     <!--PAGE CONTENT --> 
  
<div class="content-wrapper">
     <section class="content-header">
        <h1>Author</h1>
         <br>
          <a class="btn btn-primary" href="<?php echo site_url('admin/author/create_author')?>" >Add Author</a>
      </section>
<section class="content">
<div class="box box-primary ">
<div class="box-body " >

    <div class="col-md-6">
    
<div id="infoMessage" style="color:#F00"><?php echo $this->session->flashdata('message');  ?></div>
        
        
    </div>
    
    <div style="clear:both;"></div>
    
    
<?php  if(!empty($author)){   
        ?>
    
<div class="box-body" >
                        <?php ////echo $msg; ?>
						<div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                           
                                            <th>Image</th>
											
											<th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
          <?php foreach ($author as $gall):
		  ?>
		<tr class="odd gradeX" id="slider_<?php echo $gall['id']; ?>">

			<td><?php echo $gall['name']; ?></td>
		
			
			<td><img src="<?php echo base_url();?>/upload/events/min_events/<?php echo $gall['profile_pic'];?>"   width="150px"  /></td>
            
          
				
            
			<td align="center"> <a class="btn btn-warning" href="<?php echo site_url('admin/author/edit_author'."/".$gall['id'])?>" ><i class="fa fa-edit"></i></a>||<a title="Delete" class="btn  btn-danger" href="<?php echo site_url('admin/author/delete_author'."/".$gall['id'])?>" onclick="return confirm('Are You sure?');"><i class="fa fa-trash"></i></a></td>
            
            
            
            
            
		</tr>
		<?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                   
            </div>

        
        
        
        


       
        
        <?php } ?>
   
    
                	</div>
    </div>
           
           </section>
           </div>

              

            
