<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/ajxupload/upload.css"  />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/ajxupload/jquery.form.js" ></script>

 <link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/datepicker/datepicker3.css">
 <script src="<?php echo base_url()?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
 <script>
 $(function () {
 $('#datepicker').datepicker({
      autoclose: true,
      format: "dd-mm-yyyy"
    });
});
 </script>

<div class="content-wrapper">
               
              <section class="content-header">
          <h1>Add Service</h1><br>
        <a href="<?php  echo site_url('admin/events/service'); ?>"  class="btn btn-info" >New Service</a>

         
        </section>
        <section class="content">
      <div class="box box-primary">
<div class="box-body" >

	  <p style="color:red"><?php echo $message; ?></p>
                <form id="serviceformp" method="post" action="<?php  echo current_url(); ?>" enctype="multipart/form-data" >                   
				   <div class="form-group">
                   	<label for="text1" class="control-label">Date</label>
                     
                            <input type="text" name="date" class="form-control" value="<?php echo date('m/d/Y') ?>"  id="datepicker" required readonly />
                        
                	</div>	
                    <div class="form-group">
                   	<label for="text1" class="control-label">Title</label>
                     
                            <input type="text" name="title" class="form-control" value=""   required/>
                        
                	</div>
                    
                    <input type="hidden" name="category" value="service" > 
                    
                    
                    <div class="form-group">
                   	<label for="text1" class="control-label">Cover Pic</label>
                     
                            <input type="file" name="file" class="form-control" />
                        
                	</div>
                    <div class="form-group">
                   	<label for="text1" class="control-label">Url</label>
                     
                        <input type="text" name="sht_desc" class="form-control" value=""   />
<!--                        <textarea class="form-control" name="sht_desc" style="resize:none;"></textarea>-->
                        
                	</div>
				    <div class="form-group">
                        <label for="cp1" class="control-label">Description</label>

                        <div class="">
                              <?php
        $this->load->view('editor/fckeditor.php');
        $oFCKeditor = new FCKeditor('description') ;
        $oFCKeditor->BasePath = base_url().'editor/' ;
        $oFCKeditor->Height = 390;
        $oFCKeditor->Config['EnterMode'] = 'br';
        $oFCKeditor->Value = '' ;
        $oFCKeditor->name = 'description' ;
        $oFCKeditor->Create() ;
    ?>
                      </div>
                           
                     
                    </div>
                    				    
    <!--       Progress Bar-->
       <div id="p_bar" style="display:none;" class="hide">
           <div  class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" id="progressbar" style="width:0%">
                    <span id="statustxt"></span>
                </div>
           </div>
       </div>
       <!--       Progress Bar-->
					
                 
                        <input type="submit"  value="Submit" class="btn btn-success" />
                    
                </form>
						
</div>
</div>
</section>
</div>
		
<script>


 $(document).ready(function() {
        //elements
        var progressbox     = $('#progressbox');
        var progressbar     = $('#progressbar');
        var statustxt       = $('#statustxt');
        var submitbutton    = $("#SubmitButton");
        var myform          = $("#serviceform");
        var output          = $("#output");
        var completed       = '0%';
 
                $(myform).ajaxForm({
                    beforeSend: function() { //brfore sending form
                        document.getElementById("p_bar").className = "show";
                        submitbutton.attr('disabled', ''); // disable upload button
                        statustxt.empty();
                        progressbox.slideDown(); //show progressbar
                        progressbar.width(completed); //initial value 0% of progressbar
                        statustxt.html(completed); //set status text
                        statustxt.css('color','#fff'); //initial color of status text
                    },
                    uploadProgress: function(event, position, total, percentComplete) { //on progress
                        progressbar.width(percentComplete + '%') //update progressbar percent complete
                        statustxt.html(percentComplete + '%'); //update status text
                        if(percentComplete>50)
                            {
                                statustxt.css('color','#fff'); //change status text to white after 50%
                            }

                        },
                    complete: function(response) { // on complete
                        var res = output.html(response.responseText); //update element with received data
                        myform.resetForm();  // reset form
                        submitbutton.removeAttr('disabled'); //enable submit button
                        progressbox.slideUp(); // hide progressbar
                        document.getElementById("p_bar").className = "hide";
                        
                        
if(response.responseText == 1){
alert("Successfully Upload");
}else if(response.responseText == 2){
alert("Please Upload a valid Image File");
}else if(response.responseText == 3){
alert("You have no rights to upload");
}else{
alert("Uploading unsuccessful Please Try again");
}
//location.reload();
                       /// location.reload();
                    }
                   
            });
        });


</script>
