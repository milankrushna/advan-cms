<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/ajxupload/upload.css"  />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/ajxupload/jquery.form.js" ></script>    

<div class="content-wrapper">
               
              <section class="content-header">
          <h1>Edit Testimonial</h1>
<br>
              
         
        </section>
        <section class="content col-sm-12">
      <div class="box box-primary">
<div class="box-body" >

	   <a href="<?php  echo site_url('admin/events/income_testimonial'); ?>"  class="btn btn-info" >Incoming Testimonials</a>
	   <a href="<?php  echo site_url('admin/events/approve_testimonial'); ?>"  class="btn btn-info" >Approve Testimonials</a>
	  
                <form   method="post" id="editestimonial" action="<?php  site_url('admin/events/edit_testimonial').'/'.$test->id ;  ?>"  enctype="multipart/form-data" >                  
				   
				   <div class="form-group">
                   	<label for="text1" class="control-label">Firstname</label>
                     
                            <input type="text" name="first_name" class="form-control" value="<?php echo $test->first_name; ?>"   required/>
                        
                	</div>
                	
                	<div class="form-group">
                   	<label for="text1" class="control-label">Lastname</label>
                     
                            <input type="text" name="last_name" class="form-control" value="<?php echo $test->last_name; ?>"   required/>
                        
                	</div>
                	
                	<div class="form-group">
                   	<label for="text1" class="control-label">Company</label>
                     
                            <input type="text" name="company" class="form-control" value="<?php echo $test->company; ?>"   required/>
                        
                	</div>
                	
                	
                	<div class="form-group">
                   	<label for="text1" class="control-label">Phone</label>
                     
                            <input type="number" name="phone" class="form-control" value="<?php echo $test->phone; ?>"   required/>
                        
                	</div>
                	
                	<div class="form-group">
                   	<label for="text1" class="control-label">Email</label>
                     
                            <input type="email" name="email" class="form-control" value="<?php echo $test->email; ?>"   required/>
                        
                	</div>
                	
                	<div class="form-group">
                   	<label for="text1" class="control-label">Subject</label>
                     
                            <input type="text" name="subject" class="form-control" value="<?php echo $test->subject; ?>"  required/>
                        
                	</div>
                	
                	<div class="form-group">
                   	<label for="text1" class="control-label">Comment</label>
                     
                            <input type="text" name="comment" class="form-control" value="<?php echo $test->comment; ?>"   required/>
                        
                	</div>
                	
            
                    <div class="form-group">
                   	<label for="text1" class="control-label">Cover Pic</label>
                     <img src="<?php echo base_url()?>upload/events/<?php echo $test->cover_pic; ?>"  width="250px"    >
                            <input type="file" name="cover_pic" class="form-control"  />
                        
                	</div>
                    
                    
                 
                        
                                	
                					    
    <!--       Progress Bar-->
       <div id="p_bar" style="display:none;" class="hide">
           <div  class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" id="progressbar" style="width:0%">
                    <span id="statustxt"></span>
                </div>
           </div>
       </div>
       <!--       Progress Bar-->
					
					
                    
                        <input type="submit"  value="Update" class="btn btn-primary" />
                    
                </form>
				
</div>
</div>
</section>
</div>
<script>
 $(document).ready(function() {
        //elements
        var progressbox     = $('#progressbox');
        var progressbar     = $('#progressbar');
        var statustxt       = $('#statustxt');
        var submitbutton    = $("#SubmitButton");
        var myform          = $("#editestimonial");
        var output          = $("#output");
        var completed       = '0%';
 
                $(myform).ajaxForm({
                    beforeSend: function() { //brfore sending form
                        document.getElementById("p_bar").className = "show";
                        submitbutton.attr('disabled', ''); // disable upload button
                        statustxt.empty();
                        progressbox.slideDown(); //show progressbar
                        progressbar.width(completed); //initial value 0% of progressbar
                        statustxt.html(completed); //set status text
                        statustxt.css('color','#fff'); //initial color of status text
                    },
                    uploadProgress: function(event, position, total, percentComplete) { //on progress
                        progressbar.width(percentComplete + '%') //update progressbar percent complete
                        statustxt.html(percentComplete + '%'); //update status text
                        if(percentComplete>50)
                            {
                                statustxt.css('color','#fff'); //change status text to white after 50%
                            }

                        },
                    complete: function(response) { // on complete
                        var res = output.html(response.responseText); //update element with received data
                        myform.resetForm();  // reset form
                        submitbutton.removeAttr('disabled'); //enable submit button
                        progressbox.slideUp(); // hide progressbar
                        document.getElementById("p_bar").className = "hide";
                        
                        
if(response.responseText == 1){
alert("Successfully Upload");
}else if(response.responseText == 2){
alert("Please Upload a valid Image File");
}else if(response.responseText == 3){
alert("You have no rights to upload");
}else{
alert("Uploading unsuccessful Please Try again");
}
//location.reload();
                       /// location.reload();
                    }
                   
            });
        });


</script>