<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/ajxupload/upload.css"  />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/ajxupload/jquery.form.js" ></script>
 <link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/datepicker/datepicker3.css">
 <script src="<?php echo base_url()?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
 <script>
 $(function () {
 $('#datepicker').datepicker({
      autoclose: true,
     format: "dd-mm-yyyy"
    });
});
 </script>

<div class="content-wrapper">
               
              <section class="content-header">
          <h1>Edit Service</h1>
<br>
                  <a href="<?php  echo site_url('admin/events/service'); ?>"  class="btn btn-info" >Service</a>
                  <a href="<?php  echo site_url('admin/events/create_service'); ?>"  class="btn btn-info" >New</a>
         
        </section>
        <section class="content col-sm-12">
      <div class="box box-primary">
<div class="box-body" >

	  
                <form   method="post" id="editservicee" action="<?php  echo current_url(); ?>"  enctype="multipart/form-data" >                   
				   <div class="form-group">
                   	<label for="text1" class="control-label">Date</label>
                
                            <input type="text" name="date" value="<?php echo date('d-m-Y',strtotime($event->date)); ?>" class="form-control" id="datepicker" readonly required />
                        
                	</div>	
                    <div class="form-group">
                   	<label for="text1" class="control-label">Title</label>
                     
                            <input type="text" name="title" class="form-control" value="<?php echo $event->title ?>"   required/>
                        
                	</div>
                
                    <input type="hidden" name="category" value="service" > 
                    
                    
                    <div class="form-group">
                   	<label for="text1" class="control-label">Cover Pic</label><br>
                     <img src="<?php echo base_url()?>upload/events/min_events/<?php echo $event->cover_pic; ?>">
                        
                        
                            <input type="file" name="file" class="form-control" />
                        
                	</div>
                    <div class="form-group">
                   	<label for="text1" class="control-label">Url</label>
                     <input type="text" name="sht_desc" class="form-control" value="<?php echo $event->sht_desc ?>"   />
<!--                        <textarea class="form-control" name="sht_desc" style="resize:none;"><?php echo $event->sht_desc ?></textarea>-->
                        
                	</div>
				    <div class="form-group">
                        <label for="cp1" class="control-label">Description</label>

                      <div class=""> 
                              <?php
        $this->load->view('editor/fckeditor.php');
        $oFCKeditor = new FCKeditor('description') ;
        $oFCKeditor->BasePath = base_url().'editor/' ;
        $oFCKeditor->Height = 390;
        $oFCKeditor->Config['EnterMode'] = 'br';
        $oFCKeditor->Value =  $event->description;
        $oFCKeditor->name = 'description' ;
        $oFCKeditor->Create() ;
    ?>
                      </div>
                      
                    </div>
                                    				    
    <!--       Progress Bar-->
       <div id="p_bar" style="display:none;" class="hide">
           <div  class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" id="progressbar" style="width:0%">
                    <span id="statustxt"></span>
                </div>
           </div>
       </div>
       <!--       Progress Bar-->
                    
                        <input type="submit"  value="Update" class="btn btn-primary" />
                    
                </form>
				
</div>
</div>
</section>
</div>
		
<script>


 $(document).ready(function() {
        //elements
        var progressbox     = $('#progressbox');
        var progressbar     = $('#progressbar');
        var statustxt       = $('#statustxt');
        var submitbutton    = $("#SubmitButton");
        var myform          = $("#editservice");
        var output          = $("#output");
        var completed       = '0%';
 
                $(myform).ajaxForm({
                    beforeSend: function() { //brfore sending form
                        document.getElementById("p_bar").className = "show";
                        submitbutton.attr('disabled', ''); // disable upload button
                        statustxt.empty();
                        progressbox.slideDown(); //show progressbar
                        progressbar.width(completed); //initial value 0% of progressbar
                        statustxt.html(completed); //set status text
                        statustxt.css('color','#fff'); //initial color of status text
                    },
                    uploadProgress: function(event, position, total, percentComplete) { //on progress
                        progressbar.width(percentComplete + '%') //update progressbar percent complete
                        statustxt.html(percentComplete + '%'); //update status text
                        if(percentComplete>50)
                            {
                                statustxt.css('color','#fff'); //change status text to white after 50%
                            }

                        },
                    complete: function(response) { // on complete
                        var res = output.html(response.responseText); //update element with received data
                        myform.resetForm();  // reset form
                        submitbutton.removeAttr('disabled'); //enable submit button
                        progressbox.slideUp(); // hide progressbar
                        document.getElementById("p_bar").className = "hide";
                        
                        
if(response.responseText == 1){
alert("Successfully Upload");
}else if(response.responseText == 2){
alert("Please Upload a valid Image File");
}else if(response.responseText == 3){
alert("You have no rights to upload");
}else{
alert("Uploading unsuccessful Please Try again");
}
//location.reload();
                       /// location.reload();
                    }
                   
            });
        });


</script>
