<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/ajxupload/upload.css"  />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/ajxupload/jquery.form.js" ></script>    

<div class="content-wrapper">
               
              <section class="content-header">
          <h1>Add Client</h1>
<br>
              
         
        </section>
        <section class="content">
      <div class="box box-primary">
<div class="box-body" >

	 
	  
   <form   method="post" id="editestimonial" action="<?php echo current_url(); ?>"  enctype="multipart/form-data" >                  
				   
				   <div class="form-group">
                   	<label for="text1" class="control-label">Name</label>
                     
                            <input type="text" name="name" class="form-control" value=""   required/>
                        
                	</div>
                	
                	
                	<div class="form-group">
                   	<label for="text1" class="control-label">Url</label>
                     
                            <input type="text" name="url" class="form-control" value=""   />
                        
                	</div>
                	
                
                    <div class="form-group">
                   	<label for="text1" class="control-label">Cover Pic</label>
                   
                            <input type="file" name="cover_pic" class="form-control" required />
                        
                	</div>
                    
                    
                 
                        
                                	
                					    
    <!--       Progress Bar-->
       <div id="p_bar" style="display:none;" class="hide">
           <div  class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" id="progressbar" style="width:0%">
                    <span id="statustxt"></span>
                </div>
           </div>
       </div>
       <!--       Progress Bar-->
					
					
                    
                        <input type="submit"  value="Submit" class="btn btn-primary" />
                    
                </form>
				
</div>
</div>
</section>
</div>
<script>
 $(document).ready(function() {
        //elements
        var progressbox     = $('#progressbox');
        var progressbar     = $('#progressbar');
        var statustxt       = $('#statustxt');
        var submitbutton    = $("#SubmitButton");
        var myform          = $("#editestimonial");
        var output          = $("#output");
        var completed       = '0%';
 
                $(myform).ajaxForm({
                    beforeSend: function() { //brfore sending form
                        document.getElementById("p_bar").className = "show";
                        submitbutton.attr('disabled', ''); // disable upload button
                        statustxt.empty();
                        progressbox.slideDown(); //show progressbar
                        progressbar.width(completed); //initial value 0% of progressbar
                        statustxt.html(completed); //set status text
                        statustxt.css('color','#fff'); //initial color of status text
                    },
                    uploadProgress: function(event, position, total, percentComplete) { //on progress
                        progressbar.width(percentComplete + '%') //update progressbar percent complete
                        statustxt.html(percentComplete + '%'); //update status text
                        if(percentComplete>50)
                            {
                                statustxt.css('color','#fff'); //change status text to white after 50%
                            }

                        },
                    complete: function(response) { // on complete
                        var res = output.html(response.responseText); //update element with received data
                        myform.resetForm();  // reset form
                        submitbutton.removeAttr('disabled'); //enable submit button
                        progressbox.slideUp(); // hide progressbar
                        document.getElementById("p_bar").className = "hide";
                        location.reload();
                    }
                   
            });
        });


</script>
