<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/ajxupload/upload.css"
/>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/ajxupload/jquery.form.js"></script>

<link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/datepicker/datepicker3.css">
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>

<link rel="stylesheet" href="<?php echo base_url()?>assets/jquery.tag-editor.css">
<!-- END PAGE LEVEL  STYLES -->
<!--PAGE CONTENT -->

<style>
.image-thumb img {
    border: 2px solid;
    width: 100%;
    height: 100px;
}</style>

<div class="content-wrapper">
	<section class="content-header">
		<h1><?php  echo $title; ?></h1>

	</section>
	<section class="content">
		<div class="box box-primary">
			<div class="box-body">
				<div class="infoMessage" style="color:#F00">
					<?php // echo $message;?>
				</div>
				<form action="<?php echo  current_url(); ?>" enctype="multipart/form-data"
				 method="post" id="page_create">



					
					<div class="col-md-12">
						<div class="col-md-6">
							<label for="text1" class="control-label">Hotel Name</label>

							<input type="text" class="form-control" placeholder="Hotel Name" name="name" value="<?php  echo $hotelData->name; ?>" required="">
							<br>

						</div>

						<div class="col-md-6">
							<label for="text1" class="control-label">Hotel Title</label>

							<input type="text" value="<?php  echo $hotelData->sub_title; ?>" class="form-control" placeholder="Hotel Name" name="sub_title" required="">
							<br>

						</div>
					</div>
					<div class="col-md-12">

<div class="col-md-4 form-group">
	<label for="text1" class="control-label">Country</label>
	<select class="form-control" onchange="get_city(this.value);" id="" name="country" required>
		<option value="">Select a Country</option>


		<?php 
	foreach($country as $cat){ ?>
		<option <?php if($tourMaping->country_id == $cat['id']){ echo "selected"; } ?> value="<?php echo $cat['id']; ?>">
			<?php  echo $cat['countryname']; ?>
		</option>
		<?php 
	 }
	?>

	</select>
</div>

<div class="col-md-4 form-group">
	<label for="text1" class="control-label">City</label>
	<select  onchange="get_sub_city(this.value)" class="form-control" name="city" id="subcatgy">
		<option value="">Select a City</option>
		<?php 
                            foreach($city as $cat){ ?>
								<option <?php if($tourMaping->city_id == $cat['id']){ echo "selected"; } ?> value="<?php echo $cat['id']; ?>">
									<?php  echo $cat['statename']; ?>
								</option>
								<?php   } ?>
	</select>
</div>
<div class="col-md-4 form-group">
	<label for="text1" class="control-label">Sub City</label>
	<select class="form-control" name="sub_city" id="subcity">
		<option value="">Select a Sub City</option>
		<?php 
                            foreach($sub_city as $cat){ ?>
								<option <?php if($tourMaping->sub_city_id == $cat['id']){ echo "selected"; } ?> value="<?php echo $cat['id']; ?>">
									<?php  echo $cat['city']; ?>
								</option>
								<?php 
                             }
                            ?>
	</select>
</div>
</div>

<div class="col-md-12">
<div class="col-md-4 form-group">
	<label for="text1" class="control-label">Region</label>
	<select class="form-control"  id="" name="region_id" required>
		<option value="">Select a Region</option>


								<?php 
                            foreach($region as $cat){ ?>
								<option <?php if($tourMaping->region_id == $cat['id']){ echo "selected"; } ?> value="<?php echo $cat['id']; ?>">
									<?php  echo $cat['name']; ?>
								</option>
								<?php 
                             }
                            ?>

	</select>
</div>
<div class="col-md-4 form-group">
	<label for="text1" class="control-label">Special</label>
	<select class="form-control"  id="" name="special_id" required>
		<option value="">Select a Speciality</option>


		<?php 
	foreach($special as $cat){ ?>
		<option <?php if($tourMaping->special_id == $cat['id']){ echo "selected"; } ?> value="<?php echo $cat['id']; ?>">
			<?php  echo $cat['name']; ?>
		</option>
		<?php 
	 }
	?>

	</select>
</div>
<div class="col-md-4 form-group">
	<label for="text1" class="control-label">Star Rating</label>
	<select class="form-control"  id="" name="star_ratings" required>
		<option value="">Select a Star ratings</option>
		 <option <?php if($tourMaping->star_ratings == '0'){ echo "selected"; } ?> value="0">Packages </option>
		 <option <?php if($tourMaping->star_ratings == '3'){ echo "selected"; } ?> value="3">First Class (***) </option>
		 <option <?php if($tourMaping->star_ratings == '4'){ echo "selected"; } ?> value="4">Superio (****) </option>
		 <option <?php if($tourMaping->star_ratings == '5'){ echo "selected"; } ?> value="5">Deluxe (*****) </option>
		 <option <?php if($tourMaping->star_ratings == '5+'){ echo "selected"; } ?> value="5+">Super Deluxe (*****+) </option>
	</select>
</div>


</div>
					<div class="col-md-12">
					<div class="col-md-6">
							<label for="text1" class="control-label">About Hotel</label>
							<textarea class="form-control"  placeholder="Overview Description" name="about"><?php  echo $hotelData->about; ?></textarea>
						</div>
						<div class="col-md-6">
							<label for="text1" class="control-label">Hotel Address</label>
							<textarea name="hotel_address" class="form-control" placeholder="Hotel Address" ><?php  echo $hotelData->hotel_address; ?></textarea>
						</div>
					</div>

					<div class="col-md-12">
						<div class="col-md-12 form-group">
							<label for="text1" class="control-label">Map URL</label>
							<input type="text" value="<?php  echo $hotelData->map_url; ?>" class="form-control" placeholder="Map URL" name="map_url">
						</div>

					</div>
					<div class="col-md-12">
						<div class="col-md-12 form-group">
							<label for="text1" class="control-label">Hotel Website</label>
							<input value="<?php  echo $hotelData->website_link; ?>" type="text" class="form-control" placeholder="Website URL" name="website_link">
						</div>

					</div>

				
					<div class="col-md-12">
						<div class="col-md-4 form-group">
						
							<label for="">Why we like it</label>

							<textarea id="" rows="3"  name="why_we_live" class="form-control"><?php  echo $hotelData->why_we_live; ?></textarea>
						</div>
						<div class="col-md-4 form-group">
						
							<label for="">It’s perfect for</label>

							<textarea  id="" name="perfect_for" rows="3" class="form-control"><?php  echo $hotelData->perfect_for; ?></textarea>
						</div>
						<div class="col-md-4 form-group">
							<label for="">Child Policy</label>
							
							<textarea id="" rows="3" name="child_policy" class="form-control"><?php  echo $hotelData->child_policy; ?></textarea>
						</div>

					</div>

			

					<!-- <div class="col-md-12">
						<div class="col-md-12 form-group">
							<label for="">Short Description</label>
							<textarea id="" name="short_desc" rows="3" class="form-control"></textarea>
						</div>
					</div> -->


	<div class="col-md-12">

<div class="col-md-6">
	<label for="text1" class="control-label">Main Image</label>
	<div class="input-group form-group">
		<input type="text" value="<?php echo $hotelData->main_image; ?>" id="main_image" class="form-control" placeholder="Main image" name="main_image">
		<div>
		<div class="image-thumb"><img height="120px" src="<?php 
                echo str_replace('adhvan-admin/','',base_url(str_replace('adhvan/images','adhvan/_thumbs/Images',$hotelData->main_image))); ?>"><div class="action"><a onclick="removeMe(this)" alt="main_image"><i class="fa fa-trash	"></i></a></div></div>
		</div>
		<!-- this div is image container -->

<a onclick="BrowseServer('main_image','')" class="btn btn-app">
                <i class="fa fa-image"></i> Image
              </a>
	</div>
</div>
</div>

					<div class="col-md-12">
					<div id="image_container">
					<?php  
					
					$gallery_image = explode(',',$hotelData->gallery_image);
							 foreach($gallery_image as $ti){
					?>
					<div class="col-md-2 image-thumb"><input type="text" name="galleryImages[]" value="<?php  echo $ti; ?>"><img height="120px" src="<?php 
                echo str_replace('adhvan-admin/','',base_url(str_replace('adhvan/images','adhvan/_thumbs/Images', $ti))); ?>"><div class="action"><a onclick="removeMe(this)"><i class="fa fa-trash	"></i></a></div></div>
							 <?php } ?>
					</div>
					</div>
					<div class="col-md-12">
				
<div class="col-md-6 form-group">

<a onclick="BrowseServer('','image_container')" class="btn btn-app">
                <i class="fa fa-image"></i> Gallery Images
              </a>
</div>
</div>
			
					</div>
					<div class="infoMessage" style="color:#F00">
						<?php //echo $message;?>
					</div>
	<div class="form-group">
					<button type="submit"  class="btn btn-block btn-success SubmitButton">Save </button>
					</div>
				</form>


			</div>
		</div>
	</section>
</div>

<!-- END PAGE CONTENT -->

<script src="<?php echo base_url()?>assets/jquery.tag-editor.min.js"></script>
<script src="<?php echo base_url()?>assets/jquery.caret.min.js"></script>

<script src="<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/ckfinder.js"></script>

<script>


function BrowseServer(settingId,multiDisplay)
{
	var finder = new CKFinder();
	finder.basePath = "../";	
	finder.selectActionFunction = (fileUrl)=>{


		var baseUrl = '<?php echo base_url(); ?>';
		var localUrl = baseUrl.replace("adhvan-admin/",'');
		var main_url =  fileUrl.replace(localUrl, "");
		var thumb = main_url.replace('adhvan/images','adhvan/_thumbs/Images'); 

		if(settingId!=""){
			 $('#'+settingId).val(main_url);
			var imc =  $('#'+settingId).siblings()[0];
			console.log(imc);
			$(imc).html('<div class="image-thumb"><img height="120px" src="'+localUrl+thumb+'"><div class="action"><a onclick="removeMe(this)" alt="'+settingId+'" ><i class="fa fa-trash	"></i></a></div>')
			 }
		
		if(multiDisplay != ""){
		
		
		$("#"+multiDisplay).append('<div class="col-md-2 image-thumb"><input type="text" name="galleryImages[]" value='+main_url+'><img height="120px" src="'+localUrl+thumb+'"><div class="action"><a onclick="removeMe(this)" ><i class="fa fa-trash	"></i></a></div>');
	}
	};
	var api = finder.popup();

//upload/adhvan/images/tours/37.jpg
//upload/adhvan/_thumbs/Images/tours/37.jpg

}


function removeMe(dis){
	var altTag = $(dis).attr('alt');
	if(altTag !=""){
		$('#'+altTag).val("");
	}
var cc =$(dis).parent().parent().remove();

}


	/*var noteEditor = CKEDITOR.replace('notes', {

		height: 250,
		filebrowserBrowseUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/ckfinder.html',
		filebrowserUploadUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserWindowWidth: '1000',
		filebrowserWindowHeight: '700'
	});

	sht_editor.on('change', function(ev) {

		$('#sht_editor').html(sht_editor.getData());

	});
*/
var exitDay = [1];
var noDay = 1;




	$(function() {
		$('#datepicker').datepicker({
			autoclose: true,
			format: "dd-mm-yyyy"
		});
	});


	function get_city(dis) {

		// alert(id);
		$('#category').html('<option value="">Loading...</option>');

		$.get('<?php echo site_url('admin/tours/get_cnt_state/');?>/'+dis,{},
			function(resp) {

				$('#subcatgy').html(resp);

			});

	}
	function get_sub_city(dis) {

		// alert(id);
		$('#subcity').html('<option value="">Loading...</option>');

		$.get('<?php echo site_url('admin/tours/get_state_district/');?>/'+dis,{},
			function(resp) {

				$('#subcity').html(resp);

			});

	}
</script>