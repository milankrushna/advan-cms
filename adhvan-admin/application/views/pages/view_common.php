    <link href="<?= base_url();?>/assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" />
   <div class="content-wrapper">
   <section class="content-header">
          <h1>View common Section</h1>
         
        </section>
        <section class="content">
        <a class="btn btn-primary" href="<?php echo site_url('admin/common/create_common') ?>" >Add section</a>
            <div class="box box-primary">
            
<div class="box-body">
                
                       
                        <?php echo $msg; ?>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Section Name</th>
                                            <th>URL</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
          <?php foreach ($pages as $pg):?>
		<tr class="odd gradeX">
			<td><?php echo $pg->name;?></td>
			<td><?php echo $pg->url;?></td>
           	<td><?php echo anchor("admin/common/edit_common/".$pg->id, 'Edit') ;?> 
                
		</tr>
		<?php endforeach;?>                                    
                                    </tbody>
                                </table>
                            </div>                          
                        </div>
                
            </div>
</div>
</section>
</div>
    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?= base_url();?>/assets/plugins/datatables/jquery.dataTables.js"></script>
    <script src="<?= base_url();?>/assets/plugins/datatables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script>	