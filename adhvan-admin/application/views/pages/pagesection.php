    <link href="<?= base_url();?>/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
   <div id="content">
            <div class="inner">
            <div class="row">
                    <div class="col-lg-12">
                        <h2>CATEGORIES</h2>
<?php // echo $msg; ?>

                    </div>
                </div>
<form action="<?= site_url('admin/pages/save_section') ?>" method="post">
<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Sections
                        
						 <p class="pull-right"><a class="pull-right" href=""><input type="submit" value="Save Order" class="btn btn-primary"> </a></p>
						 <input type="hidden" name="page_id" value="<?php echo $page_id; ?>">
						</div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" >
                                    <thead>
                                        <tr>
                                            <th width="50%">Section Name</th>
                                            <th width="50%">Position</th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
          <?php foreach ($pages as $pg):?>
		  <?php $sec_id  =  $pg['id'];?>
		<tr class="odd gradeX">
			<td><?php echo $pg['name'];?></td>
			<td><input type="hidden" name="sect[]"   value="<?php echo $pg['id'];?>"><input name="order[<?php echo $sec_id ?>]" class="span2" value="<?php echo $pg['appear_order']; ?>" type="text"></td>
        </tr>
		<?php endforeach;?>                                    
                                    </tbody>
                                </table>
                            </div>                          
                        </div>
                    </div>
                </div>
            </div>
			</form>
</div>
</div>
    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?= base_url();?>/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?= base_url();?>/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script>	