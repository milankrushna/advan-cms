   <link href="<?= base_url();?>/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
   <div id="content">
            <div class="inner">
            <div class="row">
                    <div class="col-lg-12">
                        <h2>SECTIONS</h2>
<?php echo $msg; ?>
                </div>
<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            All Section List
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Section Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
          <?php foreach ($pages as $pg):?>
		<tr class="odd gradeX">
			<td><?php echo $pg->name;?></td>
         	<td><?php echo anchor("admin/section/edit_section/".$pg->id, 'Edit') ;?> | <?php echo anchor("admin/section/delete_section/".$pg->id, 'Delete') ;?></td>
		</tr>
		<?php endforeach;?>                                    
                                    </tbody>
                                </table>
                            </div>                          
                        </div>
                    </div>
                </div>
            </div>
</div>
</div>
    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?= base_url();?>/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?= base_url();?>/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script>	