 <link href="<?= base_url();?>/assets/css/jquery-ui.css" rel="stylesheet" />

<link rel="stylesheet" href="<?= base_url();?>/assets/plugins/uniform/themes/default/css/uniform.default.css" />

<link rel="stylesheet" href="<?= base_url();?>/assets/plugins/inputlimiter/jquery.inputlimiter.1.0.css" />

<link rel="stylesheet" href="<?= base_url();?>/assets/plugins/chosen/chosen.min.css" />

<link rel="stylesheet" href="<?= base_url();?>/assets/plugins/colorpicker/css/colorpicker.css" />

<link rel="stylesheet" href="<?= base_url();?>/assets/plugins/tagsinput/jquery.tagsinput.css" />

<link rel="stylesheet" href="<?= base_url();?>/assets/plugins/daterangepicker/daterangepicker-bs3.css" />

<link rel="stylesheet" href="<?= base_url();?>/assets/plugins/datepicker/css/datepicker.css" />

<link rel="stylesheet" href="<?= base_url();?>/assets/plugins/timepicker/css/bootstrap-timepicker.min.css" />

<link rel="stylesheet" href="<?= base_url();?>/assets/plugins/switch/static/stylesheets/bootstrap-switch.css" />

<link rel="stylesheet" href="<?= base_url();?>/assets/validate/css/screen.css" />
<script src="<?= base_url();?>/assets/validate/dist/jquery.validate.js"></script>
<script>
$().ready(function() {
	$("#loginform").validate({

		rules: {
					name : "required",
					descrioption :"required",
		},
		messages: {
					name : "<br/>This field is required!",
					description :"<br/> This field is required!",
		}

	});



});

</script>
	<script src="<?php echo base_url()?>assets/ckeditor/ckeditor.js"></script>
	<script>

		// Remove advanced tabs for all editors.

		CKEDITOR.config.removeDialogTabs = 'image:advanced;link:advanced;flash:advanced;creatediv:advanced;editdiv:advanced';

	</script>

    <!-- END PAGE LEVEL  STYLES -->
     <!--PAGE CONTENT --> 
    <div id="content">
                <div class="inner">
               <div class="row">
               <div class="col-lg-12">
                    <h2 class="page-header"><?php echo "Create Section";?></h2>
                </div>
            </div>
<div class="row">
<div class="col-lg-8" align="center">
<div id="infoMessage" style="color:#F00"><?php echo $message;?></div>
<form action="<?= site_url('admin/section/create_section') ?>" method="post" id="loginform">
 				<div class="form-group">
                    <label for="text1" class="control-label col-lg-4">Section  Name<br></label>
                    <div class="col-lg-8">
                                  <input  type="text" class="form-control" name="name"><br>
                      </div>
                	</div>	
					
									
                   <div class="form-group">
                    <label for="text1" class="control-label col-lg-4"><br/>Category <br/></label>
                    <div class="col-lg-8 odia-fo" >
      	                       <br/>	<select  multiple="multiple" name="page_id[]" class="form-control  chzn-rtl odia-fo" id="page_id">
		<?php  foreach($pages as $pg){
		?>		
        <option value="<?php echo $pg['id']; ?>" class="odia-fo" > <?php echo $pg['name']; ?> </option>
		<?php }  ?>

   	</select> <br/>
                      </div>
                	</div>
		<div class="form-group">
                    <label for="text1" class="control-label col-lg-4"><br/>Category <br/></label>
		<div class="col-lg-16" align="center" >
		<?php
			$this->load->view('editor/fckeditor.php');
 				$oFCKeditor = new FCKeditor('txt_content') ;
				$oFCKeditor->BasePath = base_url().'editor/' ;
				$oFCKeditor->Height	= 390;
				$oFCKeditor->Config['EnterMode'] = 'br';
				$oFCKeditor->Value = '' ;
				$oFCKeditor->name = 'content' ;
				$oFCKeditor->Create() ;
		?>
 		</div>	
			</div>		
	     	<div class="form-group">
                    <div class="col-lg-8">
					<br>
                        <input type="submit" id="tags" value="Submit" class="btn btn-primary pull-right" />
                    </div>
                </div>
	</form>
	   </div>
</div>
    </div>
           </div>
              </div>
                    <!-- END PAGE CONTENT -->
				
	
	