<div class="bannerbg">
  <div class="container flarabg">
    <div>
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
         <?php
		 $c= 0 ;
		  foreach($slider as $sl){
			if($c==0){
			 ?>
			  <li data-target="#carousel-example-generic" data-slide-to="<?php echo $c; ?>" class="active" ></li>
			 <?php
			}else{  ?>
			  <li data-target="#carousel-example-generic" data-slide-to="<?php echo $c; ?>" ></li>
			<?php
			}
			$c = $c+1;
			 } 
			 ?>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
         
		 <?php
		 $c= 0 ;
		  foreach($slider as $sl){
		  	$c = $c+1;
			 ?>
          <?php if($c==1){ ?>
		  <div class="item active">
           <?php }else{ ?>
		  <div class="item">
			<?php } ?>
			<div class="sliderleftdiv">
              <div class="slidebigtext"> <?php echo $sl['title']; ?> </div>
              <div class="padtop10 bansmalltext"><?php echo $sl['caption']; ?></div>
              <div class="padtop10">
                <button type="button" class="btn btn-primary buttoncolorred">Click here to learn more...</button>
              </div>
            </div>
			<div class="sliderrightdiv"><img src="<?= base_url();?>/upload/gallery/slider/<?php echo $sl['image']; ?>" alt=""/></div>
            <div class="clearfix"></div>
          </div>
       <?php } ?>
         
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!--end of the slider part-->
<div align="center"> <img src="<?= base_url();?>/assets/user/images/bannershadow.png" alt=""></div>
<?php echo $banner['content']; ?>

<?php  echo $details['content']; ?>
<!--end of the details regarding wayindia-->


<?php  echo $products['content']; ?>
<!--<div class="graybg">
  <div class="container">
    <div class="blackhearding" align="center">Our products</div>
    <div class="padtop20">
      <div class="icondiv reldiv">
        <div class="bluehearding">Inventary Management</div>
        <div class="whitebox">
          <div align="center" class="padtop20"><img src="<?= base_url();?>/assets/user/images/icon1.png" alt=""></div>
          <div class="pad15">Our strategy lies in meeting clients’ demands & requirements with our years of experience in the domain. </div>
          <div class="boxlastdiv">
            <button type="button" class="btn btn-default">Read more</button>
          </div>
        </div>
      </div>
      <div class="icondiv reldiv">
        <div class="bluehearding">Live TV</div>
        <div class="whitebox">
          <div align="center" class="padtop20"><img src="<?= base_url();?>/assets/user/images/icon2.png" alt=""></div>
          <div class="pad15">Our strategy lies in meeting clients’ demands & requirements with our years of experience in the domain. </div>
          <div class="boxlastdiv">
            <button type="button" class="btn btn-default">Read more</button>
          </div>
        </div>
      </div>
      <div class="icondiv reldiv">
        <div class="bluehearding">E-magazine</div>
        <div class="whitebox">
          <div align="center" class="padtop20"><img src="<?= base_url();?>/assets/user/images/icon3.png" alt=""></div>
          <div class="pad15">Our strategy lies in meeting clients’ demands & requirements with our years of experience in the domain. </div>
          <div class="boxlastdiv">
            <button type="button" class="btn btn-default">Read more</button>
          </div>
        </div>
      </div>
      <div class="icondiv reldiv">
        <div class="bluehearding">News Reporter Management</div>
        <div class="whitebox">
          <div align="center" class="padtop20"><img src="<?= base_url();?>/assets/user/images/icon4.png" alt=""></div>
          <div class="pad15">Our strategy lies in meeting clients’ demands & requirements with our years of experience in the domain. </div>
          <div class="boxlastdiv">
            <button type="button" class="btn btn-default">Read more</button>
          </div>
        </div>
      </div>
      <div class="icondiv reldiv">
        <div class="bluehearding">LC/Banking Solution</div>
        <div class="whitebox">
          <div align="center" class="padtop20"><img src="<?= base_url();?>/assets/user/images/icon5.png" alt=""></div>
          <div class="pad15">Our strategy lies in meeting clients’ demands & requirements with our years of experience in the domain. </div>
          <div class="boxlastdiv">
            <button type="button" class="btn btn-default">Read more</button>
          </div>
        </div>
      </div>
      <div class="icondiv reldiv">
        <div class="bluehearding">e-Commerce Solution</div>
        <div class="whitebox">
          <div align="center" class="padtop20"><img src="<?= base_url();?>/assets/user/images/icon6.png" alt=""></div>
          <div class="pad15">Our strategy lies in meeting clients’ demands & requirements with our years of experience in the domain. </div>
          <div class="boxlastdiv">
            <button type="button" class="btn btn-default">Read more</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>-->
<!--end of the product part
-->
