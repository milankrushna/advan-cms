<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/ajxupload/upload.css"  />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/ajxupload/jquery.form.js" ></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!--PAGE CONTENT --> 
    <div class="content-wrapper">
              <section class="content-header">
          <h1>Create Page</h1>
         
        </section>
        <section class="content">
<div class="box box-primary">
<div class="box-body" >
<div id="infoMessage" style="color:#F00"><?php echo $message;?></div>
<form action="<?php echo  site_url('admin/pages/create_footer'); ?>" enctype="multipart/form-data" method="post" id="page_create"/>
 				
				  <?php /* 
		<div class="form-group">
				 <label for="text1" class="control-label">Make this as Gallery  &nbsp;&nbsp;&nbsp;&nbsp;:<br></label>
					<input type="radio" name="en_gallery" value="1"> Yes
					<input type="radio" checked="checked" name="en_gallery" value="0"> No
			</div> 
				*/ ?>
				
				 
             <?php /* 
			   <div class="form-group">
				 <label for="text1" class="control-label">Enable Bottom Slider &nbsp;:</label>
					<input type="radio" name="en_btnslider" value="1"> Yes
					<input type="radio" name="en_btnslider" value="0"> No
			</div> 
				 
		
		*/ ?>
    
                  
    
                    <div class="form-group">
                    <label for="text1" class="control-label">Page  Name</label>
                    
                                  <input  type="text" class="form-control" name="name" required=""><br>
                  
                	</div>	
                
                
               <!--   <div class="form-group">
                    <label for="text1" class="control-label">Page Heading</label>
                    
                                  <input  type="text" class="form-control" name="pg_head" required=""><br>
                  
                	</div>
                
                
                
                	<div class="form-group">
                    <label for="text1" class="control-label">Page Title	</label>
                    
                                  <input  type="text" class="form-control" name="title" required=""><br>
                  
                	</div>	
                
                	
                	<div class="form-group">
                    <label for="text1" class="control-label">Meta Keyword</label>
                    
                                  <input  type="text" class="form-control" name="meta_keyword" required=""><br>
                  
                	</div>
                	
                	
                	<div class="form-group">
                    <label for="text1" class="control-label">Meta Description</label>
                    
                                  <textarea class="form-control" name="meta_description" required=""></textarea><br>
                  
                	</div>
				<div class="form-group col-md-6">
                    <label for="text1" class="control-label">Image/PDF<br></label>
                            
                                  <input  type="file" class="form-control" name="upload_image1" ><br>
            
                	</div>	
    
    			<div class="form-group col-md-6">
                    <label for="text1" class="control-label">Link</label>
                    
                                  <input  type="text" class="form-control" name="imagelink" ><br>
                  
                	</div>	
    <div class="form-group col-md-6">
                    <label for="text1" class="control-label">Image/PDF</label>
                    
                                  <input  type="file" class="form-control" name="upload_image2" ><br>
                  
                	</div>	
    
    			<div class="form-group col-md-6">
                    <label for="text1" class="control-label"> Link<br></label>
                    
                                  <input  type="text" class="form-control" name="pdflink" ><br>
                  
                	</div>-->
    
                				
                   <div class="form-group">
                    <label for="text1" class="control-label">Page Content</label>
                   <div class="">
                              <?php
                require 'editor/fckeditor.php';
 				$oFCKeditor = new FCKeditor('txt_content') ;
				$oFCKeditor->BasePath = base_url().'editor/' ;
				$oFCKeditor->Height	= 390;
				$oFCKeditor->Config['EnterMode'] = 'br';
				$oFCKeditor->Value = '' ;
				$oFCKeditor->name = 'right' ;
				$oFCKeditor->Create() ;
		?>
                      </div>
                	</div>
					
					    
    <!--       Progress Bar-->
       <div id="p_bar" style="display:none;" class="hide">
           <div  class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" id="progressbar" style="width:0%">
                    <span id="statustxt"></span>
                </div>
           </div>
       </div>
       <!--       Progress Bar-->
					
					
					
		
                        <input type="submit" value="Submit" class="btn btn-success" />
               </form>   
	
	   </div>
</div>
    </div>
           </div>
</section>
              </div>
                    <!-- END PAGE CONTENT -->
				
	
<script>


 $(document).ready(function() {
        //elements
        var progressbox     = $('#progressbox');
        var progressbar     = $('#progressbar');
        var statustxt       = $('#statustxt');
        var submitbutton    = $("#SubmitButton");
        var myform          = $("#pagecreate");
        var output          = $("#output");
        var completed       = '0%';
 
                $(myform).ajaxForm({
                    beforeSend: function() { //brfore sending form
                        document.getElementById("p_bar").className = "show";
                        submitbutton.attr('disabled', ''); // disable upload button
                        statustxt.empty();
                        progressbox.slideDown(); //show progressbar
                        progressbar.width(completed); //initial value 0% of progressbar
                        statustxt.html(completed); //set status text
                        statustxt.css('color','#fff'); //initial color of status text
                    },
                    uploadProgress: function(event, position, total, percentComplete) { //on progress
                        progressbar.width(percentComplete + '%') //update progressbar percent complete
                        statustxt.html(percentComplete + '%'); //update status text
                        if(percentComplete>50)
                            {
                                statustxt.css('color','#fff'); //change status text to white after 50%
                            }

                        },
                    complete: function(response) { // on complete
                        var res = output.html(response.responseText); //update element with received data
                        myform.resetForm();  // reset form
                        submitbutton.removeAttr('disabled'); //enable submit button
                        progressbox.slideUp(); // hide progressbar
                        document.getElementById("p_bar").className = "hide";
                        
                        
if(response.responseText == 1){
alert("Successfully Upload");
}else if(response.responseText == 2){
alert("Please Upload a valid Image File");
}else if(response.responseText == 3){
alert("You have no rights to upload");
}else{
alert("Uploading unsuccessful Please Try again");
}
//location.reload();
                       /// location.reload();
                    }
                   
            });
        });


</script>
