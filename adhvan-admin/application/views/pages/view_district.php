       <link href="<?= base_url();?>/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
   <div id="content">
    <div class="inner">
            <div class="row">
                    <div class="col-lg-12">


                        <h2>DISTRICTS</h2>
<?php echo $msg; ?>


                    </div>
                </div>
            
            
<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            All District List
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>District Name</th>
                                            <th>District Name(In Odia)</th>
                                            <th>Description</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
          <?php foreach ($districts as $dist):?>
		<tr class="odd gradeX">
			<td><?php echo $dist->name;?></td>
			<td class="odia-fo"><?php echo $dist->odia_name;?></td>
            <td><?php echo $dist->description;?></td>
			<td><?php echo anchor("admin/admin/edit_district/".$dist->id, 'Edit') ;?> | <?php echo anchor("admin/admin/delete_district/".$dist->id, 'Delete') ;?></td>
		</tr>
		<?php endforeach;?>
                                    
                                    </tbody>
                                </table>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>

<p><?php echo anchor('admin/admin/create_district', 'Create a new District')?> | <?php echo anchor('admin/auth/create_group', lang('index_create_group_link'))?></p>
</div>
</div>
    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?= base_url();?>/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?= base_url();?>/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script>