    <link href="<?= base_url();?>/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
   <div class="content-wrapper">
            
        
      <section class="content-header">
          <h1>View image</h1>
         
        </section>
      
            <section class="content">               

                     <a class="btn btn-primary" href="<?= site_url('admin/pages/add_image').'/'.$page_id ?>" >Add New Image</a><br/>
<div class="box box-primary">
                <div class="box-body">

                    <div class="panel panel-default">
<?php echo $msg; ?>
                        
                       
						<div class="">
                            <div class="table-responsive">
                                <table class="table table-striped  table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Cover Images</th>
											<th>view Images</th>
                                            <th>Action</th>
                                        </tr>

                                    </thead>

                                    <tbody>

          <?php foreach ($galleries as $gall):
		  
		
		  ?>

		<tr class="odd gradeX">

			<td><?php echo $gall->title;?></td>


			<td><img src="<?= base_url();?>/upload/product/<?php echo $gall->image;?>" height="70px"  width="200px"  /></td>
			
			<td><?php echo anchor("admin/pages/view_innerimage/".$gall->id, 'View Images') ;?></td>
			<td align="center"><?php echo anchor("admin/pages/edit_image/".$gall->id, 'Edit') ;?> | <?php echo anchor("admin/pages/delete_image/".$gall->id, 'Delete') ;?></td>
		</tr>
		<?php endforeach;?>
                                    </tbody>

                                </table>

                            </div>

                           

                        </div>

                    </div>

                </div>

            </div>

</section>

</div>



    <!-- END GLOBAL SCRIPTS -->

        <!-- PAGE LEVEL SCRIPTS -->

    <script src="<?= base_url();?>/assets/plugins/dataTables/jquery.dataTables.js"></script>

    <script src="<?= base_url();?>/assets/plugins/dataTables/dataTables.bootstrap.js"></script>

     <script>

         $(document).ready(function () {

             $('#dataTables-example').dataTable();

         });

    </script>