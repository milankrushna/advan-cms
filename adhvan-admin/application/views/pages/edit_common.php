
  <div class="content-wrapper">
               
              <section class="content-header">
          <h1>Edit Section</h1>
         
        </section>
        <section class="content">
<div class="box box-primary">
<div class="box-body" >
<div id="infoMessage" style="color:#F00"><?php echo $message;?></div>
<form action="<?= site_url('admin/common/edit_common')."/".$page->id; ?>" method="post" id="loginform">
    
    <p style="color:#3c8dbc;"><a target="framename" href="<?php echo str_replace('admin/', '', base_url('content/'.$page->url)); ?>"><?php echo str_replace('admin/', '', base_url('content/'.$page->url)); ?></a></p>
    
				<div class="form-group">
                    <label for="text1" class="control-label">Item Name<br></label>
                 
                                  <input  type="text" value="<?php echo $page->name ?>" class="form-control" name="name"><br>
                    
                	</div>	
					
					
				<div class="form-group">
                    <label for="text1" class="control-label">Content</label>
                    <div class="">
                       <textarea name="txt_content" id="contentarea" ><?php echo $page->content; ?></textarea>
                        
					<?php /*
			$this->load->view('editor/fckeditor.php');
 				$oFCKeditor = new FCKeditor('txt_content') ;
				$oFCKeditor->BasePath = base_url().'editor/' ;
				$oFCKeditor->Height	= 390;
				$oFCKeditor->Config['EnterMode'] = 'br';
				$oFCKeditor->Value = $page->content ;
				$oFCKeditor->name = 'content' ;
				$oFCKeditor->Create() ; */
		?>
                      </div>
                	</div>
	    
                        <input type="submit" id="tags" value="Update" class="btn btn-primary" />
                 
	</form>
	  
</div>
    </div>
    </section>
           </div>
              </div>
                    <!-- END PAGE CONTENT -->
						
<script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>

<script>


    CKEDITOR.replace( 'contentarea', {
    height:400,        
    filebrowserBrowseUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/ckfinder.html',
    filebrowserUploadUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    filebrowserWindowWidth: '1000',
    filebrowserWindowHeight: '700'
});
    
    
</script>
	