    <link href="<?= base_url();?>/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
      <div class="content-wrapper">
       
    <section class="content-header">
          <h1>Inner Image</h1>
         
        </section>
       
        <section class="content">
         <a class="btn btn-primary" href="<?= site_url('admin/pages/add_innerimage').'/'.$page_id ?>">Add A New Image</a>
<div class="box box-primary">
<div class="box-body" >

                    <div class="panel panel-default">
<?php  echo $msg; ?>
                        <div class="panel-heading">
                            Image List of 
                        </div>
                        
						
						<div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Images</th>
											<th>Action</th>
                                        </tr>

                                    </thead>

                                    <tbody>

          <?php foreach ($galleries as $gall):
		  
		
		  ?>

		<tr class="odd gradeX">

			<td><?php echo $gall->title;?></td>


			<td><img src="<?= base_url();?>/upload/product/<?php echo $gall->image;?>" height="70px"  width="200px"  /></td>
			
			<td align="center"><?php echo anchor("admin/pages/edit_innerimage/".$gall->id, 'Edit') ;?> | <?php echo anchor("admin/pages/delete_innerimage/".$gall->id, 'Delete') ;?></td>
		</tr>
		<?php endforeach;?>
                                    </tbody>

                                </table>

                            </div>

                           

                        </div>

                    </div>

                </div>

            </div>
</section>

</div>


    <!-- END GLOBAL SCRIPTS -->

        <!-- PAGE LEVEL SCRIPTS -->

    <script src="<?= base_url();?>/assets/plugins/dataTables/jquery.dataTables.js"></script>

    <script src="<?= base_url();?>/assets/plugins/dataTables/dataTables.bootstrap.js"></script>

     <script>

         $(document).ready(function () {

             $('#dataTables-example').dataTable();

         });

    </script>