    <link href="<?= base_url();?>/assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" />
   <div class="content-wrapper">

<section class="content-header">
          <h1>All Pages </h1> 
<br/>
         <a class="btn btn-primary" href="<?php echo site_url('admin/pages/create_footer')?>" >Add Page</a>
        </section>

        <section class="content">
   <div id="">
           
            

                <div class="">
                    <div class="panel panel-default box box-info">
                        
                       
                        <div class="table-responsive">
                            <div class="">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Page Name</th>
                                            <th>Page url</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
          <?php foreach ($footer as $pg):?> 
		<tr class="odd gradeX">
            
            <td><?php echo $pg->name;?></td>
            <td><a target="_blank" href="<?php echo str_replace('admin/', '', base_url($pg->url.'.php')); ?>"><?php echo str_replace('admin/', '', base_url($pg->url.'.php')); ?></a></td>
			<!--<td><?php if($pg->en_gallery == '0'){?> Non Gallery Page <?php }else{ ?> Gallery Page <?php } ?></td>-->
			
			
          <!--  <td><?php if($pg->en_gallery == '0'){?> <?php }else{ ?> <a href="<?= site_url('admin/pages/view_image')."/".$pg->id ; ?>">View Images</a> <?php } ?></td>-->
			<td><?php echo anchor("admin/pages/edit_footer/".$pg->id, 'Edit') ;?> | <?php echo anchor("admin/pages/delete_pg/".$pg->id, 'Delete') ;?></td>
		</tr>
		<?php endforeach;?>                                    
                                    </tbody>
                                </table>
                            </div>                          
                        </div>
                    </div>
                </div>
            

</div>
</section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?= base_url();?>/assets/plugins/datatables/jquery.dataTables.js"></script>
    <script src="<?= base_url();?>/assets/plugins/datatables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script>	