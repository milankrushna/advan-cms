    <link href="<?= base_url();?>/assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" />
   <div class="content-wrapper">

<section class="content-header">
          <h1>All Articles </h1> 
<br/>
         <a class="btn btn-primary" href="<?php echo site_url('admin/pages/create_page')?>" >Add Article</a>
        </section>

        <section class="content">
   <div id="">
           
            

                <div class="">
                    <div class="panel panel-default box box-info">
                        
                        <?php echo $msg; ?>
                        <div class="table-responsive">
                            <div class="">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            
                                            <th>#</th>
                                            <th>Article Name</th>
                                            <th>Category</th>
                                            <th>Author</th>
                                            <th>Comment</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
          <?php $s=1; foreach ($pages as $pg):?> 
		<tr class="odd gradeX">
            
            <td><?php echo $s++; ?></td>
            <td><a href="<?php echo str_replace('admin/','',base_url($pg->url)); ?>" target="_blank" ><?php echo $pg->name;?></a></td>
            <td><?php echo $pg->mname;?></td>
            <td><?php echo $pg->author_name; ?></td>
            <td><a href="javascript:void(0);" class="cmt" datatitle="<?php echo $pg->name; ?>" data_href="<?php echo str_replace('admin/', '', base_url()).$pg->url; ?>" >Comment</a></td>
            <td>
            <?php 
         if($pg->status == 0 || $pg->status == 2){ $anc = 'Publish'; }else{ $anc = 'Archive'; }
         if($pg->status == 0 ){ $anc2 = '<span class="text-red">Trash</span>'; }else if($pg->status == 1){ $anc2 = '<span class="text-green">Published</span>'; }else if($pg->status == 2){ $anc2 = '<span class="text-yellow">Draft<span>'; }
    
                          ///  echo anchor('admin/slider/actdeact/'.$gall->id, $anc,array('class' => 'act-deact','title'=>$gall->status));    ?>
                 <?php echo $anc2; ?><!--   <a href="<?php echo site_url('admin/pages/draftcnpt/'.$pg->id); ?>"  class="act-deact" title="<?php echo $pg->status; ?>" ><?php echo $anc; ?></a>-->
                
            </td>
			<td> <a class="btn btn-warning btn-xs" href="<?php echo site_url('admin/pages/edit_page'."/".$pg->id)?>" ><i class="fa fa-edit"></i></a>||<a title="Delete" class="btn  btn-danger dlt btn-xs" href="<?php echo site_url('admin/pages/delete_pg'."/".$pg->id)?>" ><i class="fa fa-trash"></i></a></td>
       
		</tr>
                  
                                        
                                     
                                        
		<?php endforeach;?>                                    
                                    </tbody>
                                </table>
                            </div>                          
                        </div>
                    </div>
                </div>
            

</div>
</section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?= base_url();?>/assets/plugins/datatables/jquery.dataTables.js"></script>
    <script src="<?= base_url();?>/assets/plugins/datatables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
         
         
           $('a.dlt').click(function(evt){
           evt.preventDefault();
          if(confirm("Are You Sure ?")){       
               var dis = this;
                $.post($(dis).attr('href'),{'delete':'dlt'},function(resp){
                    if(resp == 1){
                        $(dis).parent().parent().remove();
                    }else{
                       alert(resp);
                    }
                });
            }
        }); 
         
         
         $('a.cmt').click(function(evt){
           evt.preventDefault();
             
               var dis = this;
             var arttitle = $(dis).attr('datatitle');
                $('#art-title').text(arttitle);
             $('#comment-box').html('<span>Loading comment...</span>');
             var hrefmode =  $(dis).attr('data_href');
             var srcmode = '<?php echo site_url('admin/pages/view_comment?comment='); ?>'+hrefmode;
              $('#cmtModal').modal('show');
             var comentbox =  '<iframe style="border:0;" width="630px" height="500" src="'+srcmode+'" ></iframe>'; 
             
                $('#comment-box').html(comentbox);
            
           
        });
    
    

 $('.act-deact').click(function(ev){
             ev.preventDefault();
            
             var dis = this;
             
             $.post($(this).attr('href'),{'sts':$(this).attr('title')},function(resp){
                 
                 if(resp == 0){
                    $(dis).html("Publish");
                    $(dis).attr("title",resp);
                 }else if(resp == 1){
                    $(dis).html("Archive");
                    $(dis).attr("title",resp);
                 }
                 
             });  
         });
         

         
    </script>	

<div id="cmtModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="width: 640px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
          <h4 id="art-title" style="color:#3c8dbc"></h4>
          <span id="comment-box"></span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>