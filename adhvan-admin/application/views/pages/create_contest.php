<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/ajxupload/upload.css"  />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/ajxupload/jquery.form.js" ></script>

 <link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/datepicker/datepicker3.css">
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>

    <!-- END PAGE LEVEL  STYLES -->
     <!--PAGE CONTENT --> 
    <div class="content-wrapper">
              <section class="content-header">
          <h1>Create contest</h1>
         
        </section>
        <section class="content">
<div class="box box-primary">
<div class="box-body" >
<div id="infoMessage" style="color:#F00"><?php echo $message;?></div>
<form action="<?php echo  current_url(); ?>" enctype="multipart/form-data" method="post" id="page_create"/>
 				
			
    
                   <div class="form-group">
                    <label for="text1" class="control-label">Type Of Page</label>
                          <input  type="text" class="form-control" name="category" required="" readonly value="contest"><br>
                	</div>
          
                 
                           
    
                    <div class="form-group">
                    <label for="text1" class="control-label">Contest Name</label>
                    
                                  <input  type="text" class="form-control" name="name" required=""><br>
                  
                	</div>	
                
                
<!--
                  <div class="form-group">
                    <label for="text1" class="control-label">Sponsor Author</label>
                    
                                  <input  type="text" class="form-control" name="author" required=""><br>
                  
                	</div>
-->
                
                
                
                	<div class="form-group">
                   	<label for="text1" class="control-label">Date</label>
                     
                <input type="text" name="date" class="form-control" value="" id="datepicker" required  />
                        
                	</div>	
                	
                	
                
                	
				<div class="form-groupn">
                    <label for="text1" class="control-label">Cover Pic<br></label>
                            
                                  <input  type="file" class="form-control" name="cover_img" ><br>
            
                	</div>	
    
    
                				
                   <div class="form-group">
                    <label for="text1" class="control-label">Content</label>
                   <div class="">
                       <textarea name="txt_content" id="contentarea" ></textarea>
                       
                              <?php /*
                require 'editor/fckeditor.php';
 				$oFCKeditor = new FCKeditor('txt_content') ;
				$oFCKeditor->BasePath = base_url().'editor/' ;
				$oFCKeditor->Height	= 390;
				$oFCKeditor->Config['EnterMode'] = 'br';
				$oFCKeditor->Value = '' ;
				$oFCKeditor->name = 'right' ;
				$oFCKeditor->Create() ; */
		?>
                      </div>
                	</div>
					
                        <input type="submit" value="Submit" class="btn btn-success" />
               </form>   
	
	   </div>
</div>
    </div>
           </div>
</section>
              </div>
                    <!-- END PAGE CONTENT -->
<script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>
				
	
<script>

         CKEDITOR.replace( 'contentarea', {

    height:400,        
    filebrowserBrowseUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/ckfinder.html',
    filebrowserUploadUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    filebrowserWindowWidth: '1000',
    filebrowserWindowHeight: '700'
});


    
$(function () {
  
 $('#datepicker').datepicker({
      autoclose: true,
      format: "dd-mm-yyyy"
    });
});

    
    function get_category(dis){
        
       // alert(id);
         $('#category').html('<option value="">Loading...</option>');
        
        $.get('<?php echo site_url('admin/pages/get_sub');?>',{prob_id:dis},function(resp){
           
            $('#subcatgy').html(resp);
            
        });
         
    }
    
    
</script>
