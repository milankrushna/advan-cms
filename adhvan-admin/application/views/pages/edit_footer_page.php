<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/ajxupload/upload.css"  />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/ajxupload/jquery.form.js" ></script>    
    <!-- END PAGE LEVEL  STYLES -->
     <!--PAGE CONTENT --> 
    <div class="content-wrapper">
               
              <section class="content-header">
          <h1>Edit Page</h1><br>
                  <a class="btn btn-primary" href="<?php echo site_url(); ?>/admin/pages/create_page">Add Page</a>
                  <a class="btn btn-primary" href="<?php echo site_url(); ?>/admin/pages">Page listing</a>
         
        </section>
        <section class="content">
<div class="box box-primary">
<div class="box-body" >
<div id="infoMessage" style="color:#F00"><?php echo $message;?></div>
<form action="<?php echo site_url('admin/pages/edit_footer')."/".$page->id; ?>" method="post" id="editpageh"  enctype="multipart/form-data"  >
 		
     
                    <div class="form-group">
                    <label for="text1" class="control-label">Url : </label>
                        <span id="url_mdf"><?php echo str_replace("admin/","",base_url()); ?><a href="<?php echo str_replace("admin/","",base_url($page->url)).'.php'; ?>" target="framename" ><?php echo $page->url; ?></a> <a class="fa fa-edit" style="cursor:pointer;" onclick="modify_url('<?php  echo $page->id; ?>','<?php echo $page->url; ?>');" ></a></span>
                	</div>	
    
    
    
    
    
    
		<?php /*
		<div class="form-group">
				 <label for="text1"  class="control-label">Make this as Gallery&nbsp;&nbsp;&nbsp;&nbsp;:</label>
				<input type="radio" <?php if($page->en_gallery == 1){?> checked="checked" <?php } ?> name="en_gallery" value="1"> Yes
					<input type="radio" <?php if($page->en_gallery == 0){?> checked="checked" <?php } ?> name="en_gallery" value="0"> No
			</div> 
	 */	?>
		
    
    
			 <div class="form-group">
                    <label for="text1" class="control-label">Slider</label>
                        <select class="form-control" name="en_slider"  required>
                            <option value="">Select a Slider</option>
                            
                            <?php foreach($slider as $sl){ ?>
                            <option  <?php  if($page->en_slider == $sl['id']){ echo "selected";  } ?>   value="<?php echo $sl['id']; ?>"><?php  echo $sl['title']; ?></option>
                            <?php } ?>
                            
                        </select>
                	</div>
                           
			<?php /*
			<div class="form-group">
				 <label for="text1"  class="control-label">Enable Bottom Slider&nbsp;:</label>
				 	<input type="radio" <?php if($page->en_btnslider == 1){?> checked="checked" <?php } ?> name="en_btnslider" value="1"> Yes
					<input type="radio" <?php if($page->en_btnslider == 0){?> checked="checked" <?php } ?> name="en_btnslider" value="0"> No
			</div>	
			*/	?>
				<div class="form-group">
                    <label for="text1" class="control-label">Page  Name</label>
                 
                                  <input  type="text" value="<?php echo $page->name ?>" class="form-control" name="name">
                      
                	</div>	
                	
                	
                	<!--<div class="form-group">
                    <label for="text1" class="control-label">Page Heading</label>
                 
                                  <input  type="text" value="<?php echo $page->pg_head ?>" class="form-control" name="pg_head">
                      
                	</div>
                	
                	
					<div class="form-group">
                    <label for="text1" class="control-label">Page  Title</label>
                 
                                  <input  type="text" value="<?php echo $page->title ?>" class="form-control" name="title">
                      
                	</div>	
				
					
                	<div class="form-group">
                    <label for="text1" class="control-label">Meta Keyword<br></label>
                    
                                  <input  type="text" class="form-control" name="meta_keyword" value="<?php echo $page->meta_keyword; ?>" required=""><br>
                  
                	</div>
                	
                	
                	<div class="form-group">
                    <label for="text1" class="control-label">Meta Description</label>
                    
      <textarea class="form-control" name="meta_description" ><?php echo $page->meta_description; ?></textarea>
                  
                	</div>
					<div class="form-group col-md-6" style="padding-left: 2px; " >
                    <label for="text1" class="control-label">Image / PDF<br></label>
                            
                                  <input  type="file" class="form-control" name="upload_image1" ><br>
                
                	</div>	
    
                         
                            
                             
    
    			    <div class="form-group col-md-6">
                    <label for="text1" class="control-label"> Link<br></label>
                    
<input  type="text" class="form-control" name="sec_1_link" value="<?php echo $page->sec_1_link; ?>" ><br>
                  
                	</div>  
                	
                	<?php
                            
                            $data="";
                              if(!empty($page->sec_1_data))
                              {
                             $temp=explode('.',$page->sec_1_data);
                             
                           //  echo $page->sec_1_data;
                             
                            // print_r($temp);exit;
                             
                             $data=$temp[1];
                           

                                 if($data == 'jpg' || $data == 'jpeg' || $data == 'JPG' || $data == 'JPEG' || $data == 'PNG' || $data == 'gif'|| $data == 'png'){
                                    
                                  //  print_r($temp);exit;
                                   
                                   ?>
                                  
                                  
                                    <div id="seddata1" style="padding-left: 10px;">
                                    <img  src="<?php echo base_url('upload/page/min_page/'.$page->sec_1_data); ?>" > 
                           		<a style="cursor:pointer;" onclick="remove_image('<?php  echo $page->id;  ?>',1);">Remove</a>
                                    </div>

                                   <?php }else if($data == 'pdf' || $data == 'PDF'){
    
                                 
                                     
                                        ?>

                       <div style="padding-left: 8px; padding-bottom: 15px;">
                <a class="btn btn-info" href="<?php echo base_url('upload/page/'.$page->sec_1_data) ;?>" target="_blank" ><i class="fa fa-file-pdf-o"></i>PDF</a>
                 
                         </div>	


                                   <?php  }  } ?>
                            
                
                	
                	
                
    <div style="clear:both;"></div>
                    <div class="form-group col-md-6" style="padding-left: 2px;" >
                    <label for="text1" class="control-label">Image / PDF<br></label>
                    
                                  <input  type="file" class="form-control" name="upload_image2" >
                                  
                	</div>
                	
                	     
                        
                	
                	<div class="form-group col-md-6">
                    <label for="text1" class="control-label"> Link<br></label>
                    
                                  <input  type="text" class="form-control" name="sec_2_link" value="<?php echo $page->sec_2_link; ?>" ><br>
                  
                	</div>	
              
                	
                	 <?php
                	    
                              $ext="" ;
                              if(!empty($page->sec_2_data))
                              {
                             $temp2=explode('.',$page->sec_2_data);
                             $ext=$temp2[1];
                            
							 		 

                                 if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'PNG' || $ext == 'gif'|| $ext == 'png'){
                                    
                                  //  print_r($temp);exit;
                                   
                                   ?>
                                 <div style="padding-left: 10px;">
                             <span  id="seddata2">    <img width="200px"  src="<?php echo base_url('upload/page/'.$page->sec_2_data); ?>" >     
<a style="cursor:pointer;" onclick="remove_image('<?php  echo $page->id;  ?>',2);">Remove</a> </span>                      
                                </div>
                                   <?php }else if($ext == 'pdf' || $ext == 'PDF'){ ?>


                                  <a class="btn btn-info" href="<?php echo base_url('upload/page/'.$page->sec_2_data); ?>" target="_blank" ><i class="fa fa-file-pdf-o"></i>PDF</a>

                                   <?php  } } ?>
						    <div style="clear:both;"></div>-->
			
                   <div class="form-group">
                    <label for="text1" class="control-label " style="padding-top: 15px">Page Content</label>
                    <div class="">
					<?php
			$this->load->view('editor/fckeditor.php');
 				$oFCKeditor = new FCKeditor('txt_content') ;
				$oFCKeditor->BasePath = base_url().'editor/' ;
				$oFCKeditor->Height	= 390;
				$oFCKeditor->Config['EnterMode'] = 'br';
				$oFCKeditor->Value = $page->right ;
				$oFCKeditor->name = 'content' ;
				$oFCKeditor->Create() ;
		?>
                      </div>
                	</div>
                	
                	
                	
                	
                					    
    <!--       Progress Bar-->
       <div id="p_bar" style="display:none;" class="hide">
           <div  class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" id="progressbar" style="width:0%">
                    <span id="statustxt"></span>
                </div>
           </div>
       </div>
       <!--       Progress Bar-->
					
					
	     	
                        <input type="submit" id="tags" value="Update" class="btn btn-success" />
                    
	</form>
	   </div>
</div>
    </div>
    </section>
           </div>
             
                    <!-- END PAGE CONTENT -->
<script>


    function modify_url(pgId,pgUrl){
    
        var edtfld = '<div class="input-group input-group-sm col-sm-8"><input onkeyup="gen_url(this.value,'+pgId+');" type="text" id="url_data" value="'+pgUrl+'" class="form-control"><span class="input-group-btn"><button onclick="update_url('+pgId+');" class="btn btn-info btn-flat" type="button">Update</button></span></div><br>Your Url Wll be:  <span class="text-blue"><?php echo str_replace("admin/","",base_url()); ?><span id="gen_url" >'+pgUrl+'</span>.php</span>';
        
        $('#url_mdf').html(edtfld);
    
    
    }
    function gen_url(dis,pId){
        
      var new_url =   dis.toLowerCase().replace(/-+$/,'');
      $('#gen_url').html(new_url); 
        
        
    }
    
    function update_url(pageid){
        
     var newurl =  $('#gen_url').html();
      
        $.post('<?php echo site_url('admin/pages/change_url'); ?>',{url_data:newurl,page_id:pageid},function(res){
        
            var resp = JSON.parse(res);
              
            
        if(resp.status == 1){
           // $('#url_mdf').html(resp.message);
            alert("Url Successfully Updated"); 
            
        }else{
            alert(resp.message);
        }    
            
      });
        
    
        
    }
    
    	
	function remove_image(pgid,nof){

	
		 $.post('<?php echo site_url('admin/pages/remove_img'); ?>',{pgid:pgid,nof:nof},function(res){

			if(res == 1){
			$('#seddata'+nof).remove(); 
			}

		  });
		
	}


 $(document).ready(function() {
        //elements
        var progressbox     = $('#progressbox');
        var progressbar     = $('#progressbar');
        var statustxt       = $('#statustxt');
        var submitbutton    = $("#SubmitButton");
        var myform          = $("#editpage");
        var output          = $("#output");
        var completed       = '0%';
 
                $(myform).ajaxForm({
                    beforeSend: function() { //brfore sending form
                        document.getElementById("p_bar").className = "show";
                        submitbutton.attr('disabled', ''); // disable upload button
                        statustxt.empty();
                        progressbox.slideDown(); //show progressbar
                        progressbar.width(completed); //initial value 0% of progressbar
                        statustxt.html(completed); //set status text
                        statustxt.css('color','#fff'); //initial color of status text
                    },
                    uploadProgress: function(event, position, total, percentComplete) { //on progress
                        progressbar.width(percentComplete + '%') //update progressbar percent complete
                        statustxt.html(percentComplete + '%'); //update status text
                        if(percentComplete>50)
                            {
                                statustxt.css('color','#fff'); //change status text to white after 50%
                            }

                        },
                    complete: function(response) { // on complete
                        var res = output.html(response.responseText); //update element with received data
                        myform.resetForm();  // reset form
                        submitbutton.removeAttr('disabled'); //enable submit button
                        progressbox.slideUp(); // hide progressbar
                        document.getElementById("p_bar").className = "hide";
                        
                        
if(response.responseText == 1){
alert("Successfully Upload");
}else if(response.responseText == 2){
alert("Please Upload a valid Image File");
}else if(response.responseText == 3){
alert("You have no rights to upload");
}else{
alert("Uploading unsuccessful Please Try again");
}
//location.reload();
                       /// location.reload();
                    }
                   
            });
        });




</script>
