<script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/ajxupload/upload.css"  />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/ajxupload/jquery.form.js" ></script>    
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
   <link rel="stylesheet" href="<?php echo base_url()?>assets/jquery.tag-editor.css">
    <!-- END PAGE LEVEL  STYLES -->
     <!--PAGE CONTENT --> 
    <div class="content-wrapper">
               
              <section class="content-header">
          <h1>Edit Article</h1><br>
                  
                  
                  <a class="btn btn-primary btn-xs" href="<?php echo site_url(); ?>/admin/pages/create_page">Add Article</a>
                  <a class="btn btn-primary btn-xs" href="<?php echo site_url(); ?>/admin/pages">Article listing</a>
         
             <?php  if($page->status == 0 || $page->status == 2){ $anc = 'Publish'; }else{ $anc = 'Archive'; }  ?>
                  <a  class="btn btn-info btn-xs act-deact" href="<?php echo site_url('admin/pages/draftcnpt/'.$page->id); ?>"   title="<?php echo $page->status; ?>" ><?php echo $anc; ?></a>
                  
        </section>
        <section class="content">
<div class="box box-primary">
<div class="box-body" >
<div class="infoMessage" style="color:#F00"><?php echo $message;?></div>
    
<!--    Page Url:    <a target="_blank" href="<?php  echo 'http://mycitylinks.in/mycitylink_web/'.$page->url; ?>"><?php  echo 'http://mycitylinks.in/mycitylink_web/'.$page->url; ?></a>-->
    
<form action="<?php echo site_url('admin/pages/edit_page')."/".$page->id; ?>" method="post" id="editpageh"  enctype="multipart/form-data"  >
 		
     
                    <div class="form-group">
                    <label for="text1" class="control-label">Url : </label>
                        <span id="url_mdf"><?php echo str_replace("admin/","",base_url()); ?><a href="<?php echo str_replace("admin/","",base_url($page->url)); ?>" target="framename" ><?php echo $page->url; ?></a> <a class="fa fa-edit" style="cursor:pointer;" onclick="modify_url('<?php  echo $page->id; ?>','<?php echo $page->url; ?>');" ></a></span>
                	</div>	
    
    
    
    
    
    
		<?php /*
		<div class="form-group">
				 <label for="text1"  class="control-label">Make this as Gallery&nbsp;&nbsp;&nbsp;&nbsp;:</label>
				<input type="radio" <?php if($page->en_gallery == 1){?> checked="checked" <?php } ?> name="en_gallery" value="1"> Yes
					<input type="radio" <?php if($page->en_gallery == 0){?> checked="checked" <?php } ?> name="en_gallery" value="0"> No
			</div> 
	 */	?>
		
    <?php echo validation_errors(); ?>
    <div style="display:none;">
    <input type="checkbox" name="android" <?php if($page->android == 1){ ?> checked <?php } ?> value="1" >Android <br>
    <input type="checkbox" name="ios" <?php if($page->ios == 1){ ?> checked <?php } ?> value="1" >IOS
    </div>
    
    <input type="hidden" name="category"  value ="2">
    <input type="hidden" name="sub_cat"  value ="">
		<?php /*	 <div class="form-group">
                    <label for="text1" class="control-label">Category</label>
                        <select class="form-control" name="category"  onchange="get_category(this.value);" required>
                            <option value="">Select a Category</option>
                            
                             <?php 
                       foreach($select_cat as $scat){ ?>  
                             <option <?php  if($page->cat_id == $scat->id){ echo "selected"; } ?> value="<?php echo $scat->id; ?>"><?php echo $scat->menu; ?></option>
                            <?php 
                            }
                            ?>
                            
                            
                        </select>
                	</div>
    
    
              <div class="form-group">
                    <label for="text1" class="control-label">Sub Category</label>
                        <select class="form-control" name="sub_cat" id="subcatgy"  >
                            <option value="">Select a Sub Category</option>
                            
                           
                             <?php
                                 foreach($sub_category as $slp){ ?>
                         <option <?php  if($page->sub_catid == $slp->id){ echo "selected"; } ?>  value="<?php echo $slp->id; ?>"><?php echo $slp->menu; ?></option>
                         <?php } ?>
                            
                        </select>
                	</div>  */ ?>
                           
			<?php /*
			<div class="form-group">
				 <label for="text1"  class="control-label">Enable Bottom Slider&nbsp;:</label>
				 	<input type="radio" <?php if($page->en_btnslider == 1){?> checked="checked" <?php } ?> name="en_btnslider" value="1"> Yes
					<input type="radio" <?php if($page->en_btnslider == 0){?> checked="checked" <?php } ?> name="en_btnslider" value="0"> No
			</div>	
			*/	?>
				<div class="form-group">
                    <label for="text1" class="control-label">Article Name</label>
                 
                                  <input  type="text" value="<?php echo $page->name ?>" class="form-control" name="name">
                      
                	</div>	
                	
    <?php /*
            <div class="form-group">
                    <label for="text1" class="control-label">Meta title</label>
                <input  type="text" class="form-control" value="<?php echo $menu->meta_title; ?>" name="meta_title">
                	</div>
      <div class="form-group">
                    <label for="text1" class="control-label">Meat Description</label>
          <textarea  type="text" class="form-control" name="meta_desc"  ><?php echo $menu->meta_desc; ?></textarea>
                	</div>  
    
    <div class="form-group">
        <label for="text1" class="control-label">Meat Keyword</label>
        <textarea  type="text" class="form-control" name="meta_keyword"><?php echo $menu->meta_keyword; ?></textarea>
     </div> 
           
    */ ?>
                <input type="hidden" name="author"  value ="1">
                	
                	<!-- <div class="form-group">
                    <label for="text1" class="control-label">Author</label>
                        <select class="form-control" name="author" required>
                            <option value="">Select a Author</option>
                            
                             <?php 
                       foreach($author as $authr){ ?>  
                             <option <?php  if($page->author_id == $authr->id){ echo "selected"; } ?> value="<?php echo $authr->id; ?>"><?php echo $authr->name; ?></option>
                            <?php 
                            }
                            ?>
                            
                            
                        </select>
                	</div>
                   -->
   
					<div class="form-group">
                   	<label for="text1" class="control-label">Date</label>
                
                            <input type="text" name="date" value="<?php echo date('d-m-Y',strtotime($page->date)); ?>" class="form-control" id="datepicker"  required readonly="" />
                        
                	</div>
					
             <div class="form-group">
                   	<label for="text1" class="control-label">Tags</label>
                     
               <textarea id="tag-ctrl"   name="tags" ><?php echo $page->tags; ?></textarea>     
                	</div>
    
    
					<div class="form-group " >
                    <label for="text1" class="control-label">Cover Pic<br></label><br>
                         <img id="imgupoutput" width="117px" src="<?php echo base_url()?>upload/page/min_page3/<?php echo $page->sec_1_data; ?>">
                            
                                  <input  type="file" class="form-control" name="cover_pic"  onchange="loadFile(event)"><br>
                    <p style="color:red;">Image should be 1200x650</p>
                
                	</div>	
    
                         <div class="form-group">
                   	<label for="text1" class="control-label">Short Content</label>
                     
                           <div class="">
                    <textarea name="short_content" id="sht_editor"  style="display:none;" ><?php  echo $page->short_content; ?></textarea>
                               
                         <textarea  id="short_content"><?php  echo $page->short_content; ?></textarea>
					<?php /*
			    $this->load->view('editor/fckeditor.php');
 				$oFCKeditor = new FCKeditor('short_content');
				$oFCKeditor->BasePath = base_url().'editor/';
				$oFCKeditor->Height	= 200;
				$oFCKeditor->Config['EnterMode'] = 'br';
				$oFCKeditor->Value = $page->short_content;
				$oFCKeditor->name = 'content' ;
				$oFCKeditor->Create() ;
		       */   ?>
                      </div>     
                             
                            
                        
                	</div>	
    
    
                
						    <div style="clear:both;"></div>
			
                   <div class="form-group">
                    <label for="text1" class="control-label " style="padding-top: 15px">Article Content</label>
                    <div class="">
                        
                    <textarea name="txt_content" id="main_editor" style="display:none" ><?php  echo $page->right; ?></textarea>
                        
                        
                         <textarea  id="contentarea"><?php  echo $page->right; ?></textarea>
					<?php /*
			    $this->load->view('editor/fckeditor.php');
 				$oFCKeditor = new FCKeditor('txt_content');
				$oFCKeditor->BasePath = base_url().'editor/';
				$oFCKeditor->Height	= 390;
				$oFCKeditor->Config['EnterMode'] = 'br';
				$oFCKeditor->Value = $page->right ;
				$oFCKeditor->name = 'content' ;
				$oFCKeditor->Create() ; */
		          ?>
                      </div>
                	</div>
   
    <div id="p_bar" style="display:none;" class="hide">
         Uploading ... <span style="color:red !important" id="statustxt"></span>   
       <!--  <div  class="progress">
               <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" id="progressbar" style="width:0%"></div>
                  
                
           </div>-->
       </div>
<div class="infoMessage" style="color:#F00"><?php echo $message;?></div>
    
  <?php  if($page->status == 2){ ?>
	<button type="submit" value="2" name="save_type" id="svdrft" class="btn btn-warning SubmitButton" >Save Draft</button>
    <button type="submit" value="1" name="save_type" id="pubart" class="btn btn-primary SubmitButton" >Publish</button>
         <button  type="submit" value="3" name="save_type" class="btn btn-default SubmitButton" >Preview</button>
  <?php }else{ ?>
	<button type="submit" value="<?php echo $page->status; ?>" name="save_type" class="btn btn-primary SubmitButton" >Update</button>
  <a target="framename" href="<?php echo str_replace('admin/', '', base_url($page->url)); ?>" class="btn btn-default">Preview</a>
    <?php  } ?>


              
	</form>
	   </div>
</div>
    </div>
    </section>
           </div>
             
                    <!-- END PAGE CONTENT -->

    <script src="<?php echo base_url()?>assets/jquery.tag-editor.min.js"></script>
    <script src="<?php echo base_url()?>assets/jquery.caret.min.js"></script>

   <script>
    
       
 $('.act-deact').click(function(ev){
             ev.preventDefault();
            
             var dis = this;
             
             $.post($(this).attr('href'),{'sts':$(this).attr('title')},function(resp){
                 
                 if(resp == 0){
                    $(dis).html("Publish");
                    $(dis).attr("title",resp);
                 }else if(resp == 1){
                    $(dis).html("Archive");
                    $(dis).attr("title",resp);
                 }
                 
             });  
         });
         

       
       
        $(function() {
    
            $('#tag-ctrl').tagEditor({
                autocomplete: { delay: 0, position: { collision: 'flip' }, source: <?php echo json_encode($tags); ?> },
                forceLowercase: false,
                placeholder: 'Tag of Articles..'
            });
 });
 
            
             var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('imgupoutput');
      output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);

  };

            
            
    </script>


<script>
    
    

      var sht_editor = CKEDITOR.replace( 'short_content', {
    height:150,       
    filebrowserBrowseUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/ckfinder.html',
    filebrowserUploadUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    filebrowserWindowWidth: '1000',
    filebrowserWindowHeight: '700'
}); 
    
    sht_editor.on( 'change', function ( ev ) {

        $('#sht_editor').html(sht_editor.getData());
    
    	});
    
    
   var main_editor =   CKEDITOR.replace( 'contentarea', {
    height:400,       
    filebrowserBrowseUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/ckfinder.html',
    filebrowserUploadUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    filebrowserWindowWidth: '1000',
    filebrowserWindowHeight: '700'
});
    
     main_editor.on( 'change', function ( ev ) {

        $('#main_editor').html(main_editor.getData());
    
    	});  
    
    

    function modify_url(pgId,pgUrl){
    
        var edtfld = '<div class="input-group input-group-sm col-sm-8"><input onkeyup="gen_url(this.value,'+pgId+');" type="text" id="url_data" value="'+pgUrl+'" class="form-control"><span class="input-group-btn"><button onclick="update_url('+pgId+');" class="btn btn-info btn-flat" type="button">Update</button></span></div><br>Your Url Wll be:  <span class="text-blue"><?php echo str_replace("admin/","",base_url()); ?><span id="gen_url" >'+pgUrl+'</span></span>';
        
        $('#url_mdf').html(edtfld);
    
    
    }
    function gen_url(dis,pId){
        
      var new_url =   dis.toLowerCase().replace(/-+$/,'');
      $('#gen_url').html(new_url); 
        
        
    }
    
    function update_url(pageid){
        
     var newurl =  $('#gen_url').html();
      
        $.post('<?php echo site_url('admin/pages/change_url'); ?>',{url_data:newurl,page_id:pageid},function(res){
        
            var resp = JSON.parse(res);
              
            
        if(resp.status == 1){
           // $('#url_mdf').html(resp.message);
            alert("Url Successfully Updated"); 
            
        }else{
            alert(resp.message);
        }    
            
      });
        
    
        
    }
    
    
    function get_category(dis){
        
       // alert(id);
         $('#subcatgy').html('<option value="">Loading...</option>');
        
        $.get('<?php echo site_url('admin/pages/get_sub');?>',{prob_id:dis},function(resp){
           
            $('#subcatgy').html(resp);
            
        });
         
    }
    	
	function remove_image(pgid,nof){

	
		 $.post('<?php echo site_url('admin/pages/remove_img'); ?>',{pgid:pgid,nof:nof},function(res){

			if(res == 1){
			$('#seddata'+nof).remove(); 
			}

		  });
		
	}

    
     
    
    
 $(document).ready(function() {
        //elements
        var progressbox     = $('#progressbox');
        var progressbar     = $('#progressbar');
        var statustxt       = $('#statustxt');
        var submitbutton    = $(".SubmitButton");
        var myform          = $("#editpageh");
        var output          = $("#output");
        var completed       = '0%';
 
                $(myform).ajaxForm({
                    beforeSend: function() { //brfore sending form
                         $('.infoMessage').html('');
                        document.getElementById("p_bar").className = "show";
                        submitbutton.attr('disabled', ''); // disable upload button
                        statustxt.empty();
                        progressbox.slideDown(); //show progressbar
                        progressbar.width(completed); //initial value 0% of progressbar
                        statustxt.html(completed); //set status text
                        statustxt.css('color','red'); //initial color of status text
                    },
                    uploadProgress: function(event, position, total, percentComplete) { //on progress
                        progressbar.width(percentComplete + '%') //update progressbar percent complete
                        statustxt.html(percentComplete + '%'); //update status text
                        if(percentComplete>50)
                            {
                                statustxt.css('color','red'); //change status text to white after 50%
                            }

                        },
                    complete: function(response) { // on complete
                        var res = output.html(response.responseText); //update element with received data
                       //// myform.resetForm();  // reset form
                        submitbutton.removeAttr('disabled'); //enable submit button
                        progressbox.slideUp(); // hide progressbar
                        document.getElementById("p_bar").className = "hide";
                        
                        ///console.log(response);
                        
                        var opresp = JSON.parse(response.responseText);
                        
                        if(opresp.status == 'formerr'){
                            
                            $('.infoMessage').html(opresp.message);
                        }else{
                           
                        if(opresp.pid!=0){
                         ///$("#page_create").attr('action', '<?php echo site_url('admin/pages/edit_page') ?>'+'/'+opresp.pid);   
                        //$('#rtn_pid').val(opresp.pid);
                        $('.infoMessage').text(opresp.message);
                        $('#upcover_pic').val('');
                            
                            if(opresp.sv_type==3){
                        prw_url = '<?php echo str_replace('admin/', '', base_url('preview_load_system/')); ?>'+'/'+opresp.pid;
                            window.open(prw_url,'_blank');
                            }
                            
                             if(opresp.sv_type==1){
                        $('#svdrft').remove();
                        $('#pubart').html('Update');
                                 
                                 
                             }
                        }
                    }
                        
                    }
                   
            });
        });

    
    
    

</script>
