    <link href="<?= base_url();?>/assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" />
   <div class="content-wrapper">

<section class="content-header">
          <h1>All Sponsors Ad. </h1> 
<br/>
         <a class="btn btn-primary" href="<?php echo site_url('admin/pages/create_sponser')?>" >Add Sponsers</a>
        </section>

        <section class="content">
   <div id="">
           
            

                <div class="">
                    <div class="panel panel-default box box-info">
                        
                      
                        <div class="table-responsive">
                            <div class="">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Sponsor Name</th>
                                            <th>Sponsor Author</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
          <?php foreach ($sponsers as $pg):?> 
		<tr class="odd gradeX">
            
            <td><?php echo $pg->name;?></td>
            <td><?php echo $pg->author;?></td>
           <!-- <td><a target="_blank" href="<?php echo str_replace('admin/', '', base_url($pg->url.'.php')); ?>"><?php echo str_replace('admin/', '', base_url($pg->url.'.php')); ?></a></td>-->
			<!--<td><?php if($pg->en_gallery == '0'){?> Non Gallery Page <?php }else{ ?> Gallery Page <?php } ?></td>-->
			
			
          <!--  <td><?php if($pg->en_gallery == '0'){?> <?php }else{ ?> <a href="<?= site_url('admin/pages/view_image')."/".$pg->id ; ?>">View Images</a> <?php } ?></td>-->
			<td> <a class="btn btn-warning" href="<?php echo site_url('admin/pages/edit_sponser'."/".$pg->id)?>" ><i class="fa fa-edit"></i></a>||<a title="Delete" class="btn  btn-danger" href="<?php echo site_url('admin/pages/delete_sponser'."/".$pg->id)?>" onclick="return confirm('Are You sure?');"><i class="fa fa-trash"></i></a></td>
		</tr>
                  
                                        
                                     
                                        
		<?php endforeach;?>                                    
                                    </tbody>
                                </table>
                            </div>                          
                        </div>
                    </div>
                </div>
            

</div>
</section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?= base_url();?>/assets/plugins/datatables/jquery.dataTables.js"></script>
    <script src="<?= base_url();?>/assets/plugins/datatables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script>	