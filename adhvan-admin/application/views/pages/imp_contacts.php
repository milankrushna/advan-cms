

    
 <link href="<?= base_url();?>/assets/css/jquery-ui.css" rel="stylesheet" />
<link rel="stylesheet" href="<?= base_url();?>/assets/plugins/uniform/themes/default/css/uniform.default.css" />
<link rel="stylesheet" href="<?= base_url();?>/assets/plugins/inputlimiter/jquery.inputlimiter.1.0.css" />
<link rel="stylesheet" href="<?= base_url();?>/assets/plugins/chosen/chosen.min.css" />
<link rel="stylesheet" href="<?= base_url();?>/assets/plugins/colorpicker/css/colorpicker.css" />
<link rel="stylesheet" href="<?= base_url();?>/assets/plugins/tagsinput/jquery.tagsinput.css" />
<link rel="stylesheet" href="<?= base_url();?>/assets/plugins/daterangepicker/daterangepicker-bs3.css" />
<link rel="stylesheet" href="<?= base_url();?>/assets/plugins/datepicker/css/datepicker.css" />
<link rel="stylesheet" href="<?= base_url();?>/assets/plugins/timepicker/css/bootstrap-timepicker.min.css" />
<link rel="stylesheet" href="<?= base_url();?>/assets/plugins/switch/static/stylesheets/bootstrap-switch.css" />
    
         <!-- GLOBAL SCRIPTS -->


    <link rel="stylesheet" href="<?= base_url();?>/assets/validate/css/screen.css" />
<script src="<?= base_url();?>/assets/validate/dist/jquery.validate.js"></script>

<script>


$().ready(function() {
	// validate the comment form when it is submitted
	$("#commentForm").validate();

	// validate signup form on keyup and submit
	$("#loginform").validate({
		rules: {
			grv_no:{
				 required : true,
				 remote: "<?= site_url('admin/grievance/niyamabali') ?>"
				
				},
			description: "required",
			category_id :"required",
			district_id :"required",
			
		},
		messages: {
					grv_no:{
				 required : "<br/>Grievance No Is Required!",
				 remote: "<br/>Grievance No Must Be Unique!"
				
				},
					description: "<br/>passswod is required!",
					category_id :"<br/>Category required!",
					district_id :"<br/>District is required!",
		
		}
	});

	// propose username by combining first- and lastname
	

	//code to hide topic selection, disable for demo
	var newsletter = $("#newsletter");
	// newsletter topics are optional, hide at first
	var inital = newsletter.is(":checked");
	var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
	var topicInputs = topics.find("input").attr("disabled", !inital);
	// show when newsletter is checked
});

</script>
    
    
    
    	<script src="<?php echo base_url()?>assets/ckeditor/ckeditor.js"></script>
	<script>
		// Remove advanced tabs for all editors.
		CKEDITOR.config.removeDialogTabs = 'image:advanced;link:advanced;flash:advanced;creatediv:advanced;editdiv:advanced';
	</script>
    
<style>
.odia-fo{
	
	font-size:22px;
	}
</style>


    <!-- END PAGE LEVEL  STYLES -->
     <!--PAGE CONTENT --> 
    <div id="content">
                <div class="inner">
               <div class="row">
               <div class="col-lg-12">
                    <h2 class="page-header"><?php echo "Important  Numbers";?></h2>
                </div>
            </div>
                     
<div class="row">
<div class="col-lg-12 pull-left" align="center">


<div id="infoMessage" style="color:#F00"><?php echo $message;?></div>
								




<form action="<?= site_url('admin/pages/imp_contacts') ?>" method="post" id="loginform" >				
            

 			       <div class="form-group">
                    <label for="text1" class="control-label col-lg-4">Description</label>

                    <div class="col-lg-8">
                             	<textarea class="form-control editor1 odia-fo" id="editor1"  name="description" rows="10" name="text"><?php echo $value; ?></textarea><br />
                      </div>
                	</div>
         <div class="form-group">
                    <div class="col-lg-8">
                  <br>
                   <br>
                        <input type="submit"  value="Submit" class="btn btn-primary pull-right" />
                    </div>
                </div>
                
      </div>          
	</form>
	   </div>

	<script>

			CKEDITOR.replace( 'editor1' );

		</script>
</div>
   
    </div>

           </div>
              </div>
                    <!-- END PAGE CONTENT -->     <script src="<?= base_url();?>/assets/js/jquery-ui.min.js"></script>
 <script src="<?= base_url();?>/assets/plugins/uniform/jquery.uniform.min.js"></script>
<script src="<?= base_url();?>/assets/plugins/inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?= base_url();?>/assets/plugins/chosen/chosen.jquery.min.js"></script>
<script src="<?= base_url();?>/assets/plugins/colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="<?= base_url();?>/assets/plugins/tagsinput/jquery.tagsinput.min.js"></script>
<script src="<?= base_url();?>/assets/plugins/validVal/js/jquery.validVal.min.js"></script>
<script src="<?= base_url();?>/assets/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?= base_url();?>/assets/plugins/daterangepicker/moment.min.js"></script>
<script src="<?= base_url();?>/assets/plugins/datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?= base_url();?>/assets/plugins/timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="<?= base_url();?>/assets/plugins/switch/static/js/bootstrap-switch.min.js"></script>
<script src="<?= base_url();?>/assets/plugins/jquery.dualListbox-1.3/jquery.dualListBox-1.3.min.js"></script>
<script src="<?= base_url();?>/assets/plugins/autosize/jquery.autosize.min.js"></script>
<script src="<?= base_url();?>/assets/plugins/jasny/js/bootstrap-inputmask.js"></script>
       <script src="<?= base_url();?>/assets/js/formsInit.js"></script>
        <script>
            $(function () { formInit(); });
        </script>