<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/ajxupload/upload.css"  />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/ajxupload/jquery.form.js" ></script>    
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
 
    <!-- END PAGE LEVEL  STYLES -->
     <!--PAGE CONTENT --> 
    <div class="content-wrapper">
               
              <section class="content-header">
          <h1>Edit Sponsor</h1><br>
                  <a class="btn btn-primary" href="<?php echo site_url(); ?>/admin/pages/create_sponser">Add Sponsor</a>
                  <a class="btn btn-primary" href="<?php echo site_url(); ?>/admin/pages/all_sponsers">Sponsor listing</a>
         
        </section>
        <section class="content">
<div class="box box-primary">
<div class="box-body" >
<div id="infoMessage" style="color:#F00"><?php echo $message;?></div>
<form action="<?php echo site_url('admin/pages/edit_sponser')."/".$page->id; ?>" method="post" id="editpageh"  enctype="multipart/form-data"  >
 		
     
                    <div class="form-group">
                    <label for="text1" class="control-label">Url : </label>
                        <span id="url_mdf"><?php echo str_replace("admin/","",base_url()); ?><a href="<?php echo str_replace("admin/","",base_url($page->url)).'.php'; ?>" target="framename" ><?php echo $page->url; ?></a> <a class="fa fa-edit" style="cursor:pointer;" onclick="modify_url('<?php  echo $page->id; ?>','<?php echo $page->url; ?>');" ></a></span>
                	</div>	
    
    
    
    
    
    
		<?php /*
		<div class="form-group">
				 <label for="text1"  class="control-label">Make this as Gallery&nbsp;&nbsp;&nbsp;&nbsp;:</label>
				<input type="radio" <?php if($page->en_gallery == 1){?> checked="checked" <?php } ?> name="en_gallery" value="1"> Yes
					<input type="radio" <?php if($page->en_gallery == 0){?> checked="checked" <?php } ?> name="en_gallery" value="0"> No
			</div> 
	 */	?>
		
    <?php echo validation_errors(); ?>
    
			 <div class="form-group">
                    <label for="text1" class="control-label">Type Of Ads.</label>
                        <input  type="text" class="form-control" name="name" required="" readonly value="SponsersAd"><br>
                	</div>
    
    
            
                           
			<?php /*
			<div class="form-group">
				 <label for="text1"  class="control-label">Enable Bottom Slider&nbsp;:</label>
				 	<input type="radio" <?php if($page->en_btnslider == 1){?> checked="checked" <?php } ?> name="en_btnslider" value="1"> Yes
					<input type="radio" <?php if($page->en_btnslider == 0){?> checked="checked" <?php } ?> name="en_btnslider" value="0"> No
			</div>	
			*/	?>
				<div class="form-group">
                    <label for="text1" class="control-label">Sponsor Name</label>
                 
                                  <input  type="text" value="<?php echo $page->name ?>" class="form-control" name="name">
                      
                	</div>	
                	
                	
                	<div class="form-group">
                    <label for="text1" class="control-label">Sponsor Author</label>
                 
                                  <input  type="text" value="<?php echo $page->author ?>" class="form-control" name="author">
                      
                	</div>
                	
                	
					<div class="form-group">
                   	<label for="text1" class="control-label">Date</label>
                
                            <input type="text" name="date" value="<?php echo date('d-m-Y',strtotime($page->date)); ?>" class="form-control" id="datepicker"  required />
                        
                	</div>
					
             
					<div class="form-group col-md-6" style="padding-left: 2px; " >
                    <label for="text1" class="control-label">Cover Pic<br></label>
                         <img src="<?php echo base_url()?>upload/page/min_page3/<?php echo $page->sec_1_data; ?>">
                            
                                  <input  type="file" class="form-control" name="cover_img" ><br>
                
                	</div>	
    
                
						    <div style="clear:both;"></div>
			
                   <div class="form-group">
                    <label for="text1" class="control-label " style="padding-top: 15px">Sponsor Content</label>
                    <div class="">
                       <textarea name="txt_content" id="contentarea" ><?php echo $page->right;  ?></textarea>
                        
					<?php /*
			$this->load->view('editor/fckeditor.php');
 				$oFCKeditor = new FCKeditor('txt_content') ;
				$oFCKeditor->BasePath = base_url().'editor/' ;
				$oFCKeditor->Height	= 390;
				$oFCKeditor->Config['EnterMode'] = 'br';
				$oFCKeditor->Value = $page->right ;
				$oFCKeditor->name = 'content' ;
				$oFCKeditor->Create() ; */
		?>
                      </div>
                	</div>
                	
                	
                	
                	
                					    
    <!--       Progress Bar-->
       <div id="p_bar" style="display:none;" class="hide">
           <div  class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" id="progressbar" style="width:0%">
                    <span id="statustxt"></span>
                </div>
           </div>
       </div>
       <!--       Progress Bar-->
					
					
	     	
                        <input type="submit" id="tags" value="Update" class="btn btn-success" />
                    
	</form>
	   </div>
</div>
    </div>
    </section>
           </div>
             
                    <!-- END PAGE CONTENT -->
<script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>

<script>


             CKEDITOR.replace( 'contentarea', {

    height:400,        
    filebrowserBrowseUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/ckfinder.html',
    filebrowserUploadUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    filebrowserWindowWidth: '1000',
    filebrowserWindowHeight: '700'
});

    
    function modify_url(pgId,pgUrl){
    
        var edtfld = '<div class="input-group input-group-sm col-sm-8"><input onkeyup="gen_url(this.value,'+pgId+');" type="text" id="url_data" value="'+pgUrl+'" class="form-control"><span class="input-group-btn"><button onclick="update_url('+pgId+');" class="btn btn-info btn-flat" type="button">Update</button></span></div><br>Your Url Wll be:  <span class="text-blue"><?php echo str_replace("admin/","",base_url()); ?><span id="gen_url" >'+pgUrl+'</span>.php</span>';
        
        $('#url_mdf').html(edtfld);
    
    
    }
    function gen_url(dis,pId){
        
      var new_url =   dis.toLowerCase().replace(/-+$/,'');
      $('#gen_url').html(new_url); 
        
        
    }
    
    function update_url(pageid){
        
     var newurl =  $('#gen_url').html();
      
        $.post('<?php echo site_url('admin/pages/change_url'); ?>',{url_data:newurl,page_id:pageid},function(res){
        
            var resp = JSON.parse(res);
              
            
        if(resp.status == 1){
           // $('#url_mdf').html(resp.message);
            alert("Url Successfully Updated"); 
            
        }else{
            alert(resp.message);
        }    
            
      });
        
    
        
    }
    
    	
	function remove_image(pgid,nof){

	
		 $.post('<?php echo site_url('admin/pages/remove_img'); ?>',{pgid:pgid,nof:nof},function(res){

			if(res == 1){
			$('#seddata'+nof).remove(); 
			}

		  });
		
	}


 $(document).ready(function() {
        //elements
        var progressbox     = $('#progressbox');
        var progressbar     = $('#progressbar');
        var statustxt       = $('#statustxt');
        var submitbutton    = $("#SubmitButton");
        var myform          = $("#editpage");
        var output          = $("#output");
        var completed       = '0%';
 
                $(myform).ajaxForm({
                    beforeSend: function() { //brfore sending form
                        document.getElementById("p_bar").className = "show";
                        submitbutton.attr('disabled', ''); // disable upload button
                        statustxt.empty();
                        progressbox.slideDown(); //show progressbar
                        progressbar.width(completed); //initial value 0% of progressbar
                        statustxt.html(completed); //set status text
                        statustxt.css('color','#fff'); //initial color of status text
                    },
                    uploadProgress: function(event, position, total, percentComplete) { //on progress
                        progressbar.width(percentComplete + '%') //update progressbar percent complete
                        statustxt.html(percentComplete + '%'); //update status text
                        if(percentComplete>50)
                            {
                                statustxt.css('color','#fff'); //change status text to white after 50%
                            }

                        },
                    complete: function(response) { // on complete
                        var res = output.html(response.responseText); //update element with received data
                        myform.resetForm();  // reset form
                        submitbutton.removeAttr('disabled'); //enable submit button
                        progressbox.slideUp(); // hide progressbar
                        document.getElementById("p_bar").className = "hide";
                        
                        
if(response.responseText == 1){
alert("Successfully Upload");
}else if(response.responseText == 2){
alert("Please Upload a valid Image File");
}else if(response.responseText == 3){
alert("You have no rights to upload");
}else{
alert("Uploading unsuccessful Please Try again");
}
//location.reload();
                       /// location.reload();
                    }
                   
            });
        });

$(function () {
 $('#datepicker').datepicker({
      autoclose: true,
      format: "dd-mm-yyyy"
    });
});

 
    function get_category(dis){
        
       // alert(id);
         $('#category').html('<option value="">Loading...</option>');
        
        $.get('<?php echo site_url('admin/pages/get_sub');?>',{prob_id:dis},function(resp){
           
            $('#subcatgy').html(resp);
            
        });
         
    }
    
    

</script>
