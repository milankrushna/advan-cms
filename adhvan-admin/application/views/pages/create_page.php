   <script src="https://code.jquery.com/ui/1.10.2/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/ajxupload/upload.css"  />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/ajxupload/jquery.form.js" ></script>

 <link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/datepicker/datepicker3.css">
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>

  <link rel="stylesheet" href="<?php echo base_url()?>assets/jquery.tag-editor.css">
    <!-- END PAGE LEVEL  STYLES -->
     <!--PAGE CONTENT --> 
    <div class="content-wrapper">
              <section class="content-header">
          <h1>Create Article</h1>
         
        </section>
        <section class="content">
<div class="box box-primary">
<div class="box-body" >
<div class="infoMessage" style="color:#F00"><?php echo $message;?></div>
<form action="<?php echo  site_url('admin/pages/create_page'); ?>" enctype="multipart/form-data" method="post" id="page_create" >
 				
				  <?php /* 
		<div class="form-group">
				 <label for="text1" class="control-label">Make this as Gallery  &nbsp;&nbsp;&nbsp;&nbsp;:<br></label>
					<input type="radio" name="en_gallery" value="1"> Yes
					<input type="radio" checked="checked" name="en_gallery" value="0"> No
			</div> 
				*/ ?>
				
				 
             <?php /* 
			   <div class="form-group">
				 <label for="text1" class="control-label">Enable Bottom Slider &nbsp;:</label>
					<input type="radio" name="en_btnslider" value="1"> Yes
					<input type="radio" name="en_btnslider" value="0"> No
			</div> 
				 
		
		*/ ?>
    
    
    <input type="hidden" name="page_id" value="0" id="ajax_id" >
    
    <div style="display:none;"><input type="checkbox" name="android" <?php if($page['android'] = '1'){ ?> checked <?php } ?> value="1" >Android<br>
    <input type="checkbox" name="ios" <?php if($page['ios'] = '1'){ ?> checked <?php } ?> value="1" >IOS </div>
    
    <input type="hidden" name="category"  value ="2">
    <input type="hidden" name="sub_cat"  value ="">


              <?php /*     <div class="form-group">
                    <label for="text1" class="control-label">Category</label>
                        <select class="form-control" onchange="get_category(this.value);" id="" name="category"  required>  
                            <option value="">Select a Category</option>
                            
                           
                            <?php 
                            foreach($category as $cat){ ?>
                            <option value="<?php echo $cat->id; ?>"><?php  echo $cat->menu; ?></option>
                            <?php 
                             }
                            ?>
                            
                        </select>
                	</div>
          
                 <div class="form-group">
                    <label for="text1" class="control-label">Sub Category</label>
                        <select class="form-control" name="sub_cat" id="subcatgy" >  
                            <option value="">Select a Sub Category</option>
                            
                            
                            <?php 
                           /* foreach($slider as $sl){ ?>
                            <option value="<?php echo $sl['id']; ?>"><?php  echo $sl['title']; ?></option>
                            <?php 
                             }*/ /*
                            ?>
                            
                        </select>
                	</div>  */ ?>
                           
    
                    <div class="form-group">
                    <label for="text1" class="control-label">Article Name</label>
                    
                                  <input  type="text" class="form-control" name="name" required=""><br>
                  
                	</div>	
    <?php /*
        <div class="form-group">
                    <label for="text1" class="control-label">Meta title</label>
                <input  type="text" class="form-control" value="" name="meta_title">
                	</div>
      <div class="form-group">
                    <label for="text1" class="control-label">Meat Description</label>
          <textarea  type="text" class="form-control" name="meta_desc"  ></textarea>
                	</div>  
    
    <div class="form-group">
        <label for="text1" class="control-label">Meat Keyword</label>
        <textarea  type="text" class="form-control" name="meta_keyword"></textarea>
     </div> 
        */   ?>
    
    
    <div class="form-group">
                    <label for="text1" class="control-label">visit</label>
                    
                                  <input  type="text" class="form-control" name="visit" ><br>
                  
                	</div>	
<!--<div class="form-group">
                    <label for="text1" class="control-label">url</label>
                    
                                  <input  type="text" class="form-control" name="url" required=""><br>
                  
                	</div>-->
                <input type="hidden" name="author"  value ="1">
               <!--  <div class="form-group">
                    <label for="text1" class="control-label">Author</label>
                        <select class="form-control" name="author" id="author" required>  
                            <option value="">Select a Author</option>
                            
                            
                            <?php 
                            foreach($author as $atr){ ?>
                            <option value="<?php echo $atr->id; ?>"><?php  echo $atr->name; ?></option>
                            <?php 
                             }
                            ?>
                            
                        </select>
                	</div> -->
                           
                
                	<div class="form-group">
                   	<label for="text1" class="control-label">Date</label>
                     
                <input type="text" name="date" class="form-control" value="" id="datepicker"  readonly="" />
                        
                	</div>
     
    
                    <div class="form-group">
                   	<label for="text1" class="control-label">Tags</label>
                     
               <textarea id="tag-ctrl"   name="tags" ></textarea>     
                	</div>	
                	
                	
                
                	
				<div class="form-groupn">
                    <label for="text1" class="control-label">Cover Pic<br></label>
                            
                                  <input  type="file" class="form-control" name="cover_pic" id="upcover_pic" ><br>
                    <p style="color:red;">Image should be 1200x650</p>
                	</div>	
     
    
               
                	<div class="form-group">
                   	<label for="text1" class="control-label">Short Content</label>
                     
                         <div class="">
                             
                    <textarea name="short_content" id="sht_editor"  style="display:none;" ></textarea>
                    <textarea  id="short_content" ></textarea>
                             
                              <?php /*
                $this->load->view('editor/fckeditor.php');
 				$oFCKeditor = new FCKeditor('short_content') ;
				$oFCKeditor->BasePath = base_url().'editor/' ;
				$oFCKeditor->Height	= 200;
				$oFCKeditor->Config['EnterMode'] = 'br';
				$oFCKeditor->Value = '' ;
				$oFCKeditor->name = 'right' ;
				$oFCKeditor->Create() ;
*/		?>
                      </div>
                        
                        
                        
                	</div>	
    
    
                	<input type="hidden" value="0" name="rtn_pid" id="rtn_pid" >			
                   <div class="form-group">
                    <label for="text1" class="control-label">Article Content</label>
                     <div class="">  
                    <textarea name="txt_content" id="main_editor" style="display:none" ></textarea>
                         
                       <textarea  id="contentarea"  ></textarea>
                       
                   
                              <?php /*
                $this->load->view('editor/fckeditor.php');
 				$oFCKeditor = new FCKeditor('txt_content') ;
				$oFCKeditor->BasePath = base_url().'editor/' ;
				$oFCKeditor->Height	= 390;
				$oFCKeditor->Config['EnterMode'] = 'br';
				$oFCKeditor->Value = '' ;
				$oFCKeditor->name = 'right' ;
				$oFCKeditor->Create() ; */
		?>
                      </div>
                	</div>
	
    <div id="p_bar" style="display:none;" class="hide">
         Uploading ... <span style="color:red !important" id="statustxt"></span>   
       <!--  <div  class="progress">
               <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" id="progressbar" style="width:0%"></div>
                  
                
           </div>-->
       </div>
<div class="infoMessage" style="color:#F00"><?php echo $message;?></div>
    
    <button type="submit" value="2" name="save_type" class="btn btn-warning SubmitButton" >Save Draft</button>
    <button type="submit" value="1" name="save_type" class="btn btn-primary SubmitButton" >Publish</button>
    <button  type="submit" value="3" name="save_type" class="btn btn-default SubmitButton" >Preview</button>
               </form>    
	
   
	   </div>
            </div>
        </section>
</div>
    
                    <!-- END PAGE CONTENT -->
	 
    <script src="<?php echo base_url()?>assets/jquery.tag-editor.min.js"></script>
    <script src="<?php echo base_url()?>assets/jquery.caret.min.js"></script>

   <script>
    
        $(function() {
    
            $('#tag-ctrl').tagEditor({
                autocomplete: { delay: 0, position: { collision: 'flip' }, source: <?php echo json_encode($tags); ?> },
                forceLowercase: false,
                placeholder: 'Tag of Articles..'
            });
 });

    </script>



<script>

    
    
   var sht_editor =   CKEDITOR.replace( 'short_content', {
     
    height:150,        
    filebrowserBrowseUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/ckfinder.html',
    filebrowserUploadUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    filebrowserWindowWidth: '1000',
    filebrowserWindowHeight: '700'
});
    
    sht_editor.on( 'change', function ( ev ) {

        $('#sht_editor').html(sht_editor.getData());
    
    	});

    var main_editor =  CKEDITOR.replace( 'contentarea', {

    height:400,        
    filebrowserBrowseUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/ckfinder.html',
    filebrowserUploadUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    filebrowserWindowWidth: '1000',
    filebrowserWindowHeight: '700'
}); 
  main_editor.on( 'change', function ( ev ) {

        $('#main_editor').html(main_editor.getData());
    
    	});  
   
$(function () {
 $('#datepicker').datepicker({
      autoclose: true,
      format: "dd-mm-yyyy"
    });
});

    
    function get_category(dis){
        
       // alert(id);
         $('#category').html('<option value="">Loading...</option>');
        
        $.get('<?php echo site_url('admin/pages/get_sub');?>',{prob_id:dis},function(resp){
           
            $('#subcatgy').html(resp);
            
        });
         
    }
    
    
    
    
 $(document).ready(function() {
        //elements
        var progressbox     = $('#progressbox');
        var progressbar     = $('#progressbar');
        var statustxt       = $('#statustxt');
        var submitbutton    = $(".SubmitButton");
        var myform          = $("#page_create");
        var output          = $("#output");
        var completed       = '0%';
 
                $(myform).ajaxForm({
                    beforeSend: function() { //brfore sending form
                         $('.infoMessage').html('');
                        document.getElementById("p_bar").className = "show";
                        submitbutton.attr('disabled', ''); // disable upload button
                        statustxt.empty();
                        progressbox.slideDown(); //show progressbar
                        progressbar.width(completed); //initial value 0% of progressbar
                        statustxt.html(completed); //set status text
                        statustxt.css('color','red'); //initial color of status text
                    },
                    uploadProgress: function(event, position, total, percentComplete) { //on progress
                        progressbar.width(percentComplete + '%') //update progressbar percent complete
                        statustxt.html(percentComplete + '%'); //update status text
                        if(percentComplete>50)
                            {
                                statustxt.css('color','red'); //change status text to white after 50%
                            }

                        },
                    complete: function(response) { // on complete
                        var res = output.html(response.responseText); //update element with received data
                       //// myform.resetForm();  // reset form
                        submitbutton.removeAttr('disabled'); //enable submit button
                        progressbox.slideUp(); // hide progressbar
                        document.getElementById("p_bar").className = "hide";
                        
                        ///console.log(response);
                        
                        var opresp = JSON.parse(response.responseText);
                        
                        if(opresp.status == 'formerr'){
                            
                            $('.infoMessage').html(opresp.message);
                        }else{
                           
                        if(opresp.pid!=0){
                           
                         $("#page_create").attr('action', '<?php echo site_url('admin/pages/edit_page') ?>'+'/'+opresp.pid);   
                        $('#rtn_pid').val(opresp.pid);
                        $('.infoMessage').text(opresp.message);
                        $('#upcover_pic').val('');
                            
                            if(opresp.sv_type==3){
                        prw_url = '<?php echo str_replace('admin/', '', base_url('preview_load_system/')); ?>'+'/'+opresp.pid;
                            window.open(prw_url,'_blank');
                             location.href = '<?php echo site_url('admin/pages/edit_page') ?>'+'/'+opresp.pid;      
                            }
                            
                            if(opresp.sv_type==1 || opresp.sv_type==2){
                             location.href = '<?php echo site_url('admin/pages/edit_page') ?>'+'/'+opresp.pid;    
                        $('#svdrft').remove();
                        $('#pubart').html('Update');
                                 
                                 
                             }
                            
                        }
                            
                              
                    }
                        
                    }
                   
            });
        });

    
    
    
</script>
