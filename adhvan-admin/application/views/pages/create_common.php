 <div class="content-wrapper">
     
          <section class="content-header">
          <h1>Add Section</h1>
        </section>
        <section class="content"> 
               
<div class="box box-primary">
<div class="box-body" >
<div id="infoMessage" style="color:#F00"><?php echo $message;?></div>
<form action="<?= site_url('admin/common/create_common') ?>" method="post" id="loginform">
 				
				<div class="form-group">
                    <label for="text1" class="control-label">Item Name</label>
                 
      <input  type="text" class="form-control" name="name">
                	</div>	
					
						
				 <div class="form-group">
                    <label for="text1" class="control-label">Content</label>
                    <div class="">
                       <textarea name="txt_content" id="contentarea" ></textarea>
                        
                              <?php /*
			$this->load->view('editor/fckeditor.php');
 				$oFCKeditor = new FCKeditor('txt_content') ;
				$oFCKeditor->BasePath = base_url().'editor/' ;
				$oFCKeditor->Height	= 390;
				$oFCKeditor->Config['EnterMode'] = 'br';
				$oFCKeditor->Value = '' ;
				$oFCKeditor->name = 'content' ;
				$oFCKeditor->Create() ; */
		?>
                      </div>
                	</div>
			
                        <input type="submit" id="tags" value="Submit" class="btn btn-success" />
                   
	</form>
	   </div>
</div>
    </section>
           </div>
          
                    <!-- END PAGE CONTENT -->
				
	<script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>

<script>


    CKEDITOR.replace( 'contentarea', {
    height:400,        
    filebrowserBrowseUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/ckfinder.html',
    filebrowserUploadUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    filebrowserWindowWidth: '1000',
    filebrowserWindowHeight: '700'
});
    
    
</script>
	