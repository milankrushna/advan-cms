      
<link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/tree/jqwidgets/styles/jqx.base.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/tree/scripts/gettheme.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/tree/jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/tree/jqwidgets/jqxdata.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/tree/jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/tree/jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/tree/jqwidgets/jqxpanel.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/tree/jqwidgets/jqxtree.js"></script>

<script type="text/javascript">
	$(document).ready(function () {
		$.post("<?= site_url('admin/menu/get_tree');?>",function(msg){
			var data=msg;
			var theme = getTheme();
			var source =
			{
				datatype: "json",
				datafields: [
					{ name: 'id' },
					{ name: 'parent_menu' },
					{ name: 'menu' }
				],
				id: 'id',
				localdata: data
			};
			var dataAdapter = new $.jqx.dataAdapter(source);
			dataAdapter.dataBind();
			var records = dataAdapter.getRecordsHierarchy('id', 'parent_menu', 'items', [{ name: 'menu', map: 'label'}]);
			$('#jqxWidget').jqxTree({ source: records, width: '300px', theme: theme });
		})
	});
</script>
	   <link href="<?= base_url();?>/assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" />



    <div class="content-wrapper">

     <section class="content-header">
          <h1>Menu Listing</h1>
         
        </section>
 <section class="content">
 <a  class="btn btn-primary" href="<?= site_url('admin/menu/create_menu') ?>">Create A New Menu</a>
<div class="box box-primary">
<div class="box-body" >
                <?php /*  <div class="col-lg-4">
				   <div class="panel panel-default">
                        <div class="panel-heading">
                            Menu List(Tree View)
						</div>
                        <div class="panel-body">
                            <div class="table-responsive">
    				<div id='jqxWidget'></div>
						  
						  
                            </div>
                        </div>
                    </div>
				</div>  */ ?>
				<div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Menu (List View)
					 
					  	</div>
					
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Action</th>
                                            <th>Category Name</th>
                                            <th>Parent Menu Name</th>
                                            
                                    
                                            <th>Big Article</th>
                                            <th>Medium Article</th>
                                            <th>Footer Menu</th>
                                         <!--   <th>Footer Menu</th>-->
                                           
                                            <th>Status</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
          <?php 
          
         //   print("<pre>");
          //  print_r($menulist);exit;
    
    
    
    foreach ($menulist as $ml):?>
		<tr class="odd gradeX">
            <td>
             <a class="btn btn-warning" href="<?php echo site_url('admin/menu/edit_menu'."/".$ml['id'])?>" ><i class="fa fa-edit"></i></a>||<a title="Delete" class="btn  btn-danger" href="<?php echo site_url('admin/menu/delete_menu'."/".$ml['id'])?>" onclick="return confirm('Are You sure?');"><i class="fa fa-trash"></i></a>
            </td>
            
            
            
            
			<td><a href="#"><?php echo $ml['menu'];?></a></td>
			<td><?php echo $ml['parent_menu'];?></td>
            
            <td><?php // if($ml['parent_menu']!="N/A" && $ml['ct_menu']!=0){ ?>
            	<input type="checkbox" name="big_art"  class="big" <?php if ($ml['big_article'] == '1'){  ?> checked <?php }  ?>  value="<?php echo $ml['id']; ?>" > 
                <?php  ?>
            </td>
            
            <td>
                <?php // if($ml['parent_menu']!="N/A" && $ml['ct_menu']!=0){ ?>
            	<input type="checkbox" name="med_art" class="med"  <?php if ($ml['medium_article'] == '1'){  ?> checked <?php }  ?>  value="<?php echo $ml['id']; ?>" >
                <?php  ?>
            </td>
            
           <!--  <td>
            	<input type="checkbox" name="ft_menu" class="footer" <?php if ($ml['ft_menu'] == '1'){  ?> checked <?php }  ?>  value="<?php echo $ml['id']; ?>" >
            </td>-->
            
           <td>
            	<input type="checkbox" class="footer" name="ft_menu" <?php if ($ml['ft_menu'] == '1'){  ?> checked <?php }  ?>  value="<?php echo $ml['id']; ?>" >
            </td>
          
            
            
          
				
			<td>
<?php 
         if($ml['status'] == 0){ $anc = 'Active'; }else{ $anc = 'Deactive'; }
    
                          ///  echo anchor('admin/slider/actdeact/'.$gall->id, $anc,array('class' => 'act-deact','title'=>$gall->status));    ?>
                    <a href="<?php echo site_url('admin/menu/menu_deactive/'.$ml['id']); ?>"  class="act_deactive" title="<?php echo $ml['status']; ?>" ><?php echo $anc; ?></a>
                    
</td>
		</tr>
		<?php endforeach;?>
                                    </tbody>
                               </table>
                            </div>
                        </div>
                    </div>
                </div>
               </div>
                </div>
            </section>
<!--<p><?php echo anchor('admin/menu/create_menu', 'Create a new Menu')?> </p>-->
</div>

    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?= base_url();?>/assets/plugins/datatables/jquery.dataTables.js"></script>
    <script src="<?= base_url();?>/assets/plugins/datatables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
         
         
 $('.act_deactive').click(function(ev){
            ev.preventDefault();
            
             var  dis = this;
             
             $.post($(this).attr('href'),{'sts':$(this).attr('title')},function(resp){
                 
                 if(resp == 0){
                    $(dis).html("Active");
                    $(dis).attr("title",resp);
                 }else if(resp == 1){
                    $(dis).html("Deactive");
                    $(dis).attr("title",resp);
                 }
                 
             });  
         });
         
         /// For big article 
         
             $('input[type="checkbox"].big').click(function(){
                 
          
                 
             $('input[type="checkbox"].big').prop("disabled",true);
            var dis = this;
             ///value of which one you want to update
            var update_id = $(dis).val();
            if($(dis).prop("checked") == true){
            
                 $('#artcl_id').html('<option value="">Loading ...</option>');
                     $('#artcl_id').trigger('change');
                 $('#article_include').modal('show'); 
                $.post('<?php echo site_url('admin/menu/get_ctgy_artl') ?>',{id:update_id},function(resp,status){
                    
                 if(status == 'success'){
                 
                     $('#menu_id').val(update_id);
                      $('#menu_type').val('big');
                     
                     $('#artcl_id').html(resp).trigger('change');
               
                 }   
                    
                });
           
               
            }
            else if($(dis).prop("checked") == false){
                
                var big = 0;
                var arti_id = 0;
                //update database status through ajax
                $.post('<?php echo site_url('admin/menu/update_big_artl') ?>',{big_art:big,id:update_id,arti_id:arti_id},function(resp){
                    
                 if(resp == 1){
                   alert("successfull.");  
                   $('input[type="checkbox"].big').prop("disabled",false);  
                 } else{
				 	
				 	alert("unsuccessfull, please try again");
				 }  
                    
                });
                
            }
        });
         
         
         function update_arti(dis){
             
           var update_id =  $('#menu_id').val();
           var arti_id =  $('#artcl_id').val();
           var menu_type =  $('#menu_type').val();
             
             
             if(arti_id!=''){
                 
            $(dis).attr('disabled',true);
                 
                 ///Update section for Big Article
                if(menu_type=='big'){ 
                 
              var big = 1;
                //update database status through ajax
                $.post('<?php echo site_url('admin/menu/update_big_artl') ?>',{big_art:big,id:update_id,arti_id:arti_id},function(resp){
                    
                 if(resp == 1){

                   alert("successfull.");  
                         $('#article_include').modal('hide'); 
                     $('#artcl_id').val(null).trigger('change');
                 }else{
                   alert("Something Error Try again.");  
                     
                 }   
                   
           $(dis).removeAttr('disabled');
           $('input[type="checkbox"].big').prop("disabled",false);  
                });
                    
                }else if(menu_type=='mid'){
                ///Update section for medium Article
                    
                  
                var med = 1;
                //update database status through ajax
                $.post('<?php echo site_url('admin/menu/update_med_artl') ?>',{med_art:med,id:update_id,arti_id:arti_id},function(resp){
                    
                 if(resp == 1){
                     
                      alert("successfull.");  
                         $('#article_include').modal('hide'); 
                     $('#artcl_id').val(null).trigger('change');
                     
                 }else{
                   alert("Something Error Try again.");  
                     
                 }    
                  $(dis).removeAttr('disabled');
           $('input[type="checkbox"].med').prop("disabled",false);     
                });  
                    
                }
                 
             }else{
                 
             alert('Please Select an Article');    
                 
             }
         }
         
         
   /// For medium article      
         
    $('input[type="checkbox"].med').click(function(){
        
        
        
     $('input[type="checkbox"].med').prop("disabled",true);
            var dis = this;
             ///value of which one you want to update
            var update_id = $(dis).val();
            if($(dis).prop("checked") == true){
            
                
                
                
                  $('#artcl_id').html('<option value="">Loading ...</option>');
                     $('#artcl_id').trigger('change');
                 $('#article_include').modal('show'); 
                $.post('<?php echo site_url('admin/menu/get_ctgy_artl') ?>',{id:update_id},function(resp,status){
                    
                 if(status == 'success'){
                 
                     $('#menu_id').val(update_id);
                      $('#menu_type').val('mid');
                     
                     $('#artcl_id').html(resp).trigger('change');
               
                 }   
                    
                });
           
                
                
                
            }
            else if($(dis).prop("checked") == false){
                
                var med = 0;
                var arti_id = 0;
                //update database status through ajax
                $.post('<?php echo site_url('admin/menu/update_med_artl') ?>',{med_art:med,id:update_id,arti_id:arti_id},function(resp){
                    
                 if(resp == 1){
                   alert("successfull.");  
                   $('input[type="checkbox"].med').prop("disabled",false);  
                 } else{
				 	
				 	alert("unsuccessfull, please try again");
				 }  
                    
                });
                
            }
        }); 
         
         
      // For footer menu   
         
          $('input[type="checkbox"].footer').click(function(){
             $('input[type="checkbox"].footer').prop("disabled",true);
            var dis = this;
             ///value of which one you want to update
            var update_id = $(dis).val();
            if($(dis).prop("checked") == true){
            
                var sts = 1;
                //update database status through ajax
                $.post('<?php echo site_url('admin/menu/update_ftr_menu') ?>',{status:sts,id:update_id},function(resp){
                    
                 if(resp == 1){
                   alert("successfull.");  
                   $('input[type="checkbox"].footer').prop("disabled",false);  
                 }   
                    
                });
                
            }
            else if($(dis).prop("checked") == false){
                
                var sts = 0;
                //update database status through ajax
                $.post('<?php echo site_url('admin/menu/update_ftr_menu') ?>',{status:sts,id:update_id},function(resp){
                    
                 if(resp == 1){
                   alert("successfull.");  
                   $('input[type="checkbox"].footer').prop("disabled",false);  
                 } else{
				 	
				 	alert("unsuccessfull, please try again");
				 }  
                    
                });
                
            }
        });
         
         
         
    </script>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css"  />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/select2/select2.full.min.js" ></script>
<script>

$(document).ready(function() {
 $('#artcl_id').select2();
});
</script>

<div class="modal fade" id="article_include" style="display: none;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Default Modal</h4>
              </div>
              <div class="modal-body">
              
                <form >
                    
                    <div class="form-group">
   <label for="text1" class="control-label">Articles</label>
 <select  name="page_id"  onchange="" class="form-control " style="width:90%" id="artcl_id">
<option value="">Choose A Page.... </option>


   	</select></div>
                    <input type="hidden" value="" id="menu_id">
                    <input type="hidden" value="" id="menu_type">
                    
                  </form>
               
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" onclick="update_arti(this)"  class="btn btn-primary">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
