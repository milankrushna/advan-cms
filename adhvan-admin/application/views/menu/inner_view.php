      
<link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/tree/jqwidgets/styles/jqx.base.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/tree/scripts/gettheme.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/tree/jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/tree/jqwidgets/jqxdata.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/tree/jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/tree/jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/tree/jqwidgets/jqxpanel.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/tree/jqwidgets/jqxtree.js"></script>

<script type="text/javascript">
	$(document).ready(function () {
		$.post("<?= site_url('admin/menu/get_tree');?>",function(msg){
			var data=msg;
			var theme = getTheme();
			var source =
			{
				datatype: "json",
				datafields: [
					{ name: 'id' },
					{ name: 'parent_menu' },
					{ name: 'menu' }
				],
				id: 'id',
				localdata: data
			};
			var dataAdapter = new $.jqx.dataAdapter(source);
			dataAdapter.dataBind();
			var records = dataAdapter.getRecordsHierarchy('id', 'parent_menu', 'items', [{ name: 'menu', map: 'label'}]);
			$('#jqxWidget').jqxTree({ source: records, width: '300px', theme: theme });
		})
	});
</script>
	   <link href="<?= base_url();?>/assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" />



    <div class="content-wrapper">

     <section class="content-header">
          <h1>Inner Menu Listing</h1>
         
        </section>
 <section class="content">
 <a  class="btn btn-primary" href="<?php echo site_url('admin/menu/create_menu') ?>">Create A New Menu</a>
<div class="box box-primary">
<div class="box-body" >
                <?php /*  <div class="col-lg-4">
				   <div class="panel panel-default">
                        <div class="panel-heading">
                            Menu List(Tree View)
						</div>
                        <div class="panel-body">
                            <div class="table-responsive">
    				<div id='jqxWidget'></div>
						  
						  
                            </div>
                        </div>
                    </div>
				</div>  */ ?>
				<div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Menu (List View)
					 
					  	</div>
					
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Action</th>
                                            <th>Menu Name</th>
                                            <th>Parent Menu Name</th>
                                            <th>Link</th>
                                            <th>Images</th>
                                            <th>Status</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
          <?php foreach ($innerlist as $il):?>
		<tr class="odd gradeX">
            <td><?php echo anchor("admin/menu/edit_menu/".$il['id'], 'Edit') ;?> | <?php echo anchor("admin/menu/delete_menu/".$il['id'], 'Delete') ;?></td>
			<td><?php echo $il['menu'];?></td>
			<td><?php echo $il['parent_menu'];?></td>
            <td><a target="_blank" href="<?php echo str_replace('admin/', '', base_url($il['link'].'.php')); ?>"><?php echo str_replace('admin/', '', base_url($il['link'].'.php'));  ?></a></td>
            <td><?php if(!empty($il['image'])){ ?><img width="75px" src="<?php echo base_url(); ?>upload/menu/<?php  echo $il['image']; ?>" > <?php  }else{ echo "<b>No Image<b>"; } ?> </td>
		
		
		
		<td>
<?php 
         if($il['status2'] == 0){ $anc = 'Active'; }else{ $anc = 'Deactive'; }
    
                          ///  echo anchor('admin/slider/actdeact/'.$gall->id, $anc,array('class' => 'act-deact','title'=>$gall->status));    ?>
                    <a href="<?php echo site_url('admin/menu/inner_deactive/'.$il['id']); ?>"  class="inner_active" title="<?php echo $il['status2']; ?>" ><?php echo $anc; ?></a>
                    
</td>
		
		
		
			
		</tr>
		<?php endforeach;?>
                                    </tbody>
                               </table>
                            </div>
                        </div>
                    </div>
                </div>
               </div>
                </div>
            </section>
<!--<p><?php echo anchor('admin/menu/create_menu', 'Create a new Menu')?> </p>-->
</div>

    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?= base_url();?>/assets/plugins/datatables/jquery.dataTables.js"></script>
    <script src="<?= base_url();?>/assets/plugins/datatables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         }); 
         
         
         
                 
 $('.inner_active').click(function(ev){
            ev.preventDefault();
            
             var  dis = this;
             
             $.post($(this).attr('href'),{'sts':$(this).attr('title')},function(resp){
                 
                 if(resp == 0){
                    $(dis).html("Active");
                    $(dis).attr("title",resp);
                 }else if(resp == 1){
                    $(dis).html("Deactive");
                    $(dis).attr("title",resp);
                 }
                 
             });  
         });
         
         
         
    </script>
