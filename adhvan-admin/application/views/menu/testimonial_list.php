 <div class="content-wrapper">
               
              <section class="content-header">
          <h1>Testimonial Listing</h1>
         
        </section>
        <section class="content">
        
<div class="box box-primary">
<div class="box-body" >
                    
<!--<?php   echo $msg; ?>-->
						<div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Date s</th>
                                            <th>Title</th>
                                            <th>Category</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="testimonial">
          <?php foreach ($events as $gall):
		  ?>
		<tr class="odd gradeX" id="events_<?php echo $gall['id']; ?>" >

			<td><?php echo $gall['date'] ; ?></td>
			<td><?php echo $gall['title'] ; ?></td>
			<td><?php echo $gall['category']; ?></td>
			
			
			
			<td>
<?php 
         if($gall['status'] == 0){ $anc = 'Active'; }else{ $anc = 'Deactive'; }
    
                          ///  echo anchor('admin/slider/actdeact/'.$gall->id, $anc,array('class' => 'act-deact','title'=>$gall->status));    ?>
                    <a href="<?php echo site_url('admin/events/actdeactive/'.$gall['id']); ?>"  class="act-deactive" title="<?php echo $gall['status']; ?>" ><?php echo $anc; ?></a>
                    
</td>
            
			
			
            <td align="center"><?php echo anchor("admin/events/edit_testimonial/".$gall['id'], 'Edit') ;?> | <a onclick="return confirm('Are You Sure')" href="<?php echo site_url("admin/events/delete_event/".$gall['id']) ?>" >Delete</a></td>
		</tr>
		<?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                       
                </div>
            </div>
</div>
</section>
</div>
    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
   

    
        
<script src="<?php echo base_url()?>assets/plugins/jQueryUI/jquery-ui.js"></script>
<script>
$(document).ready(
function() {
$("#testimonial").sortable({
update : function () {
serial = $('#testimonial').sortable('serialize');
    
    
 
$.ajax({
url: "<?php echo site_url('admin/events/event_order'); ?>",
type: "post",
data: serial,
error: function(){
alert("theres an error with AJAX");
}
});
    
}
});
}
);


 $('.act-deactive').click(function(ev){
             event.preventDefault();
            
             var dis = this;
             
             $.post($(this).attr('href'),{'sts':$(this).attr('title')},function(resp){
                 
                 if(resp == 0){
                    $(dis).html("Active");
                    $(dis).attr("title",resp);
                 }else if(resp == 1){
                    $(dis).html("Deactive");
                    $(dis).attr("title",resp);
                 }
                 
             });  
         });
         

</script>      

        