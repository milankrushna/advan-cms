



  <script src="<?php echo base_url()?>assets/plugins/jQueryUI/jquery-ui.js"></script>

 <script>

$(document).ready(

function() {

$("#sortme").sortable({

update : function () {

serial = $('#sortme').sortable('serialize');

$.ajax({

url: "<?= site_url('admin/menu/save_order'); ?>",

type: "post",

data: serial,

error: function(){

alert("theres an error with AJAX");

}

});

}

});

}

);

</script>
<!---Tree view Script-->
      
<link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/tree/jqwidgets/styles/jqx.base.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/tree/scripts/gettheme.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/tree/jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/tree/jqwidgets/jqxdata.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/tree/jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/tree/jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/tree/jqwidgets/jqxpanel.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/tree/jqwidgets/jqxtree.js"></script>

<script type="text/javascript">
	$(document).ready(function () {
		$.post("<?= site_url('admin/menu/get_tree');?>",function(msg){
			var data=msg;
			var theme = getTheme();
			var source =
			{
				datatype: "json",
				datafields: [
					{ name: 'id' },
					{ name: 'parent_menu' },
					{ name: 'menu' }
				],
				id: 'id',
				localdata: data
			};
			var dataAdapter = new $.jqx.dataAdapter(source);
			dataAdapter.dataBind();
			var records = dataAdapter.getRecordsHierarchy('id', 'parent_menu', 'items', [{ name: 'menu', map: 'label'}]);
			$('#jqxWidget').jqxTree({ source: records, width: '300px', theme: theme });
		})
	});
</script>

<!---Tree view Script-->


  <div class="content-wrapper">

<section class="content-header">

        <h1>Sub Menu List : <?php echo $menu->menu; ?></h1>

      </section>

      <section class="content">

      <a  class="btn btn-primary" href="<?= site_url('admin/menu/create_menu') ?>">Create A New Menu</a>

    <div class="box box-primary ">
    
<div class="box-body ">

<div class="col-md-4">
                
				   <div class="panel panel-default">
                        <div class="panel-heading">
                            Menu List(Tree View)
						</div>
                        <div class="panel-body">
                            <div class="table-responsive">
    				<div id='jqxWidget'></div>
						  
						  
                            </div>
                        </div>
                    </div>
                <!---Tree view Script-->
			
    </div>
          




<div class="col-md-4">


 

                    <div class="panel panel-default">

                        <div class="panel-heading">

                            <b>Sub Menu Listing of <?php echo $menu->menu; ?></b>

						<?php if($menu->parent_menu != 0){?>	<p class="pull-right"><a href="<?= site_url('admin/menu/order_menu')."/".$menu->parent_menu; ?>"><b>Back</b></a></p> <?php }else{ ?>

						<p class="pull-right"><a href="<?= site_url('admin/menu/order_menu'); ?>"><b>Back</b></a></p>

						<?php } ?>

						

					  	</div>


 

                        <div class="panel-body">

                            <div class="table-responsive">

<?php if(count($menulist) == 0){?>

 No Sub Menu Exist!!

<?php

}

?>





<ul id="sortme" style="list-style-type: none;">                               

          <?php foreach ($menulist as $ml):?>



<li  style=" background: linear-gradient(#00c0ef, #00C0B4);padding: 10px;margin: 1px;" id="menu_<?php echo $ml['id'] ?> "><a style="color: #ffffff;font-weight: 700;" href="<?= site_url('admin/menu/order_submenu')."/".$ml['id']; ?>"><?php echo $ml['menu'] ?></a> </li>



		<?php endforeach;?>

</ul>                                    

                             

                            </div>

                        </div>

                    </div>

                
                </div>

        </div>

<!--<p><?php echo anchor('admin/menu/create_menu', 'Create a new Menu')?> </p>-->
 </div>
 </section>


</div>



   