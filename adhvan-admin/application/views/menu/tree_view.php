
<link rel="stylesheet" href="<?php echo base_url()?>assets/tree/jqwidgets/styles/jqx.base.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/tree/scripts/gettheme.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/tree/jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/tree/jqwidgets/jqxdata.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/tree/jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/tree/jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/tree/jqwidgets/jqxpanel.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/tree/jqwidgets/jqxtree.js"></script>

<script type="text/javascript">
	$(document).ready(function () {
		$.post("<?= site_url('admin/menu/get_tree');?>",function(msg){
			var data=msg;
			var theme = getTheme();
			var source =
			{
				datatype: "json",
				datafields: [
					{ name: 'id' },
					{ name: 'parent_menu' },
					{ name: 'menu' }
				],
				id: 'id',
				localdata: data
			};
			var dataAdapter = new $.jqx.dataAdapter(source);
			dataAdapter.dataBind();
			var records = dataAdapter.getRecordsHierarchy('id', 'parent_menu', 'items', [{ name: 'menu', map: 'label'}]);
			$('#jqxWidget').jqxTree({ source: records, width: '300px', theme: theme });
		})
	});
</script>
      
   <div id="content">

    <div class="inner">

            <div class="row">

                    <div class="col-lg-12">
                        <h2>Menu List</h2>
<?php echo $msg; ?>
                    </div>
                </div>
<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Menu List
						</div>
                        <div class="panel-body">
                            <div class="table-responsive">
    <div id='jqxWidget'></div>
						  
						  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<p><?php echo anchor('admin/menu/create_menu', 'Create a new Menu')?> </p>
</div>
</div>
    <!-- END GLOBAL SCRIPTS -->
   