<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/ajxupload/upload.css"  />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css"  />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/ajxupload/jquery.form.js" ></script>

<script>
function make_link(page_id){
	$.post('<?= site_url('admin/menu/make_link'); ?>',{ page_id : page_id },function(msg){
	
	//alert(msg)
	$('#link').val(msg);
	
	})
}
</script>



    <!-- END PAGE LEVEL  STYLES -->

     <!--PAGE CONTENT --> 

    <div class="content-wrapper">
    <section class="content-header">
        <h1>Edit Menu</h1>
      </section>
      <section class="content">
<div class="box box-primary ">
             
<div class="box-body">
<div class="col-md-8"> 
<div id="infoMessage" style="color:#F00"><?php echo $this->session->flashdata('message');  ?></div>
<form action="<?php echo current_url(); ?>" method="post" enctype="multipart/form-data" id="editmenu">
    
    
    <?php
    //   print("<pre>");
    //   print_r($menu);exit;
    
    ?>
    
     <input type="checkbox" id="mnu" name="nm_menu" <?php if($menu->ct_menu == 1){ ?> checked <?php } ?> value="1">Category Menu
                    
    
    <div class="form-group">
                    <label for="text12" class="control-label">Menu Name</label>
                  
               <input  type="text" class="form-control" value="<?php echo $menu->menu; ?>" name="menu" >
                     
                	</div>
    
    
       <div id="meta_ctgy"  <?php echo ($menu->ct_menu != '1')? 'style="display:none;"' : ''; ?> > 
           <div class="form-group">
                    <label for="text1" class="control-label">Meta title</label>
                <input  type="text" class="form-control" value="<?php echo $menu->meta_title; ?>" name="meta_title">
                	</div>
      <div class="form-group">
                    <label for="text1" class="control-label">Meat Description</label>
          <textarea  type="text" class="form-control" name="meta_desc"  ><?php echo $menu->meta_desc; ?></textarea>
                	</div>  
    
    <div class="form-group">
        <label for="text1" class="control-label">Meat Keyword</label>
        <textarea  type="text" class="form-control" name="meta_keyword"><?php echo $menu->meta_keyword; ?></textarea>
     </div> 
           
    </div>
					<div class="form-group">
                    <label for="text1" class="control-label">Parent Menu</label>
                    

      	                       	<select  name="parent_menu" class="form-control  chzn-rtl" id="parent_menu">

		<option value="">Choose A Parent.... </option>

		<?php  foreach($parents as $par){
		?>		
        <option value="<?php echo $par['id']; ?>"  <?php if($menu->parent_menu == $par['id']){ echo "Selected"; }?>   class="odia-fo" > <?php echo $par['menu']; ?> </option>
		<?php }  ?>

   	</select> 
                    
                	</div>
	
    
    
   
   <div class="form-group">
   <label for="text1" class="control-label">Open in</label>
 <select  name="target"  class="form-control  chzn-rtl" id="">
<option <?php if($menu->target == ""){ echo 'selected'; } ?> value="">Current tab</option>
<option <?php if($menu->target == "_blank"){ echo 'selected'; } ?> value="_blank">New Tab</option>

  
   	</select></div> 
    
    
    
						<div class="form-group">
                    <label for="text1" class="control-label">Link Page</label>
                   

  <select <?php if($menu->custom == 1){ ?> disabled <?php } ?> onchange="make_link(this.value);"  name="page_id" class="form-control  chzn-rtl" id="page_id">

		<option value="">Choose A Page.... </option>

		<?php  foreach($pages as $pag){
		?>		
        <option value="<?php echo $pag['id']; ?>"  <?php if($menu->page_id == $pag['id']){ echo "Selected"; }?>   class="odia-fo" > <?php echo $pag['name']; ?> </option>
		<?php }  ?>

   	</select>
                	</div>
					
					<div class="form-group">
    <input type="radio"  name="custom" id="cstm1" value="0" <?php if($menu->custom == 0){?>checked="checked"<?php } ?>  > 
         <label for="cstm1" class="control-label">: From Selected Page</label>
    </div>
    <div class="form-group">
    <input type="radio" name="custom" id="cstm2" value="1" <?php if($menu->custom == 1){?>checked="checked"<?php } ?>>
         <label for="cstm2" class="control-label">: Custom Link</label>
    </div>
    
    
                  <div class="form-group">
                    <label for="text1" class="control-label">Link</label>
                    
<input  type="text" id="link" <?php if($menu->custom == 0){ ?> readonly <?php } ?> class="form-control"   value="<?php if($menu->custom == 1){ echo $menu->link; 
}else{ echo  str_replace('admin/','',base_url($menu->link)); } ?>"   name="link" >
                       
                	</div>
	                 	
                	                               				    
    <!--       Progress Bar-->
     
       <!--       Progress Bar-->
                    
                        <input type="submit"  value="Update" class="btn btn-success " />
         
	</form>
	  
</div>
    </div>
    </div>
    </section>
           </div>
                    <!-- END PAGE CONTENT -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/select2/select2.full.min.js" ></script>
<script>

  
        
 $(document).ready(function() {
     
     $('#page_id').select2();
        
    });
    
    $('input[type="radio"]').click(function(ev){
    
        if(ev.currentTarget.id == 'cstm2'){
            
            $('select[name="page_id"]').prop('disabled',true);
            $('input[name="link"]').prop('readonly',false);
            
        }else if(ev.currentTarget.id == 'cstm1'){
            
            $('select[name="page_id"]').prop('disabled',false);
            $('input[name="link"]').prop('readonly',true);
            $('input[name="link"]').val('#');
        }
        
    });
    
    
    
        
     $('input[type="checkbox"]#mnu').click(function(){ 
     var dis = this;
         if($(dis).prop("checked") == true){
            $('#page_id').prop('disabled','true');
            $('#cstm1').prop('disabled','true');
          
              $('#meta_ctgy').show('slow');
            
         }else if($(dis).prop("checked") == false){
           
 $('#page_id').removeAttr('disabled');
            $('#cstm1').removeAttr('disabled');
             $('#meta_ctgy').hide('slow');
         }
         
         
     });
    
    
</script>	

 
    

 