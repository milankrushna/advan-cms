<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/ajxupload/upload.css"  />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css"  />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/ajxupload/jquery.form.js" ></script>

<script>

function make_link(page_id){
    if(page_id !=''){
	$.post('<?php echo site_url('admin/menu/make_link'); ?>',{ page_id : page_id },function(msg){
	
	//alert(msg)
	$('#link').val(msg);
	
	});
    }else{
       $('#link').val('#'); 
    }
}

</script>



    <!-- END PAGE LEVEL  STYLES -->

     <!--PAGE CONTENT --> 

<div class="content-wrapper">
     <section class="content-header">
        <h1>Create Menu</h1>
      </section>
<section class="content">
<div class="box box-primary">
<div class="box-body " >
<div class="col-md-8">
<div id="infoMessage" style="color:#F00"><?php echo $this->session->flashdata('message');  ?></div>

<form action="<?php echo site_url('admin/menu/create_menu') ?>" method="post" enctype="multipart/form-data" >

    <span style="color:#E81A1A"><?php echo validation_errors(); ?></span>
    
    <input type="checkbox" id="mnu" name="nm_menu" <?php if($menu['ct_menu'] = '1'){ ?> checked <?php } ?> value="1" >Category Menu
    
    
    
 				<div class="form-group">
                    <label for="text1" class="control-label">Menu Name/Category Menu</label>
                <input  type="text" class="form-control" name="menu">
                	</div>
       <div id="meta_ctgy" style=""> 
        
        <div class="form-group">
                    <label for="text1" class="control-label">Meta title</label>
                <input  type="text" class="form-control" name="meta_title">
                	</div>   
           
      <div class="form-group">
                    <label for="text1" class="control-label">Meat Description</label>
          <textarea  type="text" class="form-control" name="meta_desc"></textarea>
                	</div>  
    
    <div class="form-group">
        <label for="text1" class="control-label">Meat Keyword</label>
        <textarea  type="text" class="form-control" name="meta_keyword"></textarea>
     </div> 
           
    </div>
  
					<div class="form-group">
                    <label for="text1" class="control-label">Parent Menu</label>
                    

<select  name="parent_menu" class="form-control  chzn-rtl" id="parent_menu">

		<option value="">Choose A Parent.... </option>

<?php	 foreach($parents as $par){
		?>		
        <option value="<?php echo $par['id']; ?>" class="odia-fo" > <?php echo $par['menu']; ?> </option>
		<?php }  ?>

   	</select> 
                      
                	</div>
					
					
    <div class="form-group">
   <label for="text1" class="control-label">Open in</label>
 <select  name="target"  class="form-control  chzn-rtl" id="">
<option value="">Current tab</option>
<option value="_blank">New Tab</option>


   	</select></div>
    
   <!-- <div class="form-group">
    <label for="text1" class="control-label">Image</label>
    <input  type="file" class="form-control" name="menu_image"   > 
    </div>-->
    
    
	<div class="form-group">
    <input type="radio" checked="checked" name="custom" id="cstm1" value="0">
         <label for="cstm1" class="control-label">: From Selected Page</label>
    </div>
    <div class="form-group">
    <input type="radio" name="custom" id="cstm2" value="1">
         <label for="cstm2" class="control-label">: Custom Link</label>
    </div>
	
    <div class="form-group">
   <label for="text1" class="control-label">Link Page</label>
 <select  name="page_id" onchange="make_link(this.value);" class="form-control" id="page_id">
<option value="">Choose A Page.... </option>
<?php	 foreach($pages as $pag){
		?>		
        <option value="<?php echo $pag['id']; ?>" class="odia-fo" > <?php echo $pag['name']; ?> </option>
		<?php }  ?>

   	</select></div>
					
					
					
                   <div class="form-group">
                    <label for="text1" class="control-label">Link</label>
                    
                              <input  type="text" readonly value="#" id="link" class="form-control" name="link">
                      
                	</div>
                	                               				    
    <!--       Progress Bar-->
       <div id="p_bar" style="display:none;" class="hide">
           <div  class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" id="progressbar" style="width:0%">
                    <span id="statustxt"></span>
                </div>
           </div>
       </div>
       <!--       Progress Bar-->
                    
	    
                        <input type="submit" id="tags" value="Submit" class="btn btn-success" />
                     
                   
	</form>
    </div>  
</div>
    </div>
    </section>
           </div>
                          
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/select2/select2.full.min.js" ></script>


<script>

    $('input[type="radio"]').click(function(ev){
    
        if(ev.currentTarget.id == 'cstm2'){
            
            $('select[name="page_id"]').prop('disabled',true);
            $('input[name="link"]').prop('readonly',false);
            
        }else if(ev.currentTarget.id == 'cstm1'){
            
            $('select[name="page_id"]').prop('disabled',false);
            $('input[name="link"]').prop('readonly',true);
            $('input[name="link"]').val('#');
        }
        
    });
    
   	


 $(document).ready(function() {
     
     $('#page_id').select2();
     
        //elements
        var progressbox     = $('#progressbox');
        var progressbar     = $('#progressbar');
        var statustxt       = $('#statustxt');
        var submitbutton    = $("#SubmitButton");
        var myform          = $("#createmenu");
        var output          = $("#output");
        var completed       = '0%';
 
                $(myform).ajaxForm({
                    beforeSend: function() { //brfore sending form
                        document.getElementById("p_bar").className = "show";
                        submitbutton.attr('disabled', ''); // disable upload button
                        statustxt.empty();
                        progressbox.slideDown(); //show progressbar
                        progressbar.width(completed); //initial value 0% of progressbar
                        statustxt.html(completed); //set status text
                        statustxt.css('color','#fff'); //initial color of status text
                    },
                    uploadProgress: function(event, position, total, percentComplete) { //on progress
                        progressbar.width(percentComplete + '%') //update progressbar percent complete
                        statustxt.html(percentComplete + '%'); //update status text
                        if(percentComplete>50)
                            {
                                statustxt.css('color','#fff'); //change status text to white after 50%
                            }

                        },
                    complete: function(response) { // on complete
                        var res = output.html(response.responseText); //update element with received data
                        myform.resetForm();  // reset form
                        submitbutton.removeAttr('disabled'); //enable submit button
                        progressbox.slideUp(); // hide progressbar
                        document.getElementById("p_bar").className = "hide";
                        
                     
                        
if(response.responseText == '1'){
alert("Successfully Upload"); 
}else if(response.responseText == '2'){
alert("Please Upload a valid Image File");
}else if(response.responseText == '3'){
alert("You have no rights to upload");
}else{
alert("Uploading unsuccessful Please Try again");
}
//location.reload();
                       /// location.reload();
                    }
                   
            });
        });
    
    
     $('input[type="checkbox"]#mnu').click(function(){ 
     var dis = this;
         if($(dis).prop("checked") == true){
            $('#page_id').prop('disabled','true');
            $('#cstm1').prop('disabled','true');
             $('#link').val('#');
              $('#meta_ctgy').show('slow');
            
         }else if($(dis).prop("checked") == false){
           
 $('#page_id').removeAttr('disabled');
            $('#cstm1').removeAttr('disabled');
             $('#meta_ctgy').hide('slow');
         }
         
         
     });
    
    
    
    
    
<?php /* ?>
  $('input[type="checkbox"]').click(function(){
             $('input[type="checkbox"]').prop("disabled",true);
            var dis = this;
             ///value of which one you want to update
            var update_id = $(dis).val();
            if($(dis).prop("checked") == true){
            
                var sts = 1;
                //update database status through ajax
                $.post('<?php echo site_url('admin/menu/update_ftr_menu') ?>',{status:sts,id:update_id},function(resp){
                    
                 if(resp == 1){
                   alert("successfull.");  
                   $('input[type="checkbox"]').prop("disabled",false);  
                 }   
                    
                });
                
            }
            else if($(dis).prop("checked") == false){
                
                var sts = 0;
                //update database status through ajax
                $.post('<?php echo site_url('admin/menu/update_ftr_menu') ?>',{status:sts,id:update_id},function(resp){
                    
                 if(resp == 1){
                   alert("successfull.");  
                   $('input[type="checkbox"]').prop("disabled",false);  
                 } else{
				 	
				 	alert("unsuccessfull, please try again");
				 }  
                    
                });
                
            }
        });
    <?php  */ ?>
</script>
