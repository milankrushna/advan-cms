
	<div class="content-wrapper">
              <section class="content-header">
          <h1>Advertise Listing</h1>
         
        </section>
        <section class="content">
<div class="box box-primary">
<div class="box-body" >
	      			
	      			
					
	<div class="widget-content">
						
	      			
                                        
                                        <a href="<?php echo site_url('admin/advertise/create_new_ad')?>" class="btn btn-success pull-right">Create Ad</a>
	  				
					
	<div class="widget-content">
						
           		
						
			<table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Position</th>
                                        <th>image</th>
                                        <th>Script</th>
<!--
                                        <th>Start date</th>
                                        <th>End Date</th>
-->
                                        <th>Status</th>
                                        <th>Action</th>
                                       
                                    </tr>
                                </thead>
                                <tbody >
                                    <?php $s=1; foreach ($data as $ed){?>
                                    <tr>
                                        
                                        <td><?php echo $ed->title; ?></td>
                                        <td><?php
                                        if($ed->position==1){echo "Top";}else if($ed->position==2){echo "Bottom";}else if($ed->position==3){echo "Right";} ?></td>
                                        
                                        <td><div style="height: 103px;overflow: hidden;">
                                            <?php if($ed->ad_pic != ""){ ?> <a href="<?php echo $ed->link; ?>" target="_blank"><img style="width:100px; height:100px;" src="<?php echo base_url();?>upload/ad_image/<?php echo $ed->ad_pic;  ?>" ></a> 
                                        <?php  if (date('Y-m-d') >= $ed->end_date ){ ?>
                                            <p style="color:red"> This Advertise is out of date</p>
                                        <?php } ?>
                                            <?php } ?>
                                             </div>
                                        </td>
                                        <td><?php if($ed->script !=""){ echo "Java Script advertise"; }  ?></td>
                                       <?php /* <td><?php echo date('d-M-Y',strtotime($ed->start_date));  ?></td>
                                        <td><?php echo date('d-M-Y',strtotime($ed->end_date));  ?></td> */ ?>
                                        <td><?php if($ed->status == 0){ ?> <input type="checkbox" onchange="publish(this,'<?php echo $ed->id ?>')"><?php }else{ ?> <input type="checkbox" checked="checked" onchange="unpublish(this,'<?php echo $ed->id ?>')"><br/>Published <?php }?> </td>
                                        <td><a class="btn btn-warning" href="<?php echo site_url('admin/advertise/edit_advertise'."/".$ed->id)?>" ><i class="fa fa-edit"></i></a>||<a title="Delete" class="btn  btn-danger" href="<?php echo site_url('admin/advertise/delete_advertise'."/".$ed->id)?>" onclick="return confirm('Are you sure you want to delete this Advertise?');"><i class="fa fa-trash"></i></a></td>
                                        
                                       
                                    </tr>
                              <?php }?>
                                </tbody>
                            </table>			
      </div>
    
    </div>
    
            </div>
        </section>
</div>
<script>
function publish(dis,id){
	 $.post('<?php echo site_url('admin/advertise/publish')?>',{id:id},function(msg){

if(msg==1){
     alert('Ad successfully Publish');
	location.reload();

}else{
	alert("Error: something error with Ajax,try again");
    
}


        });

}
function unpublish(dis,id){
	 $.post('<?php echo site_url('admin/advertise/unpublish')?>',{id:id},function(msg){

if(msg==1){
    alert('Ad successfully Removed');
	location.reload();

}else{
	alert("Error: something error with Ajax,try again");
    
}


        });

}
</script>


