
<link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/datepicker/datepicker3.css">

<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>



<script>

  $(function() {
    $( "#datepicker" ).datepicker({
      minDate: new Date(),
      changeMonth: true,
      changeYear: true,
      dateFormat: "yy/mm/dd",


    });
    $( "#datepicker2" ).datepicker({
      minDate:new Date(),
      changeMonth: true,
      changeYear: true,
      dateFormat: "yy/mm/dd"
       
    });
  });

  function get_type(dis){

  if(dis==1){
  	document.getElementById("script").className = "hide";
  	document.getElementById("script_value").value = "";
  	document.getElementById("output").value = "";
  	document.getElementById("image").className = "show";
  	//document.getElementById("image").classList.add("show");
  }else if(dis==2){
  	document.getElementById("image").className = "hide";
  	document.getElementById("link_value").value = "";
  	document.getElementById("image_value").value = "";
  	document.getElementById("script").className = "show";
  	//document.getElementById("script").classList.add("show");
  }
  }


  var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('output');
      output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);

  };



  </script>

  
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/validate/css/screen.css" >
  <script src="<?php echo base_url() ?>assets/validate/dist/jquery.validate.js"></script>
  <script src="<?php echo base_url() ?>assets/validate/dist/additional-methods.js"></script>
<script>
  $(document).ready(function() {
    $(".up_from").validate();
  });
  </script>


<style>
	.hide{
		display: none;
	}
	.show{
		display: block;
	}

</style>


	
	<div class="content-wrapper">
              <section class="content-header">
          <h1>Create Advertise</h1>
         
        </section>
        <section class="content">
<div class="box box-primary">
<div class="box-body" >
	      			
	      			
					
	<div class="widget-content">
						
           		
  <form class="up_from" action="<?php echo site_url('admin/advertise/create_new_ad')?>" method="post" enctype="multipart/form-data">
                <div style="color: red;"><?php echo validation_errors(); ?></div>
		   <div class="form-group">											
		<label class="control-label" for="username">Display In</label><br>
      <input type="checkbox" value="1" name="web" checked> : Website <br>
      <input type="checkbox" value="1" name="mobile"> : Mobile <br>
      </div>
      
      
      
                <div class="form-group">											
		<label class="control-label" for="username">Position</label>
		
		<select class="form-control" name="position"  required="">

				<option value="">Select a Position</option>
				<option value="1">Top</option>
				<option value="2">Bottom</option>
				<option value="3">Right</option>
		</select>										
						
		</div> <!-- /control-group -->
      
                <div class="form-group">											
		<label class="control-label" for="username">Page</label>
		
		<select class="form-control" name="page"  required="">

				<option value="">Select a Page</option>
				<option value="home">Home</option>
				<option value="article">Article</option>
		</select>										
						
		</div> <!-- /control-group -->
      
      
      
		<div class="form-group">											
		<label class="control-label" for="username">Type</label>
		
		<select  class="form-control" name="type" onchange="get_type(this.value)">

				<option value="">Select a Type</option>
				<option value="1">Image</option>
				<option value="2">Script</option>
		</select>										
				
		</div> <!-- /control-group -->
			
       <?php /*
			<div class="form-group">						
		   <label class="control-label" for="username">Start date</label>
			
              
				<input type="text" name="start_date" class="form-control" id="datepicker" readonly="" required="">  								
			
			</div>
			<div class="form-group">						
		   <label class="control-label" for="username">End date</label>
			
              
				<input type="text" name="end_date" class="form-control" id="datepicker2" readonly="" required=""> 								
		
			</div> */	?>
		<div class="form-group">						
		   <label class="control-label" for="username">Title</label>
			
           <input type="text" name="title" class="form-control" >  
												
		
			</div>
			
			<div id="image" class="hide">
			<div  class="form-group">						
		   <label class="control-label" for="username">link</label>
			
              
				<input type="text" id="link_value" name="link" class="form-control" >  								
			</div> <!-- /controls -->
		
			<div  class="form-group">						
		   <label class="control-label" for="username">Image</label>
			
              
				<input type="file" id="image_value" name="ad_image" onchange="loadFile(event)" class="form-control" accept="image/*" >  								
			</div> <!-- /controls -->
			<p style="color: red;"> For top & bottom position upload 800x120 size image And For right Position 160x600 size Image </p>
			
		
			   <img id="output" style="width:100px; height:100px;"  />
			</div>
			<div id="script"  class="form-group  hide">						
		   <label class="control-label" for="username">Script</label>
			
              
				<textarea name="script" rows="6" id="script_value" class="form-control" required="" ></textarea>						
			
			</div>
			

                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Save</button> 
                    
                   
                </div>
           
            </form>	
    </div>
    
    </div>
    
            </div>
        </section>
</div>