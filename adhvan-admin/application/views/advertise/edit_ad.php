
<link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/datepicker/datepicker3.css">

<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>



<style>
	.hide{
		display: none;
	}
	.show{
		display: block;
	}

</style>

	<div class="content-wrapper">
              <section class="content-header">
          <h1>Edit Advertise</h1>
         
        </section>
        <section class="content">
<div class="box box-primary">
<div class="box-body" >
	      			
	      			
					
	<div class="widget-content">
						
           		
  <form class="up_from" action="<?php echo site_url('admin/advertise/edit_advertise'."/".$data->id)?>" method="post" enctype="multipart/form-data">
                <div style="color: red;"><?php echo validation_errors(); ?></div>
		
        <div class="form-group">											
		<label class="control-label" for="username">Display In</label><br>
      <input type="checkbox" value="1" name="web"  <?php echo  ($data->display_web == '1') ? 'checked' : ''; ?> > : Website <br>
      <input type="checkbox" value="1" name="mobile"  <?php echo  ($data->display_mob == '1') ? 'checked' : ''; ?> > : Mobile <br>
      </div>
      
                <div class="form-group">											
		<label class="control-label" for="username">Position</label>
		
		<select class="form-control" name="position"  required="">

				<option value="">Select a Position</option>
				<option <?php if($data->position == 1){ ?> selected="selected" <?php } ?> value="1">Top</option>
				<option <?php if($data->position == 2){ ?> selected="selected" <?php } ?> value="2">Bottom</option>
				<option <?php if($data->position == 3){ ?> selected="selected" <?php } ?> value="3">Right</option>
		</select>										
					
		</div> <!-- /control-group -->
		
      
            <div class="form-group">											
		<label class="control-label" for="username">Page</label>
		
		<select class="form-control" name="page"  required="">
 
				<option  value="">Select a Page</option>
				<option <?php if($data->page == 'home'){ ?> selected="selected" <?php } ?> value="home">Home</option>
				<option <?php if($data->page == 'article'){ ?> selected="selected" <?php } ?> value="article">Article</option>
		</select>										
						
		</div> <!-- /control-group -->
      
      
      
      
      <div class="form-group">
            
		<label class="control-label" for="username">Type</label>
		
		<select class="form-control" id="typ" name="type" onchange="get_type(this.value)">

				<option value="">Select a Type</option>
				<option <?php if($data->type == 1){ ?> selected="selected" <?php } ?> value="1">Image</option>
				<option <?php if($data->type == 2){ ?> selected="selected" <?php } ?> value="2">Script</option>
		</select>										
					
		</div> <!-- /control-group -->
			
       <?php /*
			
      <div class="form-group">						
		   <label class="control-label" for="username">Start date</label>
			
				<input type="text" name="start_date" value="<?php echo $data->start_date; ?>" class="form-control" id="datepicker" readonly="" required="">  								
			
			</div>
			
      
      <div class="form-group">						
		   <label class="control-label" for="username">End date</label>
			
				<input type="text" name="end_date" value="<?php echo $data->end_date; ?>" class="form-control" id="datepicker2" readonly="" required=""> 								
			
			</div>	
*/		?>
      
      <div class="form-group">						
		   <label class="control-label" for="username">Title</label>
			
           <input type="text" value="<?php echo $data->title; ?>" name="title" class="form-control" >  
												
		
			</div>
			
			<div id="image" class="hide">
			
                
                
                <div  class="form-group">						
		   <label class="control-label" for="username">link</label>
			
				<input type="text" id="link_value" value="<?php echo $data->link; ?>" name="link" class="form-control" >  							
			</div>
			
                
                
                <div  class="form-group">						
		   <label class="control-label" for="username">Image</label>
			
				<input type="file" id="image_value" name="ad_image" onchange="loadFile(event)" class="form-control" accept="image/*" >  							
			<p style="color: red;"> For top & bottom position upload 800x120 size image And For right Position 160x600 size Image </p>
			</div>
               
			   <img id="output" src="<?php echo base_url()?>upload/ad_image/<?php echo $data->ad_pic ?>" style="width:100px; height:100px;"  />
                
			</div>
			
      
      <div id="script"  class="form-group  hide">						
		   <label class="control-label" for="username">Script</label>
			
				<textarea name="script" rows="6" id="script_value" class="form-control" required="" ><?php echo $data->script ?></textarea>		
			</div>
			
      <?php  if($data->type==2){  echo $data->script; }  ?>
      
      
      
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Update</button> 
                    
                   
                </div>
           
            </form>	

 </div>
    
    </div>
    
            </div>
        </section>
</div>
   

<script>

$( document ).ready(function() {
 
	var dis = document.getElementById('typ').value;
	get_type(dis)
	});
  function get_type(dis){

  if(dis==1){
  	document.getElementById("script").className = "hide";
  	//document.getElementById("script_value").value = "";
  	//document.getElementById("output").value = "";
  	document.getElementById("image").className = "show";
  	//document.getElementById("image").classList.add("show");
  }else if(dis==2){
  	document.getElementById("image").className = "hide";
  	//document.getElementById("link_value").value = "";
  	//document.getElementById("image_value").value = "";
  	document.getElementById("script").className = "show";
  	//document.getElementById("script").classList.add("show");
  }
  }



  $(function() {
    $( "#datepicker" ).datepicker({
      minDate: new Date(),
      changeMonth: true,
      changeYear: true,
      dateFormat: "yy/mm/dd",


    });
    $( "#datepicker2" ).datepicker({
      minDate:new Date(),
      changeMonth: true,
      changeYear: true,
      dateFormat: "yy/mm/dd"
       
    });
  });



  var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('output');
      output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);

  };



  </script>

  
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/validate/css/screen.css" >
  <script src="<?php echo base_url() ?>assets/validate/dist/jquery.validate.js"></script>
  <script src="<?php echo base_url() ?>assets/validate/dist/additional-methods.js"></script>
<script>
  $(document).ready(function() {
    $(".up_from").validate();
  });
  </script>



