<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $webdata->admin_panel_name; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/dist/fonts_asm/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/dist/fonts_asm/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/dist/css/skins/_all-skins.min.css">
    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url()?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
   

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<!-- <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/user/fav/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/user/fav/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/user/fav/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/user/fav/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/user/fav/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/user/fav/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>assets/user/fav/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/user/fav/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/user/fav/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>assets/user/fav/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/user/fav/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/user/fav/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/user/fav/favicon-16x16.png">
<link rel="manifest" href="<?php echo base_url(); ?>assets/user/fav//manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff"> -->
      
  </head>
  <!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
  <!-- the fixed layout is not compatible with sidebar-mini -->
  <body class="hold-transition skin-blue fixed sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <?php  $user = $this->session->userdata; ?>
        <a href="<?php echo site_url('admin/dashboard')?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b><?php echo substr($webdata->name,0,1) ?>..</b></span>
          <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b><?php echo $webdata->name; ?></b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav" >
              <!-- Messages: style can be found in dropdown.less-->
        
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                  <img src="<?php echo base_url()?>assets/dist/img/avatar.jpeg" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php echo $user['email']; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?php echo base_url()?>assets/dist/img/avatar.jpeg" class="img-circle" alt="User Image">
                    <p>
                     <?php echo $user['email']; ?>
                      <small></small>
                    </p>
                  </li>
                  
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo site_url('admin/auth')?>" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo site_url('admin/auth/logout')?>" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a target="_blank" href="<?php echo site_url('admin/dashboard')?> " ><i class="fa fa-home"></i></a>
              </li>
                <li>
                <a target="framename" href="<?php echo str_replace('adhvan-admin/', '', base_url()); ?>" ><i class="fa fa-television"></i></a>
              </li>
              <!---<li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>-->
            </ul>
          </div>
        </nav>
      </header>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url()?>assets/dist/img/avatar.jpeg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $user['email']; ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu" style="overflow:auto;" >
            <li class="header">MAIN NAVIGATION</li>
           
              
              
                <li class="treeview <?php if($menu == 2){?>active<?php } ?>">
                    <a href="#"><i class="fa fa-folder-o"></i><span>Article</span></a>
                  <ul class="treeview-menu">
                <li><a href="<?php echo site_url('admin/pages')?>"><i class="fa fa-circle-o text-yellow"></i>All Articles</a></li>
                <li><a href="<?php echo site_url('admin/pages/create_page')?>"><i class="fa fa-circle-o text-aqua"></i> Create Articles</a></li>
               <!-- <li><a href="<?php echo site_url('admin/pages/create_contest')?>"><i class="fa fa-circle-o text-aqua"></i> New Contest</a></li>
             <li><a href="<?php echo site_url('admin/pages/all_contest')?>"><i class="fa fa-circle-o text-aqua"></i>All contest</a></li>--->
                    
                    
                </ul>
                </li>
                
                
                 <li class="treeview <?php if($menu == 'common'){?>active<?php } ?>">
                     <a href="#"><i class="fa fa-clone"></i><span>Common Section</span> </a>
                  <ul class="treeview-menu">
                       <li><a href="<?php echo site_url('admin/common/create_common')?>"><i class="fa fa-circle-o text-aqua"></i>Create Common</a></li>
                     <li><a href="<?php echo site_url('admin/common')?>"><i class="fa fa-circle-o text-yellow"></i>Common Section</a></li>
                     
                     <!--- <li><a href="<?php echo site_url('admin/events/client')?>"><i class="fa fa-circle-o text-aqua"></i>Client</a></li> 
                      <li><a href="<?php echo site_url('admin/events/create_client')?>"><i class="fa fa-circle-o text-aqua"></i>Create Client</a></li> -->
                
              </ul>
              </li>
              
              
              <!--<li  class="treeview <?php if($menu == 'service'){?>active <?php } ?>"> 
                  <a href="#"><i class="fa fa-list"></i><span>Service</span></a>
                  <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('admin/events/create_service')?>"><i class="fa fa-circle-o text-yellow"></i> Create Service</a></li>
                  <li><a href="<?php echo site_url('admin/events/service')?>"><i class="fa fa-circle-o text-aqua"></i> Service Listing</a></li>
                  </ul>
                </li>-->
              
              <li class="treeview <?php if($menu == 'slider'){?>active<?php } ?>">
                     <a href="#"><i class="fa fa-clone"></i><span>Slider</span> </a>
              <ul class="treeview-menu">
                   <!--<li class=""><a href="<?php echo site_url('admin/slider/create_slide')?>"><i class="fa fa-circle-o text-yellow"></i><span>Create Slider</span></a></li>-->
               <!-- <li><a href="<?php echo site_url('admin/slider/view_slide')?>"><i class="fa fa-circle-o text-yellow"></i>Slide Listing</a></li>-->
                <li><a href="<?php echo site_url('admin/slider/create_banner')?>"><i class="fa fa-circle-o text-yellow"></i>Create Banner</a></li>
                <li><a href="<?php echo site_url('admin/slider/banner')?>"><i class="fa fa-circle-o text-yellow"></i>Banner Listing</a></li>
                 
              </ul>
              </li>
                
               
               <!-- <li class="treeview <?php if($menu == 'gallery'){?>active<?php } ?>">
                 <a href="#"><i class="fa fa-folder-o"></i><span>Gallery</span> </a>
                 <ul class="treeview-menu">
               <!--<li><a href="<?php echo site_url('admin/gallery/my_album')?>"><i class="fa fa-circle-o text-yellow"></i>Photo Gallery</a></li>-->
               <li><a href="<?php echo site_url('admin/gallery/video_gallery')?>"><i class="fa fa-circle-o text-aqua"></i> Video Gallery</a></li>
               <!--<li><a href="<?php echo site_url('admin/gallery/audio_gallery')?>"><i class="fa fa-circle-o text-yellow"></i> Audio Gallery</a></li>
               <li><a href="<?php echo site_url('admin/gallery/book_gallery')?>"><i class="fa fa-circle-o text-aqua"></i> Upload Document</a></li>
                 </ul>
               </li> -->
               
               
               
               
               
             <!-- <li  class="treeview <?php if($menu == 'temp'){?>active<?php } ?>">
                  <a href="#"><i class="fa fa-list"></i><span>Template</span></a>
                  <ul class="treeview-menu">
                    
                    <li class=""><a href="<?php echo site_url('admin/template/color_picker')?>"><i class="fa fa-circle-o text-yellow"></i><span>Color Selection</span></a></li>
                    <li class=""><a href="<?php echo site_url('admin/template/static_link')?>"><i class="fa fa-circle-o text-yellow"></i><span>Static Link</span></a></li>
                    
                  </ul>
                </li>-->
              
              
              
               <!--  <li  class="treeview <?php if($menu == 'menu'){?>active<?php } ?>">
                  <a href="#"><i class="fa fa-list"></i><span>Menu/Category Section</span></a>
                  <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('admin/menu/create_menu')?>"><i class="fa fa-circle-o text-yellow"></i> Add Menu/Category </a></li>
                  <li><a href="<?php echo site_url('admin/menu')?>"><i class="fa fa-circle-o text-aqua"></i>Menu/Category Listing</a></li>
                  <li><a href="<?php echo site_url('admin/menu/order_menu')?>"><i class="fa fa-circle-o text-red"></i>Menu/Category Order</a></li>
                <li><a href="<?php echo site_url('admin/menu/inner_listing')?>"><i class="fa fa-circle-o text-red"></i>Left Menu listing</a></li>
                  <li><a href="<?php echo site_url('admin/menu/inner_order_menu')?>"><i class="fa fa-circle-o text-red"></i>Left Menu Order</a></li>-->
                  <!--<li><a href="<?php echo site_url('admin/menu/footer_menu')?>"><i class="fa fa-circle-o text-red"></i>Footer Menu/Category</a></li> 
                  
                  </ul>
                </li>  -->
               
              <!-- <li  class="treeview <?php if($menu == 'team'){?> active <?php } ?>"> 
                  <a href="#"><i class="fa fa-list"></i><span>Team</span></a>
                  <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('admin/team/create_team')?>"><i class="fa fa-circle-o text-yellow"></i>Create Team</a></li>
                  <li><a href="<?php echo site_url('admin/team')?>"><i class="fa fa-circle-o text-aqua"></i>All Team Members</a></li>
                  </ul>
                </li> -->
              <li  class="treeview <?php if($menu == 'location'){?> active <?php } ?>"> 
                  <a href="#"><i class="fa fa-location-arrow"></i><span>Location</span></a>
                  <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('admin/location/country')?>"><i class="fa fa-circle-o text-yellow"></i>Country</a></li>
                  <li><a href="<?php echo site_url('admin/location/state')?>"><i class="fa fa-circle-o text-aqua"></i>City</a></li>
                  <li><a href="<?php echo site_url('admin/location/district')?>"><i class="fa fa-circle-o text-aqua"></i>Destination</a></li>
                  <li><a href="<?php echo site_url('admin/location/region')?>"><i class="fa fa-circle-o text-aqua"></i>Region</a></li>
                  </ul>
                </li>
              <li  class="treeview <?php if($menu == 'travel'){?> active <?php } ?>"> 
                  <a href="#"><i class="fa fa-plane"></i><span>Travel</span></a>
                  <ul class="treeview-menu">
                  <li><a href="<?php echo site_url('admin/tours/tour_style')?>"><i class="fa fa-circle-o text-yellow"></i>Tour Styles</a></li>
                  <li><a href="<?php echo site_url('admin/tours/responsible_benifits')?>"><i class="fa fa-circle-o text-yellow"></i>Responsible Benifits</a></li>
                  <li><a href="<?php echo site_url('admin/tours/special')?>"><i class="fa fa-circle-o text-yellow"></i>Special</a></li>
                  <li><a href="<?php echo site_url('admin/tours')?>"><i class="fa fa-circle-o text-yellow"></i>Tours</a></li>
                  <li><a href="<?php echo site_url('admin/tours/hotels')?>"><i class="fa fa-circle-o text-aqua"></i>Hotel</a></li>
                  <li><a href="<?php echo site_url('admin/tours/experience')?>"><i class="fa fa-circle-o text-aqua"></i>Experiance</a></li>
                  </ul>
                </li>
           
        <!-- <li  class="treeview <?php if($menu == 'author'){?> active <?php } ?>"> 
              <a href="#"><i class="fa fa-list"></i><span>Authors</span></a>
              <ul class="treeview-menu">
           
           <li><a href="<?php echo site_url('admin/author/create_author')?>"><i class="fa fa-circle-o text-yellow"></i>Create Author</a></li>
           
           <li><a href="<?php echo site_url('admin/author')?>"><i class="fa fa-circle-o text-yellow"></i>All Author</a></li>
           
           
           </ul>
           
           </li> -->
              
              
              
               <!-- <li class="treeview <?php if($menu == 'advertise'){?>active<?php } ?>">
                 <a href="#"><i class="fa fa-folder-o"></i><span>Advertise</span> </a>
                 <ul class="treeview-menu">
               <li><a href="<?php echo site_url('admin/advertise/create_new_ad')?>"><i class="fa fa-circle-o text-yellow"></i>Create Ad</a></li>
               <li><a href="<?php echo site_url('admin/advertise')?>"><i class="fa fa-circle-o text-aqua"></i>Advertise listing</a></li>
              
                 </ul>
               </li> -->
              
           <!--   <li class="treeview <?php if($menu == 'pages'){?>active<?php } ?>">
                 <a href="#"><i class="fa fa-folder-o"></i><span>Footer pages</span> </a>
                 <ul class="treeview-menu">
               <li><a href="<?php echo site_url('admin/pages/create_footer')?>"><i class="fa fa-circle-o text-yellow"></i>Create Pages</a></li>
               <li><a href="<?php echo site_url('admin/pages/all_footer')?>"><i class="fa fa-circle-o text-aqua"></i>Pages listing</a></li>
              
                 </ul>
               </li>-->
               
               
              
           
       </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- =============================================== -->
