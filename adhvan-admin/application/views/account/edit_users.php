 <div class="container">
            <div class="brt-registration">
                <h3><span class="glyphicon glyphicon-user"></span> <?php echo $detail['first_name'].' '.$detail['last_name'];?>’s Profile</h3>
                <div class="rgst-frm">
                    <div style="color: #ff0000; font-weight:bold;">
<?php 
echo $this->session->flashdata('password');
?>
<?php echo validation_errors(); ?>  
</div>
                        <fieldset class="hd1">
                            <legend>Account Details</legend>
                            <p align="right" style="margin:0;font-size:12px;">* non-editable fields</p>
                            <div class="mail">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email Id*</label>
                                        <input class="disabledInput" id="" type="text" value="<?php echo $detail['email']; ?>" >
                                    </div>
                                </div>

                                <div class="col-md-6">

                                    <div class="form-group">
                                <label class="ralbl">Password: <a class="js-open-modal" href="#" data-modal-id="popup4">Click To Change Password</a>
</label>
                                <div id="popup4" class="modal-box">
                                  <header> <a href="#" class="js-modal-close close">×</a>
                                      <h4>Change Password</h4>
                                  </header>
                                  <form action="<?php echo site_url();?>/user_panel/change_password" method="post" class="gdln">
                                  <div class="modal-body">
                                   <p style="color:#2376b6;">Input your Old and New Password</p>
                                      <div class="input-group input-group">
                                      <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-lock"></span>
                                      </span>
                                      <input class="form-control" type="password" name="exit_password" placeholder="Old Password">
                                    </div> 
                                      <div class="input-group input-group">
                                      <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-lock"></span>
                                      </span>
                                      <input class="form-control" type="password" name="password" placeholder="New Password">
                                    </div>
                                      <div class="input-group input-group">
                                      <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-lock"></span>
                                      </span>
                                      <input class="form-control" type="password" name="con_password" placeholder="Confirm New Password">
                                    </div>
                                      <div class="modal-footer">
                                        <button  class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                        <button type="submit"  class="btn btn-primary" id="password_modal_save">Save changes</button>
                                    </div>
                                  </div>
                                  </form>
                                </div>
                            </div>
                                </div>
                            </div>
                        </fieldset>
                        <?php echo validation_errors(); ?>  
                        <div style="color: #f8af40; font-weight:bold;">
     <?php
    
     echo $this->session->flashdata('error');
     ?>
     </div>
     <div style="color: #f8af40; font-weight:bold;">
     <?php
     echo $this->session->flashdata('update');
     
     ?>
     </div>
                        <form action="<?php echo site_url();?>/user_panel/my_profile" method="POST">
                        <fieldset class="hd1">
                             <div class="col-md-6 prsnl">
                            <legend>Personal Details</legend>
                                 <div class="prsnlfrm">
                                 <div class="form-group">
                                    <label class="control-label">Name*</label>
                                    <input type="text" class="" name="reg[first_name]" value="<?php echo $detail['first_name'];?>" >
                                     <input type="text" name="reg[last_name]" class="" value="<?php echo $detail['last_name'];?>" >
                                    </div>
                                 <div class="form-group">
                                    <label class="control-label">Street Address*</label>
                                    <input type="text" name="reg[address]" class="sadrs" value="<?php echo $detail['address']; ?>">
                                    </div>
                                 <div class="form-group">
                                    <label class="control-label">Address Line 2</label>
                                    <input type="text" name="reg[address2]" class="sadrs" value="<?php echo $detail['address2']; ?>">
                                    </div>
                                 <div class="form-group">
                                <label class="control-label">Country*</label>
                                     <input type="text" name="reg[country]" value="<?php echo $detail['country']; ?>" >
                                 </div>
                                  <div class="form-group">
                                <label class="control-label">phone*</label>
                                     <input type="text" name="reg[phone]" value="<?php echo $detail['phone']; ?>" >
                                 </div>
                                 <div class="form-group">
                                <label>Secret Question*</label>
                                 <select id="qselect" name="reg[s_question]">
                                    <option value="">Select Qestion </option>
                                     <option  value="1"  <?php if($detail['s_question']==1){ ?> selected="selected" <?php }?> > Pet's Name </option>
                                     <option value="2" <?php if($detail['s_question'] == 2){ ?> selected="selected" <?php }?> >First School Name </option>
                                     <option value="3" <?php if($detail['s_question']==3){ ?> selected="selected" <?php }?> >Town Of Birth </option>
                                     <option value="4" <?php if($detail['s_question']==4){ ?> selected="selected" <?php }?> >Create your Qestion </option>
                                 </select>
                                    
                            </div> 
                                 <div class="form-group">
                                    <label>Answer*</label>
                                     <input type="text" name="reg[answer]" value="<?php echo $detail['answer']; ?>" >
                                 </div>
                             </div>
                             </div>
                             <!-------------------other information---------------->
                             <div class="col-md-6 prsnl">
                            <legend>Other Information</legend>
                                 <div class="form-group">
                                 <label>Alternate Email</label>
                                 <input type="text" name="reg[alternate_email]" value="<?php echo $detail['alternate_email']; ?>"  >
                                     </div>
                                 <div class="oibox">
                                    <p align="justify">
                                        <strong>Disclaimer:</strong></br>
                                 Bartaman Magazines Pvt. Ltd. Do not seek any information from its web users that are private and of any consequential value physical, emotional, legal or proprietary. The information this website need is only for providing a secure user interface with the website.</p>
                                 </div>
                            </div> 
                        </fieldset>
                        
                <!-- register-btn -->
                <div class="rgst-btn" align="center"><input type="submit" class="rsg-btn" value="Submit"> </div>  
                    </form>
                </div>
        <div class="profile-category">
            <div class="col-md-3"><button  type="button" class="btn btn-primary">Reader's Table</button></div>
            <div class="col-md-3"><a href="<?php echo site_url('user_panel/subscription')?>" class="btn btn-success">Subscription</a></div>
            <div class="col-md-3"><button type="button" class="btn btn-info">My Activity</button></div>
            <div class="col-md-3"><a href="<?php echo site_url('user_panel/')?>" class="btn btn-warning ">My Profile</a></div>
    </div>
    </div>
  
    </div>

 
    <script src="dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
//your question textbox//
    
    $("#qselect").change(function(){
            if($(this).val()=='0'){
            $("#urqstn").show();
            }
            else{
            $("#urqstn").hide();
            }
        }); 
    
</script>
<script>
    //----------popup js------------->
$(function(){

var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

    $('a[data-modal-id]').click(function(e) {
        e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
        var modalBox = $(this).attr('data-modal-id');
        $('#'+modalBox).fadeIn($(this).data());
    });  
  
  
$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });
 
});
 
$(window).resize(function() {
    $(".modal-box").css({
        top: ($(0).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(20).width() - $(".modal-box").outerWidth()) / 2
       
    });
});
 
$(window).resize();
 
});
</script>

