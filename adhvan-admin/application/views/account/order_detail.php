
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Your Orders</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
		<a class="orderbtn btn" href="<?php echo site_url('order/view_order')?>"><?php echo "<< Back to Your Order List" ?></a><br/>
        <h2 class="page-heading no-line">
            <span class="page-heading-title2">Your Order Detail</span>
        </h2>
        <!-- ../page heading-->
        <div class="page-content page-order">
            <div class="table-responsive order-detail-content">
                <table class="table table-bordered cart_summary">
                    
					<thead>
                        <tr>
                            <th class="cart_product">Product</th>
                            <th>Description</th>
                            <th>Unit price</th>
                            <th>Quantity</th>
                            <th>Discount</th>
                            <th>Total</th>
                            <!-- <th  class="action"><i class="fa fa-trash-o"></i></th> -->
                        </tr>
                    </thead>
					
                    <tbody>
					
					<?php foreach($product as $pd){ ?>
					
                        <tr>
						
						
                            <td class="cart_product">
							
                                <a href="<?php echo site_url('product/product_details')."/".$pd['detail']['id']."/".$pd['detail']['category_id']; ?>"><img src="<?php echo base_url(); ?>/assets/ajaxuploadresize/uploads/<?php echo "50_".$pd['detail']['first_img']; ?>" alt="Product"></a>
                            </td>
							
                            <td class="cart_description">
                                <p class="product-name"><a href="<?php echo site_url('product/product_details')."/".$pd['detail']['id']."/".$pd['detail']['category_id']; ?>"><?php echo $pd['product_name']?> </a></p>
                                <!-- <small class="cart_ref">SKU : #123654999</small><br> -->
                                <small><a href="#">Color : Beige</a></small><br>   
                                <small><a href="#">Size : S</a></small><br>
                                <small><a href="#">Qty : 2</a></small>
                            </td>
                            <td class="price"><span class="pull-left">&#8358; <?php echo $pd['unit_price']?></span></td>
                            <td class="price"><span class="pull-left"><?php echo $pd['num']?></span></td>
                            
                              <td class="price"><span class="pull-left">&#8358; <?php echo $pd['disc']?></span></td> 
                           
                            <td class="price">
                                <span class="pull-left">&#8358; <?php echo $pd['unit_price']*$pd['num']-$pd['disc'];?>.00</span>
                            </td>
                            <!-- <td class="action">
                                <a href="#">Delete item</a>
                            </td> -->
                        </tr>

<?php }?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2"><P class="orderbtn btn pull-left">Product ID: #<?php echo $order['order_id']?></P></td>
                            <td colspan="2"><p class="pull-left">Date: <?php echo $order['entry_date']?></p></td>
                            <td colspan="2"><p class="pull-left"><strong>Total: <?php echo $order['total_price']?></strong></p></td>
                        </tr>
                        <tr	>
						
                            
                            <td colspan="6">
							
<p class="pull-left"><?php 
 echo "<strong>Payment methode:</strong>"."".$order['payment_mode'];
 ?></p><br/>
   <hr>		
							<p class="pull-left">
							<strong>Delivery Address:</strong></p><br/>
							<p class="pull-left"><?php 
 echo $ship['full_name'];?></p><br/>
 <p class="pull-left"><?php 
 echo $ship['address_one'].",".$ship['address_two'].",".$ship['city'].",".$ship['state'].",".$ship['country']; ?></p><br/>
 <p class="pull-left"><?php 
 echo "<strong>Pin:</strong>"."".$ship['postal_code'];
 ?></p><br/>
 <p class="pull-left"><?php 
 echo "<strong>Phone:</strong>"."".$ship['phone'];
 ?></p><br/>

 
  					</td>
                            
                            
                        </tr>
                    </tfoot>    
                </table>
            </div>

            
          </div>
        </div>
    </div>