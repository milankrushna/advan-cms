  <!------------form-section-------------------->
        <div class="container">
            <div class="brt-registration">
                <h3><span class="glyphicon glyphicon-user"></span> Subham Mitra’s Payment View</h3>
                <div class="rgst-frm">
                    <form action="#" method="post" class="gdln">
                        <fieldset class="hd1">
                            <legend>Manage Your Subscription Account</legend>
                            <div class="col-md-12" align="center">
                                <!----View payment----><div align="center" class="pymnt-brt"><span class="glyphicon glyphicon-credit-card"></span> <a class="js-open-modal" href="#" data-modal-id="popup5">View Payment History</a></div>
                                <div id="popup5" class="modal-box">
                                  <header> <a href="#" class="js-modal-close close">×</a>
                                      <h4>Bartaman Magazine library :: Payment History</h4>
                                  </header>
                                    <div class="modal-body"></br>
    <div class="col-xs-12">
      <div class="table-responsive">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Sl. no.</th>
              <th>Subscription Plan</th>
              <th>Payment Date</th>
              <th>Subscription Amount</th>
              <th>Previous A/c Balance</th>
              <th>Actual A/c Balance</th>
                <th>Invoice No</th>
                <th>Status</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>01</td>
              <td>Three Months Plan</td>
              <td>	25th Feb,  2016</td>
              <td><del>&#2352;</del> 150.00</td>
              <td><del>&#2352;</del> 0.00</td>
                <td> <del>&#2352;</del> 150.00</td>
                <td> <a href="#">20160225-0671</a></td>
                <td>Order No: 1456385061-27 paid via Credit Card</td>
            </tr>
          </tbody>
          
        </table>
      </div><!--end of .table-responsive-->
    </div>
                                     
                                  </div>
                                </div>
              <!----payment details top----><div class="pymnt-dtl">
                                Latest Subscription plan: <span> Three Months Plan</span> | Subscriptioon Valid till: <span>25th May, 2016</span> | Available Balance: <span><del>&#2352;</del>  147.00</span>
                                </div>
                            </div>
                           <!------------payment plan table-->
                            <div class="col-md-12 price">

                            <?php foreach($subscription as $sc){ ?>
                                <div class="col-md-3 price">
						<div class="panel panel-info price">
								<div class="panel-heading">
										<h3 class="text-center"><?php echo $sc['name'] ?></h3>
										<p class="text-center"><?php echo $sc['validity'] ?> Days Plan</p>
								</div>
								<div class="panel-body text-center">
										<p class="lead" style="font-size:30px"><strong><del>र</del> <?php echo $sc['price'] ?>.00</strong></p>
								</div>
								<ul class="list-group list-group-flush text-center">
										<li class="list-group-item">
												<span class="glyphicon glyphicon-calendar"></span> Validity <?php echo $sc['validity'] ?> Days
										</li>
										
										<li class="list-group-item">
                                            <span class="glyphicon glyphicon-heart"></span> Total Reading Hours (Converted)</br><span style="font-size:12px;">(whichever exhausted earlier)</span>
										</li>
								</ul>
								<div class="panel-footer"> <a class="btn btn-lg btn-block btn-primary" href="<?php echo site_url('user_panel/active_plan').'/'.$sc['id'] ?>">SUBSCRIBE</a> </div>
						</div>
				</div>
				<?php } ?>
				
                            </div>
                        </fieldset>
                        
                        
                 
                    </form>
                </div>
        <div class="profile-category">
            <div class="col-md-3"><button  type="button" class="btn btn-primary">Reader's Table</button></div>
            <div class="col-md-3"><a href="<?php echo site_url('user_panel/subscription')?>" class="btn btn-success">Subscription</a></div>
            <div class="col-md-3"><button type="button" class="btn btn-info">My Activity</button></div>
            <div class="col-md-3"><a href="<?php echo site_url('user_panel/')?>" class="btn btn-warning ">My Profile</a></div>
    </div>
    </div>
   
    </div>

 
    <script src="dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript">
//your question textbox//
    
    $("#qselect").change(function(){
			if($(this).val()=='0'){
			$("#urqstn").show();
			}
			else{
			$("#urqstn").hide();
			}
		});	
    
</script>
<script>
    //----------popup js------------->
$(function(){

var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});  
  
  
$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });
 
});
 
$(window).resize(function() {
    $(".modal-box").css({
        top: ($(0).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(20).width() - $(".modal-box").outerWidth()) / 2
       
    });
});
 
$(window).resize();
 
});
</script>