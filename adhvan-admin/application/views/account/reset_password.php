

<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Reset password</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading no-line">
            <span class="page-heading-title2">Create New Password</span>
        </h2>
        <!-- ../page heading-->
        <div class="page-content page-order">
        <?php echo validation_errors(); ?>	
            <div class="heading-counter warning">
                <span>Enter Your new password to access your  Account</span>
            </div>
            <div class="order-detail-content">
			<form action="<?php echo site_url('user_panel/reset_password/'.$rst_code)?>" method="post">
                <ul>
                         
                            <li class="row padding-bottom">
                                <div class="col-sm-3">
                                    <label for="password" class="required">Password</label>
                                </div><!--/ [col] -->
                                <div class="col-sm-5">
                                    <input class="input form-control" type="password" name="password" id="password">
                                </div><!--/ [col] -->
                            </li><!--/ .row -->
                            <li class="row padding-bottom">
                                <div class="col-sm-3">
                                    <label for="password" class="required">Confirm Password</label>
                                </div><!--/ [col] -->
                                <div class="col-sm-5">
                                    <input class="input form-control" type="password" name="con_password" >
                                </div>
								<input type="hidden" value="<?php echo $id ;?>" name="id"><!--/ [col] -->
                            </li><!--/ .row -->
                            <li>
                                <div class="col-sm-3">
                                </div><!--/ [col] -->
                                <div class="col-sm-5">
                                    <button type="submit" class="button ">Continue</button>&nbsp;&nbsp;
                                   
                                </div><!--/ [col] -->
                            </li>
                        </ul>
               </form>
            </div>
        </div>
    </div>
</div>