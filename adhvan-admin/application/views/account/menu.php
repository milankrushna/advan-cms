 <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <p class="title_block">Settings</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                    <li class="active"><span></span><a href="#">Personal Information</a></li>
                                    <li><span></span><a href="#">Change Password</a></li>
                                    <li><span></span><a href="#">Addresses</a></li>
                                    <li><span></span><a href="#">Profile Settings</a></li>
                                    <li><span></span><a href="#">Deactivate Account</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
            </div>