<link rel="stylesheet" href="http://localhost/ay/assets/plugins/datepicker/datepicker3.css">
 <script src="http://localhost/ay/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
 <script>
 $(function () {
 $('#datepicker').datepicker({
      autoclose: true
    });
});
 </script>



<link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/validate/css/screen.css" /> 
<script src="<?php echo base_url()?>assets/plugins/validate/dist/jquery.validate.js"></script>
<script>

$().ready(function() {
    $("#signupForm").validate({

        rules: {
                'reg[password]': {
                    required: true,
                    minlength: 5
                },
                con_password: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
                'reg[email]': {
                    required:true,
                    email: true
                },
                firstname: "required",
                lastname: "required",
                'reg[address]':"required",
                'dob':"required",
                'reg[phone]':"required"


        },

        messages: {

            'reg[password]': {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                con_password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long",
                    equalTo: "Please enter the same password as above"
                },
                'reg[email]':{
                    required: "Please provide your Email",
                    email: "Please enter a valid email",    
                },
                'reg[first_name]': "First name is required",
                'reg[last_name]': "Last name is required",
                'reg[address]': "Street Address is required",
                'reg[phone]': "phone is required",
                'dob':"Date of birth is Required"  

        }

    });

});
</script>
    <!----------------------Join Us---->
    <div class="container">
      <div class="col-md-12 ctgry">
        <div class="col-xs-12 joinusfrm">
          <h2>Join Us</h2>
          <h3>From the Mentors :</h3>
          <p><b>“ We request you to fill these forms with honesty & integrity.
None of your personal information’s will be shared publicly except to genuine light workers. So open yourself  up and express freely, cause only then can we help you better.”</b>
</p>
          <p><b>“ Don’t write to impress but to express .” </b></p>
          <?php echo validation_errors()?>
          <form action="<?php echo site_url();?>/user_panel/create_user?user_type=<?php echo $user_type; ?>" method="post" enctype="multipart/form-data" id="signupForm"  >
            <div class="form-group">
              <label ><img id="output" style="width:180px;height:180px;cursor: pointer;" src="<?php echo base_url() ?>assets/dist/img/profile.jpg"><input type="file" onchange="loadFile(event)" style="display: none;"  name="profile_pic"  /></label>
                <div  align="center">
                        
                </div>
            </div>
            <div class="col-md-12 ju">
            <div class="form-group joinus">
              <label>Email :</label>
              <input type="email" class="nameju" name="reg[email]" value="<?php echo $email; ?>" >
            </div>
            </div>
            <div class="col-md-6 ju">
            <div class="form-group joinus">
              <label>password:</label>
              <input type="password" class="nameju" name="reg[password]" id="password" >
            </div>
            </div>
            <div class="col-md-6 ju">
            <div class="form-group joinus">
              <label>Conform password:</label>
              <input type="password" class="nameju" name="con_password" >
            </div>
            </div>
            
            <div class="col-md-6 ju">
            <div class="form-group joinus">
              <label>Exciting First Name :</label>
              <input type="text" class="nameju" name="reg[first_name]" value="<?php echo $first_name; ?>" >
            </div>
            </div>
            <div class="col-md-6 ju">
            <div class="form-group joinus">
              <label>Exciting Last Name :</label>
              <input type="text" class="nameju" name="reg[last_name]" value="<?php echo $last_name; ?>" >
            </div>
            </div>
            <div class="col-md-6 ju">
            <div class="form-group joinus">
              <label>Your landing City & country:</label>
              <input type="text" class="nameju" name="reg[address]" value="<?php echo $address; ?>" >
            </div>
            </div>
            <div class="col-md-6 ju">
            <div class="form-group joinus">
              <label>Date Of Landing On Earth (DOB) :</label>
              <input type="text" id="datepicker"  class="nameju" name="dob" value="<?php echo $dob; ?>"  readonly >
            </div>
            </div>
            <h4>Besides telepathy how else can we contact you </h4>
            
            
            <div class="col-md-6 ju">
            <div class="form-group joinus">
              <label>Education/Occupation :</label>
              <select class="nameju" name="reg[occupation]" required="">
                <option value="" class="nameju">Select your Occupation</option>
                <option class="nameju">I am a Student</option>
                <option class="nameju">I am a Employee</option>
                <option class="nameju">I am a Bussiness man</option>
                <option class="nameju">I am a Sports Man</option>
                <option class="nameju">I am a Farmer</option>
              </select>
            </div>
            </div>
            <div class="col-md-6 ju">
            <div class="form-group joinus">
              <label>Studied/Worked at :</label>
              <input type="text" class="nameju" name="reg[work_at]" placeholder="" value="<?php echo $work_at; ?>" required="">
            </div>
            </div>
            <div class="col-md-6 ju">
            <div class="form-group joinus">
              <label>Contact No. :</label>
              <input type="number" class="nameju" name="reg[phone]"  value="<?php echo $phone; ?>" >
            </div>
            </div>
            <div class="col-md-12 ju">
              <h4>Your page , blog or website if any:</h4>
              <div class="col-md-3 ju">
                <div class="form-group joinus">
              <label>Google ID :</label>
              <input type="text" class="nameju" name="reg[email_id2]" placeholder="" value="<?php echo $email_id2; ?>" >
            </div>
              </div>
              <div class="col-md-3 ju">
                <div class="form-group joinus">
              <label>Facebook ID :</label>
              <input type="text" class="nameju" name="reg[fb_id]" placeholder="" value="<?php echo $fb_id; ?>" >
            </div>
              </div>
              <div class="col-md-3 ju">
                <div class="form-group joinus">
              <label>Skype ID :</label>
              <input type="text" class="nameju" name="reg[skp_id]" placeholder="" value="<?php echo $skp_id; ?>" >
            </div>
              </div>
              <div class="col-md-3 ju">
                <div class="form-group joinus">
              <label>Twitter ID :</label>
              <input type="text" class="nameju" name="reg[twit_id]"  placeholder=""  value="<?php echo $twit_id; ?>">
            </div>
              </div>
            </div>

                        


            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>What do you feel is your purpose as a light worker ?</label>
                <textarea class="nameju" name="reg[d1]" rows="4"><?php echo $d1; ?></textarea>
              </div>
            </div>
            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>Please share your journey in brief towards realizing your life’s purpose</label>
                <textarea class="nameju" name="reg[d2]" rows="4"><?php echo $d2; ?></textarea>
              </div>
            </div>
            <div class="col-md-6 ju">
              <div class="form-group joinus">
                <label>My Unique Expression :</label>
               <select class="nameju" name="" >
                 <option>Singing</option>
                 <option>Spiritual Workshops</option>
                 <option>Public Speaking</option>
                 <option>Dance</option>
                 <option>Meditational Trainer</option>
                 <option>Painting, Designing, DJ</option>
                 <option>Guitarist</option>
                 <option>Writing</option>
                 <option>Drumming</option>
                 <option>Others</option>
               </select>
              </div>
            </div>
            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>What form of meditation do you practice and how has it changed your life.</label>
                <textarea class="nameju" name="reg[d3]" rows="4"><?php echo $d3; ?></textarea>
              </div>
            </div>
            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>Please Mention some of your most life changing Books/Articles </label>
                <textarea class="nameju" name="reg[d4]" rows="4"><?php echo $d4; ?></textarea>
              </div>
            </div>
            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>Please mention some of your spiritual teachers or role models who have inspired your life </label>
                <textarea class="nameju" name="reg[d5]" rows="4"><?php echo $d5; ?></textarea>
              </div>
            </div>
            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>What is Love for you.</label>
                <textarea class="nameju" name="reg[d6]" rows="4"><?php echo $d7; ?></textarea>
              </div>
            </div>
            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>Please share your thoughts on awareness.</label>
                <textarea class="nameju" name="reg[d7]" rows="4"><?php echo $d7; ?></textarea>
              </div>
            </div>
            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>What do you feel about consuming animals as food.</label>
                <textarea class="nameju" name="reg[d8]" rows="4"><?php echo $d8; ?></textarea>
              </div>
            </div>
            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>When presented with a situation in which the odds are against you, what goes through your mind. </label>
                <textarea class="nameju" name="reg[d9]" rows="4"><?php echo $d9; ?></textarea>
              </div>
            </div>
            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>Have you ever experienced any communication or contact with any extraterrestrial civiliaztion/Aliens? If yes please share. </label>
                <textarea class="nameju" name="reg[d9]" rows="4"><?php echo $d9; ?></textarea>
              </div>
            </div>
            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>What is the significance of collaboration in evolution. </label>
                <textarea class="nameju" name="reg[d9]" rows="4"><?php echo $d9; ?></textarea>
              </div>
            </div>
            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>What do you feel you need to take your work next level </label>
                <textarea class="nameju" name="reg[d9]" rows="4"><?php echo $d9; ?></textarea>
              </div>
            </div>

            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>What is that you have in abundance and are willing to share with other Light Workers</label>
                <textarea class="nameju" name="reg[d9]" rows="4"><?php echo $d9; ?></textarea>
              </div>
            </div>
             
            <div class="col-md-12 ju">
            <button type="submit" class="new1">Submit</button>
            </div>
          </form>
        </div>
      </div>
          </div>
    <!--Yoga single image section-->
    <div class="container">
      <div class="col-md-12 ays-yoga" align="center">
        <img src="<?php echo base_url();?>upload/images/yoga.png" class="img-responsive">
      </div>
    </div>
    <!--scroll top-->
    <div class="scroll-top-wrapper ">
  <span class="scroll-top-inner">
    <img src="images/Scroll-to-top-button.png" class="img-responsive">
  </span>
</div>



<script>
  var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('output');
      output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);

  };
</script>