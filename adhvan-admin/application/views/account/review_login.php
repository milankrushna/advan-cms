

	<script src="<?php echo base_url();?>assets/datepicker/jquery/dist/jquery.validate.js"></script>
	<script>
	$(document).ready(function() {
		
		$("#commentForm").validate({
			
			messages: {
				
				login_password: {
					required: 'Enter your password'
				},
				login_email: {
					required: 'Enter your email address'
				}
				
			}
		});
	});
	</script>
	<script>
	function check(dis){
		$dt=dis;
	$.post('<?php echo site_url('user/check_email');?>',{key:dis},function(msg){
	$('#data').html(msg);
	
	})
	}
	
	</script>
	
	<script>
	
$().ready(function() {
	$("#signupForm").validate({
		rules: {
				
				password: {
					required: true,
					minlength: 5
				},
				confirm_password: {
					required: true,
					minlength: 5,
					equalTo: "#password"
				},
				email: {
					required: true,
					email: true
				},
				
				agree: "required"
			},
			messages: {
				
				password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long"
				},
				confirm_password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long",
					equalTo: "Please enter the same password as above"
				},
				email: "Please enter a valid email address",
				agree: "Please accept our policy"
			}
		});

		

		
		// show when newsletter is checked
		newsletter.click(function() {
			topics[this.checked ? "removeClass" : "addClass"]("gray");
			topicInputs.attr("disabled", !this.checked);
		});
	});
</script>


<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Authentication</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">Authentication</span>
        </h2>
        <!-- ../page heading-->
        <div class="page-content">
            <div class="row">
                <div class="col-sm-6">
                    <div class="box-authentication">
                        <h3>Create an account</h3>
						<div id="data" style="color: #f80750"> 
	<?php 
echo $this->session->flashdata('message');

?> 
</div>
                        <p>Please enter your email address to create an account.</p>
						<form action="<?php echo site_url('user/email_sub');?>" id="signupForm" method="POST">
                        <label for="emmail_register">Email address</label>
                        <input type="email" name="email"  id="email" onchange="check(this.value);" class="form-control"><br/>
						<label for="emmail_register">Password</label>
                        <input type="password" name="password"  id="password" class="form-control"><br/>
						<label for="emmail_register">Confrim Password</label>
                        <input name="confirm_password" type="password" class="form-control"><br/>
                        <input type="submit" class="button" value="Create an account">
						</form>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="box-authentication">
                        <h3>Already registered?</h3>
						<div style="color: #ff0000; font-weight:bold;"> <?php 
echo $this->session->flashdata('login');

?> </div> 
						<form action="<?php echo site_url('user/review_login_proceed'); ?>" method="POST" id="commentForm">
                        <label for="emmail_login">Email address</label>
                        <input type="email" name="email"   required class="form-control"><br/>
                        <label for="password_login">Password</label>
                        <input type="password" name="password"    required  class="form-control">
                        <p class="forgot-pass"><a href="<?php echo site_url('user/forgot_password'); ?>">Forgot your password?</a></p>
                        <input  type="submit" class="button"  value="Sign In" >
						</form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ./page wapper-->