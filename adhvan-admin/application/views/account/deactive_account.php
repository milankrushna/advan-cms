
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">My Account</span>
			 <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Deactive Account</span>
        </div>

                    <p>Settings</p>
                   
                                <ul class="tree-menu">
                                    <li><a href="<?php echo site_url('user_panel2/my_account')?>">Personal Information</a></li>
                                    <li><a href="<?php echo site_url('user_panel2/change_password')?>">Change Password</a></li>
                                    <li><a href="<?php echo site_url('user_panel2/address')?>">Addresses</a></li>
									<li><a href="<?php echo site_url('user_panel2/deactive_account')?>">Deactivate Account</a></li>
                                </ul>
                      <div>    
                    <h3>Deactivate Account</h3>
<div style="color: #ff0000; font-size:15;"> <?php 
echo $this->session->flashdata('msg');
echo $this->session->flashdata('dev');?> 
<?php echo validation_errors(); ?>	
</div> 
					<form action="<?php echo site_url('user_panel/deactive')?>" method="POST">
                        <ul>
<div style="color: #ff0000; font-weight:bold;"><?php echo $this->session->flashdata('error');?></div>
                          <p>If you want to <strong>Deactive</strong> your customer account, you may do so below. Be sure to click the <strong>Confirm Deactivation</strong> button when you are done. </p><br/>
						  
                            <li class="row padding-bottom">
                                <div>
								<input type="password" name="password" >
 								<input type="hidden" name="deactivate" value="1">				
 								<input type="hidden" name="deactivate" value="1">								 
                                </div>
                            </li>
                            <li>
                                <div>
                                    <button type="submit" class="button ">Confirm Deactivation</button>
                                </div>
                            </li>
                        </ul>
						</form>
						</div>
					
                

                    <h3 >When you deactivate your account</h3>
                    <div>
                        <p>You are logged out of your Account .</p>
                        <p>Your public profile is no longer visible .</p>
                        <p>Your reviews/ratings are still visible, while your profile information is shown as ‘unavailable’ as a result of deactivation.</p>
                       
                        <p>You will be unsubscribed from receiving promotional emails.</p>
                        <p>Your account data is retained and is restored in case you choose to re <strong>Login</strong> your account.</p>
                    </div>
					
						<div>    
                    <h3>Delete Account</h3>

					<form action="<?php echo site_url('user_panel/delete')?>" method="POST">
                        <ul>
<div style="color: #ff0000; font-weight:bold;"><?php echo $this->session->flashdata('error');?></div>
                          <p>If you want to <strong>Delete</strong> your customer account, you may do so below. Be sure to click the <strong>Confirm Delete</strong> button when you are done. </p><br/>
                            <li class="row padding-bottom">
                                <div>
 								 <input type="password" name="password" required>
								</div>
                            </li>
                            <li>
                                <div>
                                    <button type="submit" class="button ">Confirm Delete</button>
                                </div>
                            </li>
                        </ul>
						</form>
						</div>
					
					  <h3 >When you Delete your account</h3>
                    <div>
                        <p>You are permanently Deleted from our database</p>
                        <p>Your account data can't' be retained or restored in Any circumtanses .</p>
                    </div>
