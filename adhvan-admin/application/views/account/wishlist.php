<script>
function add_cart(product_id,x){
	
	var n = "#num"+x;
	var num = $(n).val();
	if(num !='' && num>'0'){
	 var url ="<?php echo site_url('product/add_cart')?>"+"/"+product_id+"/"+num;
	 location.href=url
	 }else{
	 	alert("Please choose a numeric Value");
		$(n).focus();
	 }
}



function buy_me(product_id,x){	
	var n = "#num"+x;
	var num = $(n).val();
if(num !='' && num>'0'){
	var url ="<?php echo site_url('product/proceed')?>"+"/"+product_id+"/"+num;
	location.href=url
}else{
	 	alert("Please choose a numeric Value"); 
		$(n).focus();
}

}

</script>

<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <a href="#" title="Return to Home">My account</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">My wishlist</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block best sellers -->
                <div class="block left-module">
                    <p class="title_block">Settings</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                    <li><span></span><a href="<?php echo site_url('user/my_account')?>">Personal Information</a></li>
                                    <li><span></span><a href="<?php echo site_url('user/change_password')?>">Change Password</a></li>
                                    <li><span></span><a href="<?php echo site_url('user/address')?>">Addresses</a></li>
                                    <li class="active"><span></span><a href="<?php echo site_url('user/wishlist')?>">Wishlist</a></li>
                                    <li ><span></span><a href="<?php echo site_url();?>/order/view_order">Order</a></li>
									<li><span></span><a href="<?php echo site_url('user/deactive_account')?>">Deactivate Account</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
<div class="block left-module">
                    <p class="title_block">New products</p>
                    <div class="block_content">
                        <ul class="products-block best-sell">
						<?php foreach($latest_product as $lp){?>
						
                                <li>
                                    <div class="products-block-left">
                                        <a href="<?php echo site_url('product/product_details')."/".$lp['id']."/".$lp['category_id']; ?>">
                                            <img src="<?php echo base_url(); ?>/assets/ajaxuploadresize/uploads/<?php echo "50_".$lp['first_img']; ?>" alt="SPECIAL PRODUCTS">
                                        </a>
                                    </div>
                                    <div class="products-block-right">
                                        <p class="product-name">
                                            <a href="<?php echo site_url('product/product_details')."/".$lp['id']."/".$lp['category_id']; ?>"><?php echo substr($lp['name'],0,15)?></a>
                                        </p>
                                        <p class="product-price">&#8358;<?php echo $lp['unit_price']?></p>
                                        <p class="product-star">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-half-o"></i>
                                        </p>
                                    </div>
                                </li>
                                <?php }?>
                            </ul>
                    </div>
                </div>
                <!-- ./block best sellers  -->
                
                <!-- left silide -->
               
                <!--./left silde-->
                <!-- block best sellers -->
               
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
               <!-- page heading-->
                <h2 class="page-heading">
                    <span class="page-heading-title2">My wishlist</span>
                </h2>
                <!-- ../page heading-->
               
                <ul class="row list-wishlist" style="margin-top: -50px;">
                  
<?php if(count($product)==0){ echo "You have no Wishlist product";?>
<?php }else{?>
<?php 

$counter = 0;
foreach($product as $pdc){
	$counter = $counter+1 ;
	?>				
                    <li class="col-sm-3"style="margin-top: 90px;">
					
                        <div class="product-img">
					
                            <a href="<?php echo site_url('product/product_details')."/".$pdc['id']."/".$pdc['category_id']; ?>"><img src="<?php echo base_url(); ?>/assets/ajaxuploadresize/uploads/<?php echo "200_".$pdc['first_img']; ?>" alt="Product" style="height: 211px;"></a>
						
                        </div>
                        <h5 class="product-name">
                            <a href="<?php echo site_url('product/product_details')."/".$pdc['id']."/".$pdc['category_id']; ?>"><?php echo substr($pdc['name'],0,15);?>...</a>
                        </h5>
						<input type="hidden" id="product_id" value="<?php echo $pdc['id']; ?>">
                         <div class="priority">
                            <label>price</label>:&#8358; <?php echo $pdc['unit_price']; ?>

                        </div>
						<div class="qty">
                            <label><strong>Quantity :</strong></label>
                            <input type="text" min="1" value="" id="num<?php echo $counter; ?>" class="form-control input input-sm" >
                        </div>
                       
                        <div class="button-action">
        <a class="button-sm" style="cursor: pointer;  left: 11px; margin-top: auto; margin-right: 83px; border: 1px solid;" onclick="add_cart(<?php echo $pdc['id']; ?>,<?php echo $counter; ?>)" >Add to cart</a>
                           
						
				<a class="button-sm" style="cursor: pointer; margin-top: auto; margin-right: 3px;border: 1px solid;" onclick="buy_me(<?php echo $pdc['id']; ?>,<?php echo $counter; ?>);">Buy Now</a>
				
                           
							<a href="<?php echo site_url('user/remove_wish')."/".$pdc['id']; ?>" onclick="return confirm('Are you sure  to Remove this Product From The Wishlist!')"><i class="fa fa-close" style="margin-right: -12px;"></i></a>
                     </div>    
                     
                    </li>
					
<?php }?>
<?php }?>

                    
                </ul>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div><br/><br/><br/><br/>