<link rel="stylesheet" href="http://localhost/ay/assets/plugins/datepicker/datepicker3.css">
 <script src="http://localhost/ay/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
 <script>
 $(function () {
 $('#datepicker').datepicker({
      autoclose: true
    });
});
 </script>
<style>
  
.messagepop {
  background-color:#FFFFFF;
  border:1px solid #999999;
  cursor:default;
  display:none;
  margin-top:0;
  position:absolute;
  text-align:left;
  width:100%;
  z-index:50;
  padding: 0;
}

.modal-box {
  display: none;
  position: absolute;
  z-index: 1000;
  width: 100%;
    bottom: 0;
  background: white;
  border-bottom: 1px solid #aaa;
  border-radius: 4px;
  box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
  border: 1px solid rgba(0, 0, 0, 0.1);
  background-clip: padding-box;
}

.modal-box {
    width: 85%;

}

.modal-box header, .modal-box .modal-header {
    padding: 6px 7px;
    border-bottom: 1px solid #ddd;
    background-color: #0e3580;
    color: #fff;
}

.modal-box header h3,
.modal-box header h4,
.modal-box .modal-header h3,
.modal-box .modal-header h4 { margin: 0; }

.modal-box .modal-body {
    padding: 3px 12px;
    overflow-y: scroll;
    height: 241px;
}

.modal-box footer, .modal-box .modal-footer {
    padding: 1em;
    border-top: 0;
    background: #fff;
    text-align: right;
}

.modal-overlay {
  opacity: 0;
  filter: alpha(opacity=0);
  position: absolute;
  top: 0;
  left: 0;
  z-index: 900;
  width: 100%;
  height: auto;
  background: rgba(0, 0, 0, 0.3) !important;
}

a.close {
  line-height: 1;
  font-size: 1.5em;
  position: absolute;
  top: 5%;
  right: 2%;
  text-decoration: none;
  color: #bbb;
}

a.close:hover {
  color: #222;
  -webkit-transition: color 1s ease;
  -moz-transition: color 1s ease;
  transition: color 1s ease;
}
a.js-modal-close.close {
    background-color: #fff;
    color: #0e3580;
    padding: 0px 4px;
    border-radius: 5px;
    margin-top: -7px;
    opacity: 1.0;
}
input.rsg-btn {
    color: #fff;
    padding: 7px 15px;
    border-radius: 16px;
    margin-bottom: 10px;
    border: 0;
    background: #1e5799;
    background: -moz-linear-gradient(top, #1e5799 0%, #2989d8 23%, #207cca 58%, #7db9e8 100%);
    background: -webkit-linear-gradient(top, #1e5799 0%,#2989d8 23%,#207cca 58%,#7db9e8 100%);
    background: linear-gradient(to bottom, #1e5799 0%,#2989d8 23%,#207cca 58%,#7db9e8 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1e5799', endColorstr='#7db9e8',GradientType=0 );
}
p.mdl-brt {
    padding: 0;
    margin: 0;
    font-size: 17px !important;
    color: black;
}
p.mdlbr {
    margin: 0px;
    color: teal;
    font-size: 13px !important;
}
.modal-body p {
    font-size: 12px;
}
</style>


<link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/validate/css/screen.css" /> 
<script src="<?php echo base_url()?>assets/plugins/validate/dist/jquery.validate.js"></script>
<script>

$().ready(function() {
    $("#signupForm").validate({

        rules: {
                'reg[password]': {
                    required: true,
                    minlength: 5
                },
                con_password: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
                'reg[email]': {
                    required:true,
                    email: true
                },
                firstname: "required",
                lastname: "required",
                'reg[address]':"required",
                'dob':"required",
                'reg[phone]':"required"


        },

        messages: {

            'reg[password]': {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                con_password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long",
                    equalTo: "Please enter the same password as above"
                },
                'reg[email]':{
                    required: "Please provide your Email",
                    email: "Please enter a valid email",    
                },
                'reg[first_name]': "First name is required",
                'reg[last_name]': "Last name is required",
                'reg[address]': "Street Address is required",
                'reg[phone]': "phone is required",
                'dob':"Date of birth is Required"  

        }

    });

});
</script>
    <!----------------------Join Us---->
    <div class="container">
      <div class="col-md-12 ctgry">
        <div class="col-xs-12 joinusfrm">
          <h2>Join Us</h2>
          <h3>From the Mentors :</h3>
          <p><b>“ We request you to fill these forms with honesty & integrity.
None of your personal information’s will be shared publicly except to genuine light workers. So open yourself  up and express freely, cause only then can we help you better.”</b>
</p>
          <p><b>“ Don’t write to impress but to express .” </b></p>
          <?php echo validation_errors()?>
          <form action="<?php echo site_url();?>/user_panel/create_user?user_type=<?php echo $user_type; ?>" method="post" enctype="multipart/form-data" id="signupForm"  >
            <div class="form-group">
              <label ><img id="output" style="width:180px;height:180px;cursor: pointer;" src="<?php echo base_url() ?>assets/dist/img/profile.jpg"><input type="file" onchange="loadFile(event)" style="display: none;"  name="profile_pic"  /></label>
                <div  align="center">
                        
                </div>
            </div>
            <div class="col-md-12 ju">
            <div class="form-group joinus">
              <label>Email :</label>
              <input type="email" class="nameju" name="reg[email]" value="<?php echo $email; ?>" >
            </div>
            </div>
            <div class="col-md-6 ju">
            <div class="form-group joinus">
              <label>password:</label>
              <input type="password" class="nameju" name="reg[password]" id="password" >
            </div>
            </div>
            <div class="col-md-6 ju">
            <div class="form-group joinus">
              <label>Conform password:</label>
              <input type="password" class="nameju" name="con_password" >
            </div>
            </div>
            
            <div class="col-md-6 ju">
            <div class="form-group joinus">
              <label>Exciting First Name :</label>
              <input type="text" class="nameju" name="reg[first_name]" value="<?php echo $first_name; ?>" >
            </div>
            </div>
            <div class="col-md-6 ju">
            <div class="form-group joinus">
              <label>Exciting Last Name :</label>
              <input type="text" class="nameju" name="reg[last_name]" value="<?php echo $last_name; ?>" >
            </div>
            </div>
            <div class="col-md-6 ju">
            <div class="form-group joinus">
              <label>Your landing City & country:</label>
              <input type="text" class="nameju" name="reg[address]" value="<?php echo $address; ?>" >
            </div>
            </div>
            <div class="col-md-6 ju">
            <div class="form-group joinus">
              <label>Date Of Landing On Earth (DOB) :</label>
              <input type="text" id="datepicker"  class="nameju" name="dob" value="<?php echo $dob; ?>"  readonly >
            </div>
            </div>
            <h4>Besides telepathy how else can we contact you </h4>
            
            
            <div class="col-md-6 ju">
            <div class="form-group joinus">
              <label>Education/Occupation :</label>
              <select class="nameju" name="reg[occupation]" required="">
                <option value="" class="nameju">Select your Occupation</option>
                <option class="nameju">I am a Student</option>
                <option class="nameju">I am a Employee</option>
                <option class="nameju">I am a Bussiness man</option>
                <option class="nameju">I am a Sports Man</option>
                <option class="nameju">I am a Farmer</option>
              </select>
            </div>
            </div>
            <div class="col-md-6 ju">
            <div class="form-group joinus">
              <label>Studied/Worked at :</label>
              <input type="text" class="nameju" name="reg[work_at]" placeholder="" value="<?php echo $work_at; ?>" required="">
            </div>
            </div>
            <div class="col-md-6 ju">
            <div class="form-group joinus">
              <label>Contact No. :</label>
              <input type="number" class="nameju" name="reg[phone]"  value="<?php echo $phone; ?>" >
            </div>
            </div>
            <div class="col-md-12 ju">
              <h4>Your page , blog or website if any:</h4>
              <div class="col-md-3 ju">
                <div class="form-group joinus">
              <label>Google ID :</label>
              <input type="text" class="nameju" name="reg[email_id2]" placeholder="" value="<?php echo $email_id2; ?>" >
            </div>
              </div>
              <div class="col-md-3 ju">
                <div class="form-group joinus">
              <label>Facebook ID :</label>
              <input type="text" class="nameju" name="reg[fb_id]" placeholder="" value="<?php echo $fb_id; ?>" >
            </div>
              </div>
              <div class="col-md-3 ju">
                <div class="form-group joinus">
              <label>Skype ID :</label>
              <input type="text" class="nameju" name="reg[skp_id]" placeholder="" value="<?php echo $skp_id; ?>" >
            </div>
              </div>
              <div class="col-md-3 ju">
                <div class="form-group joinus">
              <label>Twitter ID :</label>
              <input type="text" class="nameju" name="reg[twit_id]"  placeholder=""  value="<?php echo $twit_id; ?>">
            </div>
              </div>
            </div>

                        


            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>What do you feel is your purpose as a light worker ?</label>
                <textarea class="nameju" name="reg[d1]" rows="4"><?php echo $d1; ?></textarea>
              </div>
            </div>
            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>Please share your journey in brief towards realizing your life’s purpose</label>
                <textarea class="nameju" name="reg[d2]" rows="4"><?php echo $d2; ?></textarea>
              </div>
            </div>
            <div class="col-md-6 ju">
              <div class="form-group joinus">
                <label>My Unique Expression :</label>
               <select class="nameju" name="" >
                  <option value=""> choose one</option>
                 <option>Singing</option>
                 <option>Spiritual Workshops</option>
                 <option>Public Speaking</option>
                 <option>Dance</option>
                 <option>Meditational Trainer</option>
                 <option>Painting, Designing, DJ</option>
                 <option>Guitarist</option>
                 <option>Writing</option>
                 <option>Drumming</option>
                 <option>Others</option>
               </select>
              </div>
            </div>
            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>What form of meditation do you practice and how has it changed your life.</label>
                <textarea class="nameju" name="reg[d3]" rows="4"><?php echo $d3; ?></textarea>
              </div>
            </div>
            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>Please Mention some of your most life changing Books/Articles </label>
                <textarea class="nameju" name="reg[d4]" rows="4"><?php echo $d4; ?></textarea>
              </div>
            </div>
            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>Please mention some of your spiritual teachers or role models who have inspired your life </label>
                <textarea class="nameju" name="reg[d5]" rows="4"><?php echo $d5; ?></textarea>
              </div>
            </div>
            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>What is Love for you.</label>
                <textarea class="nameju" name="reg[d6]" rows="4"><?php echo $d7; ?></textarea>
              </div>
            </div>
            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>Please share your thoughts on awareness.</label>
                <textarea class="nameju" name="reg[d7]" rows="4"><?php echo $d7; ?></textarea>
              </div>
            </div>
            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>What do you feel about consuming animals as food.</label>
                <textarea class="nameju" name="reg[d8]" rows="4"><?php echo $d8; ?></textarea>
              </div>
            </div>
            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>When presented with a situation in which the odds are against you, what goes through your mind. </label>
                <textarea class="nameju" name="reg[d9]" rows="4"><?php echo $d9; ?></textarea>
              </div>
            </div>
            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>Have you ever experienced any communication or contact with any extraterrestrial civiliaztion/Aliens? If yes please share. </label>
                <textarea class="nameju" name="reg[d9]" rows="4"><?php echo $d9; ?></textarea>
              </div>
            </div>
            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>What is the significance of collaboration in evolution. </label>
                <textarea class="nameju" name="reg[d9]" rows="4"><?php echo $d9; ?></textarea>
              </div>
            </div>
            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>What do you feel you need to take your work next level </label>
                <textarea class="nameju" name="reg[d9]" rows="4"><?php echo $d9; ?></textarea>
              </div>
            </div>

            <div class="col-md-12 ju">
              <div class="form-group joinus">
                <label>What is that you have in abundance and are willing to share with other Light Workers</label>
                <textarea class="nameju" name="reg[d9]" rows="4"><?php echo $d9; ?></textarea>
              </div>
            </div>
             
            <div class="col-md-12 ju">
            <button type="submit" class="new1">Submit</button>
            </div>
          </form>
        </div>
      </div>
          </div>
    <!--Yoga single image section-->
    <div class="container">
      <div class="col-md-12 ays-yoga" align="center">
        <img src="<?php echo base_url();?>upload/images/yoga.png" class="img-responsive">
      </div>
    </div>
    <!--scroll top-->
    <div class="scroll-top-wrapper ">
  <span class="scroll-top-inner">
    <img src="images/Scroll-to-top-button.png" class="img-responsive">
  </span>
</div>


                      <div class="form-group">
                                <input type="checkbox" class="chkbx" value="1" name="reg[t_and_c]" required="">
                                <label class="ralbl">* I have read, understood & accept the <a class="js-open-modal" href="#" data-modal-id="popup2">terms & Conditions</a>
</label>
                                <div id="popup2" class="modal-box">
                                  <header> <span  class="js-modal-close close">×</span>
                                       <h4>Terms of Use</h4>
                                  </header>
                                  <div class="modal-body">
                                    <p align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut commodo at felis vitae facilisis. Cras volutpat fringilla nunc vitae hendrerit. Donec porta id augue quis sodales.</p>
                                  </div>
                                </div>
                            </div>


<script>
    //----------popup js------------->
$(function(){

var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

    $('a[data-modal-id]').click(function(e) {
        e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
        var modalBox = $(this).attr('data-modal-id');
        $('#'+modalBox).fadeIn($(this).data());
    });  
  
  
$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });
 
});
 
$(window).resize(function() {
    $(".modal-box").css({
        top: ($(0).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(20).width() - $(".modal-box").outerWidth()) / 2
       
    });
});
 
$(window).resize();
 
});
</script>
<script>
  var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('output');
      output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);

  };
</script>