<!--<?php 

print("<pre>");
print_r($order);
?>
-->

<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">My Account</span>
			 <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Deactive Account</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <p class="title_block">Settings</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                    <li><span></span><a href="<?php echo site_url('user/my_account')?>">Personal Information</a></li>
                                    <li><span></span><a href="<?php echo site_url('user/change_password')?>">Change Password</a></li>
                                    <li><span></span><a href="<?php echo site_url('user/address')?>">Addresses</a></li>
                                    <li><span></span><a href="<?php echo site_url('user/wishlist')?>">Wishlist</a></li>
									 <li class="active"><span></span><a href="<?php echo site_url();?>/order/view_order">Order</a></li>
                                    <li><span></span><a href="<?php echo site_url('user/deactive_account')?>">Deactivate Account</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
			<div class="columns-container">
			
     <h4 class="page-heading no-line" >
            <span class="page-heading-title2"style="margin-left: 15px;font-size: 21px;line-height: 22px;">Your Order List</span>
        </h4>
        <!-- ../page heading-->
        <div class="page-content page-order col-sm-9" style="margin-top: 10px;">
            <div class="table-responsive order-detail-content">
			<?php foreach($order as $pd){?>
			
                <table class="table table-bordered cart_summary">
                    <thead>
                        <tr>
                            <th class="cart_product">#Order</th>
                            <th>Date</th>
                            <th>Amount</th>
                            <th>Discount</th>
                            <th>Total payable</th>
                            <th>Status</th>
                            <!-- <th  class="action"><i class="fa fa-trash-o"></i></th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="cart_product">
                                <a href="<?php echo site_url("order/details")."/".$pd['id']; ?>" class="orderbtn btn pull-left">#<?php echo $pd['order_id']; ?></a>
                            </td>
                            <td class="cart_description">
                                <p class="product-name"><?php echo $pd['entry_date']; ?></p>
                                
                            </td>
                            
                            <td class="price"><span class="pull-left">&#8358; <?php echo $pd['total_price']; ?></span></td>
                            <td class="qty">&#8358; <?php echo $pd['total_saving']; ?></td>
                            <td class="price">
                                <span class="pull-left">&#8358; <?php echo  $pd['total_price']-$pd['total_saving']; ?>.00</span>
                            </td>
							<td class="cart_avail"><span class="label label-success"><?php if($pd['shipping_status'] ==0){ echo "Pending"; }else{ echo "Delivered"; } ?></span></td>
                            <!-- <td class="action">
                                <a href="#">Delete item</a>
                            </td> -->
                        </tr>
                    </tbody>
                    
                </table>
<?php }?>
            </div>

            
          </div>
    </div>
          
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>