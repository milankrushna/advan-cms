<script src="<?php echo base_url();?>/assets/datepicker/jquery/dist/jquery.validate.js"></script>
	
	<script>
	$(document).ready(function() {
		
		$("#commentForm").validate({
		
			messages: {
				
				full_name: {
					required: 'Enter your Name'
				},
				country: {
					required: 'Please choose your country name'
				},
				state: {
					required: 'Please choose your state name'
				},
				city: {
					required: 'Enter your City name'
				},
				address_one: {
					required: 'Enter your Address.'
				},
				address_two: {
					required: 'Enter your address'
				},
				postal_code: {
					required: 'Enter your Postal/Zip code'
				},
				phone: {
					required: 'Enter your contact no'
				},
				
				
			}
		});
	});
	</script>

<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Shopping</span>
			</div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <p class="title_block">Settings</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                    <li><span></span><a href="<?php echo site_url('user/my_account')?>">Personal Information</a></li>
                                    <li><span></span><a href="<?php echo site_url('user/change_password')?>">Change Password</a></li>
                                    <li class="active"><span></span><a href="<?php echo site_url('user/address')?>">Addresses</a></li>
                                    <li><span></span><a href="#">Wishlist</a></li>
                                    <li><span></span><a href="<?php echo site_url('user/deactive_account')?>">Deactivate Account</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- page heading-->
                <div class="page-content checkout-page no-margin-top">
                    <h3 class="checkout-sep">Add a New Address</h3>
                    <div class="box-border">
					<form action="<?php echo site_url('user/address');?>" method="POST" id="commentForm">
					<?php echo validation_errors(); ?>
                        <ul>
                            <li class="row padding-bottom">
                                <div class="col-sm-3">
                                    <label for="first_name" class="required">Name</label>
                                </div><!--/ [col] -->
                                <div class="col-sm-5">
                                    <input name="full_name" type="text" value="<?php echo $full_name; ?>" required class="input form-control" id="first_name">
                                </div><!--/ [col] -->
                            </li><!--/ .row -->
							<li class="row padding-bottom">
                                <div class="col-sm-3">
                                    <label class="required">Country</label>
                                </div><!--/ [col] -->
                                <div class="col-sm-5">
								<select name="country" class="input form-control" onchange="get_state(this.value);" required >
	<option value="" >Select Your country</option> 
	<?php foreach($country as $dt){?>
   <option  value="<?php echo $dt['country_id'];?>"><?php echo  $dt['countryname'] ;?></option>
   <?php }?>
    </select>         </div><!--/ [col] -->
                            </li><!--/ .row -->
							
							<li class="row padding-bottom">
                                <div class="col-sm-3">
                                    <label class="required">State</label>
                                </div><!--/ [col] -->
                                <div class="col-sm-5">
								<select id="data" name="state" class="input form-control" value="<?php echo $state;?>"  required >
								
	
	</select>
                                </div><!--/ [col] -->
                            </li><!--/ .row -->
               
                            <li class="row padding-bottom">
                                <div class="col-sm-3">
                                    <label for="first_name" class="required">City</label>
                                </div><!--/ [col] -->
                                <div class="col-sm-5">
                                    <input type="text" name="city" value="<?php echo $city;?>"  type="text" required class="input form-control" id="Landmark">
                                </div><!--/ [col] -->
                            </li>
                            <li class="row padding-bottom">
                                <div class="col-sm-3">
                                    <label for="first_name" class="required">Address Line 1</label>
                                </div><!--/ [col] -->
                                <div class="col-sm-5">
                                    <input name="address_one" value="<?php echo $address_one;?>" type="text" required class="input form-control" id="City">
                                </div><!--/ [col] -->
                            </li>
                                <li class="row padding-bottom">
                                <div class="col-sm-3">
                                    <label for="first_name" class="required">Address Line 2</label>
                                </div><!--/ [col] -->
                                <div class="col-sm-5">
                                    <input name="address_two" value="<?php echo $address_two;?>"  type="text" required class="input form-control" id="City">
                                </div><!--/ [col] -->
                            </li>
                            
                            <li class="row padding-bottom">
                                <div class="col-sm-3">
                                    <label for="postal_code" class="required">Zip/Postal Code</label>
                                </div><!--/ [col] -->
                                <div class="col-sm-5">
                                    <input class="input form-control" name="postal_code" value="<?php echo $postal_code;?>" type="text" required>
                                </div><!--/ [col] -->
                            </li><!--/ .row -->
                            <li class="row padding-bottom">
                                <div class="col-sm-3">
                                    <label for="telephone" class="required">Phone Number</label>
                                </div><!--/ [col] -->
                                <div class="col-sm-5">
                                    <input class="input form-control" name="phone" value="<?php echo $phone;?>" type="number" required id="telephone">
                                </div><!--/ [col] -->
                            </li><!--/ .row -->
                            <li>
                                <div class="col-sm-3">
                                </div><!--/ [col] -->
                                <div class="col-sm-5">
                                    <button class="button ">Save Changes</button>
                                </div><!--/ [col] -->
                            </li>
                        </ul>
						</form>
                    </div>

                    <h3 class="checkout-sep">Your Saved Addresses </h3>
                    <div class="box-border">
                        <ul>
						<?php foreach($shipp_add as $spa){?>
                            <li class="row padding-bottom">
                                <div class="col-sm-6">
                                    <div class="col-sm-12 grayshade">
                                        <div class="margin-tb">
                                            <b>Name :</b> <?php echo $spa['full_name']?>
      <p><b>Address:</b><?php echo $spa['address_one'].",".$spa['address_two'];?>
	   <?php echo $spa['city'].",".$spa['state'].",".$spa['country'];?></p>
	   <b>Pin:</b><?php echo $spa['postal_code']?></br>
      <b>Phone:</b><?php echo "contact : 0". $spa['phone']?></br>
                                          <hr>
                                            <a href="<?php echo site_url('user/delete_address')."/".$spa['id'];?>">Delete Address</a>
                                        </div>
                                    </div>
                                </div><!--/ [col] -->
								</li>
								<?php }?>
								<li class="row padding-bottom">
								
                                <div class="col-sm-6">
                                    <div class="col-sm-12 grayshade">
                                        <div class="margin-tb">
                                            <h3>Full Name</h3>
                                            <p><b>Address:</b> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                                            <p><b>Phone:</b> 123-456-7890</p>
                                            <hr>
                                            <label><input type="radio" name="radio1">Default Address</label>
                                            <hr>
                                            <a href="#">Delete Address</a>
                                        </div>
                                    </div>
                                </div><!--/ [col] -->
                            </li><!--/ .row -->
                            <li class="row padding-bottom">
                                <div class="col-sm-6">
                                    <div class="col-sm-12 grayshade">
                                        <div class="margin-tb">
                                            <h3>Full Name</h3>
                                            <p><b>Address:</b> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                                            <p><b>Phone:</b> 123-456-7890</p>
                                            <hr>
                                            <label><input type="radio" name="radio1">Default Address</label>
                                            <hr>
                                            <a href="#">Delete Address</a>
                                        </div>
                                    </div>
                                </div><!--/ [col] -->
                                <div class="col-sm-6">
                                    <div class="col-sm-12 grayshade">
                                        <div class="margin-tb">
                                            <h3>Full Name</h3>
                                            <p><b>Address:</b> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                                            <p><b>Phone:</b> 123-456-7890</p>
                                            <hr>
                                            <label><input type="radio" name="radio1">Default Address</label>
                                            <hr>
                                            <a href="#">Delete Address</a>
                                        </div>
                                    </div>
                                </div><!--/ [col] -->
                            </li><!--/ .row -->
                        </ul>
						
                    </div>


                </div>
                <!-- ./Content page -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<script>

function get_state(id){

	$.post('<?php echo site_url('product/state')?>',{key:id},function(msg){
	$('#data').html(msg);
	
	})
	}
</script>