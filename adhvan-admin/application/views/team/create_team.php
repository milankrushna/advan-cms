<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/ajxupload/upload.css"  />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/ajxupload/jquery.form.js" ></script>    

<div class="content-wrapper">
               
              <section class="content-header">
          <h1>Add Team</h1>
<br>
              
         
        </section>
        <section class="content col-sm-12">
      <div class="box box-primary">
<div class="box-body" >

	<div class="widget-content">
	  
               <form action="<?php echo  site_url('admin/team/create_team'); ?>" enctype="multipart/form-data" method="post" id="team_create">
    
				   <div class="form-group">
                   	<label for="text1" class="control-label">Name</label>
                     
                            <input type="text" name="name" class="form-control" required/>
                        
                	</div>
                	
                	<div class="form-group">
                   	<label for="text1" class="control-label">Designation</label>
                     
                            <input type="text" name="designation" class="form-control"  required/>
                        
                	</div>
                	
                	<div class="form-group">
                   	<label for="text1" class="control-label">Cover Pic</label>
                   
                            <input type="file" name="file" class="form-control"  />
                        
                	</div>
                    
                    
                	
                	<div class="form-group">
                   	<label for="text1" class="control-label">Description</label>
                     
                            <input type="text" name="description" class="form-control"  required/>
                        
                	</div>
                	
            
                        <input type="submit"  value="Submit" class="btn btn-primary" />
                    
                </form>
    
          </div>
				
</div>
</div>
</section>
</div>