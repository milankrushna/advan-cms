
    <!-- END PAGE LEVEL  STYLES -->
     <!--PAGE CONTENT --> 
  
<div class="content-wrapper">
     <section class="content-header">
        <h1>Team</h1>
         <br>
          <a class="btn btn-primary" href="<?php echo site_url('admin/team/create_team')?>" >Add Team</a>
      </section>
<section class="content">
<div class="box box-primary ">
<div class="box-body " >

    <div class="col-md-6">
    
<div id="infoMessage" style="color:#F00"><?php echo $this->session->flashdata('message');  ?></div>
        
        
    </div>
    
    <div style="clear:both;"></div>
    
    
<?php  if(!empty($team)){   
        ?>
    
<div class="box-body" >
                        <?php ////echo $msg; ?>
						<div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Designation</th>
                                            <th>Image</th>
											<th>Description</th>
											<th>Status</th>
											<th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
          <?php foreach ($team as $gall):
		  ?>
		<tr class="odd gradeX" id="slider_<?php echo $gall['id']; ?>">

			<td><?php echo $gall['name']; ?></td>
			<td><?php echo $gall['designation']; ?></td>
			
			<td><img src="<?php echo base_url();?>/upload/events/min_events/<?php echo $gall['cover_pic'];?>"   width="150px"  /></td>
            
            <td><?php echo $gall['description']; ?></td>
				<td>
<?php 
         if($gall['status'] == 0){ $anc = 'Active'; }else{ $anc = 'Deactive'; }
    
                          ///  echo anchor('admin/slider/actdeact/'.$gall->id, $anc,array('class' => 'act-deact','title'=>$gall->status));    ?>
                    <a href="<?php echo site_url('admin/team/actdeact/'.$gall['id']); ?>"  class="act-deact" title="<?php echo $gall['status']; ?>" ><?php echo $anc; ?></a>
                    
</td>
            
            
			<td align="center"> <a class="btn btn-warning" href="<?php echo site_url('admin/team/edit_team'."/".$gall['id'])?>" ><i class="fa fa-edit"></i></a>||<a title="Delete" class="btn  btn-danger" href="<?php echo site_url('admin/team/delete_team'."/".$gall['id'])?>" onclick="return confirm('Are You sure?');"><i class="fa fa-trash"></i></a></td>
            
            
            
            
            
		</tr>
		<?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                   
            </div>

        
        
        
        

<script src="<?php echo base_url()?>assets/plugins/jQueryUI/jquery-ui.js"></script>
    
<script>
 $('.act-deact').click(function(ev){
             ev.preventDefault();
            
          var dis = this;
var upsd = $(dis).attr('title');
             
             
             $.post($(this).attr('href'),{'sts':$(this).attr('title')},function(resp){
                 
                 if(resp == 0){
                    $(dis).html("Active");
                    $(dis).attr("title",resp);
                 }else if(resp == 1){
                    $(dis).html("Deactive");
                    $(dis).attr("title",resp);
                 }
                 
             });  
         });
         
</script>       
        
        <?php } ?>
   
    
                	</div>
    </div>
           
           </section>
           </div>

              

            
