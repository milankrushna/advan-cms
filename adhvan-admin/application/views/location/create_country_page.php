<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/ajxupload/upload.css"
/>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/ajxupload/jquery.form.js"></script>

<link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/datepicker/datepicker3.css">
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>

<link rel="stylesheet" href="<?php echo base_url()?>assets/jquery.tag-editor.css">
<!-- END PAGE LEVEL  STYLES -->
<!--PAGE CONTENT -->

<style>
.image-thumb img {
    border: 2px solid;
    width: 100%;
    height: 100px;
}</style>

<div class="content-wrapper">
	<section class="content-header">
		<h1><?php  echo $title; ?></h1>

	</section>
	<section class="content">
		<div class="box box-primary">
			<div class="box-body">
				<div class="infoMessage" style="color:#F00">
					<?php // echo $message;?>
				</div>
				<form action="<?php echo  site_url('admin/location/create_country_page/'.$cid); ?>" enctype="multipart/form-data"
				 method="post" id="page_create">



					
					<div class="col-md-12">
						<div class="col-md-6">
							<label for="text1" class="control-label">Page Name</label>

							<input type="text" class="form-control" placeholder="Page Name" name="name" required="">
							<br>

						</div>

						<div class="col-md-6">
							<label for="text1" class="control-label">Page Title</label>

							<input type="text" class="form-control" placeholder="Page Title" name="title" required="">
							<br>

						</div>
						
					</div>
                    <div class="col-md-12">
						<div class="col-md-6 form-group">
						
							<label for="">Page Description</label>

							<textarea id="" rows="3"  name="description" class="form-control"></textarea>
						</div>
						<div class="col-md-6 form-group">
						
							<label for="">Explore Description</label>

							<textarea  id="" name="explore_desc" rows="3" class="form-control"></textarea>
						</div>
					</div>
                    <div class="col-md-12">
						<div class="col-md-4 form-group">
						
							<label for="">tour Description</label>

							<textarea id="" rows="3"  name="tour_desc" class="form-control"></textarea>
						</div>
						<div class="col-md-4 form-group">
						
							<label for="">hotel Description</label>

							<textarea  id="" name="hotel_desc" rows="3" class="form-control"></textarea>
						</div>
						<div class="col-md-4 form-group">
						
							<label for="">Weather Description</label>

							<textarea  id="" name="weather_desc" rows="3" class="form-control"></textarea>
						</div>
					</div>
                    <div class="col-md-12">
						<div class="col-md-4 form-group">
						
							<label for="">Tour Page Description</label>

							<textarea id="" rows="3"  name="tour_page_desc" class="form-control"></textarea>
						</div>
						<div class="col-md-4 form-group">
						
							<label for="">Hotel Page Description</label>

							<textarea  id="" name="hotel_page_desc" rows="3" class="form-control"></textarea>
						</div>
						<div class="col-md-4 form-group">
						
							<label for="">Experience Page Description</label>

							<textarea  id="" name="experience_page_desc" rows="3" class="form-control"></textarea>
						</div>
					</div>
                    <div class="col-md-12">
						<div class="col-md-12 form-group">
						
							<label for="">certification Description</label>

							<textarea rows="3" id="certification" name="certification" class="form-control"></textarea>
						</div>
						
					</div>

                    <div class="col-md-12">
						<div class="col-md-12 form-group" style="overflow:auto;">
							<label for="">Region Weather</label>
                            <div style="width:1700px">

						<table id="wetTable" class="table">
                        <input type="hidden" id="wetInd" value="0">
                            <tr>
                                <td>New</td>
                                <td style="min-width: 200px;">Region</td>
                                <td>1-Jan</td>
                                <td>2-Feb</td>
                                <td>3-Mar</td>
                                <td>4-Apr</td>
                                <td>5-May</td>
                                <td>6-Jun</td>
                                <td>7-Jul</td>
                                <td>8-Aug</td>
                                <td>9-Sep</td>
                                <td>10-Oct</td>
                                <td>11-Nov</td>
                                <td>12-Dec</td>
                            </tr>
                            <tr>
                                <td><a class="btn btn-success" onclick="newWeather()"><i class="fa fa-plus"></i></a></td>
                                <td><input type="text" class="form-control" name="weather_data[0][region]"></td>
                                <?php
                                $month = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
                                foreach($month as $k=>$mt){ ?>
                                <td><select  class="form-control" name="weather_data[0][<?php echo $mt; ?>]" >
                                <option value="">Select weather</option>
                                <option value="wet_1">Pleasant weather, no rain</option>
                                <option value="wet_2">High heat and humidity</option>
                                <option value="wet_3">Tropical climate, possible intermittent rain</option>
                                <option value="wet_4">Tropical climate, high chances of rain</option>
                                <option value="wet_5">Possible risk of typhoons and storms</option>
                                <option value="wet_6">Cool to cold temperature (at night)</option>
                                </select></td>
                                <?php  } ?>
                                
                            </tr>
                        </table>
						</div>
						</div>
						
					</div>

				
				
				
				

	<div class="col-md-12">

<div class="col-md-4">
	<label for="text1" class="control-label">Banner file</label>
	<div class="input-group form-group">
		<input type="hidden" id="bannerFile" class="form-control" placeholder="Main image" name="bannerFile">
		<div></div>
		<!-- this div is image container -->

<a onclick="BrowseServer('bannerFile','')" class="btn btn-app">
                <i class="fa fa-image"></i> Image
              </a>
	</div>
</div>
<div class="col-md-4">
	<label for="text1" class="control-label">Map Image</label>
	<div class="input-group form-group">
		<input type="hidden" id="explore_image" class="form-control" placeholder="Main image" name="explore_image">
		<div></div>
		<!-- this div is image container -->

<a onclick="BrowseServer('explore_image','')" class="btn btn-app">
                <i class="fa fa-image"></i> Image
              </a>
	</div>
</div>
<div class="col-md-4">
	<label for="text1" class="control-label">Video File</label>
	<div class="input-group form-group">
		<input type="text" id="video_url" class="form-control" placeholder="Main image" name="video_url" readonly>
		<div></div>
		<!-- this div is image container -->

<a onclick="BrowseServer('video_url','')" class="btn btn-app">
                <i class="fa fa-image"></i> Video
              </a>
	</div>
</div>
<div class="col-md-4">
	<label for="text1" class="control-label">Menu Image</label>
	<div class="input-group form-group">
		<input type="hidden" id="menu_image" class="form-control" placeholder="Menu image" name="menu_image" readonly>
		<div></div>
		<!-- this div is image container -->

<a onclick="BrowseServer('menu_image','')" class="btn btn-app">
                <i class="fa fa-image"></i> Video
              </a>
	</div>
</div>
</div>

			
					</div>
					<div class="infoMessage" style="color:#F00">
						<?php //echo $message;?>
					</div>
	<div class="form-group">
					<button type="submit"  class="btn btn-block btn-success SubmitButton">Save </button>
					</div>
				</form>


			</div>
		</div>
	</section>
</div>

<!-- END PAGE CONTENT -->

<script src="<?php echo base_url()?>assets/jquery.tag-editor.min.js"></script>
<script src="<?php echo base_url()?>assets/jquery.caret.min.js"></script>

<script src="<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/ckfinder.js"></script>

<script>


function BrowseServer(settingId,multiDisplay)
{
	var finder = new CKFinder();
	finder.basePath = "../";	
	finder.selectActionFunction = (fileUrl)=>{


		var baseUrl = '<?php echo base_url(); ?>';
		var localUrl = baseUrl.replace("adhvan-admin/",'');
		var main_url =  fileUrl.replace(localUrl, "");
		var thumb = main_url.replace('adhvan/images','adhvan/_thumbs/Images'); 

		if(settingId!=""){
			 $('#'+settingId).val(main_url);
			var imc =  $('#'+settingId).siblings()[0];
			console.log(imc);
			$(imc).html('<div class="image-thumb"><img height="120px" src="'+localUrl+thumb+'"><div class="action"><a onclick="removeMe(this)" alt="'+settingId+'" ><i class="fa fa-trash	"></i></a></div>')
			 }
		
		if(multiDisplay != ""){
		
		
		$("#"+multiDisplay).append('<div class="col-md-2 image-thumb"><input type="text" name="galleryImages[]" value='+main_url+'><img height="120px" src="'+localUrl+thumb+'"><div class="action"><a onclick="removeMe(this)" ><i class="fa fa-trash	"></i></a></div>');
	}
	};
	var api = finder.popup();

//upload/adhvan/images/tours/37.jpg
//upload/adhvan/_thumbs/Images/tours/37.jpg

}


function removeMe(dis){
	var altTag = $(dis).attr('alt');
	if(altTag !=""){
		$('#'+altTag).val("");
	}
var cc =$(dis).parent().parent().remove();

}


	var noteEditor = CKEDITOR.replace('certification', {

		height: 250,
		filebrowserBrowseUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/ckfinder.html',
		filebrowserUploadUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserWindowWidth: '1000',
		filebrowserWindowHeight: '700'
	});

	sht_editor.on('change', function(ev) {

		$('#sht_editor').html(sht_editor.getData());

	});

var exitDay = [1];
var noDay = 1;




	$(function() {
		$('#datepicker').datepicker({
			autoclose: true,
			format: "dd-mm-yyyy"
		});
	});


	function get_city(dis) {

		// alert(id);
		$('#category').html('<option value="">Loading...</option>');

		$.get('<?php echo site_url('admin/tours/get_cnt_state/');?>/'+dis,{},
			function(resp) {

				$('#subcatgy').html(resp);

			});

	}
	function get_sub_city(dis) {

		// alert(id);
		$('#subcity').html('<option value="">Loading...</option>');

		$.get('<?php echo site_url('admin/tours/get_state_district/');?>/'+dis,{},
			function(resp) {

				$('#subcity').html(resp);

			});

	}


function newWeather(){
    var ii = +$('#wetInd').val()+1;
   $('#wetInd').val(ii);
    var month = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var newWe = '<tr><td><a class="btn btn-danger" onclick="removeWeather(this);"><i class="fa fa-minus"></i></a></td><td><input type="text" class="form-control" name="weather_data['+ii+'][region]"></td>';
         for(mt of month){    
            newWe+=  '<td><select class="form-control" name="weather_data['+ii+']['+mt+']"><option value="">Select weather</option><option value="wet_1">Pleasant weather, no rain</option><option value="wet_2">High heat and humidity</option><option value="wet_3">Tropical climate, possible intermittent rain</option><option value="wet_4">Tropical climate, high chances of rain</option><option value="wet_5">Possible risk of typhoons and storms</option><option value="wet_6">Cool to cold temperature (at night)</option></select></td>';

             }

             newWe+= '</tr>';

$('#wetTable').append(newWe);
}


function removeWeather(dis){
    $(dis).parent().parent().remove();
}

</script>