<link href="<?php echo base_url(); ?>assets/plugins/select2/select2.css" rel="stylesheet" type="text/css"   />
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js" type="text/javascript"></script>         
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/multiselect/js/jquery.multi-select.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
        <script src="<?php echo base_url(); ?>assets/pages/jquery.form-advanced.init.js"></script>

<!-- start inner required -->

        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item active"></li>
                                </ol>
                            </div>
                            <h4 class="page-title"><?php echo (!empty($index_heading))? $index_heading : '';  ?></h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                 <div class="row">
                     
                  <div class="col-lg-10" >
                         <div class="card-box" >
                             <h4 class="m-t-0 header-title"><?php echo (!empty($index_subheading))? $$index_subheading : '';  ?></h4>
                     <!-- end inner required -->
                            
                             <div class="pull-right"> <a onclick="new_data();" class='btn btn-sm btn-success' href="javascript:void('create Country')" ><i class="mdi mdi-library-plus"></i> <span>New</span></a> <?php /*<a class='btn btn-sm btn-success' href="<?php echo site_url('auth/create_group');  ?>"><i class=" mdi mdi-account-multiple-plus"></i> <span>New Group</span></a> */ ?> </div><br>
<div class="text-pink font-14 m-b-20"><?php echo (!empty($message))? $message  : ''; ?></div>
  <form action="" method="GET"> 
      
        <table width="100%">
            <tbody>
                <tr>
                    <td>
        <div >
        <label for="field-3" class="control-label">Country</label>
       <select class="form-control select2" name="country_2" onchange="get_state_2();" id="countrydata_2">
            <option value="" >Select Country</option>
                <?php  foreach($country as $ctry){ ?>
           <option  value="<?php  echo $ctry['id']; ?>"><?php  echo $ctry['countryname']; ?></option>
           <?php } ?>

        </select>
          </div>
                    </td><td>
        <div >
       <label for="field-3" class="control-label">State</label>
    <select class="form-control select2" name="state_id_2" id="statedata_2" onchange="get_district_2()">
                                <option value="" >Select State</option>
                                  
                            </select>
          </div>
                    </td>
                    <td>
        <div >
       <label for="field-3" class="control-label">District</label>
                           <select class="form-control select2" name="distId_2" id="distData_2">
                                <option value="" >Select District</option>
                                  
                            </select>
          </div>
                    </td>
                    <td style="padding-top: 30px;padding-left: 6px;">
            <button type="submit" class="btn btn-primary">search</button>
                    </td>
                </tr>
            </tbody>
          </table>
 </form>
                             
                             <br>
<div class="table-responsive" data-pattern="priority-columns" >
<table class="table">
    <thead class="thead-default">
	<tr>
       
        <th>City name</th>
        <th>District name</th>
        <th>State name</th>
        <th>Country name</th>
        <?php if($this->ion_auth->is_admin()){ ?>        
        <th>Action</th>
        <?php  } ?>
	</tr>
    </thead>
    <tbody id="tbdy">
	<?php foreach ($alldata as $cnt){ ?>
		<tr id="city<?php echo $cnt['id']; ?>">
            
            <td><?php echo $cnt['name']; ?></td>
            <td id="cntry<?php echo $cnt['d_id']; ?>"><?php echo $cnt['districtname']; ?></td>            
            <td id="state<?php echo $cnt['s_id']; ?>" ><?php echo $cnt['statename']; ?></td>
            <td id="cntry<?php echo $cnt['c_id']; ?>"><?php echo $cnt['countryname']; ?></td>
        <?php if($this->ion_auth->is_admin()){ ?>
            
            <td><a href="javascript:void('Edit')" class="btn btn-icon btn-sm btn-primary"  onclick="edit_data('<?php echo $cnt['id']; ?>','<?php echo $cnt['c_id'] ?>','<?php echo $cnt['s_id']; ?>','<?php echo $cnt['d_id']; ?>','<?php echo $cnt['name']; ?>')" class=""><i class="mdi mdi-table-edit"></i></a>
            <a  href="<?php echo site_url('location/delete_district_city/'.$cnt['id']); ?>" class='dlt btn btn-icon btn-sm btn-danger' ><i class="mdi mdi-delete-forever"></i></a></td>
        <?php  }    ?>		
		</tr>
	<?php } ?>
    </tbody>
</table>
                             </div>
                </div>
            </div>
</div>
</div>
<script>
       function new_data(id=''){
           //$("#countrydata").select2().val(null).trigger("change");
            //$('#statedata').html('<option value="">loading...</option>').trigger('change');
            $('#citydata').val('');

           $('#edit_modal').modal('show');
           $('#valmsg').text('');
           $('#sbmt').removeAttr('disabled');
           $('#sbmt').attr('onclick',"newData();");
            }
    
    
    
     function newData(){
        var country = $('#countrydata').select2('data'),
         countryid = country.id,
         countryname = country.text,
         statedat = $('#statedata').select2('data'),
         stateid = statedat.id,
         statename = statedat.text,

         distData = $('#districtData').select2('data'),
         districtId = distData.id,
         districtName = distData.text,

         city =  $('#citydata').val(); 
   
        if(countryid !='' && stateid!=''  && districtId!='' && city!=''){
            $('#sbmt').attr('disabled', 'disabled');
            
            $('#valmsg').text('please wait...');
            $.post('<?php echo site_url('location/new_dist_city') ?>',{cid: countryid,cname:countryname,stateid:stateid,statename:statename,districtId:districtId,districtName:districtName,city:city},function(resp,status){
                
                if(status == 'success'){
                    resp =JSON.parse(resp); 
                    newid = resp.id;
                    newcntry = resp.newcntry;
                $('#sbmt').removeAttr('disabled');
                if(resp.status == '1'){
                    
                   $('#citydata').val('');
                   $('#valmsg').text(resp.message);
                    var newrows =resp.newstate;
                   $('#tbdy').prepend(newrows);
                    //$('#edit_modal').modal('hide');
                   /// $('#sbmt').removeAttr('onclick');
                }else{
                   
                  $('#valmsg').text(resp.message);  
                }
                }
                
            });
        
        }else{
          $('#valmsg').text('All field required');
           $('#countrydata').focus(); 
        }
        
        
    }
    
    
    
    
        function edit_data(city_id,cid,sid,did,cityname){
          
         $("#countrydata").select2().val(cid).trigger("change");
            get_state(sid);
          
         $("#statedata").select2().val(sid).trigger("change");
         
         $("#districtData").select2().val(did).trigger("change");
           $('#citydata').val(cityname);
           $('#cityid').val(city_id);
            
           $('#edit_modal').modal('show');
           $('#valmsg').text('');
           $('#sbmt').removeAttr('disabled');
           $('#sbmt').attr('onclick',"updatedata();");
            }
            
    
    function updatedata(){
        var country = $('#countrydata').select2('data'),
         countryid = country.id,
         countryname = country.text,
         statedat = $('#statedata').select2('data'),
         stateid = statedat.id,
         statename = statedat.text,

         distData = $('#districtData').select2('data'),
         districtId = distData.id,
         districtName = distData.text,

         city =  $('#citydata').val(), 
         cityId =  $('#cityid').val(); 
   
         
         if(countryid !='' && stateid!=''  && districtId!='' && city!=''){
            $('#sbmt').attr('disabled', 'disabled');
            
            $('#valmsg').text('please wait...');
            $.post('<?php echo site_url('location/update_district_city') ?>',{
                cityId:cityId,
                cid: countryid,
                cname:countryname,
                stateid:stateid,
                statename:statename,
                districtId:districtId,
                districtName:districtName,
                city:city
                },function(resp,status){
                
                if(status == 'success'){
                    resp =JSON.parse(resp); 
                    newid = resp.id;
                    newcntry = resp.newcntry;
                $('#sbmt').removeAttr('disabled');
                if(resp.status == '1'){
                    $('#statedata').val('');
                   $('#valmsg').text(resp.message);
                    var newrows =resp.newstate;
                   $('#city'+cityId).html(newrows);
                    //$('#edit_modal').modal('hide');
                   /// $('#sbmt').removeAttr('onclick');
                }else{
                   
                  $('#valmsg').text(resp.message);  
                }
                }
                
            });
        
        }else{
          $('#valmsg').text('All field required');
           $('#countrydata').focus(); 
        }
        
        
    }
    
    
    
    

  $('a.dlt').click(function(evt){
           evt.preventDefault();
          if(confirm("!Are You Sure ?")){       
               var dis = this;
              $(dis).text('wait...');
                $.post($(dis).attr('href'),{'delete':'dlt'},function(resp){
                    if(resp == 1){
                        $(dis).parent().parent().remove();
                    }else{
                        $(dis).html('<i class="mdi mdi-delete-forever"></i>');
                       alert(resp);
                    }
                });
            }
        });

       </script>     
            
            <!--  MODAL -->
            
  <div id="edit_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">District</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="">
                            <label for="field-3" class="control-label">Country</label>
                           <select class="form-control select2" name="country_id" onchange="get_state();" id="countrydata">
                                <option value="" >Select Country</option>
                                    <?php  foreach($country as $ctry){ ?>
                               <option value="<?php  echo $ctry['id']; ?>"><?php  echo $ctry['countryname']; ?></option>
                               <?php } ?>
                               
                            </select>
                        </div>
                        
                        <div class="">
                            <label for="field-3" class="control-label">State</label>
                           <select class="form-control select2" name="state_id" id="statedata" onchange="get_district();">
                                <option value="" >Select State</option>
                                  
                            </select>
                        </div>
                        <div class="">
                            <label for="field-3" class="control-label">District</label>
                           <select class="form-control select2" name="district" id="districtData">
                                <option value="" >Select district</option>
                                  
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="field-3" class="control-label">City</label>
          <input type="text" class="form-control" name="city" id="citydata" placeholder="City">
                            <input type="hidden" id="cityid" value="">
                        </div>
                        <p id="valmsg" class="text-danger"></p>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                <button id="sbmt" type="button"  class="btn btn-info waves-effect waves-light">Save changes</button>
            </div>
        </div>
    </div>
                            </div><!-- /.modal -->
            
            <script>
            function get_state(sid=''){
                
        var country = $('#countrydata').select2('data');
        var countryid = country.id;
            $('#statedata').html('<option value="">loading...</option>').trigger('change');
                 
                $.post('<?php echo site_url('location/get_cnt_state'); ?>'+'/'+countryid,{sid:sid},function(resp,status){
                 $('#statedata').html(resp).trigger('change');
                   
                    
                });
            } 
            
                function get_state_2(sid=''){
                
        var country = $('#countrydata_2').select2('data');
        var countryid = country.id;
            $('#statedata_2').html('<option value="">loading...</option>').trigger('change');
                 
                $.post('<?php echo site_url('location/get_cnt_state'); ?>'+'/'+countryid,{sid:sid},function(resp,status){
                 $('#statedata_2').html(resp).trigger('change');
                   
                    
                });
                
                
            }

function get_district(sid=''){
                
               <?php  if($this->input->get('distId_2')){ ?>

sid =  '<?php echo $this->input->get('distId_2');   ?>';
               <?php } ?>
                var state = $('#statedata').select2('data');
                var stateId = state.id;
                    $('#districtData').html('<option value="">loading...</option>').trigger('change');
                         
                        $.post('<?php echo site_url('location/get_state_district'); ?>'+'/'+stateId,{sid:sid},function(resp,status){
                         $('#districtData').html(resp).trigger('change');
                           
                            
                        });
                    }

        function get_district_2(sid=''){
    
    var state = $('#statedata_2').select2('data');
    var stateId = state.id;
        $('#districtData').html('<option value="">loading...</option>').trigger('change');
                
            $.post('<?php echo site_url('location/get_state_district'); ?>'+'/'+stateId,{sid:sid},function(resp,status){
                $('#distData_2').html(resp).trigger('change');
                
                
            });
        }  


    <?php /* if($this->input->get('country_2')){ ?>
        $("#countrydata_2").select2().val(<?php echo $this->input->get('country_2'); ?>).trigger("change");
<?php   } */ ?>

            </script>