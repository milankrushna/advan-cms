<link href="<?php echo base_url(); ?>assets/plugins/select2/select2.css" rel="stylesheet" type="text/css"   />
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js" type="text/javascript"></script>         
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/multiselect/js/jquery.multi-select.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
        <script src="<?php echo base_url(); ?>assets/pages/jquery.form-advanced.init.js"></script>

<!-- start inner required -->

        <div class="content-wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <section class="content-header">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                           
                            <h4 class="page-title"><?php echo (!empty($index_heading))? $index_heading : '';  ?></h4>
                        </div>
                    </div>
                </section>
                <!-- end page title end breadcrumb -->

                <section class="content">
                     
                  <div class="col-lg-8" >
                         <div class="box box-primary " >
                             <h4 class="m-t-0 header-title"><?php echo (!empty($index_subheading))? $$index_subheading : '';  ?></h4>
                     <!-- end inner required -->
                            
                             <div class="pull-right"> <a onclick="new_data();" class='btn btn-sm btn-success' href="javascript:void('create Country')" ><i class="mdi mdi-library-plus"></i> <span>New</span></a> <?php /*<a class='btn btn-sm btn-success' href="<?php echo site_url('auth/create_group');  ?>"><i class=" mdi mdi-account-multiple-plus"></i> <span>New Group</span></a> */ ?> </div><br>
<div class="text-pink font-14 m-b-20"><?php echo (!empty($message))? $message  : ''; ?></div>
  <form action="" method="GET"> 
      
        <table width="100%">
            <tbody>
                <tr>
                    <td style="width:40% !important;">
        <div >
        <label for="field-3" class="control-label">Country</label><br>
       <select class="form-control select2" name="country_2" onchange="get_state_2();" id="countrydata_2">
            <option value="" >Select Country</option>
                <?php  foreach($country as $ctry){ ?>
           <option  value="<?php  echo $ctry['id']; ?>"><?php  echo $ctry['countryname']; ?></option>
           <?php } ?>

        </select>
          </div>
                    </td><td>
        <div >
       <label for="field-3" class="control-label">City</label><br>
                           <select class="form-control select2" name="state_id_2" id="statedata_2" style="width:100% !important;">
                                <option value="" >Select City</option>
                                  
                            </select>
          </div>
                    </td>
                    <td style="padding-top: 25px;padding-left: 6px;">
            <button type="submit" class="btn btn-primary">search</button>
                    </td>
                </tr>
            </tbody>
          </table>
 </form>
                             
                             <br>
<div class="table-responsive" data-pattern="priority-columns" >
<table class="table">
    <thead class="thead-default">
	<tr>
       
        <th>Sub City name</th>
        <th>City name</th>
        <th>Country name</th>
        <?php if($this->ion_auth->is_admin()){ ?>        
        <th>Action</th>
        <?php  } ?>
	</tr>
    </thead>
    <tbody id="tbdy">
	<?php foreach ($alldata as $cnt){ ?>
		<tr id="city<?php echo $cnt['id']; ?>">
            
            <td><?php echo $cnt['city']; ?></td>
            <td id="state<?php echo $cnt['state_id']; ?>" ><?php echo $cnt['statename']; ?></td>
            <td id="cntry<?php echo $cnt['country_id']; ?>"><?php echo $cnt['countryname']; ?></td>
        <?php if($this->ion_auth->is_admin()){ ?>            
            <td><a href="javascript:void('Edit')" class="btn btn-icon btn-sm btn-primary"  onclick="edit_data('<?php echo $cnt['id']; ?>','<?php echo $cnt['country_id'] ?>','<?php echo $cnt['state_id']; ?>','<?php echo $cnt['city']; ?>')" class=""><i class="fa fa-edit"></i></a>
            <a  href="<?php echo site_url('admin/location/delete_city/'.$cnt['id']); ?>" class='dlt btn btn-icon btn-sm btn-danger' ><i class="fa fa-trash"></i></a></td>
		<?php } ?>
		</tr>
	<?php } ?>
    </tbody>
</table>
                             </div>
                </div>
            </div>
</section>
</div>
</div>
<script>
       function new_data(id=''){
           $("#countrydata").select2().val(null).trigger("change");
           $("#statedata").select2().val(null).trigger("change");
            $('#statedata').html('<option value="">loading...</option>').trigger('change');
            $('#citydata').val('');

           $('#edit_modal').modal('show');
           $('#valmsg').text('');
           $('#sbmt').removeAttr('disabled');
           $('#sbmt').attr('onclick',"newData();");
            }
    
    
    
     function newData(){
        var country = $('#countrydata').select2('data')[0];
        var countryid = country.id;
        var countryname = country.text;
        var statedat = $('#statedata').select2('data')[0];
         var stateid = statedat.id;
        var statename = statedat.text;
         var city =  $('#citydata').val(); 
   
        if(countryid !='' && stateid!='' && city!=''){
            $('#sbmt').attr('disabled', 'disabled');
            
            $('#valmsg').text('please wait...');
            $.post('<?php echo site_url('admin/location/new_city') ?>',{cid: countryid,cname:countryname,stateid:stateid,statename:statename,city:city},function(resp,status){
                
                if(status == 'success'){
                    resp =JSON.parse(resp); 
                    newid = resp.id;
                    newcntry = resp.newcntry;
                $('#sbmt').removeAttr('disabled');
                if(resp.status == '1'){
                    
                   $('#citydata').val('');
                   $('#valmsg').text(resp.message);
                    var newrows =resp.newstate;
                   $('#tbdy').prepend(newrows);
                    //$('#edit_modal').modal('hide');
                   /// $('#sbmt').removeAttr('onclick');
                }else{
                   
                  $('#valmsg').text(resp.message);  
                }
                }
                
            });
        
        }else{
          $('#valmsg').text('All field required');
           $('#countrydata').focus(); 
        }
        
        
    }
    
    
    
    
        function edit_data(city_id,cid,sid,cityname){
           
            
         $("#countrydata").select2().val(cid).trigger("change");
            get_state(sid);
          
         $("#statedata").select2().val(sid).trigger("change");
          
           $('#citydata').val(cityname);
           $('#cityid').val(city_id);
            
           $('#edit_modal').modal('show');
           $('#valmsg').text('');
           $('#sbmt').removeAttr('disabled');
           $('#sbmt').attr('onclick',"updatedata();");
            }
            
    
    function updatedata(){
       var country = $('#countrydata').select2('data')[0];
        var countryid = country.id;
        var countryname = country.text;
        var statedat = $('#statedata').select2('data')[0];
         var stateid = statedat.id;
        var statename = statedat.text;
         var city =  $('#citydata').val(); 
         var city_id =  $('#cityid').val();
   
        if(countryid !='' && stateid!='' && city!=''){
            $('#sbmt').attr('disabled', 'disabled');
            
            $('#valmsg').text('please wait...');
            $.post('<?php echo site_url('admin/location/update_city') ?>',{cid: countryid,cname:countryname,stateid:stateid,statename:statename,city:city,city_id:city_id},function(resp,status){
                
                if(status == 'success'){
                    resp =JSON.parse(resp); 
                    newid = resp.id;
                    newcntry = resp.newcntry;
                $('#sbmt').removeAttr('disabled');
                if(resp.status == '1'){
                    $('#statedata').val('');
                   $('#valmsg').text(resp.message);
                    var newrows =resp.newstate;
                   $('#city'+city_id).html(newrows);
                    //$('#edit_modal').modal('hide');
                   /// $('#sbmt').removeAttr('onclick');
                }else{
                   
                  $('#valmsg').text(resp.message);  
                }
                }
                
            });
        
        }else{
          $('#valmsg').text('All field required');
           $('#countrydata').focus(); 
        }
        
        
    }
    
    
    
    

  $('a.dlt').click(function(evt){
           evt.preventDefault();
          if(confirm("!Are You Sure ?")){       
               var dis = this;
              $(dis).text('wait...');
                $.post($(dis).attr('href'),{'delete':'dlt'},function(resp){
                    if(resp == 1){
                        $(dis).parent().parent().remove();
                    }else{
                        $(dis).html('<i class="fa fa-trash"></i>');
                       alert(resp);
                    }
                });
            }
        });

       </script>     
            
            <!--  MODAL -->
            
  <div id="edit_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Sub City</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6" style="padding-left: 0;">
                            <label for="field-3" class="control-label">Country</label><br>
                           <select class="form-control select2" name="country_id" onchange="get_state();" style="width:100% !important;" id="countrydata">
                                <option value="" >Select Country</option>
                                    <?php  foreach($country as $ctry){ ?>
                               <option value="<?php  echo $ctry['id']; ?>"><?php  echo $ctry['countryname']; ?></option>
                               <?php } ?>
                               
                            </select>
                        </div>
                        
                        <div class="col-md-6" style="padding-right: 0;">
                            <label for="field-3" class="control-label">City</label><br>
                           <select class="form-control select2" style="width:100% !important;" name="state_id" id="statedata">
                                <option value="" >Select City</option>
                                  
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="field-3" class="control-label">Sub City</label>
          <input type="text" class="form-control" name="city" id="citydata" placeholder="Sub City">
                            <input type="hidden" id="cityid" value="">
                        </div>
                        <p id="valmsg" class="text-danger"></p>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                <button id="sbmt" type="button"  class="btn btn-info waves-effect waves-light">Save changes</button>
            </div>
        </div>
    </div>
                            </div><!-- /.modal -->
            
            <script>
            function get_state(sid=''){
                
        var country = $('#countrydata').select2('data')[0];
        var countryid = country.id;
            $('#statedata').html('<option value="">loading...</option>').trigger('change');
                 
                $.post('<?php echo site_url('admin/location/get_cnt_state'); ?>'+'/'+countryid,{sid:sid},function(resp,status){
                 $('#statedata').html(resp).trigger('change');
                   
                    
                });
                
                
            } 
                function get_state_2(sid=''){
                
        var country = $('#countrydata_2').select2('data')[0];
        var countryid = country.id;
            $('#statedata_2').html('<option value="">loading...</option>').trigger('change');
                 
                $.post('<?php echo site_url('admin/location/get_cnt_state'); ?>'+'/'+countryid,{sid:sid},function(resp,status){
                 $('#statedata_2').html(resp).trigger('change');
                   
                    
                });
                
                
            }
  $("#countrydata_2").select2().val(null).trigger("change");
        $("#statedata_2").select2().val(null).trigger("change");
                
            </script>