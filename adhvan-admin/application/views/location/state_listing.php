<link href="<?php echo base_url(); ?>assets/plugins/select2/select2.css" rel="stylesheet" type="text/css"   />
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js" type="text/javascript"></script>         
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/multiselect/js/jquery.multi-select.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
        <script src="<?php echo base_url(); ?>assets/pages/jquery.form-advanced.init.js"></script>

<!-- start inner required -->

        <div class="content-wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <section class="content-header">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                           
                            <h4 class="page-title"><?php echo (!empty($index_heading))? $index_heading : '';  ?></h4>
                        </div>
                    </div>
                </section>
                <!-- end page title end breadcrumb -->

                 <section class="content">
                     
                  <div class="col-lg-8" >
                         <div class="box box-primary " >
                             <h4 class="m-t-0 header-title"><?php echo (!empty($index_subheading))? $$index_subheading : '';  ?></h4>
                     <!-- end inner required -->
                            
                             <div class="pull-right"> <a onclick="new_data();" class='btn btn-sm btn-success' href="javascript:void('create Country')" ><i class="mdi mdi-library-plus"></i> <span>New</span></a> <?php /*<a class='btn btn-sm btn-success' href="<?php echo site_url('auth/create_group');  ?>"><i class=" mdi mdi-account-multiple-plus"></i> <span>New Group</span></a> */ ?> </div><br>
<div class="text-pink font-14 m-b-20"><?php echo (!empty($message))? $message  : ''; ?></div>
      <form action="" method="GET"> 
      
        <table width="60%">
            <tbody>
                <tr><td>
        <div >
        <label for="field-3" class="control-label">Country</label>
       <select class="form-control select2" name="country" >
            <option value="" >Select Country</option>
                <?php  foreach($country as $ctry){ ?>
           <option <?php echo ($cid == $ctry['id'])? 'selected' : ''; ?> value="<?php  echo $ctry['id']; ?>"><?php  echo $ctry['countryname']; ?></option>
           <?php } ?>

        </select>
          </div>
                    </td>
                    <td style="padding-top: 25px;padding-left: 6px;">
            <button type="submit" class="btn btn-primary">search</button>
                    </td>
                </tr>
            </tbody>
          </table>
 </form>
                             <br>
                             
<div class="table-responsive" data-pattern="priority-columns" >
<table class="table table-striped table-bordered table-hover">
    <thead class="thead-default">
	<tr>
       
        <th>City name</th>
        <th>Country name</th>
        <?php if($this->ion_auth->is_admin()){ ?>        
        <th>Action</th>
        <?php  } ?>
	</tr>
    </thead>
    <tbody id="tbdy">
	<?php foreach ($alldata as $cnt){ ?>
		<tr id="state<?php echo $cnt['id']; ?>">
            
            <td><?php echo $cnt['statename']; ?></td>
            <td><span id='cntry_dat<?php echo $cnt['id'];  ?>'  ><?php echo $cnt['countryname']; ?></span></td>
        <?php if($this->ion_auth->is_admin()){ ?>           
            <td><a href="javascript:void('Edit')" class="btn btn-icon btn-sm btn-primary"  onclick="edit_data('<?php echo $cnt['id']; ?>','<?php echo $cnt['countryid'] ?>','<?php echo $cnt['statename']; ?>')" class=""><i class="fa fa-edit"></i></a>
            <a  href="<?php echo site_url('admin/location/delete_state/'.$cnt['id']); ?>" class='dlt btn btn-icon btn-sm btn-danger' ><i class="fa fa-trash"></i></a></td>
		<?php } ?>
		</tr>
	<?php } ?>
    </tbody>
</table>
                             </div>
                </div>
            </div>
</section>
</div>
</div>
<script>
       function new_data(id=''){
           $("#countrydata").select2().val(null).trigger("change");
                      $('#statedata').val('');

           $('#edit_modal').modal('show');
           $('#valmsg').text('');
           $('#sbmt').removeAttr('disabled');
           $('#sbmt').attr('onclick',"newData();");
            }
    
    
    
     function newData(){
        var country = $('#countrydata').select2('data')[0];
        var countryid = country.id;
        var countryname = country.text;
        var statedat = $('#statedata').val();
   console.log(country);
        if(countryid !='' && statedat!=''){
            $('#sbmt').attr('disabled', 'disabled');
            
            $('#valmsg').text('please wait...');
            $.post('<?php echo site_url('admin/location/new_state') ?>',{cid: countryid,cname:countryname,state:statedat},function(resp,status){
                
                if(status == 'success'){
                    resp =JSON.parse(resp); 
                    newid = resp.id;
                    newcntry = resp.newcntry;
                $('#sbmt').removeAttr('disabled');
                if(resp.status == '1'){
                    $('#statedata').val('');
                   $('#valmsg').text(resp.message);
                    var newrows =resp.newstate;
                   $('#tbdy').prepend(newrows);
                    //$('#edit_modal').modal('hide');
                   /// $('#sbmt').removeAttr('onclick');
                }else{
                   
                  $('#valmsg').text(resp.message);  
                }
                }
                
            });
        
        }else{
          $('#valmsg').text('All field required');
           $('#countrydata').focus(); 
        }
        
        
    }
    
    
    
    
        function edit_data(sid,cid,sname){
           
            
         $("#countrydata").select2().val(cid).trigger("change");
          
           $('#stateid').val(sid);
           $('#statedata').val(sname);
           $('#edit_modal').modal('show');
           $('#valmsg').text('');
           $('#sbmt').removeAttr('disabled');
           $('#sbmt').attr('onclick',"updatedata();");
            }
            
    
    function updatedata(){
        
        var country = $('#countrydata').select2('data')[0];
        console.log(country);
        var countryid = country.id;
        var sid = $('#stateid').val();
        var countryname = country.text;
        var statedat = $('#statedata').val();
        
        if(countryid !='' && statedat!=''){
            $('#sbmt').attr('disabled', 'disabled');
            
            $('#valmsg').text('please wait...');
            $.post('<?php echo site_url('admin/location/update_state') ?>',{cid: countryid,cname:countryname,state:statedat,sid:sid},function(resp,status){
                
                if(status == 'success'){
                    resp =JSON.parse(resp); 
                $('#sbmt').removeAttr('disabled');
                if(resp.status == '1'){
                    
                   $('#statedata').val('');
                   $('#valmsg').text(resp.message);
                    var newrows =resp.newstate;
                   $('#state'+sid).html(newrows);
                    $('#edit_modal').modal('hide');
                    ///$('#sbmt').removeAttr('onclick');
                }else{
                   
                  $('#valmsg').text(resp.message);  
                }
                }
                
            });
        
        }else{
          $('#valmsg').text('All field required');
           $('#countrydata').focus(); 
        }
        
        
    }
    
    
    
    

  $('a.dlt').click(function(evt){
           evt.preventDefault();
          if(confirm("!Are You Sure ?")){       
               var dis = this;
              $(dis).text('wait...');
                $.post($(dis).attr('href'),{'delete':'dlt'},function(resp){
                    if(resp == 1){
                        $(dis).parent().parent().remove();
                    }else{
                        $(dis).html('<i class="fa fa-trash"></i>');
                       alert(resp);
                    }
                });
            }
        });

       </script>     
            
            <!--  MODAL -->
            
  <div id="edit_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">City</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="">
                            <label for="field-3" class="control-label">Country</label>
                           <select class="form-control select2" name="country" id="countrydata" style="width:100% !important;">
                                <option value="" >Select Country</option>
                                    <?php  foreach($country as $ctry){ ?>
                               <option value="<?php  echo $ctry['id']; ?>"><?php  echo $ctry['countryname']; ?></option>
                               <?php } ?>
                               
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="field-3" class="control-label">City</label>
          <input type="text" class="form-control" name="state" id="statedata" placeholder="state">
                            <input type="hidden" id="stateid" value="">
                        </div>
                        <p id="valmsg" class="text-danger"></p>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                <button id="sbmt" type="button"  class="btn btn-info waves-effect waves-light">Save changes</button>
            </div>
        </div>
    </div>
                            </div><!-- /.modal -->