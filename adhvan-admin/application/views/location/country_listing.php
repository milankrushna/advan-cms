                     <!-- start inner required -->

        <div class="content-wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <section class="content-header">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            
                            <h4 class="page-title"><?php echo (!empty($index_heading))? $index_heading : '';  ?></h4>
                        </div>
                    </div>
                </section>
                <!-- end page title end breadcrumb -->

                 <section class="content">
                     
                  <div class="col-lg-8" >
                         <div class="card-box" >
                             <h4 class="m-t-0 header-title"><?php echo (!empty($index_subheading))? $$index_subheading : '';  ?></h4>
                     <!-- end inner required -->
                             
                             <div class="pull-right"> <a onclick="new_data();" class='btn btn-sm btn-success' href="javascript:void('create Country')" ><i class="mdi mdi-library-plus"></i> <span>New</span></a> <?php /*<a class='btn btn-sm btn-success' href="<?php echo site_url('auth/create_group');  ?>"><i class=" mdi mdi-account-multiple-plus"></i> <span>New Group</span></a> */ ?> </div><br>
<div class="text-pink font-14 m-b-20"><?php echo (!empty($message))? $message  : ''; ?></div>
<form action="<?php echo site_url('admin/location/country'); ?>" method="GET"> 
      
        <table width="50%">
            <tbody>
                <tr>
                    <td>
        <div >
        <label for="field-3on" class="control-label">Country Name/Id</label>
       <input type="text" placeholder="Search by Name/Id" value="<?php echo $sear_key; ?>" class="form-control select2" name="search_data" id="field-3on">
            
          </div>
                    </td> 
                    <td style="padding-top: 25px;padding-left: 6px;">
            <button type="submit" class="btn btn-primary">search</button>
                    </td>
                </tr>
            </tbody>
          </table>
 </form>  <br>      
<div class="table-responsive" data-pattern="priority-columns" >
<table class="table" id="example">
    <thead class="thead-default">
	<tr>
       
        <th>Country name</th>
        <?php if($this->ion_auth->is_admin()){ ?>
        <th>Action</th>
        <?php  } ?>
	</tr>
    </thead>
    <tfoot id="tbdy">
	<?php  foreach ($alldata as $cnt){ ?>
		<tr>
            
            <td><span id='main_dat<?php echo $cnt['id'];  ?>'  ><?php echo $cnt['countryname']; ?></span></td>
            <?php if($this->ion_auth->is_admin()){ ?>
            <td><a href="javascript:void('Edit')" class="btn btn-icon btn-sm btn-primary"  onclick="edit_data('<?php echo $cnt['id']; ?>',)" class=""><i class="fa fa-edit"></i></a>
            <a  href="<?php echo site_url('admin/location/delete_country/'.$cnt['id']); ?>" class='dlt btn btn-icon btn-sm btn-danger' ><i class="fa fa-trash"></i></a>
            <a  href="<?php echo site_url('admin/location/country_page/'.$cnt['id']); ?>" class='btn btn-icon btn-sm btn-warning' >Country Page</a>
            </td>
            <?php  } ?>
		</tr>
	<?php }  ?>
      
    </tfoot>
</table>
                             </div>
                </div>
            </div>
</section>
</div>
</div>
<script>
       function new_data(id=''){
           
          $('#countrydata').val('');
           $('#countryid').val('');
           $('#edit_modal').modal('show');
           $('#valmsg').text('');
           $('#sbmt').removeAttr('disabled');
           $('#sbmt').attr('onclick',"newData();");
            }
    
    
    
     function newData(){
        
        var countrydat = $('#countrydata').val();
        
        if(countrydat !=''){
            $('#sbmt').attr('disabled', 'disabled');
            
            $('#valmsg').text('please wait...');
            $.post('<?php echo site_url('admin/location/new_country') ?>',{cname: countrydat},function(resp,status){
                
                if(status == 'success'){
                    resp =JSON.parse(resp); 
                    newid = resp.id;
                    newcntry = resp.newcntry;
                $('#sbmt').removeAttr('disabled');
                if(resp.status == '1'){
                    $('#countrydata').val('');
                   $('#valmsg').text(resp.message);
                    var newrows ='<tr><td><span id="main_dat'+newid+'">'+newcntry+'</span></td><td><a href="javascript:void("Edit")" class="btn btn-icon btn-sm btn-primary" onclick="edit_data('+newid+',)"><i class="fa fa-edit"></i></a><a href="<?php echo site_url('admin/location/delete_country/'); ?>/'+newid+'" class="dlt btn btn-icon btn-sm btn-danger"><i class="fa fa-trash"></i></a><a  href="<?php echo site_url('admin/location/country_page/'); ?>/'+newid+'" class=" btn btn-icon btn-sm btn-warning" >Country Page</a></td></tr>';
                    
                   $('#tbdy').prepend(newrows);
                    //$('#edit_modal').modal('hide');
                   /// $('#sbmt').removeAttr('onclick');
                }else{
                   
                  $('#valmsg').text(resp.message);  
                }
                }
                
            });
        
        }else{
          $('#valmsg').text('This field required');
           $('#countrydata').focus(); 
        }
        
        
    }
    
    
    
    
        function edit_data(id){
           
            
           var country = $('#main_dat'+id).text();
           $('#countrydata').val(country);
           $('#countryid').val(id);
           $('#edit_modal').modal('show');
           $('#valmsg').text('');
           $('#sbmt').removeAttr('disabled');
           $('#sbmt').attr('onclick',"updatedata();");
            }
            
    
    function updatedata(){
        
        var countrydat = $('#countrydata').val();
        var countryid = $('#countryid').val();
        
        if(countrydat !=''){
            $('#sbmt').attr('disabled', 'disabled');
            
            $('#valmsg').text('please wait...');
            $.post('<?php echo site_url('admin/location/update_country') ?>',{cname: countrydat,cid:countryid},function(resp,status){
                
                if(status == 'success'){
                    resp =JSON.parse(resp); 
                $('#sbmt').removeAttr('disabled');
                if(resp.status == '1'){
                    
                   $('#valmsg').text(resp.message);
                   $('#main_dat'+countryid).text(countrydat);
                    $('#edit_modal').modal('hide');
                    ///$('#sbmt').removeAttr('onclick');
                }else{
                   
                  $('#valmsg').text(resp.message);  
                }
                }
                
            });
        
        }else{
          $('#valmsg').text('This field required');
           $('#countrydata').focus(); 
        }
        
        
    }
    
    
    
    

  $('a.dlt').click(function(evt){
           evt.preventDefault();
          if(confirm("!Are You Sure ?")){       
               var dis = this;
              $(dis).text('wait...');
                $.post($(dis).attr('href'),{'delete':'dlt'},function(resp){
                    if(resp == 1){
                        $(dis).parent().parent().remove();
                    }else{
                        $(dis).html('<i class="mdi mdi-delete-forever"></i>');
                       alert(resp);
                    }
                });
            }
        });

       </script>     
            
            <!--  MODAL -->
            
  <div id="edit_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Country</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="field-3" class="control-label">Country</label>
                            <input type="text" class="form-control" id="countrydata" placeholder="Country">
                            <input type="hidden" id="countryid" value="">
                        </div>
                        <p id="valmsg" class="text-danger"></p>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                <button id="sbmt" type="button"  class="btn btn-info waves-effect waves-light">Save changes</button>
            </div>
        </div>
    </div>
                            </div><!-- /.modal -->
          
            