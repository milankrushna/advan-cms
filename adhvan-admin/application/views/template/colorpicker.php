


  <script src="<?php echo base_url().'assets/plugins/jscolor/jscolor.min.js';?>"></script>

  <!--colorpicker--> 
  <div class="content-wrapper">
     <section class="content-header">
        <h1>Template Color</h1>
      </section>
<section class="content">
<div class="box box-primary ">
<div class="box-body " >

               
          <div class="content col-lg-8">       
  
               
<form action="<?= site_url('admin/template/color_picker') ?>" method="post" id="loginform">
     
     
                  <?php
                //  print('<pre>');     
                // print_r($select_color);
                
                     $getcolor=$select_color['template'];
                    // echo $getcolor;
                     $color_pick=unserialize($getcolor);
                    // print_r($color_pick);        
                     
                      
                  ?>
                 <div class="form-group">
                 <label for="text1" class="control-label">Menu Background</label>
                 
                    <div class="input-group my-colorpicker2">
                      <input type="text" class="jscolor" name="temp[menu_bg]" value="<?php if(!empty($color_pick['menu_bg'])){ echo $color_pick['menu_bg']; }  ?>">
                      
                      
                    </div><!-- /.input group -->
                      <span style="color: #ff0000"> <?php echo form_error('temp[menu_bg]'); ?></span>
                  </div>
                  
                   <div class="form-group">
                 <label for="text1" class="control-label">Banner Dot</label>
                 
                    <div class="input-group my-colorpicker2">
                      <input type="text" class="jscolor" name="temp[banner_dot]" value="<?php if(!empty($color_pick['banner_dot'])){ echo $color_pick['banner_dot']; }  ?>">
                        
                      
                    </div><!-- /.input group -->
                    <span style="color: #ff0000"> <?php echo form_error('temp[banner_dot]'); ?></span>
                  </div>
                  
                   
                   <div class="form-group">
                 <label for="text1" class="control-label">Top Button</label>
                 
                    <div class="input-group my-colorpicker2">
                      <input type="text" class="jscolor" name="temp[top_btn]" value="<?php if(!empty($color_pick['top_btn'])){ echo $color_pick['top_btn']; }  ?>">
                    
                      
                    </div><!-- /.input group -->
                    <span style="color: #ff0000">  <?php echo form_error('temp[top_btn]'); ?></span>
                  </div>
                  
                   <div class="form-group">
                 <label for="text1" class="control-label">Top Button Hover</label>
                 
                    <div class="input-group my-colorpicker2">
                      <input type="text" class="jscolor" name="temp[top_btn_hvr]" value="<?php if(!empty($color_pick['top_btn_hvr'])){ echo $color_pick['top_btn_hvr']; }  ?>">
                      
                      
                    </div><!-- /.input group -->
                    <span style="color: #ff0000"> <?php echo form_error('temp[top_btn_hvr]'); ?></span>
                  </div>
                  
                  
                   <div class="form-group">
                 <label for="text1" class="control-label">Inner Head</label>
                 
                    <div class="input-group my-colorpicker2">
                      <input type="text" class="jscolor" name="temp[inr_head]" value="<?php if(!empty($color_pick['inr_head'])){ echo $color_pick['inr_head']; }  ?>">
                    
                      
                    </div><!-- /.input group -->
                     <span style="color: #ff0000"> <?php echo form_error('temp[inr_head]'); ?></span>
                  </div>
                   
                   
                   <div class="form-group">
                 <label for="text1" class="control-label">Home Head</label>
                 
                    <div class="input-group my-colorpicker2">
                      <input type="text" class="jscolor" name="temp[home_head]" value="<?php if(!empty($color_pick['home_head'])){ echo $color_pick['home_head']; }  ?>">
                   
                     
                    </div><!-- /.input group -->
                      <span style="color: #ff0000"> <?php echo form_error('temp[home_head]'); ?></span>
                  </div>
                  
                  
                   <div class="form-group">
                 <label for="text1" class="control-label">Font Color</label>
                 
                    <div class="input-group my-colorpicker2">
                      <input type="text" class="jscolor" name="temp[font_clr]" value="<?php if(!empty($color_pick['font_clr'])){ echo $color_pick['font_clr']; }  ?>">
                    
                      
                    </div><!-- /.input group -->
                    <span style="color: #ff0000"> <?php echo form_error('temp[font_clr]'); ?></span>
                  </div>
                  
                  
                   <div class="form-group">
                 <label for="text1" class="control-label">Left Menu</label>
                 
                    <div class="input-group my-colorpicker2">
                      <input type="text" class="jscolor" name="temp[left_menu]" value="<?php if(!empty($color_pick['left_menu'])){ echo $color_pick['left_menu']; }  ?>">
                      
                      
                    </div><!-- /.input group -->
                    <span style="color: #ff0000"> <?php echo form_error('temp[left_menu]'); ?></span>
                  </div>
                  
                  
                   <div class="form-group">
                 <label for="text1" class="control-label">Link Color</label>
                 
                    <div class="input-group my-colorpicker2">
                      <input type="text" class="jscolor" name="temp[link_clr]" value="<?php if(!empty($color_pick['link_clr'])){ echo $color_pick['link_clr']; }  ?>">
                    
                      
                    </div><!-- /.input group -->
                     <span style="color: #ff0000"> <?php echo form_error('temp[link_clr]'); ?></span>
                  </div>
               
               
               
                   <div class="form-group">
                 <label for="text1" class="control-label">Link Color Hover</label>
                 
                    <div class="input-group my-colorpicker2">
                    
                      <input class="jscolor" type="text" class="form-control" name="temp[link_clr_hvr]" value="<?php if(!empty($color_pick['link_clr_hvr'])){ echo $color_pick['link_clr_hvr']; }  ?>" >
                      
                      
                    </div><!-- /.input group -->
                    <span style="color: #ff0000"> <?php echo form_error('temp[link_clr_hvr]'); ?></span>
                  </div>
                   
                   
                   <div class="form-group">
                 <label for="text1" class="control-label">Link Button</label>
                 
                    <div class="input-group my-colorpicker2">
                    
                  <input type="text" class="jscolor" name="temp[link_btn]" value="<?php if(!empty($color_pick['link_btn'])){ echo $color_pick['link_btn']; }  ?>" >
                  
                      
                    </div><!-- /.input group -->
                    <span style="color: #ff0000"> <?php echo form_error('temp[link_btn]'); ?></span>
                  </div>
                  
                  
                  <div class="form-group">
                 <label for="text1" class="control-label">Link Button Hover</label>
                 
                    <div class="input-group my-colorpicker2">
                     
                      <input type="text" class="jscolor" name="temp[link_btn_hvr]" value="<?php if(!empty($color_pick['link_btn_hvr'])){ echo $color_pick['link_btn_hvr']; }  ?>" >
                     
                      
                    </div><!-- /.input group -->
                    <span style="color: #ff0000"> <?php echo form_error('temp[link_btn_hvr]'); ?></span>
                  </div>
                  
                  
                   <div class="form-group">
                 <label for="text1" class="control-label">Content Area</label>
                 
                    <div class="input-group my-colorpicker2">
                    
                      <input type="text" class="jscolor" name="temp[content_area]" value="<?php if(!empty($color_pick['content_area'])){ echo $color_pick['content_area']; }  ?>" >
                      
                      
                    </div><!-- /.input group -->
                      <span style="color: #ff0000"> <?php echo form_error('temp[content_area]'); ?></span>
                  </div>
                  
                  <div class="form-group">
                 <label for="text1" class="control-label">Background Color</label>
                 
                    <div class="input-group my-colorpicker2">
                    
                      <input type="text" class="jscolor" name="temp[bg_clr]" value="<?php if(!empty($color_pick['bg_clr'])){ echo $color_pick['bg_clr']; }  ?>" >
                      
                      
                    </div><!-- /.input group -->
                      <span style="color: #ff0000"> <?php echo form_error('temp[bg_clr]'); ?></span>
                  </div>
                  
                 
                  <div class="form-group">
                 <label for="text1" class="control-label">Footer Color</label>
                 
                    <div class="input-group my-colorpicker2">
                     
                      <input type="text" class="jscolor" name="temp[footer_clr]" value="<?php if(!empty($color_pick['footer_clr'])){ echo $color_pick['footer_clr']; }  ?>" >
                    
                      
                    </div><!-- /.input group -->
                      <span style="color: #ff0000"> <?php echo form_error('temp[footer_clr]'); ?></span>
                  </div>
                  
                  
                   <div class="form-group">
                 <label for="text1" class="control-label">Right Side Tap Color</label>
                 
                    <div class="input-group my-colorpicker2">
        
                      <input type="text" class="jscolor" name="temp[top_clr]" value="<?php if(!empty($color_pick['top_clr'])){ echo $color_pick['top_clr']; }  ?>" >
                    
                
                    </div><!-- /.input group -->
                     <span style="color: #ff0000"/> <?php echo form_error('temp[top_clr]'); ?>
                  </div>
                  
                 
                  
                   <input type="submit" id="tags" value="Submit" class="btn btn-success" />
                   </form>
     </div>
     
     </div>
   </div>  
 </section> 
    
   </div>
    
      <script>
      
      
      	   $(function () {
      
      	 $(".my-colorpicker2").colorpicker();
      	
      	});
      </script>            