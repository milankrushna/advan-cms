<style>
    .social{
            padding: 0;
    padding-left: 33px;
    }
</style>


    <!-- END PAGE LEVEL  STYLES -->
     <!--PAGE CONTENT --> 
    <div class="content-wrapper">
              <section class="content-header">
                  <center><h1>WELCOME</h1></center>
         
        </section>
        <section class="content">
<div class="box box-primary">
<div class="box-body" >
    <div class="col-md-3"></div>
    
    <div class="col-md-6">
              <!-- USERS LIST -->
              <div class="box box-danger">
                <div class="box-header with-border">
                    <center><h3 class="box-title">Website Detail</h3></center>

                 
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                 
                      
                      
                      <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post" enctype="multipart/form-data">
              <div class="box-body">
                  <?php echo validation_errors(); ?>
                <div class="form-group">
                    <center><img src="<?php echo base_url() ?>assets/user/images/<?php echo $website_data->logo ?>" alt="User Image"></center><br>
                  <label for="inputEmail3" class="control-label">Website Logo</label>

                  <div class="">
                    <input type="file" name="logo" class="form-control"  id="" placeholder="Website Title" >
                  </div>
                </div>
                  <div class="form-group">
                  <label for="inputEmail3" class="control-label">Website Title</label>

                  <div class="">
                    <input type="text" name="title" value="<?php echo $website_data->title ?>" class="form-control" id="inputEmail3" placeholder="Website Title" required>
                  </div>
                </div>
                  <div class="form-group">
                  <label for="inputEmail3" class="control-label">Admin Title</label>

                  <div class="">
                    <input type="text" name="name" value="<?php echo $website_data->name ?>" class="form-control" id="inputEmail3" placeholder="Website Title" required>
                  </div>
                </div>
                  <div class="form-group">
                  <label for="inputEmail3" class="control-label">Meta Title</label>

                  <div class="">
                      <textarea  style="resize:none" name="meta_title" class="form-control" id="inputEmail3" placeholder="Meta Title" ><?php echo $website_data->meta_title; ?></textarea>
                  </div>
                </div>
                  
                  <div class="form-group">
                  <label for="inputEmail3" class="control-label">Meta Description</label>

                  <div class="">
                      <textarea  style="resize:none" name="meta_description" class="form-control" id="inputEmail3" placeholder="Meta Description" ><?php echo $website_data->meta_description; ?></textarea>
                  </div>
                </div>
                  
                  <!--<div class="form-group">
                  <label for="inputEmail3" class="control-label">Meta keyword</label>

                  <div class="">
                      <textarea  style="resize:none" name="meta_keyword" class="form-control" id="inputEmail3" placeholder="Meta keyword" ><?php echo $website_data->meta_keyword; ?></textarea>
                  </div>
                </div>
                  <div class="form-group">
                  <label for="inputEmail3" class="control-label">Meta Description</label>

                  <div class="">
                      <textarea  style="resize:none" name="meta_description" class="form-control" id="inputEmail3" placeholder="Meta Description" ><?php echo $website_data->meta_description; ?></textarea>
                  </div>
                </div>
                  <div class="form-group">
                  <label for="inputEmail3" class="control-label">Robots</label>

                  <div class="">
                      <textarea  style="resize:none" name="robots" class="form-control" id="inputEmail3" placeholder="robots" ><?php echo $website_data->robots; ?></textarea>
                  </div>
                </div>
                  
                  <div class="form-group">
                  <label for="inputEmail3" class="control-label">Meta Author</label>

                  <div class="">
                      <input type="text" name="meta_author" value="<?php echo $website_data->meta_author; ?>" class="form-control" id="inputEmail3" placeholder="Auther" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="control-label">Ratings</label>

                  <div class="">
                      <textarea  style="resize:none" name="rating" class="form-control" id="inputEmail3" placeholder="rating" ><?php echo $website_data->rating; ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="control-label">Language</label>

                  <div class="">
                      <textarea  style="resize:none" name="language" class="form-control" id="inputEmail3" placeholder="language" ><?php echo $website_data->language; ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="control-label">Verify-v1</label>

                  <div class="">
                      <textarea  style="resize:none" name="verify_v1" class="form-control" id="inputEmail3" placeholder="verify_v1" ><?php echo $website_data->verify_v1; ?></textarea>
                  </div>
                </div>
               <div class="form-group">
                  <label for="inputEmail3" class="control-label">y_key</label>

                  <div class="">
                      <textarea  style="resize:none" name="y_key" class="form-control" id="inputEmail3" placeholder="y_key" ><?php echo $website_data->y_key; ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="control-label">Msvalidate.01</label>

                  <div class="">
                      <textarea  style="resize:none" name="msvalidate" class="form-control" id="inputEmail3" placeholder="msvalidate" ><?php echo $website_data->msvalidate; ?></textarea>
                  </div>
                  <div class="form-group">
                  <label for="inputEmail3" class="control-label">Netinsert</label>

                  <div class="">
                      <textarea  style="resize:none" name="netinsert" class="form-control" id="inputEmail3" placeholder="netinsert" ><?php echo $website_data->netinsert; ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="control-label">Content type</label>

                  <div class="">
                      <textarea  style="resize:none" name="content_type" class="form-control" id="inputEmail3" placeholder="content type" ><?php echo $website_data->content_type; ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="control-label">Copyright</label>

                  <div class="">
                      <textarea  style="resize:none" name="copyright" class="form-control" id="inputEmail3" placeholder="copyright" ><?php echo $website_data->copyright; ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="control-label">Google-site-verification</label>

                  <div class="">
                      <textarea  style="resize:none" name="google_site_verification" class="form-control" id="inputEmail3" placeholder="google site verification" ><?php echo $website_data->google_site_verification; ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="control-label">Viewport</label>

                  <div class="">
                      <textarea  style="resize:none" name="viewport" class="form-control" id="inputEmail3" placeholder="viewport" ><?php echo $website_data->viewport; ?></textarea>
                  </div>
                </div>
                  <div class="form-group">
                  <label for="inputEmail3" class="control-label">apple-mobile-web-app-status-bar-style</label>
                  <div class="">
                      <input type="text" name=" appsts" value="<?php echo $website_data-> appsts; ?>" class="form-control" id="inputEmail3" placeholder=" appsts" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="control-label">apple-mobile-web-app-capable</label>
                  <div class="">
                      <input type="text" name=" app_cpl" value="<?php echo $website_data-> app_cpl; ?>" class="form-control" id="inputEmail3" placeholder=" app_cpl" >
                  </div>
                </div>
                 
                  <div class="form-group">
                  <label for="inputEmail3" class="control-label">Website Name</label>

                  <div class="">
                    <input type="text" name="name" value="<?php echo $website_data->name ?>" class="form-control" id="inputEmail3" placeholder="Website Name" required>
                  </div>
                </div>
                  <div class="form-group">
                  <label for="inputEmail3" class="control-label">Admin panel Title</label>

                  <div class="">
                    <input type="text" name="admin_panel_name" value="<?php echo $website_data->admin_panel_name ?>" class="form-control" id="inputEmail3" placeholder="Website Name" required>
                  </div>
                </div>
                  <div class="form-group">
                  <label for="inputEmail3" class="control-label">Description</label>

                  <div class="">
                    <input type="text" name="description" value="<?php echo $website_data->description ?>" class="form-control" id="inputEmail3" placeholder="Website Name" required>
                  </div>
                </div>-->
                  <div class="form-group">
                  <label for="inputEmail3" class="control-label">Address</label>

                  <div class="">
                      <textarea  style="resize:none" name="address" class="form-control" id="inputEmail3" placeholder="Website Address" required><?php echo $website_data->address ?></textarea>
                  </div>
                </div>
                 
                  <div class="form-group">
                  <div class="">
                      <a class="btn btn-block btn-social btn-facebook social">
                    <i class="fa fa-phone"></i><input type="text" name="phone" value="<?php echo $website_data->phone; ?>" class="form-control" id="inputEmail3" placeholder="123456789" >
                  </a>
                    
                  </div>
                </div>
                  <div class="form-group">
                  <div class="">
                      <a class="btn btn-block btn-social btn-facebook social">
                    <i class="fa fa-envelope-o"></i><input type="text" name="email" value="<?php echo $website_data->email; ?>" class="form-control" id="inputEmail3" placeholder="https://www.facebook.com" >
                  </a>
                    
                  </div>
                </div> 
                  <div class="form-group">
                  <div class="">
                      <a class="btn btn-block btn-social btn-facebook social">
                    <i class="fa fa-facebook"></i><input type="text" name="facebook" value="<?php echo $website_data->facebook; ?>" class="form-control" id="inputEmail3" placeholder="https://www.facebook.com" >
                  </a>
                    
                  </div>
                </div>
                  <div class="form-group">
                  <div class="">
                      <a class="btn btn-block btn-social btn-google social">
                    <i class="fa fa-google-plus"></i><input type="text" name="google" value="<?php echo $website_data->google ?>" class="form-control" id="inputEmail3" placeholder="https://www.google.com" >
                  </a>
                    
                  </div>
                </div>
                  <div class="form-group">
                  <div class="">
                      <a class="btn btn-block btn-social btn-twitter social">
                    <i class="fa fa-twitter"></i><input type="text" name="twitter" value="<?php echo $website_data->twitter ?>" class="form-control" id="inputEmail3" placeholder="https://www.twitter.com" >
                  </a>
                    
                  </div>
                </div>
             <div class="form-group">
                  <div class="">
                      <a class="btn btn-block btn-social btn-twitter social">
                    <i class="fa  fa-tumblr-square"></i><input type="text" name="tumblr" value="<?php echo $website_data->tumblr; ?>" class="form-control" id="inputEmail3" placeholder="https://www.tumblr.com" >
                  </a>
                    
                  </div>
                </div>
                  <div class="form-group">
                  <div class="">
                      <a class="btn btn-block btn-social btn-google btn-blog social">
                    <i class="fa fa-pinterest"></i><input type="text" name="pinterest" value="<?php echo $website_data->pinterest; ?>" class="form-control" id="inputEmail3" placeholder="https://in.pinterest.com/" >
                  </a>
                    
                  </div>
                </div>
                 <!-- <div class="form-group">
                  <div class="">
                      <a class="btn btn-block btn-social btn-linkedin social">
                    <i class="fa fa-wordpress"></i><input type="text" name="skype" value="<?php echo $website_data->skype ?>" class="form-control" id="inputEmail3" placeholder="https://www.wordpress.com" >
                  </a>
                    
                  </div>
                </div>-->
                  <div class="form-group">
                  <div class="">
                      <a class="btn btn-block btn-social btn-instagram social">
                    <i class="fa fa-instagram"></i><input type="text" name="instagram" value="<?php echo $website_data->instagram ?>" class="form-control" id="inputEmail3" placeholder="Your instagram link" >
                  </a>
                    
                  </div>
                </div> 
                  
                  <div class="form-group">
                  <div class="">
                      <a class="btn btn-block btn-social btn-google btn-blog social">
                    <i class="fa fa-youtube"></i><input type="text" name="youtube" value="<?php echo $website_data->youtube ?>" class="form-control" id="inputEmail3" placeholder="Yout Blogor link" >
                  </a>
                    
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">

                <button onclick="return confirm('Are You Sure')" type="submit" class="btn btn-info pull-right">Update</button>
              </div>
              <!-- /.box-footer -->
            </form>

                 
                </div>
              
              </div>
              <!--/.box -->
            </div>
<div class="col-md-3"></div>
<div class="col-md-12" ><div class="box box-primary">
                <div class="box-header">
<!--                  <h3 class="box-title"><i class="fa fa-code"></i> Timeline Markup</h3>-->
                </div>
                <div class="box-body">
                  <pre style="font-weight: 600;"><?php /* if(!empty($webdata->facebook)){ ?>
                        <li><a target="_blank" href="<?php echo $webdata->facebook; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <?php } ?>
                        <?php  if(!empty($webdata->google)){ ?> 
                        <li><a target="_blank" href="<?php echo $webdata->google; ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        <?php } ?>
                        <?php  if(!empty($webdata->twitter)){ ?>
                        <li><a target="_blank" href="<?php echo $webdata->twitter; ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <?php } ?>
                        <?php  if(!empty($webdata->linkdin)){ ?>  
                        <li><a target="_blank" href="<?php echo $webdata->linkdin; ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        <?php } ?>
                        <?php  if(!empty($webdata->youtube)){ ?> 
                        <li><a target="_blank" href="<?php echo $webdata->youtube; ?>"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                        <?php }?>
                        <?php  if(!empty($webdata->insta)){ ?> 
                        <li><a target="_blank" href="<?php echo $webdata->insta; ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        <?php }?>
                        <?php  if(!empty($webdata->skype)){ ?> 
                        <li><a target="_blank" href="<?php echo $webdata->skype; ?>"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                        <?php }?>
                        <?php  if(!empty($webdata->skype)){ ?> 
                        <li><a target="_blank" href="<?php echo $webdata->linkdin; ?>"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                        <?php }   */?>
                             </pre>
                </div><!-- /.box-body -->
              </div></div>
    
</div>
</div>
 </section>
    </div>
          