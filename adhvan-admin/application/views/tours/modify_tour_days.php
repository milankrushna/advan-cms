<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/ajxupload/upload.css"
/>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/ajxupload/jquery.form.js"></script>

<link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/datepicker/datepicker3.css">
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>

<link rel="stylesheet" href="<?php echo base_url()?>assets/jquery.tag-editor.css">
<!-- END PAGE LEVEL  STYLES -->
<!--PAGE CONTENT -->

<style>
.image-thumb img {
    border: 2px solid;
    width: 100%;
    height: 100px;
}
.p-0{
    padding:0;
}
</style>

<div class="content-wrapper">
	<section class="content-header">
		<h1>Create Article</h1>

	</section>
	<section class="content">
		<div class="box box-primary">
			<div class="box-body">
				<div class="infoMessage" style="color:#F00">
					<?php // echo $message;?>
				</div>
				<form action="<?php echo  current_url(); ?>" enctype="multipart/form-data"
				 method="post" id="page_create">

				<input type="hidden" value="<?php echo $tourId; ?>" name="tour_id">
					<div class="col-md-12">
                    <?php for ($x = 1; $x <= $tours->day; $x++){ 
						
						//print_r($dayData[$x]['title']);exit;
						?>
					<input type="hidden" name="tour_high[<?php echo $x; ?>][day_no]" value="<?php echo $x; ?>">
                    <div class="col-md-6">
                    <div class="box box-success">
                    <h3 class="text-center">Day  <?php echo $x; ?> </h3>
						<div class="col-md-12 p-0">
							<label for="text1" class="control-label">Title day <?php echo $x; ?></label>

							<input value="<?php echo (!empty($dayData[$x])) ? $dayData[$x]['title'] : ""; ?>" type="text" class="form-control" placeholder="Title" name="tour_high[<?php echo $x; ?>][title]" required="">
							<br>

						</div>
						<div class="col-md-12 p-0">
							<label for="day" class="control-label">Location Details day <?php echo $x; ?> </label>
							<div class="form-group">
							<input type="text" value="<?php echo !empty($dayData[$x]) ? $dayData[$x]['day_title'] : ""; ?>"  name="tour_high[<?php echo $x; ?>][day_title]" placeholder="Day" class="form-control">
							</div>
							
						</div>
						<div class="col-md-12 p-0">
							<label for="day" class="control-label">Trip details day <?php echo $x; ?></label>
							<div class="form-group">
							<textarea id="tdd_<?php echo $x; ?>" rows="3" name="tour_high[<?php echo $x; ?>][description]" class="form-control"><?php echo !empty($dayData[$x]) ? $dayData[$x]['description'] : ""; ?></textarea>
							</div>
							
						</div>
						</div>
                        </div>
                    <?php  } ?>
					</div>

					<div class="infoMessage" style="color:#F00">
						<?php //echo $message;?>
					</div>
	<div class="form-group">
					<button type="submit"  class="btn btn-block btn-success SubmitButton">Save </button>
					</div>
				</form>


			</div>
		</div>
	</section>
</div>

<!-- END PAGE CONTENT -->

<script src="<?php echo base_url()?>assets/jquery.tag-editor.min.js"></script>
<script src="<?php echo base_url()?>assets/jquery.caret.min.js"></script>

<script src="<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/ckfinder.js"></script>

<script>


<?php for ($x = 1; $x <= $tours->day; $x++){ ?>
	var sht_editor = CKEDITOR.replace('tdd_<?php echo $x;  ?>', {

		height: 250,
		filebrowserBrowseUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/ckfinder.html',
		filebrowserUploadUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserWindowWidth: '1000',
		filebrowserWindowHeight: '700'
	});
<?php  } ?>
</script>