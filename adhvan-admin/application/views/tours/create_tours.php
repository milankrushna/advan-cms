<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/ajxupload/upload.css"
/>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/ajxupload/jquery.form.js"></script>

<link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/datepicker/datepicker3.css">
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>

<link rel="stylesheet" href="<?php echo base_url()?>assets/jquery.tag-editor.css">
<!-- END PAGE LEVEL  STYLES -->
<!--PAGE CONTENT -->

<style>
.image-thumb img {
    border: 2px solid;
    width: 100%;
    height: 100px;
}</style>

<div class="content-wrapper">
	<section class="content-header">
		<h1>Create Tours</h1>

	</section>
	<section class="content">
		<div class="box box-primary">
			<div class="box-body">
				<div class="infoMessage" style="color:#F00">
					<?php // echo $message;?>
				</div>
				<form action="<?php echo  current_url(); ?>" enctype="multipart/form-data"
				 method="post" id="page_create">



					<div class="col-md-12">

						<div class="col-md-4 form-group">
							<label for="text1" class="control-label">Country</label>
							<select class="form-control" onchange="get_city(this.value);" id="" name="country" required>
								<option value="">Select a Country</option>


								<?php 
                            foreach($country as $cat){ ?>
								<option value="<?php echo $cat['id']; ?>">
									<?php  echo $cat['countryname']; ?>
								</option>
								<?php 
                             }
                            ?>

							</select>
						</div>

						<div class="col-md-4 form-group">
							<label for="text1" class="control-label">City</label>
							<select onchange="get_sub_city(this.value)" class="form-control" name="city" id="subcatgy">
								<option value="">Select a City</option>
							</select>
						</div>
						<div class="col-md-4 form-group">
							<label for="text1" class="control-label">Destination</label>
							<select class="form-control" name="sub_city" id="subcity">
								<option value="">Select a Destination</option>
							</select>
						</div>
					</div>

					<div class="col-md-12">
					<div class="col-md-4 form-group">
							<label for="text1" class="control-label">Region</label>
							<select class="form-control"  id="" name="region_id" required>
								<option value="">Select a Region</option>


								<?php 
                            foreach($region as $cat){ ?>
								<option value="<?php echo $cat['id']; ?>">
									<?php  echo $cat['name']; ?>
								</option>
								<?php 
                             }
                            ?>

							</select>
						</div>
						<div class="col-md-4 form-group">
							<label for="text1" class="control-label">Responsible benifits</label>
							<select class="form-control"  id="" name="responsible_id" required>
								<option value="">Select a Responsible benifits</option>


								<?php 
                            foreach($responsible_benifits as $cat){ ?>
								<option value="<?php echo $cat['id']; ?>">
									<?php  echo $cat['name']; ?>
								</option>
								<?php 
                             }
                            ?>

							</select>
						</div>

						<div class="col-md-4 form-group">
							<label for="text1" class="control-label">Tour style</label>
							<select class="form-control"  id="" name="tours_style_id" required>
								<option value="">Select a Tour style</option>


								<?php 
                            foreach($tour_style as $cat){ ?>
								<option value="<?php echo $cat['id']; ?>">
									<?php  echo $cat['name']; ?>
								</option>
								<?php 
                             }
                            ?>

							</select>
						</div>
					
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label for="text1" class="control-label">Tour Name</label>

							<input type="text" class="form-control" placeholder="Article Name" name="name" required="">
							<br>

						</div>
						<div class="col-md-3 ">
							<label for="day" class="control-label">Day</label>
							<div class="form-group">
							<input type="number" min="1" name="day" placeholder="Day" class="form-control">
							</div>
							
						</div>
						<div class="col-md-3 ">
							<label for="night" class="control-label">Night</label>
							<div class="form-group">
							<input type="number" min="1" name="night" placeholder="night" class="form-control">
							</div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="col-md-6">
							<label for="text1" class="control-label">Overview Title</label>
							<input type="text" class="form-control" placeholder="Overview Title" name="overview_title">
						</div>
						<div class="col-md-6">
							<label for="text1" class="control-label">Overview Description</label>
							<textarea class="form-control" placeholder="Overview Description" name="content"></textarea>
						</div>
					</div>

					<div class="col-md-12">
						<div class="col-md-12 form-group">
							<label for="text1" class="control-label">Map URL</label>
							<input type="text" class="form-control" placeholder="Map URL" name="map_url">
						</div>

					</div>

				
					<div class="col-md-12">
						<div class="col-md-4 form-group">
							<label for="">TRIP HIGHLIGHTS 1</label>
							<input type="text" name="tour_high[0][title]" class="form-control">
							<br>
							<label for="">TRIP HIGHLIGHTS1 Description</label>

							<textarea id="" rows="3"  name="tour_high[0][description]" class="form-control"></textarea>
						</div>
						<div class="col-md-4 form-group">
							<label for="">TRIP HIGHLIGHTS 2</label>
							<input type="text" name="tour_high[1][title]" class="form-control">
							<br>
							<label for="">TRIP HIGHLIGHTS2 Description</label>

							<textarea  id="" name="tour_high[1][description]" rows="3" class="form-control"></textarea>
						</div>
						<div class="col-md-4 form-group">
							<label for="">TRIP HIGHLIGHTS 3</label>
							<input type="text" name="tour_high[2][title]" class="form-control">
							<br>
							<label for="">TRIP HIGHLIGHTS3 Description</label>
							<textarea id="" rows="3" name="tour_high[2][description]" class="form-control"></textarea>
						</div>

					</div>

					<div class="col-md-12">

						<div class="col-md-6 form-group">
							<label for="">Services</label>
							<textarea name="services" id="services"></textarea>
						</div>
						<div class="col-md-6 form-group">
							<label for="">Notes</label>
							<textarea name="notes" id="notes"></textarea>

						</div>

					</div>
				

					<div class="col-md-12">
						<div class="col-md-12 form-group">
							<label for="">Short Description(seperated by
								<b>//</b>)</label>
							<textarea name="tours_point" id="" name="tours_point" rows="3" class="form-control"></textarea>
						</div>
					</div>


	<div class="col-md-12">

<div class="col-md-6">
	<label for="text1" class="control-label">Main Image</label>
	<div class="input-group form-group">
		<input type="text" id="main_image" class="form-control" placeholder="Main image" name="main_image">
		<div></div>
		<!-- this div is image container -->

<a onclick="BrowseServer('main_image','')" class="btn btn-app">
                <i class="fa fa-image"></i> Tour Banner
              </a>

	</div>
</div>
<div class="col-md-6">
	<label for="text1" class="control-label">TRIP HIGHLIGHTS Image</label>
	<div class="input-group form-group">
		<input type="text" id="thi" class="form-control" placeholder="TRIP HIGHLIGHTS Image" name="trip_image">
		<div></div>
		<!-- this div is image container -->
<a onclick="BrowseServer('thi','')" class="btn btn-app">
                <i class="fa fa-image"></i> Highlights Image
              </a>
	</div>
</div>

</div>

					<div class="col-md-12">
					<div id="image_container"></div>
					</div>
					<div class="col-md-12">
				
<div class="col-md-6 form-group">

<a onclick="BrowseServer('','image_container')" class="btn btn-app">
                <i class="fa fa-image"></i> Tour Images
              </a>
</div>
</div>
			
					</div>
					<div class="infoMessage" style="color:#F00">
						<?php //echo $message;?>
					</div>
	<div class="form-group">
					<button type="submit"  class="btn btn-block btn-success SubmitButton">Save </button>
					</div>
				</form>


			</div>
		</div>
	</section>
</div>

<!-- END PAGE CONTENT -->

<script src="<?php echo base_url()?>assets/jquery.tag-editor.min.js"></script>
<script src="<?php echo base_url()?>assets/jquery.caret.min.js"></script>

<script src="<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/ckfinder.js"></script>

<script>


function BrowseServer(settingId,multiDisplay)
{
	var finder = new CKFinder();
	finder.basePath = "../";	
	finder.selectActionFunction = (fileUrl)=>{


		var baseUrl = '<?php echo base_url(); ?>';
		var localUrl = baseUrl.replace("adhvan-admin/",'');
		var main_url =  fileUrl.replace(localUrl, "");
		var thumb = main_url.replace('adhvan/images','adhvan/_thumbs/Images'); 

		if(settingId!=""){
			 $('#'+settingId).val(main_url);
			var imc =  $('#'+settingId).siblings()[0];
			console.log(imc);
			$(imc).html('<div class="image-thumb"><img height="120px" src="'+localUrl+thumb+'"><div class="action"><a onclick="removeMe(this)" alt="'+settingId+'" ><i class="fa fa-trash	"></i></a></div>')
			 }
		
		if(multiDisplay != ""){
		
		
		$("#"+multiDisplay).append('<div class="col-md-2 image-thumb"><input type="text" name="tourImage[]" value='+main_url+'><img height="120px" src="'+localUrl+thumb+'"><div class="action"><a onclick="removeMe(this)" ><i class="fa fa-trash	"></i></a></div>');
	}
	};
	var api = finder.popup();

//upload/adhvan/images/tours/37.jpg
//upload/adhvan/_thumbs/Images/tours/37.jpg

}


function removeMe(dis){
	var altTag = $(dis).attr('alt');
	if(altTag !=""){
		$('#'+altTag).val("");
	}
var cc =$(dis).parent().parent().remove();

}

	var sht_editor = CKEDITOR.replace('services', {

		height: 250,
		filebrowserBrowseUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/ckfinder.html',
		filebrowserUploadUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserWindowWidth: '1000',
		filebrowserWindowHeight: '700'
	});
	var noteEditor = CKEDITOR.replace('notes', {

		height: 250,
		filebrowserBrowseUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/ckfinder.html',
		filebrowserUploadUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserWindowWidth: '1000',
		filebrowserWindowHeight: '700'
	});

	sht_editor.on('change', function(ev) {

		$('#sht_editor').html(sht_editor.getData());

	});

var exitDay = [1];
var noDay = 1;




	$(function() {
		$('#datepicker').datepicker({
			autoclose: true,
			format: "dd-mm-yyyy"
		});
	});


	function get_city(dis) {

		// alert(id);
		$('#category').html('<option value="">Loading...</option>');

		$.get('<?php echo site_url('admin/tours/get_cnt_state/');?>/'+dis,{},
			function(resp) {

				$('#subcatgy').html(resp);

			});

	}
	function get_sub_city(dis) {

		// alert(id);
		$('#subcity').html('<option value="">Loading...</option>');

		$.get('<?php echo site_url('admin/tours/get_state_district/');?>/'+dis,{},
			function(resp) {

				$('#subcity').html(resp);

			});

	}
</script>