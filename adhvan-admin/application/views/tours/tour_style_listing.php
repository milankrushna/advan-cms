                    <style>

                    .modal-dialog {

                        width: 807px;

                    }

                    </style>

                     <!-- start inner required -->

                     <script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>

        <div class="content-wrapper">

            <div class="container-fluid">



                <!-- Page-Title -->

                <section class="content-header">

                    <div class="col-sm-12">

                        <div class="page-title-box">

                            

                            <h4 class="page-title"><?php echo (!empty($index_heading))? $index_heading : '';  ?></h4>

                        </div>

                    </div>

                </section>

                <!-- end page title end breadcrumb -->



                 <section class="content">

                     

                  <div class="col-lg-8" >

                         <div class="card-box" >

                             <h4 class="m-t-0 header-title"><?php echo (!empty($index_subheading))? $$index_subheading : '';  ?></h4>

                     <!-- end inner required -->

                             

                             <div class="pull-right"> <a onclick="new_data();" class='btn btn-sm btn-success' href="javascript:void('create Country')" ><i class="mdi mdi-library-plus"></i> <span>New</span></a> <?php /*<a class='btn btn-sm btn-success' href="<?php echo site_url('auth/create_group');  ?>"><i class=" mdi mdi-account-multiple-plus"></i> <span>New Group</span></a> */ ?> </div><br>

<div class="text-pink font-14 m-b-20"><?php echo (!empty($message))? $message  : ''; ?></div>

<!--<form action="<?php echo site_url('admin/tour/country'); ?>" method="GET"> 

      

        <table width="50%">

            <tbody>

                <tr>

                    <td>

        <div >

        <label for="field-3on" class="control-label">Tour Style</label>

       <input type="text" placeholder="Search by Name/Id" value="<?php echo $sear_key; ?>" class="form-control select2" name="search_data" id="field-3on">

            

          </div>

                    </td> 

                    <td style="padding-top: 25px;padding-left: 6px;">

            <button type="submit" class="btn btn-primary">search</button>

                    </td>

                </tr>

            </tbody>

          </table>

 </form>  <br>-->      

<div class="table-responsive" data-pattern="priority-columns" >

<table class="table" id="example">

    <thead class="thead-default">

	<tr>

       

        <th>Tour Style</th>

        <?php if($this->ion_auth->is_admin()){ ?>

        <th>Action</th>

        <?php  } ?>

	</tr>

    </thead>

    <tfoot id="tbdy">

	<?php  foreach ($alldata as $cnt){ ?>

		<tr>

            

            <td><span id='main_dat<?php echo $cnt['id'];  ?>'  ><?php echo $cnt['name']; ?></span></td>

            <?php if($this->ion_auth->is_admin()){ ?>

            <td><a href="javascript:void('Edit')" class="btn btn-icon btn-sm btn-primary"  onclick="edit_data('<?php echo $cnt['id']; ?>',)" class=""><i class="fa fa-edit"></i></a>

            <a  href="<?php echo site_url('admin/tours/delete_style/'.$cnt['id']); ?>" class='dlt btn btn-icon btn-sm btn-danger' ><i class="fa fa-trash"></i></a></td>

            <?php  } ?>

		</tr>

	<?php }  ?>

      

    </tfoot>

</table>

                             </div>

                </div>

            </div>

</section>

</div>

</div>

<script src="<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/ckfinder.js"></script>

<script>


function BrowseServer(settingId,multiDisplay)
{
	var finder = new CKFinder();
	finder.basePath = "../";	
	finder.selectActionFunction = (fileUrl)=>{


		var baseUrl = '<?php echo base_url(); ?>';
		var localUrl = baseUrl.replace("adhvan-admin/",'');
		var main_url =  fileUrl.replace(localUrl, "");
		var thumb = main_url.replace('adhvan/images','adhvan/_thumbs/Images'); 

		if(settingId!=""){
			 $('#'+settingId).val(main_url);
			var imc =  $('#'+settingId).siblings()[0];
			console.log(imc);
			$(imc).html('<div class="image-thumb"><img height="120px" src="'+localUrl+thumb+'"><div class="action"><a onclick="removeMe(this)" alt="'+settingId+'" ><i class="fa fa-trash	"></i></a></div>')
			 }
		
		if(multiDisplay != ""){
		
		
		$("#"+multiDisplay).append('<div class="col-md-2 image-thumb"><input type="text" name="tourImage[]" value='+main_url+'><img height="120px" src="'+localUrl+thumb+'"><div class="action"><a onclick="removeMe(this)" ><i class="fa fa-trash	"></i></a></div>');
	}
	};
	var api = finder.popup();

//upload/adhvan/images/tours/37.jpg
//upload/adhvan/_thumbs/Images/tours/37.jpg

}


function removeMe(dis){
	var altTag = $(dis).attr('alt');
	if(altTag !=""){
		$('#'+altTag).val("");
	}
var cc =$(dis).parent().parent().remove();

}


       function new_data(id=''){

           

          $('#countrydata').val('');

          $('#stylePageName').val('');

          main_editor.setData("");

           $('#countryid').val('');

           $('#edit_modal').modal('show');

           $('#valmsg').text('');

           $('#sbmt').removeAttr('disabled');

           $('#sbmt').attr('onclick',"newData();");

            }

    

    

    

     function newData(){

        

        var countrydat = $('#countrydata').val();

        var pageTitle = $('#stylePageName').val();
        var main_image = $('#main_image').val();

        var pagecontent = main_editor.getData();

        

        if(countrydat !=''){

            $('#sbmt').attr('disabled', 'disabled');

            

            $('#valmsg').text('please wait...');

            $.post('<?php echo site_url('admin/tours/new_tour_style') ?>',{cname: countrydat,pageTitle :pageTitle,pagecontent : pagecontent,main_image : main_image  },function(resp,status){

                

                if(status == 'success'){

                    resp =JSON.parse(resp); 

                    newid = resp.id;

                    newcntry = resp.newcntry;

                $('#sbmt').removeAttr('disabled');

                if(resp.status == '1'){

                    $('#countrydata').val('');

                    $('#stylePageName').val('');

                    main_editor.setData("");

                   $('#valmsg').text(resp.message);

                    var newrows ='<tr><td><span id="main_dat'+newid+'">'+newcntry+'</span></td><td><a href="javascript:void("Edit")" class="btn btn-icon btn-sm btn-primary" onclick="edit_data('+newid+',)"><i class="fa fa-edit"></i></a><a href="http://localhost/event/index.php/admin/tour/delete_country/'+newid+'" class="dlt btn btn-icon btn-sm btn-danger"><i class="fa fa-trash"></i></a></td></tr>';

                    

                   $('#tbdy').prepend(newrows);

                    //$('#edit_modal').modal('hide');

                   /// $('#sbmt').removeAttr('onclick');

                }else{

                   

                  $('#valmsg').text(resp.message);  

                }

                }

                

            });

        

        }else{

          $('#valmsg').text('This field required');

           $('#countrydata').focus(); 

        }

        

        

    }

    

    

    

    

        function edit_data(id){

           

            

           var country = $('#main_dat'+id).text();



$.get('<?php echo site_url('admin/tours/get_tour_style'); ?>/'+id,(resp,status)=>{

    if(status == 'success'){



       var styleData = JSON.parse(resp);

           $('#countrydata').val(country);

           $('#stylePageName').val(styleData.page_title);
           var settingId ='main_image';
           $('#main_image').val(styleData.main_image);
			var imc =  $('#main_image').siblings()[0];
var immg = styleData.main_image;
var baseUrl = '<?php echo base_url(); ?>';
		var localUrl = baseUrl.replace("adhvan-admin/",'');
		var main_url =  immg.replace(localUrl, "");
		var thumb = main_url.replace('adhvan/images','adhvan/_thumbs/Images'); 

           $(imc).html('<div class="image-thumb"><img height="120px" src="'+localUrl+thumb+'"><div class="action"><a onclick="removeMe(this)" alt="'+main_image+'" ><i class="fa fa-trash	"></i></a></div>');
           main_editor.setData(styleData.page_content);

           $('#countryid').val(id);

           $('#edit_modal').modal('show');

           $('#valmsg').text('');

           $('#sbmt').removeAttr('disabled');

           $('#sbmt').attr('onclick',"updatedata();");

    }

});



         

            }

            

    

    function updatedata(){

        

        var countrydat = $('#countrydata').val();

        var countryid = $('#countryid').val();

        var pageTitle = $('#stylePageName').val();
        var main_image = $('#main_image').val();
        var pagecontent = main_editor.getData();



        if(countrydat !=''){

            $('#sbmt').attr('disabled', 'disabled');

            

            $('#valmsg').text('please wait...');

            $.post('<?php echo site_url('admin/tours/update_style') ?>',{cname: countrydat,cid:countryid,pageTitle :pageTitle,pagecontent : pagecontent,main_image:main_image},function(resp,status){

                

                if(status == 'success'){

                    resp =JSON.parse(resp); 

                $('#sbmt').removeAttr('disabled');

                if(resp.status == '1'){

                    

                   $('#valmsg').text(resp.message);

                   $('#main_dat'+countryid).text(countrydat);

                    $('#edit_modal').modal('hide');

                    ///$('#sbmt').removeAttr('onclick');

                }else{

                   

                  $('#valmsg').text(resp.message);  

                }

                }

                

            });

        

        }else{

          $('#valmsg').text('This field required');

           $('#countrydata').focus(); 

        }

        

        

    }

    

    

    

    



  $('a.dlt').click(function(evt){

           evt.preventDefault();

          if(confirm("!Are You Sure ?")){       

               var dis = this;

              $(dis).text('wait...');

                $.post($(dis).attr('href'),{'delete':'dlt'},function(resp){

                    if(resp == 1){

                        $(dis).parent().parent().remove();

                    }else{

                        $(dis).html('<i class="mdi mdi-delete-forever"></i>');

                       alert(resp);

                    }

                });

            }

        });



       </script>     

            

            <!--  MODAL -->

            

  <div id="edit_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h4 class="modal-title">Tour Style</h4>

            </div>

            <div class="modal-body">



                <div class="row">

                    <div class="col-md-12">

                    <input type="hidden" id="countryid" value="">

                        <div class="form-group">

                            <label for="field-3" class="control-label">Tour Style</label>

                            <input type="text" class="form-control" id="countrydata" placeholder="Tour Style Title">

                        </div>

                        <div class="form-group">

                            <label for="field-3" class="control-label">Page Name</label>

                            <input type="text" class="form-control" id="stylePageName" placeholder="Page title">

                        </div>
                        <div class="form-group">
	<label for="text1" class="control-label">Main Image</label>
	<div class="input-group form-group">
		<input type="text" id="main_image" class="form-control" placeholder="Main image" name="main_image">
		<div></div>
		<!-- this div is image container -->

<a onclick="BrowseServer('main_image','')" class="btn btn-app">
                <i class="fa fa-image"></i> Tour Banner
              </a>

	</div>
</div>
                        <div class="form-group">

                            <label for="field-3" class="control-label">Page Content</label>

                            <textarea name="" id="contentarea" cols="30" rows="10"></textarea>

                        </div>

                        <p id="valmsg" class="text-danger"></p>

                    </div>

                </div>



            </div>

            <div class="modal-footer">

                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>

                <button id="sbmt" type="button"  class="btn btn-info waves-effect waves-light">Save changes</button>

            </div>

        </div>

    </div>

                            </div><!-- /.modal -->

          

            <script>

            

            var main_editor =  CKEDITOR.replace( 'contentarea', {



height:400,        

filebrowserBrowseUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/ckfinder.html',

filebrowserUploadUrl: '<?php echo base_url(); ?>ckeditor/samples/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',

filebrowserWindowWidth: '1000',

filebrowserWindowHeight: '700'

}); 

            </script>