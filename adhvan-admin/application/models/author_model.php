<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Author_model extends CI_Model
{
	
    public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->config('ion_auth', TRUE);
		$this->load->helper('cookie');
		$this->load->helper('date');
		$this->lang->load('ion_auth');
	}
	
    
    public function create_author($data)
	{
	  
    $team =  $this->db->insert('author', $data);
        return $team;
			
	}
    
    function get_author(){
        
        $this->db->order_by('name','ASC');
     $allteam =  $this->db->get_where('author')->result_array();
        return $allteam;
        
    }
    
    function update_author($data,$id){

		$res = $this->db->update('author', $data, array('id' => $id));
		return $res;
	}
	
	public function edit_author($id = NULL)
	{
		$query  = $this->db->get_where('author',array('id =' => $id))->row_array();		  
		return $query;		
	}
    
    public function delete_author($id){
				$delete  = $this->db->delete('author', array('id' => $id)); 
				return $delete;
	}
}
    