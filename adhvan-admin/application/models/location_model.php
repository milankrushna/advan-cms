<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Ion Auth Model
*
* Author:  Milan Krushna
* 		   milankrushna@gmail.com
*	  	   
*

*/

class Location_model extends CI_Model
{
	
	
	public function __construct()
	{
		parent::__construct();
		
	}

    function getstar($table,$pageno='',$lmt=''){
    $rtndata = array();
        
        
        if($pageno && $lmt){
      $this->db->limit($lmt,$lmt*$pageno); 
        }
        if($table){
                 $this->db->order_by('countryname','ASC');

    $rtndata =  $this->db->get($table)->result_array();  
        }
        return $rtndata;
    }
    function get_region($table,$pageno='',$lmt=''){
        $rtndata = array();
        
        
        if($pageno && $lmt){
      $this->db->limit($lmt,$lmt*$pageno); 
        }
        if($table){
                 $this->db->order_by('name','ASC');

    $rtndata =  $this->db->get($table)->result_array();  
        }
        return $rtndata;
    }
  
    function getrows($table ='' ,$cond=''){
       $rtndata = array();
    if($cond){
        $this->db->where($cond);
    }
      if($table){
     $rtndata = $this->db->get($table)->row_array();  
        }
        
                return $rtndata;

    }
    
    function country_search($sk){

        $this->db->like('countryname',$sk);
        return $this->db->get('country')->result_array();

    }

    
    function get_country_page($cid){
        $this->db->select('country_page.*,country.countryname as countryname');
        $this->db->from('country_page');
        $this->db->join('country','country.id = country_page.country_id');
        $this->db->order_by('country_page.id','desc');
        $this->db->where('country_page.country_id',$cid);
      return  $this->db->get()->row();
    }

    function getstate($cid='',$pageno='',$lmt=''){

        $this->db->select('state.*,country.countryname as countryname');
        $this->db->from('state');
        $this->db->join('country','country.id = state.countryid');
        $this->db->order_by('state.statename','ASC');
        $this->db->where('state.countryid',$cid);
        if($pageno && $lmt){
            $this->db->limit($lmt,$lmt*$pageno); 
        }
      return  $this->db->get()->result_array();
    
    } 
    
    function getregion($cid='',$pageno='',$lmt=''){

        $this->db->select('region.*,country.countryname as countryname');
        $this->db->from('region');
        $this->db->join('country','country.id = region.countryid');
        $this->db->order_by('region.name','ASC');
        $this->db->where('region.countryid',$cid);
        if($pageno && $lmt){
            $this->db->limit($lmt,$lmt*$pageno); 
        }
      return  $this->db->get()->result_array();
    
    } 

    function getcity($cond,$lmt){

        $this->db->select('city.*,country.countryname as countryname,state.statename as statename');
        $this->db->from('city');
        $this->db->join('country','country.id = city.country_id');
        $this->db->join('state','state.id = city.state_id');
        $this->db->order_by('city.city','ASC');
        $this->db->where('city.country_id',$cond['country_id']);
        $this->db->where('city.state_id',$cond['state_id']);
        
        
      return  $this->db->get()->result_array();
    
    }
    
    function getDistrictCity($cond,$lmt){

        $this->db->select('district_city.*,country.countryname as countryname,state.statename as statename,city.city as districtname');
        $this->db->from('district_city');
        $this->db->join('country','country.id = district_city.c_id');
        $this->db->join('state','state.id = district_city.s_id');
        $this->db->join('city','city.id = district_city.d_id');        
        $this->db->order_by('district_city.name','ASC');
        $this->db->where('district_city.c_id',$cond['c_id']);
        $this->db->where('district_city.s_id',$cond['s_id']);
        $this->db->where('district_city.d_id',$cond['d_id']);
       return  $data =  $this->db->get()->result_array();
        
        //print("<pre>");
        //print_r($data);exit;


    }
    
    
    function create_state($data){
       $this->db->insert('state',$data);
       return $this->db->get_where('state',array('id'=>$this->db->insert_id()))->row_array();
        
    } 
    
    function create_region($data){
       $this->db->insert('region',$data);
       return $this->db->get_where('region',array('id'=>$this->db->insert_id()))->row_array();
        
    } 
    
    function create_city($data){
       $this->db->insert('city',$data);
       return $this->db->insert_id();
        
    }
    
    function create_district_city($data){
        $this->db->insert('district_city',$data);
       return $this->db->insert_id();
    }
    
    function update_state($data,$cond){
        $this->db->update('state',$data,$cond);
       return $this->db->get_where('state',$cond)->row_array();
    }
    function update_region($data,$cond){
        $this->db->update('region',$data,$cond);
       return $this->db->get_where('region',$cond)->row_array();
    }
    function update_city($data,$cond){
       return $this->db->update('city',$data,$cond);
    }
    
    function update_district_city($data,$cond){
        return $this->db->update('district_city',$data,$cond);
    }
        
	}
