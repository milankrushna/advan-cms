<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Team_model extends CI_Model
{
	
    public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->config('ion_auth', TRUE);
		$this->load->helper('cookie');
		$this->load->helper('date');
		$this->lang->load('ion_auth');
	}
	
    
    public function create_team($data)
	{
	  
    $team =  $this->db->insert('team', $data);
        return $team;
			
	}
    
    function get_team(){
        
     $allteam =  $this->db->get_where('team')->result_array();
        return $allteam;
        
    }
    
   function update_team($data,$id){

		$res = $this->db->update('team', $data, array('id' => $id));
		return $res;
	}
	
	public function edit_team($id = NULL)
	{
		$query  = $this->db->get_where('team',array('id =' => $id))->row_array();		  
		return $query;		
	}
    
    public function delete_team($id){
				$delete  = $this->db->delete('team', array('id' => $id)); 
				return $delete;
	}
    
}