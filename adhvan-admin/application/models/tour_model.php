<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Ion Auth Model
*
* Author:  Milan Krushna
* 		   milankrushna@gmail.com
*	  	   
*

*/

class Tour_model extends CI_Model
{
	
	
	public function __construct()
	{
		parent::__construct();
		
	}

    function getstar($table,$pageno='',$lmt=''){
    $rtndata = array();
        
        
        if($pageno && $lmt){
      $this->db->limit($lmt,$lmt*$pageno); 
        }
        if($table){
            if($table == "country"){
                $this->db->order_by('countryname','ASC');

            }else{

                $this->db->order_by('name','ASC');
            }

    $rtndata =  $this->db->get($table)->result_array();  
        }
        return $rtndata;
    }

   function get_tours(){
       $this->db->order_by('id','desc');
   return $this->db->get("tours")->result();
    }
   function get_hotels(){
       $this->db->order_by('id','desc');
   return $this->db->get("hotels")->result();
    }
   function get_experiance(){
       $this->db->order_by('id','desc');
   return $this->db->get("experiance")->result();
    }

    function get_single_tours($tid){
        return $this->db->get_where("tours",array('id'=>$tid))->row();
    }
    function get_single_experiance($tid){
        return $this->db->get_where("experiance",array('id'=>$tid))->row();
    }
    function get_single_hotel($tid){
        return $this->db->get_where("hotels",array('id'=>$tid))->row();
    }
    function get_tour_maping($tid,$traveltypeId){
      
        return $this->db->get_where("travel_maping",array('type_id'=>$tid,'travel_type'=>$traveltypeId))->row();
    }
    function get_tour_trip($tid){
        return $this->db->get_where("tours_trip",array('tour_id'=>$tid))->result();
    }

   function get_day_data($tid=""){
        return $this->db->get_where("tours_day",array('tour_id'=>$tid))->result_array();
    }
   function get_city($cntid=""){
       //echo $cntid;
        return $this->db->get_where("state",array('countryid'=>$cntid))->result_array();
    }
   function get_sub_city($cntid=""){
        return $this->db->get_where("city",array('state_id'=>$cntid))->result_array();
    }

    function putDayData($cond,$data){
       $availData = $this->db->get_where('tours_day',$cond)->num_rows();
       if($availData == 1){
        $this->db->update('tours_day',$data,$cond);
       }else{
        $this->db->insert('tours_day',$data);
       }
       return true;
    }
function update_tour($data,$cond){
    return   $this->db->update('tours',$data,$cond);
}
function update_hotel($data,$cond){
    return   $this->db->update('hotels',$data,$cond);
}
function upddate_experiance($data,$cond){
    return   $this->db->update('experiance',$data,$cond);
}
function update_tour_maping($data,$cond){
    return   $this->db->update('travel_maping',$data,$cond);
}
function update_tour_trip($data,$cond){
    print_r($data);
    return   $this->db->update('tours_trip',$data,$cond);
}

    function get_newTours_id(){
        $this->db->select_max('id');
return $this->db->get('tours')->row();
    }
   
    function insert_tour($tt_data){
        $this->db->insert('tours',$tt_data);
        return $this->db->insert_id();
    }
    function insert_Hotel($tt_data){
        $this->db->insert('hotels',$tt_data);
        return $this->db->insert_id();
    }
    function insert_experiance($tt_data){
        $this->db->insert('experiance',$tt_data);
        return $this->db->insert_id();
    }
    function insert_tour_maping($tt_data){
        $this->db->insert('travel_maping',$tt_data);
        return $this->db->insert_id();
    }
    function insert_tour_trip($tt_data){
       return $this->db->insert_batch('tours_trip',$tt_data);
        //return $this->db->insert_id();
    }



	}
