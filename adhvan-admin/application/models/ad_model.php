<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Ad_model extends CI_Model

{



	public function __construct()

	{

		parent::__construct();

		$this->load->database();


		$this->load->helper('cookie');
		$this->load->library('session');
		$this->load->helper('date');

	}

	
	function insert_ad($data){

		return $this->db->insert('advertise',$data);

	}
	function get_advertise(){
	return	$this->db->get('advertise')->result();

	}
	function get_advertise2($id){
		return	$this->db->get_where('advertise',array('id'=>$id))->row();
	}
	// function publish_ad($id){
	// 	$data['status'] = 1;
	// 	$cond['id'] = $id;
	// 	return $this->db->update('advertise',$data,$cond);
	// }
	// function unpublish_ad($id){
	// 	$data['status'] = 0;
	// 	$cond['id'] = $id;
	// 	return $this->db->update('advertise',$data,$cond);
	// }
	function update_ad($data,$id){
	
		$cond['id'] = $id;
		return $this->db->update('advertise',$data,$cond);
	}
	
	
	}