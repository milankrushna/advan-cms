$(function(){
	$("body").on("click", "#refreshimg", function(){
		$.post("http://localhost/captcha/assets/validate/demo/captcha/newsession.php");
		$("#captchaimage").load("http://localhost/captcha/assets/validate/demo/captcha/image_req.php");
		return false;
	});

	$("#captchaform").validate({
		rules: {
			captcha: {
				required: true,
				remote: "process.php"
			}
		},
		messages: {
			captcha: "Correct captcha is required. Click the captcha to generate a new one"
		},
		submitHandler: function() {
			alert("Correct captcha!");
		},
		success: function(label) {
			label.addClass("valid").text("Valid captcha!")
		},
		onkeyup: false
	});

});
