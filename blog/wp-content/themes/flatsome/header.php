<!DOCTYPE html>
<!--[if IE 9 ]> <html <?php language_attributes(); ?> class="ie9 <?php flatsome_html_classes(); ?>"> <![endif]-->
<!--[if IE 8 ]> <html <?php language_attributes(); ?> class="ie8 <?php flatsome_html_classes(); ?>"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?> class="<?php flatsome_html_classes(); ?>"> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<link rel="shortcut icon" type="/image/x-icon" href="images/favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/css?family=Fjalla+One|Roboto&display=swap" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://kit.fontawesome.com/325b04c152.js"></script>
<link href="../css/menu.css" rel="stylesheet" type="text/css"/>
<!--<link href="css/adv-common.css" rel="stylesheet" type="text/css"/>
--><link href="../css/mega-menu.css" rel="stylesheet" type="text/css"/>

<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<!--<link href="css/style2.css" rel="stylesheet" type="text/css"/>
--><link href="../css/style.css" rel="stylesheet" type="text/css"/>
 <script src="../js/jquery-2.1.3.min.js" type="text/javascript"></script>
 <style>
 footer ul li { margin-left:0px; list-style:none;}
 
 </style>
 
	<?php wp_head(); ?>
</head>

<body <?php body_class(); // Body classes is added from inc/helpers-frontend.php ?>>

<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'flatsome' ); ?></a>

<div id="wrapper">

<?php do_action('flatsome_before_header'); ?>

<header id="header" class="header <?php flatsome_header_classes();  ?>">
   <div class="header-wrapper">
	<?php
		get_template_part('template-parts/header/header', 'wrapper');
	?>
   </div><nav class="main-nav white stick-fixed  js-transparent main-nav-line small-height">
    <div class="full-wrapper relative clearfix">
        <div class="nav-logo-wrap"><a href="#" class="logo exo-logo1 fadeInLoad exo-logo2 small-height"></a></div>
        <div class="navbar-mobile small-height" style="height: 65px; line-height: 65px; width: 65px;"><i class="fa fa-bars"></i></div>
        <div class="inner-nav navbar-desktop">
            <ul class="clearlist scroll scroll-nav">
                <li class="menu-select"><a href="#" class="menu-has-sub" style="height: 65px; line-height: 65px;"><span>Destinations</span></a>
                    <div class="menu-sub menu-sub-effect1 top-nav-dest mega-dest" style="display: none;">
                        <div class="row tab-wrap-img mb-10">
                            <div class="col-sm-5ths">
                                <div class="img-overlay-black tab-img"> <a href="#"><span>VIETNAM</span> <img src="../images/vietnam-nav.png" alt="Destinations Vietnam" title="Destinations Vietnam" class="resp"></a></div>
                                <div
                                    class="mega-dest-link">
                                    <p><a href="#">All Tours</a></p>
                                    <p><a href="#">Preferred Hotels</a></p>
                                    <p><a href="#">Experiences</a></p>
                            </div>
                        </div>
                        <div class="col-sm-5ths">
                            <div class="img-overlay-black tab-img"> <a href="#"><span>THAILAND</span> <img src="images/thailand-nav.png" alt="Destinations Thailand" title="Destinations Thailand" class="resp"></a></div>
                            <div
                                class="mega-dest-link">
                                <p><a href="#">All Tours</a></p>
                                <p><a href="#">Preferred Hotels</a></p>
                                <p><a href="#">Experiences</a></p>
                        </div>
                    </div>
                    <div class="col-sm-5ths">
                        <div class="img-overlay-black tab-img"> <a href="#"><span>MYANMAR</span> <img src="images/myanmar-nav.png" alt="Destinations Myanmar" title="Destinations Myanmar" class="resp"></a></div>
                        <div
                            class="mega-dest-link">
                            <p><a href="#">All Tours</a></p>
                            <p><a href="#">Preferred Hotels</a></p>
                            <p><a href="#">Experiences</a></p>
                    </div>
        </div>
        <div class="col-sm-5ths">
            <div class="img-overlay-black tab-img"> <a href="#"><span>LAOS</span> <img src="images/laos.png" alt="Destinations laos" title="Destinations laos" class="resp"></a></div>
            <div class="mega-dest-link">
                <p><a href="#">All Tours</a></p>
                <p><a href="#">Preferred Hotels</a></p>
                <p><a href="#">Experiences</a></p>
            </div>
        </div>
        <div class="col-sm-5ths">
            <div class="img-overlay-black tab-img"> <a href="#"><span>CAMBODIA</span> <img src="images/cambodia.png" alt="Destinations cambodia" title="Destinations cambodia" class="resp"></a></div>
            <div
                class="mega-dest-link">
                <p><a href="#">All Tours</a></p>
                <p><a href="#">Preferred Hotels</a></p>
                <p><a href="#">Experiences</a></p>
        </div>
    </div>
    <div class="col-sm-5ths">
        <div class="img-overlay-black tab-img"> <a href="#"><span>INDONESIA</span> <img src="images/indonesia.png" alt="Destinations indonesia" title="Destinations indonesia" class="resp"></a></div>
        <div
            class="mega-dest-link">
            <p><a href="#">All Tours</a></p>
            <p><a href="#">Preferred Hotels</a></p>
            <p><a href="#">Experiences</a></p>
    </div>
    </div>
    <div class="col-sm-5ths">
        <div class="img-overlay-black tab-img"> <a href="#"><span>JAPAN</span> <img src="images/japan.png" alt="Destinations japan" title="Destinations japan" class="resp"></a></div>
        <div class="mega-dest-link">
            <p><a href="#">All Tours</a></p>
            <p><a href="#">Preferred Hotels</a></p>
            <p><a href="#">Experiences</a></p>
        </div>
    </div>
    <div class="col-sm-5ths">
        <div class="img-overlay-black tab-img"> <a href="#"><span>CHINA</span> <img src="images/china.png" alt="Destinations china" title="Destinations china" class="resp"></a></div>
        <div class="mega-dest-link">
            <p><a href="#">All Tours</a></p>
            <p><a href="#">Preferred Hotels</a></p>
            <p><a href="#">Experiences</a></p>
        </div>
    </div>
    <div class="col-sm-5ths">
        <div class="img-overlay-black tab-img"> <a href="#"><span>MALAYSIA</span> <img src="images/malaysia.png" alt="Destinations malaysia" title="Destinations malaysia" class="resp"></a></div>
        <div class="mega-dest-link">
            <p><a href="#">All Tours</a></p>
            <p><a href="#">Preferred Hotels</a></p>
            <p><a href="#">Experiences</a></p>
        </div>
    </div>
    <div class="col-sm-5ths">
        <div class="img-overlay-black tab-img"> <a href="#"><span>SINGAPORE</span> <img src="https://www.exotravel.com/assets/img/uploads/nav-singapore.jpg" alt="Destinations singapore" title="Destinations singapore" class="resp"></a></div>
        <div
            class="mega-dest-link">
            <p><a href="#">All Tours</a></p>
            <p><a href="#">Preferred Hotels</a></p>
            <p><a href="#">Experiences</a></p>
    </div>
    </div>
    <div class="col-sm-5ths col-nav-multi">
        <div class="img-overlay-black tab-img"> <a href="#"><span>MULTI COUNTRY</span> <img src="images/multi-country.png" alt="Destinations multi-country" title="Destinations multi-country" class="resp"></a></div>
        <div
            class="mega-dest-link">
            <p><a href="#">All Tours</a></p>
    </div>
    </div>
    </div>
    </div>
    </li>
    <li class="menu-select"><a href="#" class="menu-has-sub" style="height: 65px; line-height: 65px;"><span>TOUR STYLES</span></a>
        <div class="menu-sub menu-sub-effect1 mega-style" style="display: none;">
            <div class="box-type-4">
                <ul>
                    <li class="nav-icon nav-icon-classic"><a href="#">Classic Journeys</a></li>
                    <li class="nav-icon nav-icon-beach"><a href="#">BEACH</a></li>
                    <li class="nav-icon nav-icon-nature"><a href="#">Nature &amp; Wildlife</a></li>
                    <li class="nav-icon nav-icon-active"><a href="#">ACTIVE TRAVEL</a></li>
                    <li class="nav-icon nav-icon-responsible"><a href="#">RESPONSIBLE TRAVEL</a></li>
                </ul>
            </div>
            <div class="box-type-4">
                <ul>
                    <li class="nav-icon nav-icon-family"><a href="#">Family</a></li>
                    <li class="nav-icon nav-icon-homestay"><a href="#">HOME STAY</a></li>
                    <li class="nav-icon nav-icon-honeymoon"><a href="#">Honeymoon</a></li>
                    <li class="nav-icon nav-icon-undiscovered"><a href="#">UNDISCOVERED ASIA</a></li>
                </ul>
            </div>
            <div class="box-type-4">
                <ul>
                    <li class="nav-icon nav-icon-cruise"><a href="#">Cruising</a></li>
                    <li class="nav-icon nav-icon-golf"><a href="#">GOLF</a></li>
                    <li class="nav-icon nav-icon-shorttrips"><a href="#">SHORT TRIPS</a></li>
                    <li class="nav-icon nav-icon-culinary"><a href="#">CULINARY</a></li>
                </ul>
            </div>
            
        </div>
    </li>
    
    
    
    <li class="menu-select"><a href="https://www.b9net.com/adhvan-new/hotels/adhvan" class="menu-has-sub" style="height: 65px; line-height: 65px;"><span>Hotel</span></a></li>
    <li class="menu-select"><a href="https://www.b9net.com/adhvan-new/adhvanpage/our-philosophy" class="menu-has-sub" style="height: 65px; line-height: 65px;"><span>About Adhvan</span></a></li>
    <li class="menu-select"><a href="https://www.b9net.com/adhvan-new/adhvanpage/diversity-of-asia" class="menu-has-sub" style="height: 65px; line-height: 65px;"><span>Diversity of Asia</span></a></li>
    <li class="menu-select"><a href="https://www.b9net.com/adhvan-new/adhvanpage/why-adhvan" class="menu-has-sub" style="height: 65px; line-height: 65px;"><span>Why Adhvan</span></a></li>
    <li class="menu-select"><a href="#" class="menu-has-sub" style="height: 65px; line-height: 65px;"><span>Contact Us</span></a></li>
    <li class="menu-select"><a href="#" class="menu-has-sub" style="height: 65px; line-height: 65px;"><span>Bog</span></a></li>

        
    </li>
    
   
    
    </ul>
    </div>
    </div>
</nav>
   
</header>

<?php do_action('flatsome_after_header'); ?>

<main id="main" class="<?php flatsome_main_classes();  ?>">
