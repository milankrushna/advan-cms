<?php
/**
 * The template for displaying the footer.
 *
 * @package flatsome
 */

global $flatsome_opt;
?>

</main><!-- #main -->

 
<footer class="text-white">
<div class="container">
<div class="fotter-in">
<img src="../images/logo-footer.png" class="img-fluid"/>
</div>
<div class="fotter-in">
<h5 class="text-left text-white font-weight-bold"> IMPORTANT LINKS</h5>
<ul>
<li><a href="#">Experiences</a></li>
<li><a href="#">Transport</a></li>
<li><a href="#">Itineraries</a></li>
<li><a href="#">Hotel</a></li>

</ul>
</div>
<div class="fotter-in">
<h5 class="text-left text-white font-weight-bold"> COMPANY INFO</h5>
<ul>
<li><a href="#">About Adhvan</a></li>
<li><a href="#">Privacy Policy</a></li>
<li><a href="#">Terms & Conditions</a></li>
<li><a href="#">Contact Us</a></li>
<li><a href="#">Sitemap</a></li>
</ul>
</div>
<div class="fotter-in">
<ul>
<li><i class="fab fa-facebook-f"></i> Facebook</li>
<li><i class="fab fa-twitter"></i> Twitter</li>
<li><i class="fab fa-pinterest-p"></i> Pinterest</li>
<li><i class="fab fa-linkedin-in"></i> LinkedIn</li>
<li><i class="fab fa-google-plus-g"></i> Google+</li>
<li><i class="fas fa-rss"></i> RSS</li>


</ul>
</div>
<div class="fotter-in">
<h5 class="text-left text-white font-weight-bold pb-3"> Sign Up</h5>
<form>
 <input type="text" class="form-control" placeholder="Email Address" id="usr">
 <button type="button" class="btn btn-success">Sign Up</button>
 </form>

</div>

</div>
</footer>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

 <script src="https://www.exotravel.com/assets/js/all-libs.min.js" type="053917cbfaea0bfa9710b45a-text/javascript"></script>

<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/a2bd7673/cloudflare-static/rocket-loader.min.js" data-cf-settings="053917cbfaea0bfa9710b45a-|49" defer></script>





</body>
</html>